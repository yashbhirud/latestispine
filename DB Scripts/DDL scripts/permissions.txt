--
-- PostgreSQL database dump
--

-- Dumped from database version 13.0
-- Dumped by pg_dump version 13.0

-- Started on 2021-02-04 12:12:34

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 232 (class 1259 OID 47700)
-- Name: sec_permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sec_permission (
    id uuid NOT NULL,
    create_ts timestamp without time zone,
    created_by character varying(50),
    version integer DEFAULT 1 NOT NULL,
    update_ts timestamp without time zone,
    updated_by character varying(50),
    delete_ts timestamp without time zone,
    deleted_by character varying(50),
    permission_type integer,
    target character varying(100),
    value_ integer,
    role_id uuid
);


ALTER TABLE public.sec_permission OWNER TO postgres;

--
-- TOC entry 3238 (class 0 OID 47700)
-- Dependencies: 232
-- Data for Name: sec_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('d80f85d3-f65c-a9fe-6050-bf47599517c8', '2021-01-29 16:24:03.71', 'admin', 1, '2021-01-29 16:24:03.71', NULL, NULL, NULL, 10, 'sys$SendingMessage.browse', 0, 'f4f199fc-998f-11a3-15f7-fdc22be45721');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('e880b2c0-9406-06a0-ff23-78c668893b66', '2021-01-29 16:24:03.709', 'admin', 1, '2021-01-29 16:24:03.709', NULL, NULL, NULL, 10, 'screenProfiler', 0, 'f4f199fc-998f-11a3-15f7-fdc22be45721');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('e27f07ed-edf2-a782-9b71-2cb20213848f', '2021-01-29 16:24:03.71', 'admin', 1, '2021-01-29 16:24:03.71', NULL, NULL, NULL, 10, 'performanceStatistics', 0, 'f4f199fc-998f-11a3-15f7-fdc22be45721');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('97dbb50c-2ee9-3bb2-0b65-0a4c758e676a', '2021-01-29 16:24:03.709', 'admin', 1, '2021-01-29 16:24:03.709', NULL, NULL, NULL, 10, 'printDomain', 0, 'f4f199fc-998f-11a3-15f7-fdc22be45721');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('0543ed3a-abb6-5218-2575-fd750ea6b501', '2021-01-29 16:24:03.71', 'admin', 1, '2021-01-29 16:24:03.71', NULL, NULL, NULL, 10, 'sys$LockInfo.browse', 0, 'f4f199fc-998f-11a3-15f7-fdc22be45721');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('b6cca2ad-ece2-9e9c-f3d0-6e1c8e1f12c8', '2021-01-29 16:24:03.709', 'admin', 1, '2021-01-29 16:24:03.709', NULL, NULL, NULL, 10, 'sys$Category.browse', 0, 'f4f199fc-998f-11a3-15f7-fdc22be45721');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('ee4c69cd-3707-6bab-18a1-b940a5545e8f', '2021-01-29 16:24:03.71', 'admin', 1, '2021-01-29 16:24:03.71', NULL, NULL, NULL, 10, 'sys$ScheduledTask.browse', 0, 'f4f199fc-998f-11a3-15f7-fdc22be45721');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('2f5eeeed-a2e4-a1e1-7f7c-b18eb799ed52', '2021-01-29 16:24:03.709', 'admin', 1, '2021-01-29 16:24:03.709', NULL, NULL, NULL, 10, 'entityRestore', 0, 'f4f199fc-998f-11a3-15f7-fdc22be45721');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('450edef6-f5b7-3b48-7d47-a9a05586dcc3', '2021-01-29 16:24:03.709', 'admin', 1, '2021-01-29 16:24:03.709', NULL, NULL, NULL, 10, 'entityLog', 0, 'f4f199fc-998f-11a3-15f7-fdc22be45721');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('86d0d678-1237-766d-629b-07992dcbba28', '2021-01-29 16:24:03.71', 'admin', 1, '2021-01-29 16:24:03.71', NULL, NULL, NULL, 10, 'entityInspector.browse', 0, 'f4f199fc-998f-11a3-15f7-fdc22be45721');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('7f65f96b-e357-9d61-a4d8-21f08e51796f', '2021-01-29 16:24:03.709', 'admin', 1, '2021-01-29 16:24:03.709', NULL, NULL, NULL, 10, 'sec$UserSessionEntity.browse', 0, 'f4f199fc-998f-11a3-15f7-fdc22be45721');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('b7217e49-f6e7-efe8-4ffb-3efe5c435ace', '2021-01-29 16:24:03.709', 'admin', 1, '2021-01-29 16:24:03.709', NULL, NULL, NULL, 10, 'jmxConsole', 0, 'f4f199fc-998f-11a3-15f7-fdc22be45721');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('67b1e5d1-b8ef-cc12-77b5-1ad53c6331b5', '2021-01-29 16:24:03.709', 'admin', 1, '2021-01-29 16:24:03.709', NULL, NULL, NULL, 10, 'serverLog', 0, 'f4f199fc-998f-11a3-15f7-fdc22be45721');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('54f7c1a4-2930-aadd-1bde-60bca7f9903f', '2021-01-29 16:24:03.71', 'admin', 1, '2021-01-29 16:24:03.71', NULL, NULL, NULL, 10, 'sys$FileDescriptor.browse', 0, 'f4f199fc-998f-11a3-15f7-fdc22be45721');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('828c176f-debd-fbd5-0134-84e4621c1a31', '2021-01-29 16:24:03.71', 'admin', 1, '2021-01-29 16:24:03.71', NULL, NULL, NULL, 10, 'dashboard$WidgetTemplate.browse', 0, 'f4f199fc-998f-11a3-15f7-fdc22be45721');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('1e0d5f00-50bd-046f-072e-105bf7c5648b', '2021-01-29 16:24:03.709', 'admin', 1, '2021-01-29 16:24:03.709', NULL, NULL, NULL, 10, 'reports', 0, 'f4f199fc-998f-11a3-15f7-fdc22be45721');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('ad3a7187-9378-1ac2-20f8-8902dc932c7f', '2021-01-29 16:24:03.71', 'admin', 1, '2021-01-29 16:24:03.71', NULL, NULL, NULL, 10, 'appProperties', 0, 'f4f199fc-998f-11a3-15f7-fdc22be45721');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('eb6e1502-f63d-2aa6-2c1b-191263e9cef5', '2021-01-29 16:24:03.709', 'admin', 1, '2021-01-29 16:24:03.709', NULL, NULL, NULL, 10, 'emailtemplates$EmailTemplate.browse', 0, 'f4f199fc-998f-11a3-15f7-fdc22be45721');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('8d8ea828-f709-9836-f643-a51e007ec233', '2021-01-29 16:24:03.709', 'admin', 1, '2021-01-29 16:24:03.709', NULL, NULL, NULL, 10, 'sec$SessionLogEntry.browse', 0, 'f4f199fc-998f-11a3-15f7-fdc22be45721');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('bb665066-8c22-7f2d-8113-ee20c646581d', '2021-01-29 16:24:53.086', 'admin', 1, '2021-01-29 16:24:53.086', NULL, NULL, NULL, 10, 'dashboard$WidgetTemplate.browse', 0, 'a90be025-43f4-020d-9d45-378d03f439b7');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('030395a1-ee6a-1458-9b84-3a766daa9377', '2021-01-29 16:24:53.086', 'admin', 1, '2021-01-29 16:24:53.086', NULL, NULL, NULL, 10, 'masters-vault', 0, 'a90be025-43f4-020d-9d45-378d03f439b7');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('34e536ac-5d60-27ea-6932-5cf700c86306', '2021-01-29 16:24:53.086', 'admin', 1, '2021-01-29 16:24:53.086', NULL, NULL, NULL, 10, 'reports', 0, 'a90be025-43f4-020d-9d45-378d03f439b7');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('941857ab-61f9-3060-7452-7231c20d10d0', '2021-01-29 16:24:53.085', 'admin', 1, '2021-01-29 16:24:53.085', NULL, NULL, NULL, 10, 'administration', 0, 'a90be025-43f4-020d-9d45-378d03f439b7');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('27274a87-8564-e1a8-9abd-0cc06979b223', '2021-01-29 16:24:53.086', 'admin', 1, '2021-01-29 16:24:53.086', NULL, NULL, NULL, 10, 'printDomain', 0, 'a90be025-43f4-020d-9d45-378d03f439b7');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('07a83f8b-90a4-0a1b-2b5a-ae6937b276fd', '2021-01-29 16:29:07.438', 'admin', 1, '2021-01-29 16:29:07.438', NULL, NULL, NULL, 10, 'dashboard', 0, 'a90be025-43f4-020d-9d45-378d03f439b7');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('061a82a8-c930-6181-3bb1-583258d99b07', '2021-01-29 16:29:19.062', 'admin', 1, '2021-01-29 16:29:19.062', NULL, NULL, NULL, 10, 'dashboard', 0, 'f4f199fc-998f-11a3-15f7-fdc22be45721');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('4dcb4714-513a-8b8a-e42d-e3fd99a9566b', '2021-01-29 16:24:53.086', 'admin', 2, '2021-01-29 16:24:53.086', NULL, '2021-02-04 11:39:15.226', 'admin', 10, 'settings', 0, 'a90be025-43f4-020d-9d45-378d03f439b7');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('0a857b84-30bc-5c28-fa7c-23809bfdde3b', '2021-02-04 11:39:15.223', 'admin', 1, '2021-02-04 11:39:15.223', NULL, NULL, NULL, 10, 'aboutWindow', 0, 'a90be025-43f4-020d-9d45-378d03f439b7');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('627886ee-cf0d-484c-ee03-cd0f3726ec43', '2021-02-04 11:39:47.284', 'admin', 1, '2021-02-04 11:39:47.284', NULL, NULL, NULL, 10, 'aboutWindow', 0, 'f4f199fc-998f-11a3-15f7-fdc22be45721');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('78979261-e10b-fba6-7163-4e3c6e6ea0dc', '2021-02-04 11:40:47.982', 'admin', 1, '2021-02-04 11:40:47.982', NULL, NULL, NULL, 10, 'dashboard', 0, '3c980ddf-b76f-6cc7-e5b8-918f50a15e5e');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('a6961d31-3c41-75a0-fd27-8760c483aa91', '2021-02-04 11:40:47.982', 'admin', 1, '2021-02-04 11:40:47.982', NULL, NULL, NULL, 10, 'reports', 0, '3c980ddf-b76f-6cc7-e5b8-918f50a15e5e');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('1a3b899c-2499-1200-f967-0872314482b4', '2021-02-04 11:41:54.231', 'admin', 1, '2021-02-04 11:41:54.231', NULL, NULL, NULL, 20, 'vault_UploadSummary:create', 0, '3c980ddf-b76f-6cc7-e5b8-918f50a15e5e');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('d18ca135-ccf4-2d3c-20bf-a561b79c3226', '2021-02-04 11:41:54.228', 'admin', 1, '2021-02-04 11:41:54.228', NULL, NULL, NULL, 20, 'vault_Master:delete', 0, '3c980ddf-b76f-6cc7-e5b8-918f50a15e5e');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('f687e608-56e8-92e5-0bc1-016afc9676ed', '2021-02-04 11:41:54.228', 'admin', 1, '2021-02-04 11:41:54.228', NULL, NULL, NULL, 20, 'vault_Api:create', 0, '3c980ddf-b76f-6cc7-e5b8-918f50a15e5e');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('de4bc931-ca26-f4f7-40cd-56f3f84d434d', '2021-02-04 11:41:54.23', 'admin', 1, '2021-02-04 11:41:54.23', NULL, NULL, NULL, 20, 'vault_UploadSummary:update', 0, '3c980ddf-b76f-6cc7-e5b8-918f50a15e5e');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('c024cbc3-4b44-6ba3-5cc7-be918f763d8d', '2021-02-04 11:41:54.228', 'admin', 1, '2021-02-04 11:41:54.228', NULL, NULL, NULL, 20, 'vault_Integration:delete', 0, '3c980ddf-b76f-6cc7-e5b8-918f50a15e5e');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('941a56e0-1d46-83ed-5012-dba983beb478', '2021-02-04 11:41:54.228', 'admin', 1, '2021-02-04 11:41:54.228', NULL, NULL, NULL, 20, 'vault_UploadSummary:delete', 0, '3c980ddf-b76f-6cc7-e5b8-918f50a15e5e');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('8dd36caa-ba11-6723-1c6e-dd481bdece1d', '2021-02-04 11:41:54.228', 'admin', 1, '2021-02-04 11:41:54.228', NULL, NULL, NULL, 20, 'vault_Integration:create', 0, '3c980ddf-b76f-6cc7-e5b8-918f50a15e5e');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('b0a4a8d4-83f3-3110-9016-cae46263c593', '2021-02-04 11:41:54.228', 'admin', 1, '2021-02-04 11:41:54.228', NULL, NULL, NULL, 20, 'vault_Integration:update', 0, '3c980ddf-b76f-6cc7-e5b8-918f50a15e5e');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('d6b130c2-ee58-4fc6-3b00-7b1f298590a9', '2021-02-04 11:41:54.231', 'admin', 1, '2021-02-04 11:41:54.231', NULL, NULL, NULL, 20, 'vault_Api:update', 0, '3c980ddf-b76f-6cc7-e5b8-918f50a15e5e');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('49b507db-fec7-a4e3-a6db-428ac197eff5', '2021-02-04 11:41:54.231', 'admin', 1, '2021-02-04 11:41:54.231', NULL, NULL, NULL, 20, 'vault_Api:delete', 0, '3c980ddf-b76f-6cc7-e5b8-918f50a15e5e');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('0797b48d-3e56-0774-1176-5f6c2177ca8e', '2021-02-04 11:41:54.231', 'admin', 1, '2021-02-04 11:41:54.231', NULL, NULL, NULL, 20, 'vault_Master:create', 0, '3c980ddf-b76f-6cc7-e5b8-918f50a15e5e');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('dae259f2-4da2-cad0-2bd5-d4f09e7304d6', '2021-02-04 11:41:54.227', 'admin', 1, '2021-02-04 11:41:54.227', NULL, NULL, NULL, 20, 'vault_Master:update', 0, '3c980ddf-b76f-6cc7-e5b8-918f50a15e5e');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('847c4ab0-6711-4f4d-d607-419da639c6b0', '2021-02-04 11:41:54.228', 'admin', 2, '2021-02-04 11:41:54.228', NULL, '2021-02-04 11:46:06.233', 'admin', 20, 'vault_UploadSummary:read', 0, '3c980ddf-b76f-6cc7-e5b8-918f50a15e5e');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('d0c70cf5-52e4-accf-8d77-8e5b0981888d', '2021-02-04 11:41:54.231', 'admin', 2, '2021-02-04 11:41:54.231', NULL, '2021-02-04 11:46:06.234', 'admin', 20, 'vault_Api:read', 0, '3c980ddf-b76f-6cc7-e5b8-918f50a15e5e');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('3bc649c4-cfb2-943f-bbbf-3c32025ad28a', '2021-02-04 11:41:54.228', 'admin', 2, '2021-02-04 11:41:54.228', NULL, '2021-02-04 11:46:06.231', 'admin', 20, 'vault_Integration:read', 0, '3c980ddf-b76f-6cc7-e5b8-918f50a15e5e');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('6be70279-079e-7db3-f9c3-63ab4f6f7038', '2021-02-04 11:41:54.228', 'admin', 2, '2021-02-04 11:41:54.228', NULL, '2021-02-04 11:46:06.236', 'admin', 20, 'vault_Master:read', 0, '3c980ddf-b76f-6cc7-e5b8-918f50a15e5e');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('8bf3f970-4e04-f97a-9427-873c643ad457', '2021-02-04 12:04:37.186', 'admin', 1, '2021-02-04 12:04:37.186', NULL, NULL, NULL, 10, 'help', 0, '3c980ddf-b76f-6cc7-e5b8-918f50a15e5e');
INSERT INTO public.sec_permission (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, permission_type, target, value_, role_id) VALUES ('ce785797-b112-288b-b222-5f3c08b6a81c', '2021-02-04 12:04:37.186', 'admin', 1, '2021-02-04 12:04:37.186', NULL, NULL, NULL, 10, 'administration', 0, '3c980ddf-b76f-6cc7-e5b8-918f50a15e5e');


--
-- TOC entry 3106 (class 2606 OID 47705)
-- Name: sec_permission sec_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sec_permission
    ADD CONSTRAINT sec_permission_pkey PRIMARY KEY (id);


--
-- TOC entry 3104 (class 1259 OID 47711)
-- Name: idx_sec_permission_unique; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX idx_sec_permission_unique ON public.sec_permission USING btree (role_id, permission_type, target) WHERE (delete_ts IS NULL);


--
-- TOC entry 3107 (class 2606 OID 47706)
-- Name: sec_permission sec_permission_role; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sec_permission
    ADD CONSTRAINT sec_permission_role FOREIGN KEY (role_id) REFERENCES public.sec_role(id);


-- Completed on 2021-02-04 12:12:35

--
-- PostgreSQL database dump complete
--

