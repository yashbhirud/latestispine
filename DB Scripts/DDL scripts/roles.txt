--
-- PostgreSQL database dump
--

-- Dumped from database version 13.0
-- Dumped by pg_dump version 13.0

-- Started on 2021-02-04 12:12:56

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 227 (class 1259 OID 47625)
-- Name: sec_role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sec_role (
    id uuid NOT NULL,
    create_ts timestamp without time zone,
    created_by character varying(50),
    version integer DEFAULT 1 NOT NULL,
    update_ts timestamp without time zone,
    updated_by character varying(50),
    delete_ts timestamp without time zone,
    deleted_by character varying(50),
    sys_tenant_id character varying(255),
    name character varying(255) NOT NULL,
    loc_name character varying(255),
    description character varying(1000),
    is_default_role boolean,
    role_type integer,
    security_scope character varying(255)
);


ALTER TABLE public.sec_role OWNER TO postgres;

--
-- TOC entry 3238 (class 0 OID 47625)
-- Dependencies: 227
-- Data for Name: sec_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.sec_role (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, sys_tenant_id, name, loc_name, description, is_default_role, role_type, security_scope) VALUES ('f4f199fc-998f-11a3-15f7-fdc22be45721', '2021-01-29 16:24:03.71', 'admin', 1, '2021-01-29 16:24:03.71', NULL, NULL, NULL, NULL, 'BU Administrator', 'BU Administrator', NULL, NULL, NULL, 'GENERIC_UI');
INSERT INTO public.sec_role (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, sys_tenant_id, name, loc_name, description, is_default_role, role_type, security_scope) VALUES ('a90be025-43f4-020d-9d45-378d03f439b7', '2021-01-29 16:24:53.086', 'admin', 1, '2021-01-29 16:24:53.086', NULL, NULL, NULL, NULL, 'BU User', 'BU User', NULL, NULL, NULL, 'GENERIC_UI');
INSERT INTO public.sec_role (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, sys_tenant_id, name, loc_name, description, is_default_role, role_type, security_scope) VALUES ('6962f7b8-d179-d9d6-d8a4-036cb2a7ad9f', '2021-01-29 16:29:58.236', 'admin', 1, '2021-01-29 16:29:58.236', NULL, NULL, NULL, NULL, 'Company Administrator', 'Company Administrator', NULL, NULL, NULL, 'GENERIC_UI');
INSERT INTO public.sec_role (id, create_ts, created_by, version, update_ts, updated_by, delete_ts, deleted_by, sys_tenant_id, name, loc_name, description, is_default_role, role_type, security_scope) VALUES ('3c980ddf-b76f-6cc7-e5b8-918f50a15e5e', '2021-02-04 11:40:47.982', 'admin', 1, '2021-02-04 11:40:47.982', NULL, NULL, NULL, NULL, 'Read Only', 'Read-Only', NULL, NULL, NULL, 'GENERIC_UI');


--
-- TOC entry 3107 (class 2606 OID 47633)
-- Name: sec_role sec_role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sec_role
    ADD CONSTRAINT sec_role_pkey PRIMARY KEY (id);


--
-- TOC entry 3104 (class 1259 OID 47634)
-- Name: idx_sec_role_uniq_name; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX idx_sec_role_uniq_name ON public.sec_role USING btree (name) WHERE ((delete_ts IS NULL) AND (sys_tenant_id IS NULL));


--
-- TOC entry 3105 (class 1259 OID 47635)
-- Name: idx_sec_role_uniq_name_sys_tenant_id_nn; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX idx_sec_role_uniq_name_sys_tenant_id_nn ON public.sec_role USING btree (name, sys_tenant_id) WHERE ((delete_ts IS NULL) AND (sys_tenant_id IS NOT NULL));


-- Completed on 2021-02-04 12:12:57

--
-- PostgreSQL database dump complete
--

