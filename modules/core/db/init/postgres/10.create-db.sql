-- begin VAULT_ERR_SUMMARY
create table vault_err_summary (
    ID bigserial,
    create_ts timestamp,
    created_by varchar(50),
    update_ts timestamp,
    updated_by varchar(50),
    --
    err_col_name varchar(255),
    err_datatype varchar(500),
    err_desc text,
    err_row_no integer,
    err_ups_id bigint,
    --
    primary key (ID)
)^
-- end VAULT_ERR_SUMMARY
-- begin VAULT_API_AUDIT
create table vault_api_audit (
    ava_id bigserial,
    create_ts timestamp not null,
    created_by varchar(50) not null,
    delete_ts timestamp,
    deleted_by varchar(50),
    update_ts timestamp,
    updated_by varchar(50),
    --
    ava_api_id varchar(255),
    ava_api_biz_owner varchar(255),
    ava_api_dependency varchar(255),
    ava_api_desc text,
    ava_api_dest varchar(255),
    ava_api_payload varchar(255),
    ava_api_int_msg_size integer,
    ava_api_int_owner varchar(255),
    ava_api_lob varchar(50),
    AVA_API_MST_BIZ_UNIT_ID bigint,
    AVA_API_MST_CLASS_ID bigint,
    AVA_API_MST_COMPLEXITY_ID bigint,
    AVA_API_MST_COUNTRY_ID bigint,
    AVA_API_MST_DESIGN_PATTERN_ID bigint,
    AVA_API_MST_DEST_PAYLOAD_TYPE_ID bigint,
    AVA_API_MST_DEST_PROTOCOL_ID bigint,
    AVA_API_MST_OPS_ID bigint,
    AVA_API_MST_PAYLOAD_SECUR_CLASS_ID bigint,
    AVA_API_MST_PLATFORM_ID bigint,
    AVA_API_MST_PRIORITY_ID bigint,
    AVA_API_MST_PROCESS_TYPE_ID bigint,
    AVA_API_MST_REGION_ID bigint,
    AVA_API_MST_SDLC_STATUS_ID bigint,
    AVA_API_MST_SRC_PAYLOAD_TYPE_ID bigint,
    AVA_API_MST_SRC_PROTOCOL_ID bigint,
    ava_api_name varchar(500),
    ava_api_parent_id bigint,
    ava_api_platform_version varchar(255),
    ava_api_proj varchar(255),
    ava_api_release varchar(255),
    ava_api_rem text,
    ava_api_throughput integer,
    ava_api_ups_id bigint,
    ava_api_workflow_status varchar(50),
    ava_api_workstream varchar(255),
    ava_int_asset_id text,
    operation_name varchar(255),
    --
    primary key (ava_id)
)^
-- end VAULT_API_AUDIT
-- begin VAULT_INTEGRATION
create table vault_integration (
    ID bigserial,
    create_ts timestamp,
    created_by varchar(50),
    delete_ts timestamp,
    deleted_by varchar(50),
    update_ts timestamp,
    updated_by varchar(50),
    --
    int_asset_id varchar(255) not null,
    int_biz_owner varchar(255),
    int_code_repo varchar(500),
    int_dependency varchar(255),
    int_desc text,
    int_dest varchar(255),
    int_func_spec_loc varchar(500),
    int_impl_pattern varchar(500),
    int_lob varchar(50),
    int_msg_size integer,
    int_mst_biz_unit_id bigint,
    int_mst_complexity_id bigint,
    int_mst_country_id bigint,
    int_mst_design_pattern_id bigint,
    int_mst_dest_payload_type_id bigint,
    int_mst_dest_protocol_id bigint,
    int_mst_ops_id bigint,
    int_mst_payload_sec_class_id bigint,
    int_mst_platform_id bigint,
    int_platform_version text,
    int_mst_priority_id bigint,
    int_mst_process_type_id bigint,
    int_mst_region_id bigint,
    int_mst_sdlc_status_id bigint,
    int_mst_special_ops_id bigint,
    int_mst_src_payload_type_id bigint,
    int_mst_src_protocol_id bigint,
    int_name varchar(500),
    int_owner varchar(255),
    int_payload varchar(255),
    int_proj varchar(255),
    int_rem text,
    int_src_sys varchar(255),
    int_tech_spec_loc varchar(500),
    int_throughput integer,
    int_ups_id bigint,
    INT_WORKFLOW_STATUS varchar(50),
    int_workstream varchar(255),
    int_release varchar(255),
    int_developer varchar(255),
    int_architect varchar(255),
    int_version varchar(255),
    --
    primary key (ID)
)^
-- end VAULT_INTEGRATION
-- begin VAULT_API
create table vault_api (
    ID bigserial,
    create_ts timestamp,
    created_by varchar(50),
    delete_ts timestamp,
    deleted_by varchar(50),
    update_ts timestamp,
    updated_by varchar(50),
    --
    api_id varchar(255) not null,
    api_biz_owner varchar(255),
    api_dependency varchar(255),
    api_desc text,
    api_dest varchar(255),
    api_payload varchar(255),
    api_int_msg_size integer,
    api_int_owner varchar(255),
    api_lob varchar(50),
    api_mst_biz_unit_id bigint,
    api_mst_class_id bigint,
    api_mst_complexity_id bigint,
    api_mst_country_id bigint,
    api_mst_design_pattern_id bigint,
    api_mst_dest_payload_type_id bigint,
    api_mst_dest_protocol_id bigint,
    api_mst_ops_id bigint,
    api_mst_payload_secur_class_id bigint,
    api_mst_platform_id bigint,
    api_platform_version varchar(255),
    api_mst_priority_id bigint,
    api_mst_process_type_id bigint,
    api_mst_region_id bigint,
    api_mst_sdlc_status_id bigint,
    api_mst_src_payload_type_id bigint,
    api_mst_src_protocol_id bigint,
    api_name varchar(500),
    api_proj varchar(255),
    api_release varchar(255),
    api_rem text,
    api_throughput integer,
    api_ups_id bigint,
    API_WORKFLOW_STATUS varchar(50),
    api_workstream varchar(255),
    int_asset_id text,
    --
    primary key (ID)
)^
-- end VAULT_API
-- begin VAULT_INTEGRATION_API
create table vault_integration_api (
    ID bigserial,
    create_ts timestamp,
    created_by varchar(50),
    update_ts timestamp,
    updated_by varchar(50),
    --
    vault_api_id bigint not null,
    vault_integration_id bigint not null,
    --
    primary key (ID)
)^
-- end VAULT_INTEGRATION_API
-- begin VAULT_INTEGRATION_AUDIT
create table vault_integration_audit (
    avi_id bigserial,
    create_ts timestamp not null,
    created_by varchar(50) not null,
    delete_ts timestamp,
    deleted_by varchar(50),
    update_ts timestamp,
    updated_by varchar(50),
    --
    avi_int_id bigint not null,
    avi_int_asset_id varchar(255),
    avi_int_biz_owner varchar(255),
    avi_int_code_repo varchar(500),
    avi_int_dependency varchar(255),
    avi_int_desc text,
    avi_int_dest varchar(255),
    avi_int_func_spec_loc varchar(500),
    avi_int_impl_pattern varchar(500),
    avi_int_lob varchar(50),
    avi_int_msg_size integer,
    AVI_INT_MST_BIZ_UNIT_ID bigint,
    AVI_INT_MST_COMPLEXITY_ID bigint,
    AVI_INT_MST_COUNTRY_ID bigint,
    AVI_INT_MST_DESIGN_PATTERN_ID bigint,
    AVI_INT_MST_DEST_PAYLOAD_TYPE_ID bigint,
    AVI_INT_MST_DEST_PROTOCOL_ID bigint,
    AVI_INT_MST_OPS_ID bigint,
    AVI_INT_MST_PAYLOAD_SEC_CLASS_ID bigint,
    AVI_INT_MST_PLATFORM_ID bigint,
    AVI_INT_MST_PRIORITY_ID bigint,
    AVI_INT_MST_PROCESS_TYPE_ID bigint,
    AVI_INT_MST_REGION_ID bigint,
    AVI_INT_MST_SDLC_STATUS_ID bigint,
    AVI_INT_MST_SPECIAL_OPS_ID bigint,
    AVI_INT_MST_SRC_PAYLOAD_TYPE_ID bigint,
    AVI_INT_MST_SRC_PROTOCOL_ID bigint,
    avi_int_name varchar(500),
    avi_int_owner varchar(255),
    avi_int_payload text,
    avi_int_platform_version varchar(255),
    avi_int_proj varchar(255),
    avi_int_rem text,
    avi_int_src_sys varchar(255),
    avi_int_tech_spec_loc varchar(500),
    avi_int_throughput integer,
    avi_int_ups_id bigint,
    avi_int_workflow_status varchar(50),
    avi_int_workstream varchar(255),
    operation_name varchar(255),
    avi_int_developer varchar(255),
    avi_int_architect varchar(255),
    avi_int_version varchar(255),
    avi_int_release varchar(255),
    --
    primary key (avi_id)
)^
-- end VAULT_INTEGRATION_AUDIT
-- begin VAULT_UPLOAD_SUMMARY
create table vault_upload_summary (
    ID bigserial,
    create_ts timestamp not null,
    created_by varchar(50) not null,
    --
    ups_datatype varchar(500),
    ups_error_records integer,
    ups_filename varchar(255),
    ups_lob varchar(50),
    ups_success_records integer,
    --
    primary key (ID)
)^
-- end VAULT_UPLOAD_SUMMARY
-- begin VAULT_MASTER
create table vault_master (
    ID bigserial,
    create_ts timestamp,
    created_by varchar(50),
    delete_ts timestamp,
    deleted_by varchar(50),
    update_ts timestamp,
    updated_by varchar(50),
    --
    mst_code varchar(20) not null,
    mst_desc text,
    mst_isactive boolean not null,
    mst_type varchar(500) not null,
    --
    primary key (ID)
)^
-- end VAULT_MASTER
-- begin SEC_USER
alter table SEC_USER add column ALLOW_TECH_SPEC boolean ^
alter table SEC_USER add column ALLOW_FUNC_SPEC boolean ^
alter table SEC_USER add column ALLOW_CODE_LOC boolean ^
alter table SEC_USER add column DTYPE varchar(31) ^
update SEC_USER set DTYPE = 'extuser$ExtUser' where DTYPE is null ^
-- end SEC_USER
