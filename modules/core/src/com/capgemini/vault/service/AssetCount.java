package com.capgemini.vault.service;

import com.haulmont.cuba.security.entity.User;
import org.springframework.stereotype.Service;


import javax.inject.Inject;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Query;
import com.haulmont.cuba.core.Transaction;
import com.haulmont.cuba.security.entity.User;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.Map;


@Service(iAssetCountService.NAME)
public class AssetCount implements iAssetCountService {
    @Inject
    private Persistence persistence;

    public Map<String,Integer> getTotalCount(boolean bu, User user){

        Map<String,Integer> map = new HashMap<>();
        try(Transaction tx = persistence.createTransaction()){
            Query intQuery;
            Query apiQuery;
            if(Boolean.TRUE.equals(bu)){
                intQuery =  persistence.getEntityManager().createNativeQuery("select count(1) from vault_integration where int_lob = ?1");
                intQuery.setParameter(1,user.getGroup().getName());
            }else if(user != null){
                intQuery =  persistence.getEntityManager().createNativeQuery("select count(1) from vault_integration where created_by = ?1");
                intQuery.setParameter(1,user.getLogin());
            }else{
                intQuery =  persistence.getEntityManager().createNativeQuery("select count(1) from vault_integration");
            }
            Long intCount = (Long)intQuery.getSingleResult();

            if(Boolean.TRUE.equals(bu)){
                apiQuery =  persistence.getEntityManager().createNativeQuery("select count(1) from vault_api where api_lob = ?1");
                apiQuery.setParameter(1,user.getGroup().getName());
            }else if(user != null){
                apiQuery =  persistence.getEntityManager().createNativeQuery("select count(1) from vault_api where created_by = ?1");
                apiQuery.setParameter(1,user.getLogin());
            }else{
                apiQuery =  persistence.getEntityManager().createNativeQuery("select count(1) from vault_api");
            }
            Long apiCount = (Long)apiQuery.getSingleResult();
            map.put("Integration", Integer.valueOf(intCount.intValue()));
            map.put("Api", Integer.valueOf(apiCount.intValue()));
        }

        return map;
    }

}