package com.capgemini.vault.service;

import com.capgemini.vault.entity.VaultApi;
import com.capgemini.vault.entity.VaultIntegrationApi;
import com.capgemini.vault.entity.VaultIntegration;
import com.capgemini.vault.entity.Status;
import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Transaction;
import com.haulmont.cuba.core.TypedQuery;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service(iAssociatedDetailsService.NAME)
public class AssociatedDetails implements iAssociatedDetailsService {

    @Inject
    private Persistence persistence;

    /**
     * function for getting associated integrations for the mentioned api object.
     *
     * @param object               Int/Api object
     * @return List of associated integration/api objects
     */
    public List<?> getAssociatedIntApi(Object object) {
        try(Transaction tx = persistence.createTransaction()){

            TypedQuery<VaultIntegrationApi> query;
            if(object instanceof VaultApi){
                query = persistence.getEntityManager().createQuery("select e from vault_IntegrationApi e where e.vaultApi=?1", VaultIntegrationApi.class);
            }else{
                query = persistence.getEntityManager().createQuery("select e from vault_IntegrationApi e where e.vaultIntegration=?1", VaultIntegrationApi.class);
            }
            query.setViewName("vaultApiIntegration-browse-view");
            query.setParameter(1,object);
            List<VaultIntegrationApi> list = query.getResultList();
            if(!list.isEmpty()){
                if(object instanceof VaultApi){
                    return list.stream().map(VaultIntegrationApi::getVaultIntegration).collect(Collectors.toList());
                }else{
                    return list.stream().map(VaultIntegrationApi::getVaultApi).collect(Collectors.toList());
                }

            }
            return Collections.emptyList();
        }
    }


    /**
     * function for sending associated api's for approval.
     *
     * @param apiDetails               Api object to be approved.
     */
    public void sendAssociatedApiForApp(List<VaultApi> apiDetails){

        try(Transaction tx = persistence.createTransaction()){
            if(!apiDetails.isEmpty()){
                EntityManager em= persistence.getEntityManager();
                for(VaultApi api : apiDetails){
                    if(api.getApiWorkflowStatus().equals(Status.DRAFT)){
                        api.setApiWorkflowStatus(Status.PENDING_FOR_APPROVAL);
                        em.merge(api);
                    }
                }
            }
            tx.commit();
        }
    }


    /**
     * function for approving associated list of api's .
     *
     * @param apiDetails               Api object to be approved.
     * @param isBu                      Checks if the user is a business user
     */
    public void approveAssociatedApi(List<VaultApi> apiDetails, boolean isBu) {
        try(Transaction tx = persistence.createTransaction()){
            if(!apiDetails.isEmpty()){
                EntityManager em= persistence.getEntityManager();
                for(VaultApi api : apiDetails){
                    if(api.getApiWorkflowStatus().equals(Status.PENDING_FOR_APPROVAL) || ((isBu) && !api.getApiWorkflowStatus().equals(Status.ERROR))){
                        api.setApiWorkflowStatus(Status.APPROVED);
                        em.merge(api);
                    }
                }
            }
            tx.commit();
        }
    }


    /**
     * function for rejecting associated list of api's .
     *
     * @param apiDetails               Api object to be approved.
     */
    public void rejectAssociatedApi(List<VaultApi> apiDetails) {
        try(Transaction tx = persistence.createTransaction()){
            if(!apiDetails.isEmpty()){
                EntityManager em= persistence.getEntityManager();
                for(VaultApi api : apiDetails){
                    if(api.getApiWorkflowStatus().equals(Status.PENDING_FOR_APPROVAL)){
                        api.setApiWorkflowStatus(Status.DRAFT);
                        em.merge(api);
                    }
                }
            }
            tx.commit();
        }
    }


    /**
     * function for sending associated integrations for approval.
     *
     * @param intDetails               Integration object to be approved.
     */
    public void sendAssociatedIntForApp(List<VaultIntegration> intDetails) {

        try(Transaction tx = persistence.createTransaction()){
            if(!intDetails.isEmpty()){
                EntityManager em= persistence.getEntityManager();
                for(VaultIntegration intDetail : intDetails){
                    if(intDetail.getIntWorkflowStatus().equals(Status.DRAFT)){
                        intDetail.setIntWorkflowStatus(Status.PENDING_FOR_APPROVAL);
                        em.merge(intDetail);
                    }
                }
                tx.commit();
            }

        }

    }


    /**
     * function for approving associated list of integrations .
     *
     * @param intDetails                Integration object to be approved.
     * @param isBu                      Checks if the user is a business user
     */
    public void approveAssociatedInt(List<VaultIntegration> intDetails, boolean isBu) {
        try(Transaction tx = persistence.createTransaction()){
            if(!intDetails.isEmpty()){
                EntityManager em= persistence.getEntityManager();
                for(VaultIntegration intDetail : intDetails){
                    if(intDetail.getIntWorkflowStatus().equals(Status.PENDING_FOR_APPROVAL) || ((isBu) && !intDetail.getIntWorkflowStatus().equals(Status.ERROR))){
                        intDetail.setIntWorkflowStatus(Status.APPROVED);
                        em.merge(intDetail);
                    }
                }
            }
            tx.commit();
        }

    }


    /**
     * function for rejecting associated list of Integrations .
     *
     * @param intDetails               Integration object to be approved.
     */
    public void rejectAssociatedInt(List<VaultIntegration> intDetails) {
        try(Transaction tx = persistence.createTransaction()){
            if(!intDetails.isEmpty()){
                EntityManager em= persistence.getEntityManager();
                for(VaultIntegration intDetail : intDetails){
                    if(intDetail.getIntWorkflowStatus().equals(Status.PENDING_FOR_APPROVAL)){
                        intDetail.setIntWorkflowStatus(Status.DRAFT);
                        em.merge(intDetail);
                    }
                }
            }
            tx.commit();
        }
    }
    public List<String> getAssetList () {
        try(Transaction tx = persistence.createTransaction()){
            TypedQuery<VaultIntegration> query = null;
            EntityManager em = persistence.getEntityManager();
            query = em.createQuery("select e from vault_Integration e", VaultIntegration.class);
            List<VaultIntegration> list = query.getResultList();
            if (!list.isEmpty()) {
                return list.stream().map(VaultIntegration::getIntAsset).collect(Collectors.toList());
            }
        }
        return Collections.emptyList();
    }
}