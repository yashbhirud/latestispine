package com.capgemini.vault.service;

import com.capgemini.vault.entity.*;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Transaction;
import com.haulmont.cuba.core.TypedQuery;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.UserSessionSource;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.pdfbox.jbig2.util.log.Logger;
import org.apache.pdfbox.jbig2.util.log.LoggerFactory;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import javax.inject.Inject;
import java.io.IOException;
import java.util.*;

@Service(iExportService.NAME)
public class Export implements iExportService {

    @Inject
    private Persistence persistence;
    @Inject
    private UserSessionSource userSessionSource;
    private Logger log = LoggerFactory.getLogger(Export.class);
    @Inject
    private DataManager dataManager;


    /**
     *Export the data from table
     *
     * @param isMain             To identify whether request is from staging or main table
     * @return byte[]
     */
    public byte[] exportVault(boolean isMain) throws IOException {                            //Export Integration and Api details
        XSSFWorkbook workbook = new XSSFWorkbook();
        exportInt(workbook,isMain);
        exportApi(workbook,isMain);
        exportMasterData(workbook);
        workbook.createSheet("Overview");
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        workbook.write(outputStream);
        workbook.close();
        return outputStream.toByteArray();
    }

    private void exportMasterData(XSSFWorkbook workbook) {
        XSSFSheet sheet = workbook.createSheet("References");
        List<String> colnames=Constants.masterColumns;
        Row headers=sheet.createRow(0);
        sheet.setDefaultColumnWidth(20);
        for (int i=0;i<colnames.size();i++)
        {
            Cell headerCell = headers.createCell(i);
            headerCell.setCellStyle(Helper.getHeaderStyle(workbook));
            headerCell.setCellValue(colnames.get(i));
            //sheet.autoSizeColumn(i);
        }

        List<VaultMaster> masterData=getMasterData();
        if(masterData.isEmpty()){
            return;
        }
        for (int i = 0; i < masterData.size(); i++) {
            Row newRow = sheet.createRow(i + 1);
            for (int j = 0; j < colnames.size(); j++) {
                Cell cell = newRow.createCell(j);
                cell.setCellStyle(Helper.getStyle(workbook));
                if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase("Type") && (masterData.get(i).getMstType()!=null))
                    cell.setCellValue(masterData.get(i).getMstType());
                else if ((colnames.get(cell.getColumnIndex()).equalsIgnoreCase("Code")) && (masterData.get(i).getMstCode()!=null))
                    cell.setCellValue(masterData.get(i).getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase("Description") && (masterData.get(i).getMstDesc()!=null))
                    cell.setCellValue(masterData.get(i).getMstDesc());
            }
        }
    }

    private List<VaultMaster> getMasterData() {
        List<VaultMaster> list= dataManager.load(VaultMaster.class).query("e.mstIsactive=true order by e.mstType").list();
        return list;
    }

    /**/

    /**
     *Export the data from API sheet
     *
     * @param workbook             To write into
     * @param isMain                 To identify staging or main
     *
     * */
    public void exportApi(XSSFWorkbook workbook, boolean isMain){              //Export Api details.
        XSSFSheet sheet = workbook.createSheet("APIs");
        List<String> colnames=Constants.apiColumns;
        Row headers=sheet.createRow(0);
        sheet.setDefaultColumnWidth(20);
        for (int i=0;i<colnames.size();i++)
        {
            Cell headerCell = headers.createCell(i);
            headerCell.setCellStyle(Helper.getHeaderStyle(workbook));
            headerCell.setCellValue(colnames.get(i));
        }
        List<VaultApi> apiData=getApiData(isMain);
        if(apiData.isEmpty()){
            return;
        }
        for (int i = 0; i < apiData.size(); i++)
        {
            Row newRow=sheet.createRow(i+1);
            for (int j = 0; j < colnames.size(); j++)
            {
                Cell cell=newRow.createCell(j);
                cell.setCellStyle(Helper.getStyle(workbook));
                if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_NO))
                    cell.setCellValue(cell.getRowIndex());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_ID) && (apiData.get(i).getApi()!=null))
                    cell.setCellValue(apiData.get(i).getApi());
                else if ((colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_ASSET_ID)) && (apiData.get(i).getIntAsset()!=null))
                    cell.setCellValue(apiData.get(i).getIntAsset());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_NAME) && (apiData.get(i).getApiName()!=null))
                    cell.setCellValue(apiData.get(i).getApiName());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_DESC) && (apiData.get(i).getApiDesc()!=null))
                    cell.setCellValue(apiData.get(i).getApiDesc());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_CLASSIFICATION) && (apiData.get(i).getApiMstClass()!=null))
                    cell.setCellValue(apiData.get(i).getApiMstClass().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_OPERATION) && (apiData.get(i).getApiMstOps()!=null))
                    cell.setCellValue(apiData.get(i).getApiMstOps().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_PAYLOAD_SECURE_CLASS) && (apiData.get(i).getApiMstPayloadSecurClass()!=null))
                    cell.setCellValue(apiData.get(i).getApiMstPayloadSecurClass().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_BUSINESS_UNIT) && (apiData.get(i).getApiMstBizUnit()!=null))
                    cell.setCellValue(apiData.get(i).getApiMstBizUnit().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_REGION) && (apiData.get(i).getApiMstRegion()!=null))
                    cell.setCellValue(apiData.get(i).getApiMstRegion().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_COUNTRY) && (apiData.get(i).getApiMstCountry()!=null))
                    cell.setCellValue(apiData.get(i).getApiMstCountry().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_PROJECT) && (apiData.get(i).getApiProj()!=null))
                    cell.setCellValue(apiData.get(i).getApiProj());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_WORKSTREAM) && (apiData.get(i).getApiWorkstream()!=null))
                    cell.setCellValue(apiData.get(i).getApiWorkstream());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_RELEASE) && (apiData.get(i).getApiRelease()!=null))
                    cell.setCellValue(apiData.get(i).getApiRelease());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_PRIORITY) && (apiData.get(i).getApiMstPriority()!=null))
                    cell.setCellValue(apiData.get(i).getApiMstPriority().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_STATUS) && (apiData.get(i).getApiMstSdlcStatus()!=null))
                    cell.setCellValue(apiData.get(i).getApiMstSdlcStatus().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_PLATFORM) && (apiData.get(i).getApiMstPlatform()!=null))
                    cell.setCellValue(apiData.get(i).getApiMstPlatform().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_PLATFORM_VERSION) && (apiData.get(i).getApiPlatformVersion()!=null))
                    cell.setCellValue(apiData.get(i).getApiPlatformVersion());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_SRC_PROTOCOL) && (apiData.get(i).getApiMstSrcProtocol()!=null))
                    cell.setCellValue(apiData.get(i).getApiMstSrcProtocol().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_SRC_PAYLOAD_TYPE) && (apiData.get(i).getApiMstSrcPayloadType()!=null))
                    cell.setCellValue(apiData.get(i).getApiMstSrcPayloadType().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_DESTINATION) && (apiData.get(i).getApiDest()!=null))
                    cell.setCellValue(apiData.get(i).getApiDest());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_DEST_PROTOCOL) && (apiData.get(i).getApiMstDestProtocol()!=null))
                    cell.setCellValue(apiData.get(i).getApiMstDestProtocol().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_DEST_PAYLOADTYPE) && (apiData.get(i).getApiMstDestPayloadType()!=null))
                    cell.setCellValue(apiData.get(i).getApiMstDestPayloadType().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_DESIGN_PATTERN) && (apiData.get(i).getApiMstDesignPattern()!=null))
                    cell.setCellValue(apiData.get(i).getApiMstDesignPattern().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_PROCESSING_TYPE) && (apiData.get(i).getApiMstProcessType()!=null))
                    cell.setCellValue(apiData.get(i).getApiMstProcessType().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_COMPLEXITY) && (apiData.get(i).getApiMstComplexity()!=null))
                    cell.setCellValue(apiData.get(i).getApiMstComplexity().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_THROUGHPUT) && (apiData.get(i).getApiThroughput()!=null))
                    cell.setCellValue(apiData.get(i).getApiThroughput());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_MSG_SIZE) && (apiData.get(i).getApiIntMsgSize()!=null))
                    cell.setCellValue(apiData.get(i).getApiIntMsgSize());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_DEPENDENCY) && (apiData.get(i).getApiDependency()!=null))
                    cell.setCellValue(apiData.get(i).getApiDependency());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_OWNER) && (apiData.get(i).getApiIntOwner()!=null))
                    cell.setCellValue(apiData.get(i).getApiIntOwner());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_BUSINESS_OWNER) && (apiData.get(i).getApiBizOwner()!=null))
                    cell.setCellValue(apiData.get(i).getApiBizOwner());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_REMARKS) && (apiData.get(i).getApiRem()!=null))
                    cell.setCellValue(apiData.get(i).getApiRem());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.API_PAYLOAD) && (apiData.get(i).getApiPayload()!=null))
                    cell.setCellValue(apiData.get(i).getApiPayload());
            }
        }
        int rowCount = sheet.getPhysicalNumberOfRows();
        dataValidation(rowCount,colnames,sheet);
    }


    /**
     *Export the data from Integration sheet
     *
     * @param workbook             To write into
     * @param isMain                 To identify staging or main
     *
     * */
    public void exportInt(XSSFWorkbook workbook,boolean isMain){   //Export Integration details.
        XSSFSheet sheet = workbook.createSheet("Integrations");
        List<String> colnames=Constants.intColumns;
        Row headers=sheet.createRow(0);
        sheet.setDefaultColumnWidth(20);
        for (int i=0;i<colnames.size();i++)
        {
            Cell headerCell = headers.createCell(i);
            headerCell.setCellStyle(Helper.getHeaderStyle(workbook));
            headerCell.setCellValue(colnames.get(i));
        }
        List<VaultIntegration> intData=getIntData(isMain);
        if(intData.isEmpty()){
            return;
        }
        for (int i = 0; i < intData.size(); i++)
        {
            Row newRow=sheet.createRow(i+1);
            for (int j = 0; j < colnames.size(); j++)
            {
                Cell cell=newRow.createCell(j);
                cell.setCellStyle(Helper.getStyle(workbook));
                if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_NO))
                    cell.setCellValue(cell.getRowIndex());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_ID) && (intData.get(i).getIntAsset()!=null))
                    cell.setCellValue(intData.get(i).getIntAsset());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_NAME) && (intData.get(i).getIntName()!=null))
                    cell.setCellValue(intData.get(i).getIntName());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_DESC) && (intData.get(i).getIntDesc()!=null))
                    cell.setCellValue(intData.get(i).getIntDesc());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_PAYLOAD)  && (intData.get(i).getIntPayload()!=null))
                    cell.setCellValue(intData.get(i).getIntPayload());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_PAYLOAD_SECURE_CLASS) && (intData.get(i).getIntMstPayloadSecClass()!=null))
                    cell.setCellValue(intData.get(i).getIntMstPayloadSecClass().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_OPERATION) && (intData.get(i).getIntMstOps()!=null))
                    cell.setCellValue(intData.get(i).getIntMstOps().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_BUSINESS_UNIT) && (intData.get(i).getIntMstBizUnit()!=null))
                    cell.setCellValue(intData.get(i).getIntMstBizUnit().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_REGION) && (intData.get(i).getIntMstRegion()!=null))
                    cell.setCellValue(intData.get(i).getIntMstRegion().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_COUNTRY) && (intData.get(i).getIntMstCountry()!=null))
                    cell.setCellValue(intData.get(i).getIntMstCountry().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_PROJECT) && (intData.get(i).getIntProj()!=null))
                    cell.setCellValue(intData.get(i).getIntProj());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_WORKSTREAM) && (intData.get(i).getIntWorkstream()!=null))
                    cell.setCellValue(intData.get(i).getIntWorkstream());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_STATUS) && (intData.get(i).getIntMstSdlcStatus()!=null))
                    cell.setCellValue(intData.get(i).getIntMstSdlcStatus().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_PLATFORM) && (intData.get(i).getIntMstPlatform()!=null))
                    cell.setCellValue(intData.get(i).getIntMstPlatform().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_PLATFORM_VERSION) && (intData.get(i).getIntPlatformVersion()!=null))
                    cell.setCellValue(intData.get(i).getIntPlatformVersion());//**
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_SRC_PROTOCOL) && (intData.get(i).getIntMstSrcProtocol()!=null))
                    cell.setCellValue(intData.get(i).getIntMstSrcProtocol().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_SRC_PAYLOAD_TYPE) && (intData.get(i).getIntMstSrcPayloadType()!=null))
                    cell.setCellValue(intData.get(i).getIntMstSrcPayloadType().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_DESTINATION) && (intData.get(i).getIntDest()!=null))
                    cell.setCellValue(intData.get(i).getIntDest());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_DEST_PROTOCOL) && (intData.get(i).getIntMstDestProtocol()!=null))
                    cell.setCellValue(intData.get(i).getIntMstDestProtocol().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_DEST_PAYLOAD_TYPE) && (intData.get(i).getIntMstDestPayloadType()!=null))
                    cell.setCellValue(intData.get(i).getIntMstDestPayloadType().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_DESIGN_PATTERN) && (intData.get(i).getIntMstDesignPattern()!=null))
                    cell.setCellValue(intData.get(i).getIntMstDesignPattern().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_PROCESSING_TYPE) && (intData.get(i).getIntMstProcessType()!=null))
                    cell.setCellValue(intData.get(i).getIntMstProcessType().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_COMPLEXITY) && (intData.get(i).getIntMstComplexity()!=null))
                    cell.setCellValue(intData.get(i).getIntMstComplexity().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_SPECIAL_OPS) && (intData.get(i).getIntMstSpecialOps()!=null))
                    cell.setCellValue(intData.get(i).getIntMstSpecialOps().getMstCode());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_IMPLEMENTATION_PATTERN) && (intData.get(i).getIntImplPattern()!=null))
                    cell.setCellValue(intData.get(i).getIntImplPattern());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_MSG_SIZE) && (intData.get(i).getIntMsgSize()!=null))
                    cell.setCellValue(intData.get(i).getIntMsgSize());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_DEPENDENCY) && (intData.get(i).getIntDependency()!=null))
                    cell.setCellValue(intData.get(i).getIntDependency());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_OWNER) && (intData.get(i).getIntOwner()!=null))
                    cell.setCellValue(intData.get(i).getIntOwner());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_BUSINESS_OWNER) && (intData.get(i).getIntBizOwner()!=null))
                    cell.setCellValue(intData.get(i).getIntBizOwner());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_REMARKS) && (intData.get(i).getIntRem()!=null))
                    cell.setCellValue(intData.get(i).getIntRem());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_FUNC_SPEC_LOC) && (intData.get(i).getIntFuncSpecLoc()!=null))
                    cell.setCellValue(intData.get(i).getIntFuncSpecLoc());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_TECH_SPEC_LOC) && (intData.get(i).getIntTechSpecLoc()!=null))
                    cell.setCellValue(intData.get(i).getIntTechSpecLoc());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_CODE_LOC) && (intData.get(i).getIntCodeRepo()!=null))
                    cell.setCellValue(intData.get(i).getIntCodeRepo());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_ARCHITECT) && (intData.get(i).getIntCodeRepo()!=null))
                    cell.setCellValue(intData.get(i).getIntCodeRepo());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_DEVELOPER) && (intData.get(i).getIntCodeRepo()!=null))
                    cell.setCellValue(intData.get(i).getIntCodeRepo());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_VERSION) && (intData.get(i).getIntCodeRepo()!=null))
                    cell.setCellValue(intData.get(i).getIntCodeRepo());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_SOURCE) && (intData.get(i).getIntSrcSys()!=null))
                    cell.setCellValue(intData.get(i).getIntSrcSys());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_THROUGHPUT) && (intData.get(i).getIntThroughput()!=null))
                    cell.setCellValue(intData.get(i).getIntThroughput());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_RELEASE) && (intData.get(i).getIntRelease()!=null))
                    cell.setCellValue(intData.get(i).getIntRelease());
                else if (colnames.get(cell.getColumnIndex()).equalsIgnoreCase(Constants.INT_PRIORITY) && (intData.get(i).getIntMstPriority()!=null))
                    cell.setCellValue(intData.get(i).getIntMstPriority().getMstCode());
            }
        }
        int rowCount = sheet.getPhysicalNumberOfRows();
        dataValidation(rowCount,colnames,sheet);
    }


    /**
     *Get integration data from db
     *
     * @param isMain                      To identify staging or main
     * @return List<VaultApi>           List of API's
     *
     * */
    public List<VaultApi> getApiData(boolean isMain)                                                   //Retrieve Api data from db.
    {
        TypedQuery<VaultApi> query;
        try (Transaction tx = persistence.createTransaction()){
            if(isMain){                                             //data from main table
                query = persistence.getEntityManager().createQuery("select e from vault_Api e where  e.apiWorkflowStatus = 'AP'", VaultApi.class);
            }
            else if(!isMain && userSessionSource.getUserSession().getRoles().contains("BU Administrator")) {            //data from staging table where user is BU admin
                query = persistence.getEntityManager().createQuery("select e from vault_Api e where e.apiLob=?1 and (e.apiWorkflowStatus <> 'AP')", VaultApi.class);
                query.setParameter(1,userSessionSource.getUserSession().getUser().getGroup().getName());
            }
            else if(!isMain && userSessionSource.getUserSession().getRoles().contains("BU User")) {            //data from staging table where user is BU user
                query = persistence.getEntityManager().createQuery("select e from vault_Api e where e.createdBy=?1 and (e.apiWorkflowStatus <> 'AP')", VaultApi.class);
                query.setParameter(1,userSessionSource.getUserSession().getUser().getLogin());
            }
            else{
                query = persistence.getEntityManager().createQuery("select e from vault_Api e where (e.apiWorkflowStatus <> 'AP')", VaultApi.class);
            }
            query.setViewName("vaultApi-view");
            if (!query.getResultList().isEmpty())
                return query.getResultList();
        }
        catch (Exception e){                           //Transaction exception.
            log.error(e.getMessage());
        }
        return Collections.emptyList();
    }


    /**
     *Get integration data from db
     *
     * @param isMain                      To identify staging or main
     * @return List<VaultIntegration>   List of integrations
     *
     * */
    public List<VaultIntegration> getIntData(boolean isMain)                               //Retrieve Integration data from db.
    {
        TypedQuery<VaultIntegration> query;
        try (Transaction tx = persistence.createTransaction()){
            if(isMain){                                             //data from main table
                query = persistence.getEntityManager().createQuery("select e from vault_Integration e where  e.intWorkflowStatus = 'AP'", VaultIntegration.class);
            }
            else if(!isMain && userSessionSource.getUserSession().getRoles().contains("BU Administrator")) {            //data from staging table where user is BU admin
                query = persistence.getEntityManager().createQuery("select e from vault_Integration e where e.intLob=?1 and e.intWorkflowStatus <> 'AP'", VaultIntegration.class);
                query.setParameter(1,userSessionSource.getUserSession().getUser().getGroup().getName());
            }
            else if(!isMain && userSessionSource.getUserSession().getRoles().contains("BU User")) {            //data from staging table where user is BU user
                query = persistence.getEntityManager().createQuery("select e from vault_Integration e where e.createdBy=?1 and (e.intWorkflowStatus <> 'AP')", VaultIntegration.class);
                query.setParameter(1,userSessionSource.getUserSession().getUser().getLogin());
            }
            else{
                query = persistence.getEntityManager().createQuery("select e from vault_Integration e where (e.intWorkflowStatus <> 'AP')", VaultIntegration.class);
            }
            query.setViewName("vaultIntegration-view");
            if (!query.getResultList().isEmpty())
                return query.getResultList();
        }
        catch(Exception e)                      //Transaction exception.
        {
            log.error(e.getMessage());
        }
        return Collections.emptyList();
    }

    public byte[] export() {
        XSSFWorkbook workbook = new XSSFWorkbook();
        int rowCount=100;
        XSSFSheet sheet1 = workbook.createSheet("Integrations");
        List<String> int_colnames = Constants.intColumns;
        Row int_headers = sheet1.createRow(0);
        sheet1.setDefaultColumnWidth(20);
        for (int i = 0; i < int_colnames.size(); i++) {
            Cell headerCell = int_headers.createCell(i);
            headerCell.setCellStyle(Helper.getHeaderStyle(workbook));
            headerCell.setCellValue(int_colnames.get(i));
            //sheet1.setDefaultColumnStyle(i,Helper.getStyle(workbook));
        }
        sheet1.setAutoFilter(new CellRangeAddress(0,0,0,int_colnames.size()));
        dataValidation(rowCount,int_colnames,sheet1);
        int col_index=int_colnames.indexOf("ID");
        //sheet1.setColumnHidden(1,true);
        for(int i=1;i<rowCount;i++){
            Cell cell = sheet1.createRow(i).createCell(col_index,CellType.FORMULA);
            cell.setCellFormula("IF(AC"+ (i+1) + "<>\"\",AC"+(i+1)+",\"\")&IF(A"+(i+1)+"<>\"\",CONCATENATE(\"_\",A"+(i+1)+"),\"\")");
        }


        XSSFSheet sheet2 = workbook.createSheet("APIs");
        List<String> api_colnames = Constants.apiColumns;
        Row api_headers = sheet2.createRow(0);
        sheet2.setDefaultColumnWidth(20);
        for (int i = 0; i < api_colnames.size(); i++) {
            Cell headerCell = api_headers.createCell(i);
            headerCell.setCellStyle(Helper.getHeaderStyle(workbook));
            headerCell.setCellValue(api_colnames.get(i));
        }
        sheet2.setAutoFilter(new CellRangeAddress(0,0,0,api_colnames.size()));
        dataValidation(rowCount,api_colnames,sheet2);
        //sheet2.setColumnHidden(1,true);
        for(int i=1;i<rowCount;i++){
            Cell cell = sheet2.createRow(i).createCell(col_index,CellType.FORMULA);
            cell.setCellFormula("IF(Y"+ (i+1) + "<>\"\",Y"+(i+1)+",\"\")&IF(A"+(i+1)+"<>\"\",CONCATENATE(\"_\",A"+(i+1)+"),\"\")");
        }

        exportMasterData(workbook);
        workbook.createSheet("Instructions");
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);
            workbook.close();
            return outputStream.toByteArray();
        } catch (Exception e) {
            byte[] b=null;
            return b;
        }
    }
    private void dataValidation(int rowCount,List<String> colnames, XSSFSheet sheet) {
        for(String column: colnames) {
            DataValidation dataValidation = null;
            DataValidationConstraint constraint = null;
            DataValidationHelper validationHelper = null;
            List<VaultMaster> masterList = getMasterData();
            List<String> countryList = new ArrayList<>();
            validationHelper = new XSSFDataValidationHelper(sheet);
            CellRangeAddressList addressList = new CellRangeAddressList(1, rowCount, colnames.indexOf(column), colnames.indexOf(column));
            for (VaultMaster master : masterList) {
                if (column.equalsIgnoreCase("Source Protocol") || column.equalsIgnoreCase("Destination Protocol")) {
                    column = "Network Protocol";
                } else if (column.equalsIgnoreCase("Source Payload Type") || column.equalsIgnoreCase("Destination Payload Type")) {
                    column = "Payload Type";
                } else if (column.equalsIgnoreCase("Payload Security Classification")) {
                    column = "Security Classification";
                }
                Boolean a = master.getMstType().equalsIgnoreCase(column) ? countryList.add(master.getMstCode()) : null;
            }
            if (!countryList.isEmpty()) {
                constraint = validationHelper.createExplicitListConstraint(countryList.toArray(new String[0]));
                dataValidation = validationHelper.createValidation(constraint, addressList);
                dataValidation.setShowErrorBox(true);
                sheet.addValidationData(dataValidation);
            }
        }
    }
}