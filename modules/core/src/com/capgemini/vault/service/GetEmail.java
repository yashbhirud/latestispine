package com.capgemini.vault.service;

import com.capgemini.vault.entity.Constants;
import com.capgemini.vault.entity.VaultApi;
import com.capgemini.vault.entity.VaultIntegration;
import com.haulmont.addon.emailtemplates.core.EmailTemplatesAPI;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Query;
import com.haulmont.cuba.core.Transaction;
import com.haulmont.cuba.core.TypedQuery;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.security.entity.Group;
import com.haulmont.cuba.security.entity.User;
import org.apache.pdfbox.jbig2.util.log.Logger;
import org.apache.pdfbox.jbig2.util.log.LoggerFactory;
import org.eclipse.persistence.config.QueryHints;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.persistence.QueryHint;
import java.util.ArrayList;
import java.util.List;

@Service(iGetEmailService.NAME)
public class GetEmail implements iGetEmailService {

    @Inject
    private Persistence persistence;

    @Inject
    private EmailTemplatesAPI emailTemplatesAPI;
    private Logger log = LoggerFactory.getLogger(GetEmail.class);
    @Inject
    private DataManager dataManager;


    /**
     * function for sending data approval notification.
     *
     * @param user User object to check authority
     */
    public void sendDataApprovalPendingNotification(User user) {
        List<String> buemail = new ArrayList<>();
        try (Transaction tx = persistence.createTransaction()) {
            TypedQuery<User> query = persistence.getEntityManager().createQuery("select u from sec$User u where u.group.id = :group", User.class);
            query.setParameter("group", user.getGroup().getId());
            List<User> allUsers = query.getResultList();
            List<String> names = new ArrayList<>();
            for (User u : allUsers) {
                if (u.getUserRoles().get(0).getRole().getName().contains("BU Administrator")) {
                    buemail.add(u.getEmail());
                    names.add(u.getName());
                }
            }
            String allBuEmails = String.join(",", buemail);
            String name = String.join("/", names);
            tx.commit();
            emailTemplatesAPI.buildFromTemplate("approval_request")
                    .setTo(allBuEmails)
                    .setBodyParameter("name", name)
                    .setCc(user.getEmail())
                    .sendEmail();
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    /**
     * function for sending data approval notification.
     *
     * @param userLogin               User Login to check authority
     * @param buEmail                 email of Bu user
     */
    public void sendDataApprovalNotification(String userLogin, String buEmail){
        User usr=getEndUserEmailId(userLogin);
        try{
            emailTemplatesAPI.buildFromTemplate("data_approval")
                    .setTo(usr.getEmail())
                    .setBodyParameter("name", usr.getName())
                    .setCc(buEmail)
                    .sendEmail();
        }
        catch (Exception e){
            log.error(e.getMessage());
        }
    }


    /**
     * function for sending data rejection notification.
     *
     * @param userLogin               User Login to check authority
     * @param buEmail                 email of Bu user
     */
    public void sendDataRejectionNotification(String userLogin, String buEmail){
        User usr=getEndUserEmailId(userLogin);
        try{
            emailTemplatesAPI.buildFromTemplate("data_rejection")
                    .setTo(usr.getEmail())
                    .setBodyParameter("name", usr.getName())
                    .setCc(buEmail)
                    .sendEmail();
        }
        catch (Exception e){
            log.error(e.getMessage());
        }
    }


    /**
     * function for getting user email id using the login.
     *
     * @param userLogin               User Login to check authority
     * @return email                    email id of user.
     */
    public User getEndUserEmailId(String userLogin){
        try(Transaction tx=persistence.createTransaction()) {
            TypedQuery<User> query = persistence.getEntityManager().createQuery("select u from sec$User u where u.login = :login", User.class);
            query.setParameter("login", userLogin);
            return query.getFirstResult();
        }
    }


    /**
     * function for sending mail for resetting password .
     *
     * @param login               User Login to check authority
     * @param password            User password for confirmation
     */
    public void sendResetPasswordEmail(String login, String password){
        User usr = getEndUserEmailId(login);
        try{
            emailTemplatesAPI.buildFromTemplate("reset_password")
                    .setTo(usr.getEmail())
                    .setBodyParameter("name",usr.getName())
                    .setBodyParameter("password",password)
                    .sendEmail();
        }
        catch (Exception e){
            log.error(e.getMessage());
        }
    }

    public List<String> getIntValue(String col,String screen){
        Transaction tx1 = persistence.createTransaction();
        col= Constants.getIntColumnName().get(col);
        Query query1;
        if(screen.equals("main")) {
           query1 = persistence.getEntityManager().createNativeQuery("select distinct " + col + " from vault_Integration where int_workflow_status='AP'");
        }
        else{
           query1 = persistence.getEntityManager().createNativeQuery("select distinct " + col + " from vault_Integration where int_workflow_status<>'AP'");
        }
        List<String> list = query1.getResultList();
        if(list.contains(null)){
            list.remove(null);
        }
        if(list.contains("")){
            list.remove("");
        }
        tx1.commit();
        return list;
    }
    public List<VaultApi> getApiValue(String col,String screen){
        Transaction tx1 = persistence.createTransaction();
        col=Constants.getApiColumnName().get(col);
        Query query1;
        if(screen.equals("main")) {
           query1 = persistence.getEntityManager().createNativeQuery("select distinct " + col + " from vault_Api where api_workflow_status='AP'");
        }else {
           query1 = persistence.getEntityManager().createNativeQuery("select distinct " + col + " from vault_Api where api_workflow_status<>'AP'");
        }
        List<VaultApi> list = query1.getResultList();
        if(list.contains(null)){
            list.remove(null);
        }
        if(list.contains("")){
            list.remove("");
        }
        tx1.commit();
        return list;
    }

}