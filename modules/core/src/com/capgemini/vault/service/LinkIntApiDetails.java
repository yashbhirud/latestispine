package com.capgemini.vault.service;

import com.capgemini.vault.entity.VaultApi;
import com.capgemini.vault.entity.VaultIntegrationApi;
import com.capgemini.vault.entity.VaultIntegration;
import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Transaction;
import com.haulmont.cuba.core.TypedQuery;
import com.haulmont.cuba.core.global.DataManager;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service(iLinkIntApiDetailsService.NAME)


public class LinkIntApiDetails implements iLinkIntApiDetailsService {

    @Inject
    public Persistence persistence;
    @Inject
    protected DataManager dataManager;


    /**
     * function for saving the linked object for the list of api for an integration.
     *
     * @param apiDetailsList            List of Api objects
     * @param integration               Integration object
     */
    public void commitIntApiDetails(Collection<VaultApi> apiDetailsList, VaultIntegration integration){

        try(Transaction tx = persistence.createTransaction()){

            TypedQuery<VaultIntegrationApi> query = persistence.getEntityManager().createQuery("select e from vault_IntegrationApi e where e.vaultIntegration=?1", VaultIntegrationApi.class);
            query.setViewName("vaultApiIntegration-browse-view");
            query.setParameter(1,integration);
            List<VaultIntegrationApi> list = query.getResultList();
            if (!list.isEmpty()){
                List<VaultApi> apiList = list.stream().map(VaultIntegrationApi::getVaultApi)
                        .collect(Collectors.toList());
                List<VaultApi> deleteList = new ArrayList<>(CollectionUtils.subtract(apiList,apiDetailsList));
                List<VaultApi> addList = new ArrayList<>(CollectionUtils.subtract(apiDetailsList,apiList));

                EntityManager em = persistence.getEntityManager();
                deleteIntApiLink(deleteList,integration,em);
                addIntApiLink(addList,integration,em);
            }else{
                for(VaultApi api : apiDetailsList){
                    VaultIntegrationApi intApiDetails = dataManager.create(VaultIntegrationApi.class);
                    intApiDetails.setVaultApi(api);
                    intApiDetails.setVaultIntegration(integration);
                    persistence.getEntityManager().persist(intApiDetails);
                }
            }
            tx.commit();
        }
    }


    /**
     * function for saving the linked object for the list of integrations for an api.
     *
     * @param intDetailsList            List of integration objects
     * @param api                       Api object
     */
    public void commitApiIntDetails(Collection<VaultIntegration> intDetailsList, VaultApi api){

        try(Transaction tx = persistence.createTransaction()){

            TypedQuery<VaultIntegrationApi> query = persistence.getEntityManager().createQuery("select e from vault_IntegrationApi e where e.vaultApi=?1", VaultIntegrationApi.class);
            query.setViewName("vaultApiIntegration-browse-view");
            query.setParameter(1,api);
            List<VaultIntegrationApi> list = query.getResultList();
            if (!list.isEmpty()){
                List<VaultIntegration> intList = list.stream().map(VaultIntegrationApi::getVaultIntegration)
                        .collect(Collectors.toList());
                List<VaultIntegration> deleteList = new ArrayList<>(CollectionUtils.subtract(intList,intDetailsList));
                List<VaultIntegration> addList = new ArrayList<>(CollectionUtils.subtract(intDetailsList,intList));
                updateAssetId(intDetailsList,api);
                EntityManager em = persistence.getEntityManager();
                deleteApiIntLink(deleteList,api,em);
                addApiIntLink(addList,api,em);
            }else{
                for(VaultIntegration intD : intDetailsList){
                    VaultIntegrationApi intApiDetails = dataManager.create(VaultIntegrationApi.class);
                    intApiDetails.setVaultApi(api);
                    intApiDetails.setVaultIntegration(intD);
                    persistence.getEntityManager().persist(intApiDetails);
                }
                updateAssetId(intDetailsList,api);
            }
            tx.commit();
        }
    }


    /**
     * function for updating the int asset id of apis after linking.
     *
     * @param intDetailsList            List of Integration objects
     * @param api                       api object
     */
    private void updateAssetId(Collection<VaultIntegration> intDetailsList, VaultApi api) {
        String assetId;
        if(intDetailsList.isEmpty()){
            assetId = null;
        }
        else {
            List<String> assetIdList = intDetailsList.stream().map(VaultIntegration::getIntAsset).collect(Collectors.toList());
            assetId = StringUtils.join(assetIdList, ',');
        }
        api.setIntAsset(assetId);
        persistence.getEntityManager().merge(api);

    }


    /**
     * function for deleting the link between api and integration.
     *
     * @param objList                   List of Integration objects
     * @param obj                       api object
     * @param em                       Entity manager object
     */
    public void deleteApiIntLink (List<VaultIntegration> objList,VaultApi obj,EntityManager em)//to check if corresponding integration record is present.
    {
        for(VaultIntegration intObj : objList){
            List<VaultIntegrationApi> list ;
            TypedQuery<VaultIntegrationApi> query = em.createQuery("select e from vault_IntegrationApi e where e.vaultIntegration=?1 and e.vaultApi=?2", VaultIntegrationApi.class);
            query.setViewName("vaultIntegrationApi-view");
            query.setParameter(1,intObj);
            query.setParameter(2,obj);
            list = query.getResultList();
            if (!list.isEmpty()){
                VaultIntegrationApi intApiDetail = list.get(0);
                em.remove(intApiDetail);
            }
        }
    }


    /**
     * function for deleting the link between integration and api.
     *
     * @param objList                   List of Api objects
     * @param obj                       Integration object
     * @param em                        Entity manager object
     */
    public void deleteIntApiLink (List<VaultApi> objList,VaultIntegration obj,EntityManager em)//to check if corresponding integration record is present.
    {
        for(VaultApi apiObj : objList){
            List<VaultIntegrationApi> list ;
            TypedQuery<VaultIntegrationApi> query = em.createQuery("select e from vault_IntegrationApi e where e.vaultIntegration=?1 and e.vaultApi=?2", VaultIntegrationApi.class);
            query.setViewName("vaultIntegrationApi-view");
            query.setParameter(1,obj);
            query.setParameter(2,apiObj);
            list = query.getResultList();
            if (!list.isEmpty()){
                VaultIntegrationApi intApiDetail = list.get(0);
                em.remove(intApiDetail);
            }
        }
    }


    /**
     * function for adding the link between api and integration.
     *
     * @param objList                   List of Integration objects
     * @param obj                       api object
     * @param em                       Entity manager object
     */
    public void addApiIntLink (List<VaultIntegration> objList,VaultApi obj,EntityManager em)//to check if corresponding integration record is present.
    {
        for(VaultIntegration intObj : objList){
            VaultIntegrationApi intApiDetails = dataManager.create(VaultIntegrationApi.class);
            intApiDetails.setVaultApi(obj);
            intApiDetails.setVaultIntegration(intObj);
            em.persist(intApiDetails);
        }
    }


    /**
     * function for adding the link between integration and api.
     *
     * @param objList                   List of Api objects
     * @param obj                       Integration object
     * @param em                        Entity manager object
     */
    public void addIntApiLink (List<VaultApi> objList,VaultIntegration obj,EntityManager em)//to check if corresponding integration record is present.
    {
        for(VaultApi apiObj : objList){
            VaultIntegrationApi intApiDetails = dataManager.create(VaultIntegrationApi.class);
            intApiDetails.setVaultApi(apiObj);
            intApiDetails.setVaultIntegration(obj);
            em.persist(intApiDetails);
        }
    }
}