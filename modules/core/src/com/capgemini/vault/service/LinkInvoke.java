package com.capgemini.vault.service;

import org.apache.commons.compress.archivers.zip.ZipUtil;
import org.apache.pdfbox.jbig2.util.log.Logger;
import org.apache.pdfbox.jbig2.util.log.LoggerFactory;
import org.springframework.stereotype.Service;
import com.capgemini.vault.entity.Constants;
import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

import java.awt.*;
import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.*;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

@Service(iLinkInvokeService.NAME)
public class LinkInvoke implements iLinkInvokeService{

    private Logger log = LoggerFactory.getLogger(LinkInvoke.class);
    List<String> filesListInDir = new ArrayList<String>();
    public List<Object> linkinvokefunction(String url) {

        if (!url.matches("^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]")) {
            return null;
        }
        if(url.contains("gitlab.com")) {
            return invokeGitLabLink(url);
        }
        else if(url.contains("bitbucket.org")) {
            return invokeBitBucketLink(url);
        }
        else {
            return invokeExchangeLink(url);
        }
    }

    private List<Object> invokeExchangeLink(String url) {
        try {
            URI uri = new URI(url);
            Desktop.getDesktop().browse(uri);
            return Collections.emptyList();
        } catch (Exception e) {
            return null;
        }
    }

    private List<Object> invokeBitBucketLink(String url) {
        List<Object> list = new ArrayList();
        String username=url.split("/")[3];
        String repoName=url.split("/")[4];
        String filename = repoName.split(".git")[0];
        URI uri = null;
        try {
            uri = new URI(url);
            URL url1 = new URL("https://bitbucket.org/"+username+"/"+repoName+"/get/HEAD.zip");
            HttpURLConnection connection = (HttpURLConnection) url1.openConnection();
            String userpass = username + ":" + Constants.BIT_BUCKET_PASSWORD;
            String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userpass.getBytes(StandardCharsets.UTF_8)));
            connection.setRequestProperty("Authorization", basicAuth);
            connection.setRequestMethod("GET");
            InputStream connectionDataStream = connection.getInputStream();
            byte[] b;
            b = new byte[(int) connectionDataStream.available()];
            connectionDataStream.read(b);
            list.add(b);
            list.add(filename+".zip");
            return list;
        } catch (Exception e) {
            try {
                Desktop.getDesktop().browse(uri);
            } catch (IOException ex) {
                return null;
            }
            return null;
        }
    }

    private List<Object> invokeGitLabLink(String url) {
        URI uri = null;
        try {
            uri = new URI(url);
            String repoLink = null;
            String branch = null;
            String filePath = null;
            List<Object> list = new ArrayList();

            String[] splitForLink = url.split("/-/");
            repoLink = splitForLink[0] + ".git";
            String secondPart = splitForLink[1];
            String[] combinedPath = secondPart.split("/", 3);
            String fileType = combinedPath[0];
            branch = combinedPath[1];
            filePath = combinedPath[2];

            File localPath = new File("C:\\git");
            FileUtils.deleteDirectory(localPath);
            localPath.mkdir();
            Git repo = null;

            if (fileType.equals("archive")) {

                filePath = filePath.split("\\?")[1];
                filePath = filePath.split("=")[1];
                filePath = URLDecoder.decode(filePath,StandardCharsets.UTF_8.name());
            } else if (fileType.equals("raw")) {
                filePath = filePath.split("\\?")[0];
            }

            repo = Git.cloneRepository()
                    .setURI(repoLink)
                    .setDirectory(localPath)
                    .setCredentialsProvider(new UsernamePasswordCredentialsProvider(Constants.GIT_USERNAME, Constants.GIT_PASSWORD))
                    .setBranchesToClone(Arrays.asList(branch))
                    .setCloneAllBranches(false)
                    .setCloneSubmodules(true)
                    .setNoCheckout(true)
                    .call();
            repo.checkout().setStartPoint("origin/" + branch).addPath(filePath).call();
            repo.close();
            byte[] b = new byte[1024];
            String path = localPath + "\\" + filePath.replace('/', '\\');
            String filename = filePath.split("/")[filePath.split("/").length - 1];
            if (fileType.equals("archive")) {
                try {
                    pack(path,localPath+"\\"+filename+".zip");
                    list.add(null);
                    list.add("saved");
                    return list;
                }
                catch (Exception ioe) {
                    ioe.printStackTrace();
                }


            } else if (fileType.equals("raw")) {
                RandomAccessFile file;
                file = new RandomAccessFile(path, "r");
                b = new byte[(int) file.length()];
                file.readFully(b);
                file.close();
                list.add(b);
                list.add(filename);
                return list;
            }
            return null;
        } catch (Exception e) {
            Desktop desktop = Desktop.getDesktop();
            try {
                desktop.browse(uri);
                return null;
            } catch (IOException ex) {
                return null;
            }
        }
    }
    public static void pack(String sourceDirPath, String zipFilePath) throws IOException {
        Path p = Files.createFile(Paths.get(zipFilePath));
        try (ZipOutputStream zs = new ZipOutputStream(Files.newOutputStream(p));
             ByteArrayOutputStream bs=new ByteArrayOutputStream();
        ) {
            Path pp = Paths.get(sourceDirPath);
            Files.walk(pp)
                    .filter(path -> !Files.isDirectory(path))
                    .forEach(path -> {
                        ZipEntry zipEntry = new ZipEntry(pp.relativize(path).toString());
                        try {
                            zs.putNextEntry(zipEntry);
                            Files.copy(path, zs);
                            zs.closeEntry();
                        } catch (IOException e) {
                        }
                    });
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}