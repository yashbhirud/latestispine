package com.capgemini.vault.service;
import com.capgemini.vault.entity.*;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Transaction;
import com.haulmont.cuba.core.TypedQuery;
import com.haulmont.cuba.security.entity.User;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.pdfbox.jbig2.util.log.Logger;
import org.apache.pdfbox.jbig2.util.log.LoggerFactory;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import javax.inject.Inject;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;


@Service(iReadFileService.NAME)
public class ReadFile implements iReadFileService {

    @Inject
    public Persistence persistence;
    private Logger log = LoggerFactory.getLogger(ReadFile.class);

    /**
     * function for reading file , called from controller
     *
     * @param fileid            File object for reading file
     * @param name              file name of the imported file
     * @param user              User object - contains login , role , name


     * @return string message regarding upload.
     */
    public String readVault(File fileid, String name, User user) {
        try (FileInputStream file = new FileInputStream(fileid);
             XSSFWorkbook workbook = new XSSFWorkbook(file)){
            VaultUploadSummary apiupl=null;
            VaultUploadSummary intupl=null;
            int flaga=0,flagi=0;
            if (workbook.getNumberOfSheets() != 4) {
                return "Invalid excel format. Please use given reference excel template.";
            }
            XSSFSheet integrationSheet = workbook.getSheet(Constants.INT_SHEET);
            boolean check = checkIfWorkbookEmpty(workbook);
            if (check) {
                return "Could not process. Minimum 1 record required in Integration/API sheet";
            }
            if (integrationSheet != null) {
                intupl=readIntDetails(integrationSheet, name, user);
                if (intupl.getUpsSuccessRecords()==null)
                    flagi=1;
            }
            XSSFSheet apiSheet = workbook.getSheet(Constants.API_SHEET);
            if (apiSheet != null) {
                apiupl=readApiDetails(apiSheet, name, user);
                if (apiupl.getUpsSuccessRecords()==null)
                    flaga=1;
            }
            if (apiSheet == null && integrationSheet == null) {
                log.error("Sheets not found");
                return "Neither API nor Integration Sheet found";
            }

            log.info("File uploaded successfully");

            if (flaga==0 && flagi==0)
                return "File uploaded";
            else
                return "File already uploaded.Only changed records updated.";
        }
        catch(IOException e)
        {
            log.error("File Upload failed due to exception");
        }
        return "File Upload failed due to exception";
    }



    /**
     * Populates error object in db.
     *
     * @param uploadObj            Upload Summary object for error record.

     */
    private void populateErrList(List<VaultErrSummary> errorList, VaultUploadSummary uploadObj)
    {
        //persisting errors in error table.
        for(VaultErrSummary errorObj:errorList)
        {
            errorObj.setErrUps(uploadObj);
            persistence.getEntityManager().persist(errorObj);
        }
        errorList.clear();
    }



    /**
     *reading api details from sheet.
     *
     * @param apiSheet             XSSF sheet object
     * @param filename          file name of the imported file
     * @param user              User object - contains login , role , name


     * @return upload summary object
     */
    public VaultUploadSummary readApiDetails(XSSFSheet apiSheet,String filename,User user)
    {
        List<VaultErrSummary> apiErrorList=new ArrayList<>();
        int apiRowErrorCount=0;
        int apiSuccessCount=0;
        VaultUploadSummary uploadSummary=persistUpload(Constants.API_SHEET,filename);
        int apiErrorCount=0;
        try(Transaction tx = persistence.createTransaction()) {
            String apiId = null;
            String validIntValues="";
            VaultIntegration intasset ;
            VaultMaster classification ;
            VaultMaster supportedOperations ;
            VaultMaster securityClassification ;
            VaultMaster brandOrBu ;
            VaultMaster region ;
            VaultMaster country ;
            VaultMaster priority ;
            VaultMaster status ;
            VaultMaster platform ;
            VaultMaster sourceProtocol;
            VaultMaster sourcePayloadType;
            VaultMaster processingType;
            Status internalStatus;
            VaultMaster destinationProtocol ;
            VaultMaster destinationPayloadType;
            VaultMaster designPattern ;
            VaultMaster complexity ;
            DataFormatter formatter = new DataFormatter();
            List<String> validApiId=new ArrayList<>();

            //contains column names  from xl sheet.
            List<String> colnames = new ArrayList<>();
            colnames=addColumns(colnames,apiSheet);
            Iterator<Row> rowIterator = apiSheet.iterator();
            rowIterator.next();

            //iterator for row
            while (rowIterator.hasNext()) {
                VaultApi api = new VaultApi();
                apiErrorCount=0;
                Row row = rowIterator.next();
                //Iterator<Cell> columnIterator = row.cellIterator();
                internalStatus=Status.DRAFT;
                outerloop:
                if (!checkIfRowIsEmpty(row)) {
                    Set<VaultIntegration> validIntObj = new HashSet<>();
                    //iterator for cell
                    for(int i = 1;i<colnames.size();i++){
                        //while (columnIterator.hasNext()) {
                        Cell cell = row.getCell(i);
                        if(cell==null){
                            cell = row.createCell(i);
                            cell.setCellValue("");
                        }
                        Object value;
                        if (Constants.getRestrictedColumns().contains(colnames.get(cell.getColumnIndex()))) {
                            continue;
                        }
                        switch (colnames.get(cell.getColumnIndex()).toUpperCase()) {
                            case Constants.API_ID: {
                                value = readCellByType(cell);
                                if (value != "") {
                                    apiId = value.toString();
                                    VaultApi check = getApiDetails(apiId);
                                    if (check != null && !check.getCreatedBy().equals(user.getLogin())) {
                                        if (user.getUserRoles().contains(Constants.BU_ADMINISTRATOR_ROLE)) {
                                            if (!getGroup(check.getCreatedBy()).equals(user.getGroup().getName())) {
                                                persistError(apiErrorList, Constants.DUPLICATE_API_ID_ERROR_MSG, row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                                apiErrorCount++;
                                                break outerloop;
                                            }
                                        } else {
                                            persistError(apiErrorList, Constants.DUPLICATE_API_ID_ERROR_MSG, row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                            apiErrorCount++;
                                            break outerloop;
                                        }
                                    }
                                    if (validApiId.contains(apiId)) {
                                        persistError(apiErrorList, Constants.DUPLICATE_API_ID_ERROR_MSG, row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                        apiErrorCount++;
                                        break outerloop;
                                    } else
                                        validApiId.add(apiId);
                                    api.setApi(apiId);
                                } else {
                                    persistError(apiErrorList, Constants.API_ID_NOT_NULL, row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                    apiErrorCount++;
                                    break outerloop;
                                }
                            }
                            break;

                            case Constants.API_ASSET_ID: {
                                value = readCellByType(cell);
                                if (value != "") {
                                    validIntObj = new HashSet<>();
                                    // for single int id
                                    if (cell.getCellType() == CellType.NUMERIC) {
                                        intasset = checkInt(value.toString(), user);
                                        if (intasset == null) {
                                            persistError(apiErrorList, Constants.GET_INT_ID_NOT_AVAIABLE(value.toString()), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                            internalStatus = Status.ERROR;
                                            apiErrorCount++;
                                            validIntValues = "";
                                            break;
                                        }
                                        validIntValues = intasset.getIntAsset();
                                        validIntObj.add(intasset);
                                    }
                                    //for multiple int ids.
                                    else {
                                        List<String> intList = Arrays.asList(value.toString().split(","));
                                        Set<String> validList = new HashSet<>();
                                        for (String temp : intList) {
                                            intasset = checkInt(temp, user);
                                            if (intasset == null) {
                                                persistError(apiErrorList, Constants.GET_INT_ID_NOT_AVAIABLE(temp), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                                internalStatus = Status.ERROR;
                                                apiErrorCount++;
                                            } else {
                                                validList.add(temp);
                                                validIntObj.add(intasset);
                                            }
                                        }
                                        validIntValues = String.join(",", validList);
                                    }
                                } else {
                                    validIntValues = "blank";
                                }

                            }
                            break;
                            case Constants.API_NAME: {
                                value = readCellByType(cell);
                                if (value != "") {
                                    api.setApiName(value.toString());
                                } else {
                                    api.setApiName("");
                                    persistError(apiErrorList, Constants.API_NAME_NOT_NULL, row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                    internalStatus = Status.ERROR;
                                    apiErrorCount++;
                                }

                            }
                            break;
                            case Constants.API_DESC: {
                                value = readCellByType(cell);
                                if(value!=""){
                                    api.setApiDesc(value.toString());
                                }
                                else{
                                    api.setApiDesc("");
                                }
                            }
                            break;
                            case Constants.API_CLASSIFICATION: {
                                value = readCellByType(cell);
                                if (value != "") {
                                    classification = getMasterDataEntity( value.toString(), "Classification");
                                    if (classification == null) {
                                        persistError(apiErrorList, Constants.GET_VALUE_INVALID_DESC(Constants.API_CLASSIFICATION), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                        internalStatus = Status.ERROR;
                                        apiErrorCount++;
                                    }
                                }
                                else{
                                    classification=null;
                                    persistError(apiErrorList,Constants.GET_NOT_NULL_DESC(Constants.API_CLASSIFICATION), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                    internalStatus=Status.ERROR;
                                    apiErrorCount++;
                                }
                                api.setApiMstClass(classification);
                            }
                            break;
                            case Constants.API_OPERATION: {
                                value = readCellByType(cell);
                                if(value!=""){
                                    supportedOperations = getMasterDataEntity(value.toString(), "Operation");
                                    if (supportedOperations==null){
                                        persistError(apiErrorList,Constants.GET_VALUE_INVALID_DESC(Constants.API_OPERATION), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                        internalStatus=Status.ERROR;
                                        apiErrorCount++;
                                    }
                                }
                                else{
                                    supportedOperations=null;
                                    persistError(apiErrorList,Constants.GET_NOT_NULL_DESC(Constants.API_OPERATION), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                    internalStatus=Status.ERROR;
                                    apiErrorCount++;
                                }
                                api.setApiMstOps(supportedOperations);
                            }
                            break;
                            case Constants.API_PAYLOAD_SECURE_CLASS: {
                                value = readCellByType(cell);
                                if(value!=""){
                                    securityClassification = getMasterDataEntity( value.toString(), "Security Classification");
                                    if (securityClassification==null){
                                        persistError(apiErrorList,Constants.GET_VALUE_INVALID_DESC(Constants.API_PAYLOAD_SECURE_CLASS), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                        internalStatus=Status.ERROR;
                                        apiErrorCount++;
                                    }
                                }
                                else{
                                    securityClassification=null;
                                }
                                api.setApiMstPayloadSecurClass(securityClassification);
                            }
                            break;
                            case Constants.API_BUSINESS_UNIT: {
                                value = readCellByType(cell);
                                if(value!=""){
                                    brandOrBu = getMasterDataEntity( value.toString(), "Business Unit");
                                    if (brandOrBu==null){
                                        persistError(apiErrorList,Constants.GET_VALUE_INVALID_DESC(Constants.API_BUSINESS_UNIT), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                        internalStatus=Status.ERROR;
                                        apiErrorCount++;
                                    }
                                }
                                else{
                                    brandOrBu=null;
                                    persistError(apiErrorList,Constants.GET_NOT_NULL_DESC(Constants.API_BUSINESS_UNIT), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                    internalStatus=Status.ERROR;
                                    apiErrorCount++;
                                }
                                api.setApiMstBizUnit(brandOrBu);
                            }
                            break;
                            case Constants.API_REGION: {
                                value = readCellByType(cell);
                                if(value!=""){
                                    region = getMasterDataEntity( formatter.formatCellValue(cell), "Region");
                                    if (region==null){
                                        persistError(apiErrorList,Constants.GET_VALUE_INVALID_DESC(Constants.API_REGION), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                        internalStatus=Status.ERROR;
                                        apiErrorCount++;
                                    }
                                }
                                else{
                                    region=null;
                                    persistError(apiErrorList,Constants.GET_NOT_NULL_DESC(Constants.API_REGION), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                    internalStatus=Status.ERROR;
                                    apiErrorCount++;
                                }
                                api.setApiMstRegion(region);
                            }
                            break;
                            case Constants.API_COUNTRY: {
                                value = readCellByType(cell);
                                if(value!=""){
                                    country = getMasterDataEntity( value.toString(), "Country");
                                    if (country==null){
                                        persistError(apiErrorList,Constants.GET_NOT_NULL_DESC(Constants.API_COUNTRY), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                        internalStatus = Status.ERROR;
                                        apiErrorCount++;
                                    }
                                }
                                else{
                                    country=null;
                                }
                                api.setApiMstCountry(country);
                            }
                            break;
                            case Constants.API_PROJECT: {
                                value = readCellByType(cell);

                                if(value!=""){
                                    api.setApiProj(value.toString());
                                }
                                else{
                                    api.setApiProj("");
                                }
                            }
                            break;
                            case Constants.API_WORKSTREAM: {
                                value = readCellByType(cell);
                                if(value!=""){
                                    api.setApiWorkstream(value.toString());
                                }
                                else{
                                    api.setApiWorkstream("");
                                }
                            }
                            break;
                            case Constants.API_RELEASE: {
                                value = readCellByType(cell);
                                if(value!=""){
                                    api.setApiRelease(value.toString());
                                }
                                else{
                                    api.setApiRelease("");
                                }
                            }
                            break;
                            case Constants.API_PRIORITY: {
                                value = readCellByType(cell);
                                if(value!=""){
                                    priority = getMasterDataEntity( value.toString(), "Priority");
                                    if (priority==null){
                                        persistError(apiErrorList,Constants.GET_VALUE_INVALID_DESC(Constants.API_PRIORITY), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                        internalStatus = Status.ERROR;
                                        apiErrorCount++;
                                    }
                                }
                                else{
                                    priority=null;
                                }
                                api.setApiMstPriority(priority);
                            }
                            break;
                            case Constants.API_STATUS: {
                                value = readCellByType(cell);
                                if(value!=""){
                                    status = getMasterDataEntity( value.toString(), "Status");
                                    if (status==null){
                                        persistError(apiErrorList,Constants.GET_VALUE_INVALID_DESC(Constants.API_STATUS), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                        internalStatus=Status.ERROR;
                                        apiErrorCount++;
                                    }
                                }
                                else{
                                    status=null;
                                    persistError(apiErrorList,Constants.GET_NOT_NULL_DESC(Constants.API_STATUS), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                    internalStatus=Status.ERROR;
                                    apiErrorCount++;
                                }
                                api.setApiMstSdlcStatus(status);
                            }
                            break;
                            case Constants.API_PLATFORM: {
                                value = readCellByType(cell);
                                if(value!=""){
                                    platform = getMasterDataEntity( value.toString(), "Platform");
                                    if (platform==null){
                                        persistError(apiErrorList,Constants.GET_VALUE_INVALID_DESC(Constants.API_PLATFORM), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                        internalStatus=Status.ERROR;
                                        apiErrorCount++;
                                    }
                                }
                                else{
                                    platform=null;
                                    persistError(apiErrorList,Constants.GET_NOT_NULL_DESC(Constants.API_PLATFORM), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                    internalStatus=Status.ERROR;
                                    apiErrorCount++;
                                }
                                api.setApiMstPlatform(platform);
                            }
                            break;
                            case Constants.API_PLATFORM_VERSION: {
                                value = readCellByType(cell);
                                if(value!=""){
                                    api.setApiPlatformVersion(value.toString());
                                }
                                else{
                                    api.setApiPlatformVersion("");
                                    persistError(apiErrorList,Constants.GET_NOT_NULL_DESC(Constants.API_PLATFORM_VERSION), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                    internalStatus=Status.ERROR;
                                    apiErrorCount++;
                                }
                            }
                            break;
                            case Constants.API_SRC_PROTOCOL: {
                                value = readCellByType(cell);
                                if(value!=""){
                                    sourceProtocol = getMasterDataEntity( value.toString(), "Network Protocol");
                                    if (sourceProtocol==null){
                                        persistError(apiErrorList,Constants.GET_VALUE_INVALID_DESC(Constants.API_SRC_PROTOCOL), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                        internalStatus=Status.ERROR;
                                        apiErrorCount++;
                                    }
                                }
                                else{
                                    sourceProtocol=null;
                                    persistError(apiErrorList,Constants.GET_NOT_NULL_DESC(Constants.API_SRC_PROTOCOL), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                    internalStatus=Status.ERROR;
                                    apiErrorCount++;
                                }
                                api.setApiMstSrcProtocol(sourceProtocol);
                            }
                            break;
                            case Constants.API_SRC_PAYLOAD_TYPE: {
                                value = readCellByType(cell);
                                if(value!=""){
                                    sourcePayloadType = getMasterDataEntity( value.toString(), "Payload Type");
                                    if (sourcePayloadType==null){
                                        persistError(apiErrorList,Constants.GET_VALUE_INVALID_DESC(Constants.API_SRC_PAYLOAD_TYPE), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                        internalStatus=Status.ERROR;
                                        apiErrorCount++;
                                    }
                                }
                                else{
                                    sourcePayloadType=null;
                                    persistError(apiErrorList,Constants.GET_NOT_NULL_DESC(Constants.API_SRC_PAYLOAD_TYPE), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                    internalStatus=Status.ERROR;
                                    apiErrorCount++;
                                }
                                api.setApiMstSrcPayloadType(sourcePayloadType);
                            }
                            break;
                            case Constants.API_DESTINATION: {
                                value = readCellByType(cell);
                                if(value!=""){
                                    api.setApiDest(value.toString());
                                }
                                else{
                                    api.setApiDest("");
                                }
                            }
                            break;
                            case Constants.API_DEST_PROTOCOL: {
                                value = readCellByType(cell);
                                if(value!=""){
                                    destinationProtocol = getMasterDataEntity(value.toString(), "Network Protocol");
                                    if (destinationProtocol==null){
                                        persistError(apiErrorList,Constants.GET_VALUE_INVALID_DESC(Constants.API_DEST_PROTOCOL), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                        internalStatus=Status.ERROR;
                                        apiErrorCount++;
                                    }
                                }
                                else{
                                    destinationProtocol=null;
                                    persistError(apiErrorList,Constants.GET_NOT_NULL_DESC(Constants.API_DEST_PROTOCOL), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                    internalStatus=Status.ERROR;
                                    apiErrorCount++;
                                }
                                api.setApiMstDestProtocol(destinationProtocol);
                            }
                            break;
                            case Constants.API_DEST_PAYLOADTYPE: {
                                value = readCellByType(cell);
                                if(value!=""){
                                    destinationPayloadType = getMasterDataEntity( value.toString(), "Payload Type");
                                    if (destinationPayloadType==null){
                                        persistError(apiErrorList,Constants.GET_VALUE_INVALID_DESC(Constants.API_DEST_PAYLOADTYPE), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                        internalStatus=Status.ERROR;
                                        apiErrorCount++;
                                    }
                                }
                                else{
                                    destinationPayloadType=null;
                                    persistError(apiErrorList,Constants.GET_NOT_NULL_DESC(Constants.API_DEST_PAYLOADTYPE), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                    internalStatus=Status.ERROR;
                                    apiErrorCount++;
                                }
                                api.setApiMstDestPayloadType(destinationPayloadType);
                            }
                            break;
                            case Constants.API_DESIGN_PATTERN: {
                                value = readCellByType(cell);
                                if(value!=""){
                                    designPattern = getMasterDataEntity( value.toString(), "Design Pattern");
                                    if (designPattern==null){
                                        persistError(apiErrorList,Constants.GET_VALUE_INVALID_DESC(Constants.API_DESIGN_PATTERN), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                        internalStatus=Status.ERROR;
                                        apiErrorCount++;
                                    }
                                }
                                else{
                                    designPattern=null;
                                    persistError(apiErrorList,Constants.GET_NOT_NULL_DESC(Constants.API_DESIGN_PATTERN), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                    internalStatus=Status.ERROR;
                                    apiErrorCount++;
                                }
                                api.setApiMstDesignPattern(designPattern);
                            }
                            break;
                            case Constants.API_PROCESSING_TYPE: {
                                value = readCellByType(cell);
                                if(value!=""){
                                    processingType = getMasterDataEntity( value.toString(), "Processing Type");
                                    if (processingType==null){
                                        persistError(apiErrorList,Constants.GET_VALUE_INVALID_DESC(Constants.API_PROCESSING_TYPE), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                        internalStatus=Status.ERROR;
                                        apiErrorCount++;
                                    }
                                }
                                else{
                                    processingType=null;
                                    persistError(apiErrorList,Constants.GET_NOT_NULL_DESC(Constants.API_PROCESSING_TYPE), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                    internalStatus=Status.ERROR;
                                    apiErrorCount++;
                                }
                                api.setApiMstProcessType(processingType);
                            }
                            break;
                            case Constants.API_COMPLEXITY: {
                                value = readCellByType(cell);
                                if(value!=""){
                                    complexity = getMasterDataEntity( value.toString(), "Complexity");
                                    if (complexity==null){
                                        persistError(apiErrorList,Constants.GET_VALUE_INVALID_DESC(Constants.API_COMPLEXITY), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                        internalStatus = Status.ERROR;
                                        apiErrorCount++;
                                    }
                                }
                                else{
                                    complexity=null;
                                }
                                api.setApiMstComplexity(complexity);
                            }
                            break;
                            case Constants.API_THROUGHPUT: {
                                value = readCellByType(cell);
                                if (value != "") {
                                    if (NumberUtils.isCreatable(value.toString())) {
                                        api.setApiThroughput(Integer.parseInt(value.toString()));
                                    }
                                    else{
                                        persistError(apiErrorList,Constants.GET_VALUE_INVALID_DESC(Constants.API_THROUGHPUT), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                        internalStatus=Status.ERROR;
                                        apiErrorCount++;
                                    }
                                }
                            }
                            break;
                            case Constants.API_MSG_SIZE: {
                                value = readCellByType(cell);
                                if(value!=""){
                                    if (NumberUtils.isCreatable(value.toString())) {
                                        api.setApiIntMsgSize(Integer.parseInt(value.toString()));
                                    }
                                    else{
                                        persistError(apiErrorList,Constants.GET_VALUE_INVALID_DESC(Constants.API_MSG_SIZE), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                        internalStatus=Status.ERROR;
                                        apiErrorCount++;
                                    }
                                }
                            }
                            break;
                            case Constants.API_DEPENDENCY: {
                                value = readCellByType(cell);
                                if(value!=""){
                                    api.setApiDependency(value.toString());
                                }
                                else{
                                    api.setApiDependency("");
                                }
                            }
                            break;
                            case Constants.API_OWNER: {
                                value = readCellByType(cell);
                                if(value!=""){
                                    api.setApiIntOwner(value.toString());
                                }
                                else{
                                    api.setApiIntOwner("");
                                }
                            }
                            break;
                            case Constants.API_BUSINESS_OWNER: {
                                value = readCellByType(cell);
                                if(value!=""){
                                    api.setApiBizOwner(value.toString());
                                }
                                else{
                                    api.setApiBizOwner("");
                                    persistError(apiErrorList,Constants.GET_NOT_NULL_DESC(Constants.API_BUSINESS_OWNER), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.API_SHEET);
                                    internalStatus=Status.ERROR;
                                    apiErrorCount++;
                                }
                            }
                            break;
                            case Constants.API_REMARKS: {
                                value = readCellByType(cell);
                                if(value!=""){
                                    api.setApiRem(value.toString());
                                }
                                else{
                                    api.setApiRem("");
                                }
                            }
                            break;
                            case Constants.API_PAYLOAD: {
                                value = readCellByType(cell);
                                if(value!=""){
                                    api.setApiPayload(value.toString());
                                }
                                else{
                                    api.setApiPayload("");
                                }
                            }
                            break;
                            default: {
                                persistError(apiErrorList,"Column not found",0,colnames.get(cell.getColumnIndex()),Constants.API_SHEET);
                                apiErrorCount++;
                            }
                        }
                    }//end while for cell

                    //setting of data from xl to our obj.
                    api.setApiWorkflowStatus(internalStatus);
                    if (validIntValues.length() != 0) {
                        if (validIntValues.equals("blank"))
                            validIntValues = "";
                        api.setIntAsset(validIntValues);
                    } else {
                        api.setIntAsset("");
                    }
                    api.setApiLob(user.getGroup().getName());
                    api.setApiUps(uploadSummary);
                    VaultApi dbApi = getApiDetails(apiId);

                    //for the case where record is totally new.
                    if (dbApi == null)
                    {
                        persistence.getEntityManager().persist(api);
                        persistence.getEntityManager().flush();
                        persistLinked(validIntObj, api);
                        if (!api.getApiWorkflowStatus().equals(Status.ERROR))
                            apiSuccessCount++;
                    }
                    //for the case where the record is already present and we ignore.
                    else if (equateApiObjects(dbApi, api))
                    {
                        if(apiErrorCount>=1){
                            apiRowErrorCount++;
                        }
                        continue;
                    }
                    //update record
                    else
                    {
                        if (api.getApiWorkflowStatus().equals(Status.APPROVED))
                            api.setApiWorkflowStatus(Status.DRAFT);
                        api.setId(dbApi.getId());
                        api.setIntApiDetails(dbApi.getIntApiDetails());
                        persistence.getEntityManager().merge(api);
                        if (!api.getApiWorkflowStatus() .equals( Status.ERROR))
                            apiSuccessCount++;
                        updateLinked(validIntObj, api, dbApi);
                    }
                }
                if(apiErrorCount>=1){
                    apiRowErrorCount++;
                }

            }//end while for row.

            if(apiRowErrorCount!=0 || apiSuccessCount!=0) {
                uploadSummary.setUpsSuccessRecords(apiSuccessCount);
                uploadSummary.setUpsErrorRecords(apiRowErrorCount);
                persistence.getEntityManager().merge(uploadSummary);
            }
            else {
                persistence.getEntityManager().remove(uploadSummary);
            }
            populateErrList(apiErrorList,uploadSummary);
            tx.commit();
            validApiId.clear();
        }//end try wr.
        catch (Exception e){
            log.error(e.getMessage());
        }

        return uploadSummary;
    }


    /**
     *persisting upload summary object.
     *
     *
     * @param name                  Datatype of the upload object
     * @param filename              file name of the imported file


     * @return upload summary object
     */
    private VaultUploadSummary persistUpload(String name, String filename){
        try(Transaction tx = persistence.createTransaction()) {
            VaultUploadSummary uploadSummary= new VaultUploadSummary();
            uploadSummary.setUpsFilename(filename);
            uploadSummary.setUpsDatatype(name);
            persistence.getEntityManager().persist(uploadSummary);
            persistence.getEntityManager().flush();
            tx.commit();
            return uploadSummary;
        }
    }


    /**
     *reading Integration details from sheet.
     *
     * @param integrationSheet             XSSF sheet object
     * @param filename          file name of the imported file
     * @param user              User object - contains login , role , name


     * @return upload summary object
     */
    private VaultUploadSummary readIntDetails(XSSFSheet integrationSheet, String filename, User user)
    {
        List<VaultErrSummary> intErrorList=new ArrayList<>();
        VaultUploadSummary uploadSummary=persistUpload(Constants.INT_SHEET,filename);
        int intRowErrorCount=0;
        int intSuccessCount=0;
        int intErrorCount;
        try(Transaction tx=persistence.createTransaction()) {
            String assetid = null;
            VaultMaster crud;
            VaultMaster payloadSecureClass;
            VaultMaster brandOrBu;
            VaultMaster region;
            VaultMaster country;
            VaultMaster priority;
            VaultMaster status;
            VaultMaster platform;
            VaultMaster sourceProtocol;
            VaultMaster sourcePayloadType;
            VaultMaster processingType;
            VaultMaster destinationProtocol;
            VaultMaster destinationPayloadType;
            VaultMaster designPattern ;
            VaultMaster complexity;
            Status internalStatus;
            VaultMaster specialops;
            List<String> validIntIds=new ArrayList<>();

            //contains column names  from xl sheet.
            List<String> colnames = new ArrayList<>();
            colnames=addColumns(colnames,integrationSheet);
            Iterator<Row> rowIterator = integrationSheet.iterator();
            rowIterator.next();

            //Iterator for row.
            while (rowIterator.hasNext()) {
                VaultIntegration intDetails=new VaultIntegration();
                intErrorCount=0;
                Row row = rowIterator.next();
                //Iterator<Cell> columnIterator = row.cellIterator();
                internalStatus=Status.DRAFT;
                outerloop:
                if (!checkIfRowIsEmpty(row)) {
                    //Iterator for cell.
                    for(int i = 1;i<colnames.size();i++){
                    //while (columnIterator.hasNext()) {
                        Cell cell = row.getCell(i);
                        if(cell==null){
                            cell = row.createCell(i);
                            cell.setCellValue("");
                        }
                        Object value=null;
                        if (Constants.getRestrictedColumns().contains(colnames.get(cell.getColumnIndex()))) {
                            continue;
                        }
                        switch (colnames.get(cell.getColumnIndex()).toUpperCase()) {
                            case Constants.INT_ID: {
                                value = readCellByType(cell);
                                if (value != "") {
                                    assetid = value.toString();
                                    VaultIntegration check = getIntegrationEntity(assetid);
                                    if (check != null && !check.getCreatedBy().equals(user.getLogin())) {
                                        if (user.getUserRoles().contains(Constants.BU_ADMINISTRATOR_ROLE)) {
                                            if (!getGroup(check.getCreatedBy()).equals(user.getGroup().getName())) {
                                                persistError(intErrorList, Constants.DUPLICATE_API_ID_ERROR_MSG, row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                                intErrorCount++;
                                                break outerloop;
                                            }
                                        } else {
                                            persistError(intErrorList, Constants.DUPLICATE_INT_ID_ERROR_MSG, row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                            intErrorCount++;
                                            break outerloop;
                                        }
                                    }
                                    if (validIntIds.contains(assetid)) {
                                        persistError(intErrorList, Constants.DUPLICATE_INT_ID_ERROR_MSG, row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                        intErrorCount++;
                                        break outerloop;
                                    } else
                                        validIntIds.add(assetid);
                                } else {
                                    assetid = "";
                                    persistError(intErrorList, Constants.INT_ID_NOT_NULL, row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                    intErrorCount++;
                                    break outerloop;
                                }
                            }
                            break;
                            case Constants.INT_NAME: {
                                value = readCellByType(cell);
                                if (value != "") {
                                    intDetails.setIntName(value.toString());
                                } else {
                                    intDetails.setIntName("");
                                    persistError(intErrorList, Constants.GET_NOT_NULL_DESC(Constants.INT_NAME), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                    internalStatus = Status.ERROR;
                                    intErrorCount++;
                                }
                            }
                            break;
                            case Constants.INT_DESC: {
                                value = readCellByType(cell);
                                if (value != "")
                                    intDetails.setIntDesc(value.toString());
                                else
                                    intDetails.setIntDesc("");
                            }
                            break;
                            case Constants.INT_PAYLOAD: {
                                value = readCellByType(cell);
                                if (value != "")
                                    intDetails.setIntPayload(value.toString());
                                else
                                    intDetails.setIntPayload("");
                            }
                            break;
                            case Constants.INT_PAYLOAD_SECURE_CLASS: {
                                value = readCellByType(cell);
                                if (value != "") {
                                    payloadSecureClass = getMasterDataEntity(value.toString(), "Security Classification");
                                    if (payloadSecureClass == null) {
                                        persistError(intErrorList, Constants.GET_VALUE_INVALID_DESC(Constants.INT_PAYLOAD_SECURE_CLASS), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                        internalStatus = Status.ERROR;
                                        intErrorCount++;
                                    }
                                } else {
                                    payloadSecureClass = null;
                                    persistError(intErrorList, Constants.GET_NOT_NULL_DESC(Constants.INT_PAYLOAD_SECURE_CLASS), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                    internalStatus = Status.ERROR;
                                    intErrorCount++;
                                }
                                intDetails.setIntMstPayloadSecClass(payloadSecureClass);
                            }
                            break;
                            case Constants.INT_OPERATION: {
                                value = readCellByType(cell);
                                if (value != "") {
                                    crud = getMasterDataEntity(value.toString(), "Operation");
                                    if (crud == null) {
                                        persistError(intErrorList, Constants.GET_VALUE_INVALID_DESC(Constants.INT_OPERATION), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                        internalStatus = Status.ERROR;
                                        intErrorCount++;
                                    }
                                } else {
                                    crud = null;
                                    persistError(intErrorList, Constants.GET_NOT_NULL_DESC(Constants.INT_OPERATION), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                    internalStatus = Status.ERROR;
                                    intErrorCount++;
                                }
                                intDetails.setIntMstOps(crud);
                            }
                            break;
                            case Constants.INT_BUSINESS_UNIT: {
                                value = readCellByType(cell);
                                if (value != "") {
                                    brandOrBu = getMasterDataEntity(value.toString(), "Business Unit");
                                    if (brandOrBu == null) {
                                        persistError(intErrorList, Constants.GET_VALUE_INVALID_DESC(Constants.INT_BUSINESS_UNIT), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                        internalStatus = Status.ERROR;
                                        intErrorCount++;
                                    }
                                } else {
                                    brandOrBu = null;
                                    persistError(intErrorList, Constants.GET_NOT_NULL_DESC(Constants.INT_BUSINESS_UNIT), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                    internalStatus = Status.ERROR;
                                    intErrorCount++;
                                }
                                intDetails.setIntMstBizUnit(brandOrBu);
                            }
                            break;
                            case Constants.INT_REGION: {
                                value = readCellByType(cell);
                                if (value != "") {
                                    region = getMasterDataEntity(value.toString(), "Region");
                                    if (region == null) {
                                        persistError(intErrorList, Constants.GET_VALUE_INVALID_DESC(Constants.INT_REGION), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                        internalStatus = Status.ERROR;
                                        intErrorCount++;
                                    }
                                } else {
                                    region = null;
                                    persistError(intErrorList, Constants.GET_NOT_NULL_DESC(Constants.INT_REGION), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                    internalStatus = Status.ERROR;
                                    intErrorCount++;
                                }
                                intDetails.setIntMstRegion(region);
                            }
                            break;
                            case Constants.INT_PRIORITY: {
                                value = readCellByType(cell);
                                if (value != "") {
                                    priority = getMasterDataEntity(value.toString(), "Priority");
                                    if (priority == null) {
                                        persistError(intErrorList, Constants.GET_VALUE_INVALID_DESC(Constants.INT_PRIORITY), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                        internalStatus = Status.ERROR;
                                        intErrorCount++;
                                    }
                                } else
                                    priority = null;
                                intDetails.setIntMstPriority(priority);
                            }
                            break;
                            case Constants.INT_COUNTRY: {
                                value = readCellByType(cell);
                                if (value != "") {
                                    country = getMasterDataEntity(value.toString(), "Country");
                                    if (country == null) {
                                        persistError(intErrorList, Constants.GET_VALUE_INVALID_DESC(Constants.INT_COUNTRY), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                        internalStatus = Status.ERROR;
                                        intErrorCount++;
                                    }
                                } else
                                    country = null;
                                intDetails.setIntMstCountry(country);
                            }
                            break;
                            case Constants.INT_PROJECT: {
                                value = readCellByType(cell);
                                if (value != "")
                                    intDetails.setIntProj(value.toString());
                                else
                                    intDetails.setIntProj("");
                            }
                            break;
                            case Constants.INT_WORKSTREAM: {
                                value = readCellByType(cell);
                                if (value != "")
                                    intDetails.setIntWorkstream(value.toString());
                                else
                                    intDetails.setIntWorkstream("");
                            }
                            break;
                            case Constants.INT_STATUS: {
                                value = readCellByType(cell);
                                if (value != "") {
                                    status = getMasterDataEntity(value.toString(), "Status");
                                    if (status == null) {
                                        persistError(intErrorList, Constants.GET_VALUE_INVALID_DESC(Constants.INT_STATUS), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                        internalStatus = Status.ERROR;
                                        intErrorCount++;
                                    }
                                } else {
                                    status = null;
                                    persistError(intErrorList, Constants.GET_NOT_NULL_DESC(Constants.INT_STATUS), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                    internalStatus = Status.ERROR;
                                    intErrorCount++;
                                }
                                intDetails.setIntMstSdlcStatus(status);
                            }
                            break;
                            case Constants.INT_PLATFORM: {
                                value = readCellByType(cell);
                                if (value != "") {
                                    platform = getMasterDataEntity(value.toString(), "Platform");
                                    if (platform == null) {
                                        persistError(intErrorList, Constants.GET_VALUE_INVALID_DESC(Constants.INT_PLATFORM), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                        internalStatus = Status.ERROR;
                                        intErrorCount++;
                                    }
                                } else {
                                    platform = null;
                                    persistError(intErrorList, Constants.GET_NOT_NULL_DESC(Constants.INT_PLATFORM), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                    internalStatus = Status.ERROR;
                                    intErrorCount++;
                                }
                                intDetails.setIntMstPlatform(platform);
                            }
                            break;
                            case Constants.INT_PLATFORM_VERSION: {
                                value = readCellByType(cell);
                                if (value != "") {
                                    intDetails.setIntPlatformVersion(value.toString());
                                } else {
                                    intDetails.setIntPlatformVersion("");
                                    persistError(intErrorList, Constants.GET_NOT_NULL_DESC(Constants.INT_PLATFORM_VERSION), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                    internalStatus = Status.ERROR;
                                    intErrorCount++;
                                }

                            }
                            break;
                            case Constants.INT_SRC_PROTOCOL: {
                                value = readCellByType(cell);
                                if (value != "") {
                                    sourceProtocol = getMasterDataEntity(value.toString(), "Network Protocol");
                                    if (sourceProtocol == null) {
                                        persistError(intErrorList, Constants.GET_VALUE_INVALID_DESC(Constants.INT_SRC_PROTOCOL), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                        internalStatus = Status.ERROR;
                                        intErrorCount++;
                                    }
                                } else {
                                    sourceProtocol = null;
                                    persistError(intErrorList, Constants.GET_NOT_NULL_DESC(Constants.INT_SRC_PROTOCOL), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                    internalStatus = Status.ERROR;
                                    intErrorCount++;
                                }
                                intDetails.setIntMstSrcProtocol(sourceProtocol);
                            }
                            break;
                            case Constants.INT_SRC_PAYLOAD_TYPE: {
                                value = readCellByType(cell);
                                if (value != "") {
                                    sourcePayloadType = getMasterDataEntity(value.toString(), "Payload Type");
                                    if (sourcePayloadType == null) {
                                        persistError(intErrorList, Constants.GET_VALUE_INVALID_DESC(Constants.INT_SRC_PAYLOAD_TYPE), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                        internalStatus = Status.ERROR;
                                        intErrorCount++;
                                    }
                                } else {
                                    sourcePayloadType = null;
                                    persistError(intErrorList, Constants.GET_NOT_NULL_DESC(Constants.INT_SRC_PAYLOAD_TYPE), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                    internalStatus = Status.ERROR;
                                    intErrorCount++;
                                }
                                intDetails.setIntMstSrcPayloadType(sourcePayloadType);
                            }
                            break;
                            case Constants.INT_DESTINATION: {
                                value = readCellByType(cell);
                                if (value != "")
                                    intDetails.setIntDest(value.toString());
                                else
                                    intDetails.setIntDest("");
                            }
                            break;
                            case Constants.INT_THROUGHPUT: {
                                value = readCellByType(cell);
                                if (value != "") {
                                    if (NumberUtils.isCreatable(value.toString())) {
                                        intDetails.setIntThroughput(Integer.parseInt(value.toString()));
                                    }
                                    else{
                                        persistError(intErrorList,Constants.GET_VALUE_INVALID_DESC(Constants.INT_THROUGHPUT), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                        internalStatus=Status.ERROR;
                                        intErrorCount++;
                                    }
                                }
                            }
                            break;
                            case Constants.INT_DEST_PROTOCOL: {
                                value = readCellByType(cell);
                                if (value != "") {
                                    destinationProtocol = getMasterDataEntity(value.toString(), "Network Protocol");
                                    if (destinationProtocol == null) {
                                        persistError(intErrorList, Constants.GET_VALUE_INVALID_DESC(Constants.INT_DEST_PROTOCOL), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                        internalStatus = Status.ERROR;
                                        intErrorCount++;
                                    }
                                } else {
                                    destinationProtocol = null;
                                    persistError(intErrorList, Constants.GET_NOT_NULL_DESC(Constants.INT_DEST_PROTOCOL), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                    internalStatus = Status.ERROR;
                                    intErrorCount++;
                                }
                                intDetails.setIntMstDestProtocol(destinationProtocol);
                            }
                            break;
                            case Constants.INT_DEST_PAYLOAD_TYPE: {
                                value = readCellByType(cell);
                                if (value != "") {
                                    destinationPayloadType = getMasterDataEntity(value.toString(), "Payload Type");
                                    if (destinationPayloadType == null) {
                                        persistError(intErrorList, Constants.GET_VALUE_INVALID_DESC(Constants.INT_DEST_PAYLOAD_TYPE), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                        internalStatus = Status.ERROR;
                                        intErrorCount++;
                                    }
                                } else {
                                    destinationPayloadType = null;
                                    persistError(intErrorList, Constants.GET_NOT_NULL_DESC(Constants.INT_DEST_PAYLOAD_TYPE), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                    internalStatus = Status.ERROR;
                                    intErrorCount++;
                                }
                                intDetails.setIntMstDestPayloadType(destinationPayloadType);
                            }
                            break;
                            case Constants.INT_DESIGN_PATTERN: {
                                value = readCellByType(cell);
                                if (value != "") {
                                    designPattern = getMasterDataEntity(value.toString(), "Design Pattern");
                                    if (designPattern == null) {
                                        persistError(intErrorList, Constants.GET_VALUE_INVALID_DESC(Constants.INT_DESIGN_PATTERN), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                        internalStatus = Status.ERROR;
                                        intErrorCount++;
                                    }
                                } else {
                                    designPattern = null;
                                    persistError(intErrorList, Constants.GET_NOT_NULL_DESC(Constants.INT_DESIGN_PATTERN), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                    internalStatus = Status.ERROR;
                                    intErrorCount++;
                                }
                                intDetails.setIntMstDesignPattern(designPattern);
                            }
                            break;
                            case Constants.INT_PROCESSING_TYPE: {
                                value = readCellByType(cell);
                                if (value != "") {
                                    processingType = getMasterDataEntity(value.toString(), "Processing Type");
                                    if (processingType == null) {
                                        persistError(intErrorList, Constants.GET_VALUE_INVALID_DESC(Constants.INT_PROCESSING_TYPE), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                        internalStatus = Status.ERROR;
                                        intErrorCount++;
                                    }
                                } else {
                                    processingType = null;
                                    persistError(intErrorList, Constants.GET_NOT_NULL_DESC(Constants.INT_PROCESSING_TYPE), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                    internalStatus = Status.ERROR;
                                    intErrorCount++;
                                }
                                intDetails.setIntMstProcessType(processingType);
                            }
                            break;
                            case Constants.INT_COMPLEXITY: {
                                value = readCellByType(cell);
                                if (value != "") {
                                    complexity = getMasterDataEntity(value.toString(), "Complexity");
                                    if (complexity == null) {
                                        persistError(intErrorList, Constants.GET_VALUE_INVALID_DESC(Constants.INT_COMPLEXITY), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                        internalStatus = Status.ERROR;
                                        intErrorCount++;
                                    }
                                } else {
                                    complexity = null;
                                }
                                intDetails.setIntMstComplexity(complexity);
                            }
                            break;
                            case Constants.INT_SPECIAL_OPS: {
                                value = readCellByType(cell);
                                if (value != "") {
                                    specialops = getMasterDataEntity(value.toString(), "Special Ops");
                                    if (specialops == null) {
                                        persistError(intErrorList, Constants.GET_VALUE_INVALID_DESC(Constants.INT_SPECIAL_OPS), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                        internalStatus = Status.ERROR;
                                        intErrorCount++;
                                    }
                                } else {
                                    specialops = null;
                                }
                                intDetails.setIntMstSpecialOps(specialops);
                            }
                            break;
                            case Constants.INT_IMPLEMENTATION_PATTERN: {
                                value = readCellByType(cell);
                                if (value != "")
                                    intDetails.setIntImplPattern(value.toString());
                                else {
                                    intDetails.setIntImplPattern("");
                                    persistError(intErrorList, Constants.GET_NOT_NULL_DESC(Constants.INT_IMPLEMENTATION_PATTERN), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                    internalStatus = Status.ERROR;
                                    intErrorCount++;
                                }
                            }
                            break;
                            case Constants.INT_MSG_SIZE: {
                                value = readCellByType(cell);
                                if (value != "") {
                                    if (NumberUtils.isCreatable(value.toString())) {
                                        intDetails.setIntMsgSize(Integer.parseInt(value.toString()));
                                    }
                                    else{
                                        persistError(intErrorList,Constants.GET_VALUE_INVALID_DESC(Constants.INT_MSG_SIZE), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                        internalStatus=Status.ERROR;
                                        intErrorCount++;
                                    }
                                }
                            }
                            break;
                            case Constants.INT_DEPENDENCY: {
                                value = readCellByType(cell);
                                if (value != "")
                                    intDetails.setIntDependency(value.toString());
                                else
                                    intDetails.setIntDependency("");
                            }
                            break;
                            case Constants.INT_OWNER: {
                                value = readCellByType(cell);
                                if (value != "")
                                    intDetails.setIntOwner(value.toString());
                                else
                                    intDetails.setIntOwner("");
                            }
                            break;
                            case Constants.INT_BUSINESS_OWNER: {
                                value = readCellByType(cell);
                                if (value != "")
                                    intDetails.setIntBizOwner(value.toString());
                                else {
                                    intDetails.setIntBizOwner("");
                                    persistError(intErrorList, Constants.GET_NOT_NULL_DESC(Constants.INT_BUSINESS_OWNER), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                    internalStatus = Status.ERROR;
                                    intErrorCount++;
                                }
                            }
                            break;
                            case Constants.INT_REMARKS: {
                                value = readCellByType(cell);
                                if (value != "")
                                    intDetails.setIntRem(value.toString());
                                else
                                    intDetails.setIntRem("");
                            }
                            break;
                            case Constants.INT_FUNC_SPEC_LOC: {
                                value = readCellByType(cell);
                                if (value != "")
                                    intDetails.setIntFuncSpecLoc(value.toString());
                                else
                                    intDetails.setIntFuncSpecLoc("");
                            }
                            break;
                            case Constants.INT_SOURCE: {
                                value = readCellByType(cell);
                                if (value != "")
                                    intDetails.setIntSrcSys(value.toString());
                                else
                                    intDetails.setIntSrcSys("");
                            }
                            break;
                            case Constants.INT_TECH_SPEC_LOC: {
                                value = readCellByType(cell);
                                if (value != "")
                                    intDetails.setIntTechSpecLoc(value.toString());
                                else {
                                    intDetails.setIntTechSpecLoc("");
                                    persistError(intErrorList, Constants.GET_NOT_NULL_DESC(Constants.INT_TECH_SPEC_LOC), row.getRowNum() + 1, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                    internalStatus = Status.ERROR;
                                    intErrorCount++;
                                }
                            }
                            break;
                            case Constants.INT_CODE_LOC: {
                                value = readCellByType(cell);
                                if (value != "")
                                    intDetails.setIntCodeRepo(value.toString());
                                else
                                    intDetails.setIntCodeRepo("");
                            }
                            break;
                            case Constants.INT_RELEASE: {
                                value = readCellByType(cell);
                                if (value != "")
                                    intDetails.setIntRelease(value.toString());
                                else
                                    intDetails.setIntRelease("");
                            }
                            break;
                            case Constants.INT_DEVELOPER: {
                                value = readCellByType(cell);
                                if (value != "") {
                                    System.out.println("if");
                                    intDetails.setIntDeveloper(value.toString());
                                }
                                else {
                                    intDetails.setIntDeveloper("");
                                    System.out.println("Setter: " + intDetails.getIntDeveloper());
                                }
                            }
                            break;
                            case Constants.INT_ARCHITECT: {
                                value = readCellByType(cell);
                                if (value != "")
                                    intDetails.setIntArchitect(value.toString());
                                else
                                    intDetails.setIntArchitect("");
                            }
                            break;
                            case Constants.INT_VERSION: {
                                value = readCellByType(cell);
                                if (value != "")
                                    intDetails.setIntVersion(value.toString());
                                else
                                    intDetails.setIntVersion("");
                            }
                            break;
                            default: {
                                persistError(intErrorList, "Column not found", 0, colnames.get(cell.getColumnIndex()), Constants.INT_SHEET);
                                intErrorCount++;
                            }
                        }

                    }//end while for coloumn

                    //setting of data from excel to our object.
                    intDetails.setIntAsset(assetid);

                    intDetails.setIntWorkflowStatus(internalStatus);
                    intDetails.setIntUps(uploadSummary);
                    intDetails.setIntLob(user.getGroup().getName());
                    VaultIntegration dbIntDetails = getIntegrationEntity(assetid);


                    //for the case where record is totally new.
                    if (dbIntDetails == null)
                    {
                        persistence.getEntityManager().persist(intDetails);
                        if(intDetails.getIntWorkflowStatus()!=Status.ERROR)
                            intSuccessCount++;
                    }
                    //for the case where the record is already present and we ignore.
                    else if (equateIntObjects(dbIntDetails,intDetails))
                    {
                        if(intErrorCount>=1){
                            intRowErrorCount++;
                        }
                        continue;
                    }
                    //update
                    else
                    {
                        if (intDetails.getIntWorkflowStatus()==Status.APPROVED)
                            intDetails.setIntWorkflowStatus(Status.DRAFT);
                        intDetails.setId(dbIntDetails.getId());
                        persistence.getEntityManager().merge(intDetails);
                        if(intDetails.getIntWorkflowStatus()!=Status.ERROR)
                            intSuccessCount++;
                    }
                }
                if(intErrorCount>=1){
                    intRowErrorCount++;
                }
            }//end while for row.

            if(intRowErrorCount!=0 || intSuccessCount!=0) {
                uploadSummary.setUpsSuccessRecords(intSuccessCount);
                uploadSummary.setUpsErrorRecords(intRowErrorCount);
                persistence.getEntityManager().merge(uploadSummary);
            }
            else {
                persistence.getEntityManager().remove(uploadSummary);
            }
            populateErrList(intErrorList,uploadSummary);
            tx.commit();
            validIntIds.clear();
        }
        catch (Exception e){
            //Transaction exception.
            e.printStackTrace();
            log.error(e.getMessage());
        }

        return uploadSummary;
    }

    /**
     * function for checking empty row.
     *
     * @param colnames               List to store column names.
     * @param sheet                  sheet object to get column names
     * @return List of string column names
     */
    private List<String> addColumns(List<String> colnames,XSSFSheet sheet)
    {
        for (int i = 0; i < sheet.getRow(0).getPhysicalNumberOfCells(); i++) {
            colnames.add(sheet.getRow(0).getCell(i).getStringCellValue().trim());
        }
        return colnames;
    }

    /**
     * function for checking empty row.
     *
     * @param row               Row object for checking empty row
     * @return boolean result.
     */
    private boolean checkIfRowIsEmpty(Row row)
    {
        if (row == null) {
            return true;
        }
        if (row.getLastCellNum() <= 0) {
            return true;
        }
        for (int cellNum = row.getFirstCellNum(); cellNum < row.getLastCellNum(); cellNum++) {
            Cell cell = row.getCell(cellNum);
            if(cellNum==1){
                if(cell.getCellType()==CellType.FORMULA && cell.getRichStringCellValue().getString().equals("")){
                    continue;
                }
                else{
                    if(cell.getRichStringCellValue().getString().equals(""))
                        continue;
                    else
                        return false;
                }
            }
            if (cell != null && cell.getCellType() != CellType.BLANK && StringUtils.isNotBlank(cell.toString()) ){
                return false;
            }
        }
        return true;
    }


    /**
     * function for getting API object from api id.
     *
     * @param apiId              API id
     * @return API object.
     */
    private VaultApi getApiDetails (String apiId)
    {
        try (Transaction tx = persistence.createTransaction()){
            TypedQuery<VaultApi> query = persistence.getEntityManager().createQuery("select e from vault_Api e where e.api=?1", VaultApi.class);
            query.setViewName("vaultApi-view");
            query.setParameter(1,apiId);
            if (!query.getResultList().isEmpty())
                return query.getResultList().get(0);
        }
        catch(IllegalArgumentException e)
        {
            log.error(e.getMessage());
        }
        return null;
    }


    /**
     * function for getting linked object from int and api ids.
     *
     * @param intObj               Integration object
     * @param apiObj               API object
     * @return Linked object.
     */
    private VaultIntegrationApi getLinkedFromDb (VaultIntegration intObj,VaultApi apiObj)
    {
        try (Transaction tx = persistence.createTransaction()){
            TypedQuery<VaultIntegrationApi> query = persistence.getEntityManager().createQuery("select e from vault_IntegrationApi e where e.vaultIntegration=?1 and e.vaultApi=?2", VaultIntegrationApi.class);
            query.setViewName("vaultIntegrationApi-view");
            query.setParameter(1,intObj);
            query.setParameter(2,apiObj);
            if (!query.getResultList().isEmpty())
                return query.getResultList().get(0);
        }
        catch(IllegalArgumentException e)
        {
            log.error(e.getMessage());
        }
        log.warn("Linked obj not found");
        return null;
    }


    /**
     * function for getting corresponding integration record.
     *
     * @param intId               Integration asset id
     * @return Integration object.
     */
    private VaultIntegration getIntegrationEntity (String intId)
    {
        try (Transaction tx = persistence.createTransaction()){
            TypedQuery<VaultIntegration> query = persistence.getEntityManager().createQuery("select e from vault_Integration e where e.intAsset=?1", VaultIntegration.class);
            query.setViewName("vaultIntegration-view");
            query.setParameter(1,intId);
            if (!query.getResultList().isEmpty())
                return query.getResultList().get(0);
        }
        catch(IllegalArgumentException e)
        {
            log.error(e.getMessage());
        }
        return null;
    }


    /**
     * function for checking integrations
     *
     * @param intId               Int asset id
     * @param user                user object for checking groups

     * @return Integration object.
     */
    private VaultIntegration checkInt(String intId, User user){
        try (Transaction tx = persistence.createTransaction()){
            TypedQuery<VaultIntegration> query = persistence.getEntityManager().createQuery("select e from vault_Integration e where (e.intAsset=?1 and e.intLob=?2) or (e.intAsset=?1 and e.intWorkflowStatus='AP') ", VaultIntegration.class);
            query.setViewName("vaultIntegration-view");
            query.setParameter(1,intId);
            query.setParameter(2,user.getGroup().getName());
            if (!query.getResultList().isEmpty())
                return query.getResultList().get(0);
        }
        catch(IllegalArgumentException e)
        {
            log.error(e.getMessage());
        }
        log.warn("Invalid Integration");
        return null;
    }


    /**
     * function for getting the Master data object

     * @param description       master data desc.
     * @param type              master data type.
     * @return Master data object
     */
    private VaultMaster getMasterDataEntity(String description, String type)
    {

        try (Transaction tx = persistence.createTransaction()) {
            TypedQuery<VaultMaster> query = persistence.getEntityManager().createQuery("select e from vault_Master e where e.mstType=?1 and e.mstCode=?2 and e.mstIsactive='true'", VaultMaster.class);
            query.setViewName("vaultMaster-view");
            query.setParameter(1, type);
            query.setParameter(2, description);
            if (!query.getResultList().isEmpty()) {
                return query.getResultList().get(0);
            }
        }
        log.warn("Master data not found");
        return null;
    }


    /**
     * function for checking user by login
     *
     * @param login            login obtained from user.
     * @return string group name.
     */
    private String getGroup(String login){
        try(Transaction tx=persistence.createTransaction()) {
            TypedQuery<User> query = persistence.getEntityManager().createQuery("select u from sec$User u where u.login = :login", User.class);
            query.setParameter("login", login);
            List<User> allUsers = query.getResultList();
            if (!allUsers.isEmpty()) {
                return allUsers.get(0).getGroup().getName();
            }
            log.warn("Group not found");
            return null;
        }
    }


    /**
     *adding errors in error list.
     *
     *
     * @param errorDesc                 Api obj from db
     * @param rowNum                    Row number of record
     * @param colName                   Column Nmae of record
     * @param dataType                  Data type of error record.
     *
     */
    public void persistError(List<VaultErrSummary> errorList,String errorDesc, int rowNum, String colName, String dataType)
    {
        VaultErrSummary errorObj=new VaultErrSummary();
        errorObj.setErrDesc(errorDesc);
        errorObj.setErrRowNo(rowNum);
        errorObj.setErrColName(colName);
        errorObj.setErrDatatype(dataType);
        errorList.add(errorObj);
    }


    /**
     *persist linked objects.
     *
     *
     * @param validIntObj                  List of valid int objects
     * @param api                          Api objects to be linked


     * @return upload summary object
     */
    private void persistLinked(Set<VaultIntegration> validIntObj, VaultApi api)
    {
        if (!validIntObj.isEmpty()) {
            for (VaultIntegration temp : validIntObj) {
                VaultIntegrationApi linked=new VaultIntegrationApi();
                linked.setVaultIntegration(temp);
                linked.setVaultApi(api);
                persistence.getEntityManager().persist(linked);
            }
        }
    }


    /**
     *update linked objects.
     *
     *
     * @param validIntObj                  List of valid int objects
     * @param api                          Api objects to be linked
     * @param dbApi                        Api objects already in db.

     * @return upload summary object
     */
    private void updateLinked(Set<VaultIntegration> validIntObj, VaultApi api, VaultApi dbApi)
    {
        if (!validIntObj.isEmpty()) {

            List<VaultIntegration> intObjDb = new ArrayList<>();
            for (VaultIntegrationApi temp : api.getIntApiDetails())
            {
                intObjDb.add(temp.getVaultIntegration());
            }
            Collection<VaultIntegration> uncommonadd = CollectionUtils.subtract(validIntObj, intObjDb);
            Collection<VaultIntegration> uncommonsub = CollectionUtils.subtract(intObjDb, validIntObj);
            //Remove link
            if (uncommonadd.isEmpty())
            {
                for (VaultIntegration x : uncommonsub) {
                    VaultIntegrationApi linked=new VaultIntegrationApi();
                    if (getLinkedFromDb(x,api)!=null)
                        linked.setId(getLinkedFromDb(x,api).getId());
                    linked=persistence.getEntityManager().merge(linked);
                    persistence.getEntityManager().remove(linked);
                }

            }
            //Add link
            else if (uncommonsub.isEmpty())
            {
                for (VaultIntegration x : uncommonadd) {
                    VaultIntegrationApi linked=new VaultIntegrationApi();
                    linked.setVaultIntegration(x);
                    linked.setVaultApi(api);
                    persistence.getEntityManager().persist(linked);
                    persistence.getEntityManager().flush();
                }
            }
            //Remove and add link.
            else
            {
                for (VaultIntegration x : uncommonsub) {
                    VaultIntegrationApi linked=new VaultIntegrationApi();
                    if (getLinkedFromDb(x,api)!=null)
                        linked.setId(getLinkedFromDb(x,api).getId());
                    linked=persistence.getEntityManager().merge(linked);
                    persistence.getEntityManager().remove(linked);
                }
                for (VaultIntegration x : uncommonadd) {
                    VaultIntegrationApi linked=new VaultIntegrationApi();
                    linked.setVaultIntegration(x);
                    linked.setVaultApi(api);
                    persistence.getEntityManager().persist(linked);
                    persistence.getEntityManager().flush();
                }
            }
        }
    }


    /**
     *equals for api objects.
     *
     *
     * @param dbApi                 Api obj from db
     * @param xlApi                 Api obj from db


     * @return boolean whether equal or not
     */
    private boolean equateApiObjects(VaultApi dbApi, VaultApi xlApi)
    {
        return Objects.equals(dbApi.getApi(), xlApi.getApi()) &&
                Objects.equals(dbApi.getApiBizOwner(), xlApi.getApiBizOwner()) &&
                Objects.equals(dbApi.getApiDependency(), xlApi.getApiDependency()) &&
                Objects.equals(dbApi.getApiDesc(), xlApi.getApiDesc()) &&
                Objects.equals(dbApi.getApiDest(), xlApi.getApiDest()) &&
                Objects.equals(dbApi.getApiPayload(), xlApi.getApiPayload()) &&
                Objects.equals(dbApi.getApiIntMsgSize(), xlApi.getApiIntMsgSize()) &&
                Objects.equals(dbApi.getApiIntOwner(), xlApi.getApiIntOwner()) &&
                Objects.equals(dbApi.getApiMstBizUnit(), xlApi.getApiMstBizUnit()) &&
                Objects.equals(dbApi.getApiMstClass(), xlApi.getApiMstClass()) &&
                Objects.equals(dbApi.getApiMstComplexity(), xlApi.getApiMstComplexity()) &&
                Objects.equals(dbApi.getApiMstCountry(), xlApi.getApiMstCountry()) &&
                Objects.equals(dbApi.getApiMstDesignPattern(), xlApi.getApiMstDesignPattern()) &&
                Objects.equals(dbApi.getApiMstDestPayloadType(), xlApi.getApiMstDestPayloadType()) &&
                Objects.equals(dbApi.getApiMstDestProtocol(), xlApi.getApiMstDestProtocol()) &&
                Objects.equals(dbApi.getApiMstOps(), xlApi.getApiMstOps()) &&
                Objects.equals(dbApi.getApiMstPayloadSecurClass(), xlApi.getApiMstPayloadSecurClass()) &&
                Objects.equals(dbApi.getApiMstPlatform(), xlApi.getApiMstPlatform()) &&
                Objects.equals(dbApi.getApiPlatformVersion(), xlApi.getApiPlatformVersion()) &&
                Objects.equals(dbApi.getApiMstPriority(), xlApi.getApiMstPriority()) &&
                Objects.equals(dbApi.getApiMstProcessType(), xlApi.getApiMstProcessType()) &&
                Objects.equals(dbApi.getApiMstRegion(), xlApi.getApiMstRegion()) &&
                Objects.equals(dbApi.getApiMstSdlcStatus(), xlApi.getApiMstSdlcStatus()) &&
                Objects.equals(dbApi.getApiMstSrcPayloadType(), xlApi.getApiMstSrcPayloadType()) &&
                Objects.equals(dbApi.getApiMstSrcProtocol(), xlApi.getApiMstSrcProtocol()) &&
                Objects.equals(dbApi.getApiName(), xlApi.getApiName()) &&
                Objects.equals(dbApi.getApiProj(), xlApi.getApiProj()) &&
                Objects.equals(dbApi.getApiRelease(), xlApi.getApiRelease()) &&
                Objects.equals(dbApi.getApiRem(), xlApi.getApiRem()) &&
                Objects.equals(dbApi.getApiThroughput(), xlApi.getApiThroughput()) &&
                Objects.equals(dbApi.getApiWorkstream(), xlApi.getApiWorkstream()) &&
                Objects.equals(dbApi.getIntAsset(), xlApi.getIntAsset());
    }


    /**
     *equals for int objects.
     *
     *
     * @param dbInt                  Integration obj from db
     * @param xlInt                   Integration obj from db


     * @return boolean whether equal or not
     */
    private boolean equateIntObjects(VaultIntegration dbInt, VaultIntegration xlInt)
    {
        return Objects.equals(dbInt.getIntAsset(), xlInt.getIntAsset()) &&
                Objects.equals(dbInt.getIntBizOwner(), xlInt.getIntBizOwner()) &&
                Objects.equals(dbInt.getIntCodeRepo(), xlInt.getIntCodeRepo()) &&
                Objects.equals(dbInt.getIntDependency(), xlInt.getIntDependency()) &&
                Objects.equals(dbInt.getIntDesc(), xlInt.getIntDesc()) &&
                Objects.equals(dbInt.getIntDest(), xlInt.getIntDest()) &&
                Objects.equals(dbInt.getIntFuncSpecLoc(), xlInt.getIntFuncSpecLoc()) &&
                Objects.equals(dbInt.getIntImplPattern(), xlInt.getIntImplPattern()) &&
                Objects.equals(dbInt.getIntLob(), xlInt.getIntLob()) &&
                Objects.equals(dbInt.getIntMsgSize(), xlInt.getIntMsgSize()) &&
                Objects.equals(dbInt.getIntMstBizUnit(), xlInt.getIntMstBizUnit()) &&
                Objects.equals(dbInt.getIntMstComplexity(), xlInt.getIntMstComplexity()) &&
                Objects.equals(dbInt.getIntMstCountry(), xlInt.getIntMstCountry()) &&
                Objects.equals(dbInt.getIntMstDesignPattern(), xlInt.getIntMstDesignPattern()) &&
                Objects.equals(dbInt.getIntMstDestPayloadType(), xlInt.getIntMstDestPayloadType()) &&
                Objects.equals(dbInt.getIntMstDestProtocol(), xlInt.getIntMstDestProtocol()) &&
                Objects.equals(dbInt.getIntMstOps(), xlInt.getIntMstOps()) &&
                Objects.equals(dbInt.getIntMstPayloadSecClass(), xlInt.getIntMstPayloadSecClass()) &&
                Objects.equals(dbInt.getIntMstPlatform(), xlInt.getIntMstPlatform()) &&
                Objects.equals(dbInt.getIntPlatformVersion(), xlInt.getIntPlatformVersion()) &&
                Objects.equals(dbInt.getIntMstPriority(), xlInt.getIntMstPriority()) &&
                Objects.equals(dbInt.getIntMstProcessType(), xlInt.getIntMstProcessType()) &&
                Objects.equals(dbInt.getIntMstRegion(), xlInt.getIntMstRegion()) &&
                Objects.equals(dbInt.getIntMstSdlcStatus(), xlInt.getIntMstSdlcStatus()) &&
                Objects.equals(dbInt.getIntMstSpecialOps(), xlInt.getIntMstSpecialOps()) &&
                Objects.equals(dbInt.getIntMstSrcPayloadType(), xlInt.getIntMstSrcPayloadType()) &&
                Objects.equals(dbInt.getIntMstSrcProtocol(), xlInt.getIntMstSrcProtocol()) &&
                Objects.equals(dbInt.getIntName(), xlInt.getIntName()) &&
                Objects.equals(dbInt.getIntOwner(), xlInt.getIntOwner()) &&
                Objects.equals(dbInt.getIntPayload(), xlInt.getIntPayload()) &&
                Objects.equals(dbInt.getIntProj(), xlInt.getIntProj()) &&
                Objects.equals(dbInt.getIntRem(), xlInt.getIntRem()) &&
                Objects.equals(dbInt.getIntSrcSys(), xlInt.getIntSrcSys()) &&
                Objects.equals(dbInt.getIntTechSpecLoc(), xlInt.getIntTechSpecLoc()) &&
                Objects.equals(dbInt.getIntThroughput(), xlInt.getIntThroughput()) &&
                Objects.equals(dbInt.getIntWorkstream(), xlInt.getIntWorkstream());
    }


    /**
     *switch case for reading cell by its type
     *
     *
     * @param cell                    Cell read from excel

     * @return Object the value read from the cell
     */
    private Object readCellByType(Cell cell) {
        // Read the cell value using its cell type
        Object value=null;
        DataFormatter formatter = new DataFormatter();
        switch (cell.getCellTypeEnum()) {
            case STRING:
                value = cell.getStringCellValue().trim();
                break;
            case NUMERIC:
                value = formatter.formatCellValue(cell);
                break;
            case FORMULA:
                System.out.println("row" +cell.getRow().getRowNum()+"cell "+cell.getColumnIndex());
                value = cell.getRichStringCellValue().getString();
                break;
            case BLANK:
            case BOOLEAN:
            case ERROR:
                value="";
                break;
            default:
                value="";
        }
        return value;
    }
    /**
     *check if sheet is empty
     *
     *
     * @param sheet                    Sheet to check

     * @return boolean                 returns true if sheet is empty
     */
    private boolean checkIfSheetEmpty(Sheet sheet) {
        if (checkIfRowIsEmpty(sheet.getRow(0)))
            return true;
        for (int i = 1; i <= sheet.getPhysicalNumberOfRows(); i++)
            if (!checkIfRowIsEmpty(sheet.getRow(i)))
                return false;
        return true;
    }

    /**
     *check if sheet is empty
     *
     * @param workbook                 Workbook to check
     *
     * @return boolean                 returns true if workbook is empty
     */
    private boolean checkIfWorkbookEmpty(XSSFWorkbook workbook) {
        XSSFSheet sheet1 = workbook.getSheet(Constants.INT_SHEET);
        XSSFSheet integrationSheet = workbook.getSheet(Constants.API_SHEET);
        if(checkIfSheetEmpty(sheet1) && checkIfSheetEmpty(integrationSheet))
            return true;
        return false;
    }
}


