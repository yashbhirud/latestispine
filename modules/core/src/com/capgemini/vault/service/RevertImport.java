package com.capgemini.vault.service;

import com.capgemini.vault.entity.*;
import com.haulmont.cuba.core.*;
import org.apache.pdfbox.jbig2.util.log.Logger;
import org.apache.pdfbox.jbig2.util.log.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service(iRevertImportService.NAME)
public class RevertImport implements iRevertImportService {

    @Inject
    private Persistence persistence;

    private Logger log = LoggerFactory.getLogger(RevertImport.class);


    /**
     *Rolls back the selected operations
     *
     * @param uplobj             Upload summary object
     * @return boolean isSuccess
     */
    public boolean rollBack(VaultUploadSummary uplobj)
    {
        if (uplobj.getUpsDatatype().equals(Constants.API_SHEET))
        {
            return deleteApiByUpl(uplobj);
        }
        else if(uplobj.getUpsDatatype().equals(Constants.INT_SHEET))
        {
            return deleteIntByUpl(uplobj);
        }
        log.warn("No object rolled back");
        return false;
    }


    /**
     *Deletes api object of the corresponding api
     *
     * @param uplobj             Upload summary object
     * @return boolean isSuccess
     */
    private boolean deleteApiByUpl(VaultUploadSummary uplobj) {
        boolean isSuccess=true;
        try{
            List<VaultApi> list;
            list=getApiFromUpl(uplobj);
            int successCount=uplobj.getUpsSuccessRecords()+uplobj.getUpsErrorRecords();
            for(VaultApi ap: list) {
                if(ap.getApiWorkflowStatus().equals(Status.APPROVED)){
                    isSuccess=false;
                    Transaction tx4 = persistence.createTransaction();
                    VaultApi vaultApi = new VaultApi();
                    VaultApiAudit vaultAudit;
                    Query query = persistence.getEntityManager().createNativeQuery("select max(ava_id) from vault_api_audit where (ava_api_id =?id1 and ava_api_workflow_status='AP' and ava_id not in (select max(ava_id) from vault_api_audit where ava_api_id =?id1))");
                    query.setParameter("id1", ap.getApi());
                    if (query.getFirstResult()==null) {
                        vaultApi.setId(ap.getId());
                        vaultApi.setApiWorkflowStatus(Status.DRAFT);
                    } else {
                        long id = (long) query.getFirstResult();
                        TypedQuery<VaultApiAudit> query1 = persistence.getEntityManager().createQuery("select e from vault_ApiAudit e where e.id =?1", VaultApiAudit.class);
                        query1.setViewName("vaultApiAudit-view");
                        query1.setParameter(1,id);
                        vaultAudit = query1.getFirstResult();
                        vaultApi.setId(ap.getId());
                        vaultApi.setApiWorkflowStatus(Status.DRAFT);
                        vaultApi.setIntAsset(vaultAudit.getAvaIntAsset());
                        vaultApi.setApiName(vaultAudit.getAvaApiName());
                        vaultApi.setApiMstPayloadSecurClass(vaultAudit.getAvaApiMstPayloadSecurClass());
                        vaultApi.setApiMstBizUnit(vaultAudit.getAvaApiMstBizUnit());
                        vaultApi.setApiMstProcessType(vaultAudit.getAvaApiMstProcessType());
                        vaultApi.setApiBizOwner(vaultAudit.getAvaApiBizOwner());
                        vaultApi.setApiMstDestProtocol(vaultAudit.getAvaApiMstDestProtocol());
                        vaultApi.setApiMstDesignPattern(vaultAudit.getAvaApiMstDesignPattern());
                        vaultApi.setApiMstComplexity(vaultAudit.getAvaApiMstComplexity());
                        vaultApi.setApiThroughput(vaultAudit.getAvaApiThroughput());
                        vaultApi.setApiDependency(vaultAudit.getAvaApiDependency());
                        vaultApi.setApiIntMsgSize(vaultAudit.getAvaApiIntMsgSize());
                        vaultApi.setApiIntOwner(vaultAudit.getAvaApiIntOwner());
                        vaultApi.setApiRem(vaultAudit.getAvaApiRem());
                        vaultApi.setApiMstCountry(vaultAudit.getAvaApiMstCountry());
                        vaultApi.setApiDesc(vaultAudit.getAvaApiDesc());
                        vaultApi.setApiMstClass(vaultAudit.getAvaApiMstClass());
                        vaultApi.setApiMstPlatform(vaultAudit.getAvaApiMstPlatform());
                        vaultApi.setApiPlatformVersion(vaultAudit.getAvaApiPlatformVersion());
                        vaultApi.setApiProj(vaultAudit.getAvaApiProj());
                        vaultApi.setApiMstRegion(vaultAudit.getAvaApiMstRegion());
                        vaultApi.setApiMstPriority(vaultAudit.getAvaApiMstPriority());
                        vaultApi.setApiRelease(vaultAudit.getAvaApiRelease());
                        vaultApi.setApiMstSdlcStatus(vaultAudit.getAvaApiMstSdlcStatus());
                        vaultApi.setApiMstOps(vaultAudit.getAvaApiMstOps());
                        vaultApi.setApiWorkstream(vaultAudit.getAvaApiWorkstream());
                        vaultApi.setApiDest(vaultAudit.getAvaApiDest());
                        vaultApi.setApiMstSrcProtocol(vaultAudit.getAvaApiMstSrcProtocol());
                        vaultApi.setApiMstDestPayloadType(vaultAudit.getAvaApiMstDestPayloadType());
                        vaultApi.setIntAsset(vaultAudit.getAvaIntAsset());
                        vaultApi.setApiMstSrcPayloadType(vaultAudit.getAvaApiMstSrcPayloadType());
                    }
                    persistence.getEntityManager().merge(vaultApi);
                    tx4.commit();
                    continue;
                }
                Transaction tx0 = persistence.createTransaction();
                Query query0 = persistence.getEntityManager().createNativeQuery("delete  from vault_api_audit where ava_api_parent_id=?id0");
                query0.setParameter("id0", ap.getId()).executeUpdate();
                tx0.commit();
                Transaction tx = persistence.createTransaction();
                Query query = persistence.getEntityManager().createNativeQuery("delete  from vault_integration_api  where vault_api_id=?id1");
                query.setParameter("id1",ap.getId()).executeUpdate();
                tx.commit();
                Transaction tx1 = persistence.createTransaction();
                Query query1 = persistence.getEntityManager().createNativeQuery("delete  from vault_api where id=?id2");
                int deletedCount= query1.setParameter("id2",ap.getId()).executeUpdate();
                successCount=successCount-deletedCount;
                tx1.commit();
                Transaction tx2 = persistence.createTransaction();
                Query query2 = persistence.getEntityManager().createNativeQuery("delete  from vault_err_summary where err_ups_id=?id3");
                query2.setParameter("id3",uplobj.getId()).executeUpdate();
                tx2.commit();
            }
            if(isSuccess) {
                Transaction tx2 = persistence.createTransaction();
                Query query2 = persistence.getEntityManager().createNativeQuery("delete  from vault_err_summary where err_ups_id=?id3");
                query2.setParameter("id3",uplobj.getId()).executeUpdate();
                tx2.commit();
                Transaction tx3 = persistence.createTransaction();
                Query query3 = persistence.getEntityManager().createNativeQuery("delete  from vault_upload_summary  where id=?id4");
                query3.setParameter("id4", uplobj.getId()).executeUpdate();
                tx3.commit();
                list.clear();
                return isSuccess;
            }
            Transaction tx4 = persistence.createTransaction();
            VaultUploadSummary uploadObject = new VaultUploadSummary();
            uploadObject.setUpsSuccessRecords(successCount);
            uploadObject.setUpsErrorRecords(0);
            uploadObject.setId(uplobj.getId());
            persistence.getEntityManager().merge(uploadObject);
            tx4.commit();
            list.clear();
        }
        catch(Exception e)
        {
            log.error(e.getMessage());
        }
        return isSuccess;
    }


    /**
     *Deletes Integration object of the corresponding api
     *
     * @param uplobj             Upload summary object
     * @return boolean isSuccess
     */
    private boolean deleteIntByUpl(VaultUploadSummary uplobj) {
        boolean isSuccess=true;
        try{
            List<VaultIntegration> list;
            List<VaultIntegration> list1 = new ArrayList<>();
            list=getIntFromUpl(uplobj);
            int successCount=uplobj.getUpsSuccessRecords();
            for(VaultIntegration ap: list) {
                if (ap.getIntWorkflowStatus().equals(Status.APPROVED)) {
                    isSuccess=false;
                    Transaction tx4 = persistence.createTransaction();
                    VaultIntegration vaultInt = new VaultIntegration();
                    VaultIntegrationAudit vaultAudit;
                    Query query = persistence.getEntityManager().createNativeQuery("select max(avi_id) from vault_integration_audit where (avi_int_asset_id =?id1 and avi_int_workflow_status='AP' and avi_id not in (select max(avi_id) from vault_integration_audit where avi_int_asset_id =?id1))");
                    query.setParameter("id1", ap.getIntAsset());
                    if (query.getFirstResult()==null) {
                        vaultInt.setId(ap.getId());
                        vaultInt.setIntWorkflowStatus(Status.DRAFT);
                    } else {
                        long id = (long) query.getFirstResult();
                        TypedQuery<VaultIntegrationAudit> query1 = persistence.getEntityManager().createQuery("select e from vault_IntegrationAudit e where e.id =?1", VaultIntegrationAudit.class);
                        query1.setViewName("vaultIntegrationAudit-view");
                        query1.setParameter(1,id);
                        vaultAudit = query1.getFirstResult();
                        vaultInt.setId(ap.getId());
                        vaultInt.setIntAsset(vaultAudit.getAviIntAsset());
                        vaultInt.setIntName(vaultAudit.getAviIntName());
                        vaultInt.setIntDesc(vaultAudit.getAviIntDesc());
                        vaultInt.setIntMstOps(vaultAudit.getAviIntMstOps());
                        vaultInt.setIntPayload(vaultAudit.getAviIntPayload());
                        vaultInt.setIntMstPayloadSecClass(vaultAudit.getAviIntMstPayloadSecClass());
                        vaultInt.setIntMstBizUnit(vaultAudit.getAviIntMstBizUnit());
                        vaultInt.setIntMstRegion(vaultAudit.getAviIntMstRegion());
                        vaultInt.setIntMstCountry(vaultAudit.getAviIntMstCountry());
                        vaultInt.setIntProj(vaultAudit.getAviIntProj());
                        vaultInt.setIntWorkstream(vaultAudit.getAviIntWorkstream());
                        vaultInt.setIntMstSdlcStatus(vaultAudit.getAviIntMstSdlcStatus());
                        vaultInt.setIntMstPlatform(vaultAudit.getAviIntMstPlatform());
                        vaultInt.setIntPlatformVersion(vaultAudit.getAviIntPlatformVersion());
                        vaultInt.setIntMstSrcProtocol(vaultAudit.getAviIntMstSrcProtocol());
                        vaultInt.setIntMstSrcPayloadType(vaultAudit.getAviIntMstSrcPayloadType());
                        vaultInt.setIntDest(vaultAudit.getAviIntDest());
                        vaultInt.setIntMstDestProtocol(vaultAudit.getAviIntMstDestProtocol());
                        vaultInt.setIntMstDestPayloadType(vaultAudit.getAviIntMstDestPayloadType());
                        vaultInt.setIntMstDesignPattern(vaultAudit.getAviIntMstDesignPattern());
                        vaultInt.setIntMstProcessType(vaultAudit.getAviIntMstProcessType());
                        vaultInt.setIntMstSpecialOps(vaultAudit.getAviIntMstSpecialOps());
                        vaultInt.setIntImplPattern(vaultAudit.getAviIntImplPattern());
                        vaultInt.setIntMstComplexity(vaultAudit.getAviIntMstComplexity());
                        if(vaultAudit.getAviIntMsgSize()!=null)
                            vaultInt.setIntMsgSize(vaultAudit.getAviIntMsgSize());
                        vaultInt.setIntDependency(vaultAudit.getAviIntDependency());
                        vaultInt.setIntOwner(vaultAudit.getAviIntOwner());
                        vaultInt.setIntBizOwner(vaultAudit.getAviIntBizOwner());
                        vaultInt.setIntRem(vaultAudit.getAviIntRem());
                        vaultInt.setIntFuncSpecLoc(vaultAudit.getAviIntFuncSpecLoc());
                        vaultInt.setIntWorkflowStatus(Status.DRAFT);
                        vaultInt.setIntTechSpecLoc(vaultAudit.getAviIntTechSpecLoc());
                        vaultInt.setIntCodeRepo(vaultAudit.getAviIntCodeRepo());
                        vaultInt.setIntSrcSys(vaultAudit.getAviIntSrcSys());
                        vaultInt.setIntMstPriority(vaultAudit.getAviIntMstPriority());
                        vaultInt.setIntLob(vaultAudit.getAviIntLob());
                        vaultInt.setIntArchitect(vaultAudit.getAviIntArchitect());
                        vaultInt.setIntDeveloper(vaultAudit.getAviIntDeveloper());
                        vaultInt.setIntVersion(vaultAudit.getAviIntVersion());
                        if(vaultAudit.getAviIntThroughput()!=null)
                            vaultInt.setIntThroughput(vaultAudit.getAviIntThroughput());
                    }
                    persistence.getEntityManager().merge(vaultInt);
                    tx4.commit();
                    continue;
                }
                list1.add(ap);
                Transaction tx0 = persistence.createTransaction();
                Query query0 = persistence.getEntityManager().createNativeQuery("delete  from vault_integration_audit where avi_int_id=?id0");
                query0.setParameter("id0", ap.getId()).executeUpdate();
                tx0.commit();
                Transaction tx = persistence.createTransaction();
                Query query = persistence.getEntityManager().createNativeQuery("delete  from vault_integration_api where vault_integration_id=?id1");
                query.setParameter("id1", ap.getId()).executeUpdate();
                tx.commit();
                Transaction tx1 = persistence.createTransaction();
                Query query1 = persistence.getEntityManager().createNativeQuery("delete from vault_integration where id=?id2");
                int deletedCount= query1.setParameter("id2", ap.getId()).executeUpdate();
                successCount=successCount-deletedCount;
                tx1.commit();
                Transaction tx2 = persistence.createTransaction();
                Query query2 = persistence.getEntityManager().createNativeQuery("delete  from vault_err_summary where err_ups_id=?id3");
                query2.setParameter("id3", uplobj.getId()).executeUpdate();
                tx2.commit();
            }
            if(isSuccess) {
                Transaction tx2 = persistence.createTransaction();
                Query query2 = persistence.getEntityManager().createNativeQuery("delete  from vault_err_summary where err_ups_id=?id3");
                query2.setParameter("id3", uplobj.getId()).executeUpdate();
                tx2.commit();
                Transaction tx3 = persistence.createTransaction();
                Query query3 = persistence.getEntityManager().createNativeQuery("delete  from vault_upload_summary  where id=?id4");
                query3.setParameter("id4", uplobj.getId()).executeUpdate();
                tx3.commit();
                changeApiStatus(list1);
                list.clear();
                return isSuccess;
            }
            Transaction tx4 = persistence.createTransaction();
            VaultUploadSummary uploadObject = new VaultUploadSummary();
            uploadObject.setUpsSuccessRecords(successCount);
            uploadObject.setUpsErrorRecords(0);
            uploadObject.setId(uplobj.getId());
            persistence.getEntityManager().merge(uploadObject);
            tx4.commit();
            changeApiStatus(list1);
            list.clear();
        }
        catch(Exception e)
        {
            log.error(e.getMessage());
        }
        return isSuccess;
    }


    /**
     *Chnages api status after rollback changes.
     *
     * @param list             List of Integration object
     *
     */
    private void changeApiStatus(List<VaultIntegration> list) {
        List<VaultApi> apiList;
        VaultUploadSummary uploadSummary = new VaultUploadSummary();
        int successCount=0, errorCount =0;
        try (Transaction tx = persistence.createTransaction()) {
            TypedQuery<VaultApi> query = persistence.getEntityManager().createQuery("select e from vault_Api e", VaultApi.class);
            query.setViewName("vaultApi-view");
            apiList = query.getResultList();
            if(apiList!=null){
                uploadSummary = apiList.get(0).getApiUps();
                successCount = uploadSummary.getUpsSuccessRecords();
            }
            for(VaultApi ap: apiList){
                List<String> intList = Arrays.asList(ap.getIntAsset().split(","));
                List<String> finalInt = new ArrayList<>();
                List<String> ints = new ArrayList<>();
                for(VaultIntegration ind: list){
                    ints.add(ind.getIntAsset());
                }
                for(String u : intList){
                    if(!ints.contains(u)){
                        finalInt.add(u);
                    }
                }
                String validIntValues = String.join(",", finalInt);
                if(!ap.getIntAsset().equals(validIntValues)) {
                    VaultApi apd = new VaultApi();
                    apd.setApiWorkflowStatus(Status.ERROR);
                    errorCount++;
                    successCount--;
                    apd.setId(ap.getId());
                    persistence.getEntityManager().merge(apd);
                }
            }
            uploadSummary.setUpsSuccessRecords(successCount);
            uploadSummary.setUpsErrorRecords(errorCount);
            persistence.getEntityManager().merge(uploadSummary);
            tx.commit();
        }
    }

    /**
     *Gets api object of the corresponding api
     *
     * @param uplobj             Upload summary object
     * @return list of api objects
     */
    private List<VaultApi> getApiFromUpl(VaultUploadSummary uplobj) {
        try (Transaction tx = persistence.createTransaction()){
            TypedQuery<VaultApi> query = persistence.getEntityManager().createQuery("select e from vault_Api e where e.apiUps=?1", VaultApi.class);
            query.setViewName("vaultApi-view");
            query.setParameter(1,uplobj);
            if (!query.getResultList().isEmpty())
                return query.getResultList();
        }
        catch(IllegalArgumentException e)
        {
            log.error(e.getMessage());
        }
        log.warn("No Integration obj found");
        return Collections.emptyList();
    }


    /**
     *Gets int object of the corresponding api
     *
     * @param uplobj             Upload summary object
     * @return list of int objects
     */
    private List<VaultIntegration> getIntFromUpl(VaultUploadSummary uplobj) {
        try (Transaction tx = persistence.createTransaction()){
            TypedQuery<VaultIntegration> query = persistence.getEntityManager().createQuery("select e from vault_Integration e where e.intUps=?1", VaultIntegration.class);
            query.setViewName("vaultIntegration-view");
            query.setParameter(1,uplobj);
            if (!query.getResultList().isEmpty())
                return query.getResultList();
        }
        catch(IllegalArgumentException e)
        {
            log.error(e.getMessage());
        }
        log.warn("No Integration obj found");
        return Collections.emptyList();
    }
}