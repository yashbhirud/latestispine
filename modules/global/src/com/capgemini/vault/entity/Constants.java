package com.capgemini.vault.entity;

import java.util.*;

public class Constants {

    public static final List<String> restrictedColumns = new ArrayList<>();
    public static List<String> getRestrictedColumns() {
        restrictedColumns.add("API#");
        restrictedColumns.add("INT#");
        return restrictedColumns;
    }

    public static final String INT_SHEET = "Integrations";

    public static final String API_SHEET = "APIs";

    public static final String API_NO ="API#";

    public static final String API_ID = "ID";


    public static final String API_NAME="NAME";


    public static final String API_DESC="DESCRIPTION";


    public static final String API_ASSET_ID="INT ASSET ID";


    public static final String API_CLASSIFICATION="CLASSIFICATION";


    public static final String API_COUNTRY="COUNTRY";


    public static final String API_REGION="REGION";


    public static final String API_BUSINESS_UNIT="BUSINESS UNIT";


    public static final String API_PLATFORM="PLATFORM";


    public static final String API_PLATFORM_VERSION="PLATFORM VERSION";


    public static final String API_OPERATION="OPERATION";


    public static final String API_DESIGN_PATTERN="DESIGN PATTERN";


    public static final String API_PROCESSING_TYPE="PROCESSING TYPE";


    public static final String API_SRC_PROTOCOL="SOURCE PROTOCOL";


    public static final String API_SRC_PAYLOAD_TYPE="SOURCE PAYLOAD TYPE";


    public static final String API_DESTINATION="DESTINATION";


    public static final String API_DEST_PROTOCOL="DESTINATION PROTOCOL";


    public static final String API_DEST_PAYLOADTYPE="DESTINATION PAYLOAD TYPE";


    public static final String API_PAYLOAD="PAYLOAD";


    public static final String API_PAYLOAD_SECURE_CLASS="PAYLOAD SECURITY CLASSIFICATION";


    public static final String API_MSG_SIZE="MESSAGE SIZE";


    public static final String API_OWNER="OWNER";


    public static final String API_BUSINESS_OWNER="BUSINESS  OWNER";


    public static final String API_PROJECT="PROJECT";


    public static final String API_WORKSTREAM="WORKSTREAM";


    public static final String API_RELEASE="RELEASE";


    public static final String API_PRIORITY="PRIORITY";


    public static final String API_STATUS="STATUS";


    public static final String API_COMPLEXITY="COMPLEXITY";


    public static final String API_THROUGHPUT="THROUPUT";


    public static final String API_DEPENDENCY="DEPENDENCY";


    public static final String API_REMARKS="REMARKS";

    public static final String INT_NO ="INT#";

    public static final String INT_ID ="ID";


    public static final String INT_NAME="NAME";


    public static final String INT_DESC="DESCRIPTION";


    public static final String INT_COUNTRY="COUNTRY";


    public static final String INT_REGION="REGION";


    public static final String INT_BUSINESS_UNIT="BUSINESS UNIT";


    public static final String INT_PLATFORM="PLATFORM";


    public static final String INT_PLATFORM_VERSION="PLATFORM VERSION";


    public static final String INT_OPERATION="OPERATION";

    public static final String INT_SPECIAL_OPS="SPECIAL OPS";


    public static final String INT_DESIGN_PATTERN="DESIGN PATTERN";


    public static final String INT_PROCESSING_TYPE="PROCESSING TYPE";


    public static final String INT_IMPLEMENTATION_PATTERN="IMPLEMENTATION PATTERN";


    public static final String INT_SOURCE="SOURCE";


    public static final String INT_SRC_PROTOCOL="SOURCE PROTOCOL";


    public static final String INT_SRC_PAYLOAD_TYPE="SOURCE PAYLOAD TYPE";


    public static final String INT_DESTINATION="DESTINATION";


    public static final String INT_DEST_PROTOCOL="DESTINATION PROTOCOL";


    public static final String INT_DEST_PAYLOAD_TYPE="DESTINATION PAYLOAD TYPE";


    public static final String INT_PAYLOAD="PAYLOAD";


    public static final String INT_PAYLOAD_SECURE_CLASS="PAYLOAD SECURITY CLASSIFICATION";


    public static final String INT_MSG_SIZE="MESSAGE SIZE";


    public static final String INT_OWNER="OWNER";


    public static final String INT_BUSINESS_OWNER="BUSINESS  OWNER";


    public static final String INT_PROJECT="PROJECT";


    public static final String INT_WORKSTREAM="WORKSTREAM";


    public static final String INT_RELEASE="RELEASE";


    public static final String INT_PRIORITY="PRIORITY";


    public static final String INT_STATUS="STATUS";


    public static final String INT_FUNC_SPEC_LOC="FUNCTIONAL SPEC LOCATION";


    public static final String INT_TECH_SPEC_LOC="TECHNICAL SPEC LOCATION";


    public static final String INT_CODE_LOC="CODE LOCATION";


    public static final String INT_COMPLEXITY="COMPLEXITY";


    public static final String INT_THROUGHPUT="THROUPUT";


    public static final String INT_DEPENDENCY="DEPENDENCY";


    public static final String INT_REMARKS="REMARKS";

    public static final String INT_DEVELOPER="DEVELOPER";

    public static final String INT_ARCHITECT="ARCHITECT";

    public static final String INT_VERSION="VERSION";

    public static final String BU_ADMINISTRATOR_ROLE = "BU ADMINISTRATOR";

    public static final String DUPLICATE_API_ID_ERROR_MSG = "API ID SHOULD NOT BE REPEATED";

    public static final String API_ID_NOT_NULL = "API ID SHOULD NOT BE NULL";

    public static final String API_NAME_NOT_NULL = "API NAME SHOULD NOT BE NULL";

    public static final String INT_ID_NOT_NULL = "INTEGRATION ID SHOULD NOT BE NULL";

    //public static final String INT_ID_NOT_AVAIABLE = "THE MENTIONED INTEGRATION ID DOES NOT EXIST";

    public static final String DUPLICATE_INT_ID_ERROR_MSG = "INTEGRATION ID SHOULD NOT BE REPEATED";


    public static final List<String> intColumns=Arrays.asList(INT_NO,INT_ID,INT_NAME,INT_DESC,INT_COUNTRY,INT_REGION,INT_BUSINESS_UNIT,INT_PLATFORM,INT_PLATFORM_VERSION,INT_OPERATION,INT_DESIGN_PATTERN,INT_SPECIAL_OPS,INT_PROCESSING_TYPE,INT_IMPLEMENTATION_PATTERN,INT_SOURCE,INT_SRC_PROTOCOL,INT_SRC_PAYLOAD_TYPE,INT_DESTINATION,INT_DEST_PROTOCOL,INT_DEST_PAYLOAD_TYPE,INT_PAYLOAD,INT_PAYLOAD_SECURE_CLASS,INT_MSG_SIZE,INT_FUNC_SPEC_LOC,INT_TECH_SPEC_LOC,INT_CODE_LOC,INT_OWNER,INT_BUSINESS_OWNER, INT_PROJECT,INT_WORKSTREAM,INT_RELEASE,INT_PRIORITY,INT_STATUS,INT_COMPLEXITY, INT_THROUGHPUT ,INT_DEPENDENCY,INT_REMARKS,INT_ARCHITECT,INT_DEVELOPER,INT_VERSION);

    public static final List<String> apiColumns = Arrays.asList(API_NO,API_ID,API_ASSET_ID,API_NAME,API_DESC,API_CLASSIFICATION,API_COUNTRY,API_REGION,API_BUSINESS_UNIT, API_PLATFORM,API_PLATFORM_VERSION,API_OPERATION,API_DESIGN_PATTERN,API_PROCESSING_TYPE,API_SRC_PROTOCOL,API_SRC_PAYLOAD_TYPE, API_DESTINATION,API_DEST_PROTOCOL,API_DEST_PAYLOADTYPE,API_PAYLOAD,API_PAYLOAD_SECURE_CLASS,API_MSG_SIZE,API_OWNER,API_BUSINESS_OWNER, API_PROJECT,API_WORKSTREAM,API_RELEASE,API_PRIORITY,API_STATUS,API_COMPLEXITY,API_THROUGHPUT,API_DEPENDENCY,API_REMARKS);

    public static final String GET_INT_ID_NOT_AVAIABLE(String name){
        return (name + " INTEGRATION ID DOES NOT EXIST");
    }
    public static final String GET_NOT_NULL_DESC(String name){
        return (name + " VALUE SHOULD NOT BE NULL");
    }
    public static final String GET_VALUE_INVALID_DESC(String name){
        return (name + " VALUE IS INVALID");
    }
    public static final List<String> masterColumns=Arrays.asList("Type","Code","Description");

    public static final String GIT_REPO_LINK="https://gitlab.com/cloudintegration/iSpine.git";
    public static final String GIT_USERNAME="cloudintegration";
    public static final String GIT_PASSWORD="Capgemini@2020";

    public static final String BIT_BUCKET_PASSWORD="Iamvvs29!";

    public static final Map<String,String> columnTypeMap = new HashMap<>();

    public static Map<String, String> getIntColumnName() {
        columnTypeMap.put("intAsset", "int_asset_id");
        columnTypeMap.put("intName", "int_name");
        columnTypeMap.put("intDesc", "int_desc");
        columnTypeMap.put("intMstCountry", "int_mst_country_id");
        columnTypeMap.put("intMstRegion", "int_mst_region_id");
        columnTypeMap.put("intMstBizUnit", "int_mst_biz_unit_id");
        columnTypeMap.put("intMstPlatform", "int_mst_platform_id");
        columnTypeMap.put("intPlatformVersion", "int_platform_version");
        columnTypeMap.put("intMstOps", "int_mst_ops_id");
        columnTypeMap.put("intMstDesignPattern", "int_mst_design_patern_id");
        columnTypeMap.put("intMstSpecialOps", "int_mst_special_ops_id");
        columnTypeMap.put("intMstProcessType", "int_mst_process_type_id");
        columnTypeMap.put("intImplPattern", "int_impl_pattern");
        columnTypeMap.put("intSrcSys", "int_src_sys");
        columnTypeMap.put("intMstSrcProtocol", "int_mst_src_protocol_id");
        columnTypeMap.put("intMstSrcPayloadType", "int_mst_src_payload_type_id");
        columnTypeMap.put("intDest", "int_dest");
        columnTypeMap.put("intMstDestProtocol", "int_mst_dest_protocol_id");
        columnTypeMap.put("intMstDestPayloadType", "int_mst_dest_payload_type_id");
        columnTypeMap.put("intMstPayloadSecClass", "int_mst_payload_sec_class_id");
        columnTypeMap.put("intMsgSize", "int_msg_size");
        columnTypeMap.put("intOwner", "int_owner");
        columnTypeMap.put("intBizOwner", "int_biz_owner");
        columnTypeMap.put("intProj", "int_proj");
        columnTypeMap.put("intWorkstream", "int_workstream");
        columnTypeMap.put("intRelease", "int_release");
        columnTypeMap.put("intMstPriority", "int_mst_prority_id");
        columnTypeMap.put("intMstSdlcStatus", "int_mst_sdlc_status_id");
        columnTypeMap.put("intMstComplexity", "int_mst_complexity_id");
        columnTypeMap.put("intThroughput", "int_throughput");
        columnTypeMap.put("intDependency", "int_dependency");
        columnTypeMap.put("intRem", "int_rem");
        columnTypeMap.put("intWorkflowStatus", "int_workflow_status");
        columnTypeMap.put("intLob", "int_Lob");
        columnTypeMap.put("intArchitect", "int_Architect");
        columnTypeMap.put("intVersion", "int_Version");
        columnTypeMap.put("intDeveloper", "int_Developer");
        columnTypeMap.put("intPayload", "int_Payload");
        columnTypeMap.put("techSpecLoc", "int_tech_spec_loc");
        columnTypeMap.put("codeRepo", "int_code_repo");
        columnTypeMap.put("funcSpecLoc", "int_func_spec_loc");
        return columnTypeMap;
    }
    public static Map<String, String> getApiColumnName() {
        columnTypeMap.put("api", "api_id");
        columnTypeMap.put("intAsset", "int_asset_id");
        columnTypeMap.put("apiName", "api_name");
        columnTypeMap.put("apiDesc", "api_desc");
        columnTypeMap.put("apiPayload", "api_payload");
        columnTypeMap.put("apiProj", "api_proj");
        columnTypeMap.put("apiWorkstream", "api_workstream");
        columnTypeMap.put("apiRelease", "api_release");
        columnTypeMap.put("apiPlatformVersion", "api_platform_version");
        columnTypeMap.put("apiDest", "api_dest");
        columnTypeMap.put("apiDependency", "api_dependency");
        columnTypeMap.put("apiIntOwner", "api_int_owner");
        columnTypeMap.put("apiBizOwner", "api_biz_owner");
        columnTypeMap.put("apiRem", "api_rem");
        columnTypeMap.put("apiWorkflowStatus", "api_workflow_tatus");
        columnTypeMap.put("apiLob", "api_lob");
        return columnTypeMap;
    }
}
