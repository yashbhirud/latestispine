package com.capgemini.vault.entity;

import com.haulmont.cuba.core.entity.annotation.Extends;
import com.haulmont.cuba.security.entity.User;

import javax.persistence.Column;
import javax.persistence.Entity;

@Extends(User.class)
@Entity(name = "extuser$ExtUser")
public class ExtUser extends User {
    private static final long serialVersionUID = 3893281506452494415L;

    @Column(name = "allow_tech_spec")
    private Boolean allowTechSpec = false;

    @Column(name = "allow_func_spec")
    private Boolean allowFuncSpec = false;

    @Column(name = "allow_code_loc")
    private Boolean allowCodeLoc = false;

    public Boolean getAllowTechSpec() {
            return allowTechSpec;
        }

        public void setAllowTechSpec(Boolean allowTechSpec) {
            this.allowTechSpec = allowTechSpec;
        }

        public Boolean getAllowFuncSpec() {
            return allowFuncSpec;
        }

        public void setAllowFuncSpec(Boolean allowFuncSpec) {
            this.allowFuncSpec = allowFuncSpec;
        }

        public Boolean getAllowCodeLoc() {
            return allowCodeLoc;
        }

        public void setAllowCodeLoc(Boolean allowCodeLoc) {
            this.allowCodeLoc = allowCodeLoc;
        }
    }