package com.capgemini.vault.entity;

import com.haulmont.chile.core.annotations.Composition;
import com.haulmont.chile.core.annotations.MetaProperty;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseIdentityIdEntity;
import com.haulmont.cuba.core.entity.Creatable;
import com.haulmont.cuba.core.entity.SoftDelete;
import com.haulmont.cuba.core.entity.Updatable;
import com.haulmont.cuba.core.global.DdlGeneration;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.CREATE_ONLY)
@Table(name = "vault_api")
@Entity(name = "vault_Api")
@NamePattern("%s %s|api,apiName")
public class VaultApi extends BaseIdentityIdEntity implements SoftDelete, Updatable, Creatable {
    private static final long serialVersionUID = 7621888114518506164L;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_ts")
    private Date createTs;

    @Composition
    @OneToMany(mappedBy = "vaultApi")
    private List<VaultIntegrationApi> intApiDetails;

    @Column(name = "created_by", length = 50)
    private String createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "delete_ts")
    private Date deleteTs;

    @Column(name = "deleted_by", length = 50)
    private String deletedBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_ts")
    private Date updateTs;

    @Column(name = "updated_by", length = 50)
    private String updatedBy;

    @Column(name = "api_id", nullable = false)
    @NotNull
    private String api;

    @Column(name = "api_biz_owner")
    private String apiBizOwner;

    @Column(name = "api_dependency")
    private String apiDependency;

    @Lob
    @Column(name = "api_desc")
    private String apiDesc;

    @Column(name = "api_dest")
    private String apiDest;

    @Column(name = "api_payload")
    private String apiPayload;

    @Column(name = "api_int_msg_size")
    private Integer apiIntMsgSize;

    @Column(name = "api_int_owner")
    private String apiIntOwner;

    @Column(name = "api_lob", length = 50)
    private String apiLob;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "api_mst_biz_unit_id")
    private VaultMaster apiMstBizUnit;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "api_mst_class_id")
    private VaultMaster apiMstClass;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "api_mst_complexity_id")
    private VaultMaster apiMstComplexity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "api_mst_country_id")
    private VaultMaster apiMstCountry;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "api_mst_design_pattern_id")
    private VaultMaster apiMstDesignPattern;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "api_mst_dest_payload_type_id")
    private VaultMaster apiMstDestPayloadType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "api_mst_dest_protocol_id")
    private VaultMaster apiMstDestProtocol;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "api_mst_ops_id")
    private VaultMaster apiMstOps;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "api_mst_payload_secur_class_id")
    private VaultMaster apiMstPayloadSecurClass;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "api_mst_platform_id")
    private VaultMaster apiMstPlatform;

    @Column(name = "api_platform_version")
    private String apiPlatformVersion;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "api_mst_priority_id")
    private VaultMaster apiMstPriority;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "api_mst_process_type_id")
    private VaultMaster apiMstProcessType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "api_mst_region_id")
    private VaultMaster apiMstRegion;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "api_mst_sdlc_status_id")
    private VaultMaster apiMstSdlcStatus;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "api_mst_src_payload_type_id")
    private VaultMaster apiMstSrcPayloadType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "api_mst_src_protocol_id")
    private VaultMaster apiMstSrcProtocol;

    @Column(name = "api_name", length = 500)
    private String apiName;

    @Column(name = "api_proj")
    private String apiProj;

    @Column(name = "api_release")
    private String apiRelease;

    @Lob
    @Column(name = "api_rem")
    private String apiRem;

    @Column(name = "api_throughput")
    private Integer apiThroughput;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "api_ups_id")
    private VaultUploadSummary apiUps;

    @Column(name = "API_WORKFLOW_STATUS")
    private String apiWorkflowStatus;

    @Column(name = "api_workstream")
    private String apiWorkstream;

    @Column(name = "int_asset_id")
    @Lob
    private String intAsset;

    public List<VaultIntegrationApi> getIntApiDetails() {
        return intApiDetails;
    }

    public void setIntApiDetails(List<VaultIntegrationApi> intApiDetails) {
        this.intApiDetails = intApiDetails;
    }

    public void setApiWorkflowStatus(Status apiWorkflowStatus) {
        this.apiWorkflowStatus = apiWorkflowStatus == null ? null : apiWorkflowStatus.getId();
    }

    public Status getApiWorkflowStatus() {
        return apiWorkflowStatus == null ? null : Status.fromId(apiWorkflowStatus);
    }

    @Override
    public Boolean isDeleted() {
        return deleteTs != null;
    }

    public String getIntAsset() {
        return intAsset;
    }

    public void setIntAsset(String intAsset) {
        this.intAsset = intAsset;
    }

    public String getApiWorkstream() {
        return apiWorkstream;
    }

    public void setApiWorkstream(String apiWorkstream) {
        this.apiWorkstream = apiWorkstream;
    }

    public VaultUploadSummary getApiUps() {
        return apiUps;
    }

    public void setApiUps(VaultUploadSummary apiUps) {
        this.apiUps = apiUps;
    }

    public Integer getApiThroughput() {
        return apiThroughput;
    }

    public void setApiThroughput(Integer apiThroughput) {
        this.apiThroughput = apiThroughput;
    }

    public String getApiRem() {
        return apiRem;
    }

    public void setApiRem(String apiRem) {
        this.apiRem = apiRem;
    }

    public String getApiRelease() {
        return apiRelease;
    }

    public void setApiRelease(String apiRelease) {
        this.apiRelease = apiRelease;
    }

    public String getApiProj() {
        return apiProj;
    }

    public void setApiProj(String apiProj) {
        this.apiProj = apiProj;
    }

    public String getApiName() {
        return apiName;
    }

    public void setApiName(String apiName) {
        this.apiName = apiName;
    }

    public VaultMaster getApiMstSrcProtocol() {
        return apiMstSrcProtocol;
    }

    public void setApiMstSrcProtocol(VaultMaster apiMstSrcProtocol) {
        this.apiMstSrcProtocol = apiMstSrcProtocol;
    }

    public VaultMaster getApiMstSrcPayloadType() {
        return apiMstSrcPayloadType;
    }

    public void setApiMstSrcPayloadType(VaultMaster apiMstSrcPayloadType) {
        this.apiMstSrcPayloadType = apiMstSrcPayloadType;
    }

    public VaultMaster getApiMstSdlcStatus() {
        return apiMstSdlcStatus;
    }

    public void setApiMstSdlcStatus(VaultMaster apiMstSdlcStatus) {
        this.apiMstSdlcStatus = apiMstSdlcStatus;
    }

    public VaultMaster getApiMstRegion() {
        return apiMstRegion;
    }

    public void setApiMstRegion(VaultMaster apiMstRegion) {
        this.apiMstRegion = apiMstRegion;
    }

    public VaultMaster getApiMstProcessType() {
        return apiMstProcessType;
    }

    public void setApiMstProcessType(VaultMaster apiMstProcessType) {
        this.apiMstProcessType = apiMstProcessType;
    }

    public VaultMaster getApiMstPriority() {
        return apiMstPriority;
    }

    public void setApiMstPriority(VaultMaster apiMstPriority) {
        this.apiMstPriority = apiMstPriority;
    }

    public String getApiPlatformVersion() {
        return apiPlatformVersion;
    }

    public void setApiPlatformVersion(String apiPlatformVersion) {
        this.apiPlatformVersion = apiPlatformVersion;
    }

    public VaultMaster getApiMstPlatform() {
        return apiMstPlatform;
    }

    public void setApiMstPlatform(VaultMaster apiMstPlatform) {
        this.apiMstPlatform = apiMstPlatform;
    }

    public VaultMaster getApiMstPayloadSecurClass() {
        return apiMstPayloadSecurClass;
    }

    public void setApiMstPayloadSecurClass(VaultMaster apiMstPayloadSecurClass) {
        this.apiMstPayloadSecurClass = apiMstPayloadSecurClass;
    }

    public VaultMaster getApiMstOps() {
        return apiMstOps;
    }

    public void setApiMstOps(VaultMaster apiMstOps) {
        this.apiMstOps = apiMstOps;
    }

    public VaultMaster getApiMstDestProtocol() {
        return apiMstDestProtocol;
    }

    public void setApiMstDestProtocol(VaultMaster apiMstDestProtocol) {
        this.apiMstDestProtocol = apiMstDestProtocol;
    }

    public VaultMaster getApiMstDestPayloadType() {
        return apiMstDestPayloadType;
    }

    public void setApiMstDestPayloadType(VaultMaster apiMstDestPayloadType) {
        this.apiMstDestPayloadType = apiMstDestPayloadType;
    }

    public VaultMaster getApiMstDesignPattern() {
        return apiMstDesignPattern;
    }

    public void setApiMstDesignPattern(VaultMaster apiMstDesignPattern) {
        this.apiMstDesignPattern = apiMstDesignPattern;
    }

    public VaultMaster getApiMstCountry() {
        return apiMstCountry;
    }

    public void setApiMstCountry(VaultMaster apiMstCountry) {
        this.apiMstCountry = apiMstCountry;
    }

    public VaultMaster getApiMstComplexity() {
        return apiMstComplexity;
    }

    public void setApiMstComplexity(VaultMaster apiMstComplexity) {
        this.apiMstComplexity = apiMstComplexity;
    }

    public VaultMaster getApiMstClass() {
        return apiMstClass;
    }

    public void setApiMstClass(VaultMaster apiMstClass) {
        this.apiMstClass = apiMstClass;
    }

    public VaultMaster getApiMstBizUnit() {
        return apiMstBizUnit;
    }

    public void setApiMstBizUnit(VaultMaster apiMstBizUnit) {
        this.apiMstBizUnit = apiMstBizUnit;
    }

    public String getApiLob() {
        return apiLob;
    }

    public void setApiLob(String apiLob) {
        this.apiLob = apiLob;
    }

    public String getApiIntOwner() {
        if(StringUtils.isBlank(apiIntOwner)){
            return null;
        }
        else if(apiIntOwner.matches("^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$")){
            return (String.format("<a href=\"mailto:%s\" target=\"_blank\">%s</a>", apiIntOwner, apiIntOwner));
        }
        else
            return apiIntOwner;
    }

    public void setApiIntOwner(String apiIntOwner) {
        this.apiIntOwner = apiIntOwner;
    }

    public Integer getApiIntMsgSize() {
        return apiIntMsgSize;
    }

    public void setApiIntMsgSize(Integer apiIntMsgSize) {
        this.apiIntMsgSize = apiIntMsgSize;
    }

    public String getApiPayload() {
        return apiPayload;
    }

    public void setApiPayload(String apiPayload) {
        this.apiPayload = apiPayload;
    }

    public String getApiDest() {
        return apiDest;
    }

    public void setApiDest(String apiDest) {
        this.apiDest = apiDest;
    }

    public String getApiDesc() {
        return apiDesc;
    }

    public void setApiDesc(String apiDesc) {
        this.apiDesc = apiDesc;
    }

    public String getApiDependency() {
        return apiDependency;
    }

    public void setApiDependency(String apiDependency) {
        this.apiDependency = apiDependency;
    }

    public String getApiBizOwner() {
        if(StringUtils.isBlank(apiBizOwner)){
            return null;
        }
        else if(apiBizOwner.matches("^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$")){
            return (String.format("<a href=\"mailto:%s\" target=\"_blank\">%s</a>", apiBizOwner, apiBizOwner));
        }
        else
            return apiBizOwner;
    }

    public void setApiBizOwner(String apiBizOwner) {
        this.apiBizOwner = apiBizOwner;
    }

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }

    @Override
    public String getUpdatedBy() {
        return updatedBy;
    }

    @Override
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public Date getUpdateTs() {
        return updateTs;
    }

    @Override
    public void setUpdateTs(Date updateTs) {
        this.updateTs = updateTs;
    }

    @Override
    public String getDeletedBy() {
        return deletedBy;
    }

    @Override
    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    @Override
    public Date getDeleteTs() {
        return deleteTs;
    }

    @Override
    public void setDeleteTs(Date deleteTs) {
        this.deleteTs = deleteTs;
    }

    @Override
    public String getCreatedBy() {
        return createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public Date getCreateTs() {
        return createTs;
    }

    @Override
    public void setCreateTs(Date createTs) {
        this.createTs = createTs;
    }


    /*@MetaProperty(related = {"apiBizOwner"})
    public String getBizOwner() {
        if(StringUtils.isBlank(apiBizOwner)){
            return null;
        }
        else if(apiBizOwner.matches("^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$")){
            return (String.format("<a href=\"mailto:%s\" target=\"_blank\">%s</a>", apiBizOwner, apiBizOwner));
        }
        else
            return apiBizOwner;
    }

    @MetaProperty(related = {"apiIntOwner"})
    public String getIntOwner() {
        if(StringUtils.isBlank(apiIntOwner)){
            return null;
        }
        else if(apiIntOwner.matches("^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$")){
            return (String.format("<a href=\"mailto:%s\" target=\"_blank\">%s</a>", apiIntOwner, apiIntOwner));
        }
        else
            return apiIntOwner;
    }*/
}