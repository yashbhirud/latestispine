package com.capgemini.vault.entity;

import com.haulmont.cuba.core.entity.BaseIdentityIdEntity;
import com.haulmont.cuba.core.entity.Creatable;
import com.haulmont.cuba.core.entity.SoftDelete;
import com.haulmont.cuba.core.entity.Updatable;
import com.haulmont.cuba.core.entity.annotation.OnDelete;
import com.haulmont.cuba.core.global.DdlGeneration;
import com.haulmont.cuba.core.global.DeletePolicy;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.util.Date;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.CREATE_ONLY)
@Table(name = "vault_api_audit")
@Entity(name = "vault_ApiAudit")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "ava_id"))
})
public class VaultApiAudit extends BaseIdentityIdEntity implements SoftDelete, Updatable, Creatable {
    private static final long serialVersionUID = 8633866654594383035L;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_ts", nullable = false)
    private Date createTs;

    @Column(name = "created_by", nullable = false, length = 50)
    private String createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "delete_ts")
    private Date deleteTs;

    @Column(name = "deleted_by", length = 50)
    private String deletedBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_ts")
    private Date updateTs;

    @Column(name = "updated_by", length = 50)
    private String updatedBy;

    @Column(name = "ava_api_id")
    private String avaApi;

    @Column(name = "ava_api_biz_owner")
    private String avaApiBizOwner;

    @Column(name = "ava_api_dependency")
    private String avaApiDependency;

    @Lob
    @Column(name = "ava_api_desc")
    private String avaApiDesc;

    @Column(name = "ava_api_dest")
    private String avaApiDest;

    @Column(name = "ava_api_payload")
    private String avaApiPayload;

    @Column(name = "ava_api_int_msg_size")
    private Integer avaApiIntMsgSize;

    @Column(name = "ava_api_int_owner")
    private String avaApiIntOwner;

    @Column(name = "ava_api_lob", length = 50)
    private String avaApiLob;

    @JoinColumn(name = "AVA_API_MST_BIZ_UNIT_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster avaApiMstBizUnit;

    @JoinColumn(name = "AVA_API_MST_CLASS_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster avaApiMstClass;

    @JoinColumn(name = "AVA_API_MST_COMPLEXITY_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster avaApiMstComplexity;

    @JoinColumn(name = "AVA_API_MST_COUNTRY_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster avaApiMstCountry;

    @JoinColumn(name = "AVA_API_MST_DESIGN_PATTERN_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster avaApiMstDesignPattern;

    @JoinColumn(name = "AVA_API_MST_DEST_PAYLOAD_TYPE_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster avaApiMstDestPayloadType;

    @JoinColumn(name = "AVA_API_MST_DEST_PROTOCOL_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster avaApiMstDestProtocol;

    @JoinColumn(name = "AVA_API_MST_OPS_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster avaApiMstOps;

    @JoinColumn(name = "AVA_API_MST_PAYLOAD_SECUR_CLASS_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster avaApiMstPayloadSecurClass;

    @JoinColumn(name = "AVA_API_MST_PLATFORM_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster avaApiMstPlatform;

    @JoinColumn(name = "AVA_API_MST_PRIORITY_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    @OnDelete(DeletePolicy.CASCADE)
    private VaultMaster avaApiMstPriority;

    @JoinColumn(name = "AVA_API_MST_PROCESS_TYPE_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster avaApiMstProcessType;

    @JoinColumn(name = "AVA_API_MST_REGION_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster avaApiMstRegion;

    @JoinColumn(name = "AVA_API_MST_SDLC_STATUS_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster avaApiMstSdlcStatus;

    @JoinColumn(name = "AVA_API_MST_SRC_PAYLOAD_TYPE_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster avaApiMstSrcPayloadType;

    @JoinColumn(name = "AVA_API_MST_SRC_PROTOCOL_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster avaApiMstSrcProtocol;

    @Column(name = "ava_api_name", length = 500)
    private String avaApiName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ava_api_parent_id")
    private VaultApi avaApiParent;

    @Column(name = "ava_api_platform_version")
    private String avaApiPlatformVersion;

    @Column(name = "ava_api_proj")
    private String avaApiProj;

    @Column(name = "ava_api_release")
    private String avaApiRelease;

    @Lob
    @Column(name = "ava_api_rem")
    private String avaApiRem;

    @Column(name = "ava_api_throughput")
    private Integer avaApiThroughput;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ava_api_ups_id")
    private VaultUploadSummary avaApiUps;

    @Column(name = "ava_api_workflow_status", length = 50)
    private String avaApiWorkflowStatus;

    @Column(name = "ava_api_workstream")
    private String avaApiWorkstream;

    @Column(name = "ava_int_asset_id")
    @Lob
    private String avaIntAsset;

    @Column(name = "operation_name")
    private String operationName;

    public void setAvaApiMstSrcProtocol(VaultMaster avaApiMstSrcProtocol) {
        this.avaApiMstSrcProtocol = avaApiMstSrcProtocol;
    }

    public VaultMaster getAvaApiMstSrcProtocol() {
        return avaApiMstSrcProtocol;
    }

    public void setAvaApiMstSrcPayloadType(VaultMaster avaApiMstSrcPayloadType) {
        this.avaApiMstSrcPayloadType = avaApiMstSrcPayloadType;
    }

    public VaultMaster getAvaApiMstSrcPayloadType() {
        return avaApiMstSrcPayloadType;
    }

    public void setAvaApiMstSdlcStatus(VaultMaster avaApiMstSdlcStatus) {
        this.avaApiMstSdlcStatus = avaApiMstSdlcStatus;
    }

    public VaultMaster getAvaApiMstSdlcStatus() {
        return avaApiMstSdlcStatus;
    }

    public void setAvaApiMstRegion(VaultMaster avaApiMstRegion) {
        this.avaApiMstRegion = avaApiMstRegion;
    }

    public VaultMaster getAvaApiMstRegion() {
        return avaApiMstRegion;
    }

    public void setAvaApiMstProcessType(VaultMaster avaApiMstProcessType) {
        this.avaApiMstProcessType = avaApiMstProcessType;
    }

    public VaultMaster getAvaApiMstProcessType() {
        return avaApiMstProcessType;
    }

    public void setAvaApiMstPriority(VaultMaster avaApiMstPriority) {
        this.avaApiMstPriority = avaApiMstPriority;
    }

    public VaultMaster getAvaApiMstPriority() {
        return avaApiMstPriority;
    }

    public void setAvaApiMstPlatform(VaultMaster avaApiMstPlatform) {
        this.avaApiMstPlatform = avaApiMstPlatform;
    }

    public VaultMaster getAvaApiMstPlatform() {
        return avaApiMstPlatform;
    }

    public void setAvaApiMstPayloadSecurClass(VaultMaster avaApiMstPayloadSecurClass) {
        this.avaApiMstPayloadSecurClass = avaApiMstPayloadSecurClass;
    }

    public VaultMaster getAvaApiMstPayloadSecurClass() {
        return avaApiMstPayloadSecurClass;
    }

    public void setAvaApiMstOps(VaultMaster avaApiMstOps) {
        this.avaApiMstOps = avaApiMstOps;
    }

    public VaultMaster getAvaApiMstOps() {
        return avaApiMstOps;
    }

    public void setAvaApiMstDestProtocol(VaultMaster avaApiMstDestProtocol) {
        this.avaApiMstDestProtocol = avaApiMstDestProtocol;
    }

    public VaultMaster getAvaApiMstDestProtocol() {
        return avaApiMstDestProtocol;
    }

    public void setAvaApiMstDestPayloadType(VaultMaster avaApiMstDestPayloadType) {
        this.avaApiMstDestPayloadType = avaApiMstDestPayloadType;
    }

    public VaultMaster getAvaApiMstDestPayloadType() {
        return avaApiMstDestPayloadType;
    }

    public void setAvaApiMstDesignPattern(VaultMaster avaApiMstDesignPattern) {
        this.avaApiMstDesignPattern = avaApiMstDesignPattern;
    }

    public VaultMaster getAvaApiMstDesignPattern() {
        return avaApiMstDesignPattern;
    }

    public void setAvaApiMstCountry(VaultMaster avaApiMstCountry) {
        this.avaApiMstCountry = avaApiMstCountry;
    }

    public VaultMaster getAvaApiMstCountry() {
        return avaApiMstCountry;
    }

    public void setAvaApiMstComplexity(VaultMaster avaApiMstComplexity) {
        this.avaApiMstComplexity = avaApiMstComplexity;
    }

    public VaultMaster getAvaApiMstComplexity() {
        return avaApiMstComplexity;
    }

    public void setAvaApiMstClass(VaultMaster avaApiMstClass) {
        this.avaApiMstClass = avaApiMstClass;
    }

    public VaultMaster getAvaApiMstClass() {
        return avaApiMstClass;
    }

    public void setAvaApiMstBizUnit(VaultMaster avaApiMstBizUnit) {
        this.avaApiMstBizUnit = avaApiMstBizUnit;
    }

    public VaultMaster getAvaApiMstBizUnit() {
        return avaApiMstBizUnit;
    }

    @Override
    public Boolean isDeleted() {
        return deleteTs != null;
    }

    public String getOperationName() {
        return operationName;
    }

    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }

    public String getAvaIntAsset() {
        return avaIntAsset;
    }

    public void setAvaIntAsset(String avaIntAsset) {
        this.avaIntAsset = avaIntAsset;
    }

    public String getAvaApiWorkstream() {
        return avaApiWorkstream;
    }

    public void setAvaApiWorkstream(String avaApiWorkstream) {
        this.avaApiWorkstream = avaApiWorkstream;
    }

    public String getAvaApiWorkflowStatus() {
        return avaApiWorkflowStatus;
    }

    public void setAvaApiWorkflowStatus(String avaApiWorkflowStatus) {
        this.avaApiWorkflowStatus = avaApiWorkflowStatus;
    }

    public VaultUploadSummary getAvaApiUps() {
        return avaApiUps;
    }

    public void setAvaApiUps(VaultUploadSummary avaApiUps) {
        this.avaApiUps = avaApiUps;
    }

    public Integer getAvaApiThroughput() {
        return avaApiThroughput;
    }

    public void setAvaApiThroughput(Integer avaApiThroughput) {
        this.avaApiThroughput = avaApiThroughput;
    }

    public String getAvaApiRem() {
        return avaApiRem;
    }

    public void setAvaApiRem(String avaApiRem) {
        this.avaApiRem = avaApiRem;
    }

    public String getAvaApiRelease() {
        return avaApiRelease;
    }

    public void setAvaApiRelease(String avaApiRelease) {
        this.avaApiRelease = avaApiRelease;
    }

    public String getAvaApiProj() {
        return avaApiProj;
    }

    public void setAvaApiProj(String avaApiProj) {
        this.avaApiProj = avaApiProj;
    }

    public String getAvaApiPlatformVersion() {
        return avaApiPlatformVersion;
    }

    public void setAvaApiPlatformVersion(String avaApiPlatformVersion) {
        this.avaApiPlatformVersion = avaApiPlatformVersion;
    }

    public VaultApi getAvaApiParent() {
        return avaApiParent;
    }

    public void setAvaApiParent(VaultApi avaApiParent) {
        this.avaApiParent = avaApiParent;
    }

    public String getAvaApiName() {
        return avaApiName;
    }

    public void setAvaApiName(String avaApiName) {
        this.avaApiName = avaApiName;
    }

    public String getAvaApiLob() {
        return avaApiLob;
    }

    public void setAvaApiLob(String avaApiLob) {
        this.avaApiLob = avaApiLob;
    }

    public String getAvaApiIntOwner() {
        if(StringUtils.isBlank(avaApiIntOwner)){
            return null;
        }
        else if(avaApiIntOwner.matches("^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$")){
            return (String.format("<a href=\"mailto:%s\" target=\"_blank\">%s</a>", avaApiIntOwner, avaApiIntOwner));
        }
        else
            return avaApiIntOwner;
    }

    public void setAvaApiIntOwner(String avaApiIntOwner) {
        this.avaApiIntOwner = avaApiIntOwner;
    }

    public Integer getAvaApiIntMsgSize() {
        return avaApiIntMsgSize;
    }

    public void setAvaApiIntMsgSize(Integer avaApiIntMsgSize) {
        this.avaApiIntMsgSize = avaApiIntMsgSize;
    }

    public String getAvaApiPayload() {
        return avaApiPayload;
    }

    public void setAvaApiPayload(String avaApiEntityName) {
        this.avaApiPayload = avaApiPayload;
    }

    public String getAvaApiDest() {
        return avaApiDest;
    }

    public void setAvaApiDest(String avaApiDest) {
        this.avaApiDest = avaApiDest;
    }

    public String getAvaApiDesc() {
        return avaApiDesc;
    }

    public void setAvaApiDesc(String avaApiDesc) {
        this.avaApiDesc = avaApiDesc;
    }

    public String getAvaApiDependency() {
        return avaApiDependency;
    }

    public void setAvaApiDependency(String avaApiDependency) {
        this.avaApiDependency = avaApiDependency;
    }

    public String getAvaApiBizOwner() {
        if(StringUtils.isBlank(avaApiBizOwner)){
            return null;
        }
        else if(avaApiBizOwner.matches("^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$")){
            return (String.format("<a href=\"mailto:%s\" target=\"_blank\">%s</a>", avaApiBizOwner, avaApiBizOwner));
        }
        else
            return avaApiBizOwner;
    }

    public void setAvaApiBizOwner(String avaApiBizOwner) {
        this.avaApiBizOwner = avaApiBizOwner;
    }

    public String getAvaApi() {
        return avaApi;
    }

    public void setAvaApi(String avaApi) {
        this.avaApi = avaApi;
    }

    @Override
    public String getUpdatedBy() {
        return updatedBy;
    }

    @Override
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public Date getUpdateTs() {
        return updateTs;
    }

    @Override
    public void setUpdateTs(Date updateTs) {
        this.updateTs = updateTs;
    }

    @Override
    public String getDeletedBy() {
        return deletedBy;
    }

    @Override
    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    @Override
    public Date getDeleteTs() {
        return deleteTs;
    }

    @Override
    public void setDeleteTs(Date deleteTs) {
        this.deleteTs = deleteTs;
    }

    @Override
    public String getCreatedBy() {
        return createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public Date getCreateTs() {
        return createTs;
    }

    @Override
    public void setCreateTs(Date createTs) {
        this.createTs = createTs;
    }
}