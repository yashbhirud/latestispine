package com.capgemini.vault.entity;

import com.haulmont.cuba.core.entity.BaseIdentityIdEntity;
import com.haulmont.cuba.core.entity.Creatable;
import com.haulmont.cuba.core.entity.Updatable;
import com.haulmont.cuba.core.global.DdlGeneration;

import javax.persistence.*;
import java.util.Date;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.CREATE_ONLY)
@Table(name = "vault_err_summary")
@Entity(name = "vault_ErrSummary")
public class VaultErrSummary extends BaseIdentityIdEntity implements Updatable, Creatable {
    private static final long serialVersionUID = -2775479988738157804L;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_ts")
    private Date createTs;

    @Column(name = "created_by", length = 50)
    private String createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_ts")
    private Date updateTs;

    @Column(name = "updated_by", length = 50)
    private String updatedBy;

    @Column(name = "err_col_name")
    private String errColName;

    @Column(name = "err_datatype", length = 500)
    private String errDatatype;

    @Lob
    @Column(name = "err_desc")
    private String errDesc;

    @Column(name = "err_row_no")
    private Integer errRowNo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "err_ups_id")
    private VaultUploadSummary errUps;

    public VaultUploadSummary getErrUps() {
        return errUps;
    }

    public void setErrUps(VaultUploadSummary errUps) {
        this.errUps = errUps;
    }

    public Integer getErrRowNo() {
        return errRowNo;
    }

    public void setErrRowNo(Integer errRowNo) {
        this.errRowNo = errRowNo;
    }

    public String getErrDesc() {
        return errDesc;
    }

    public void setErrDesc(String errDesc) {
        this.errDesc = errDesc;
    }

    public String getErrDatatype() {
        return errDatatype;
    }

    public void setErrDatatype(String errDatatype) {
        this.errDatatype = errDatatype;
    }

    public String getErrColName() {
        return errColName;
    }

    public void setErrColName(String errColName) {
        this.errColName = errColName;
    }

    @Override
    public String getUpdatedBy() {
        return updatedBy;
    }

    @Override
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public Date getUpdateTs() {
        return updateTs;
    }

    @Override
    public void setUpdateTs(Date updateTs) {
        this.updateTs = updateTs;
    }

    @Override
    public String getCreatedBy() {
        return createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public Date getCreateTs() {
        return createTs;
    }

    @Override
    public void setCreateTs(Date createTs) {
        this.createTs = createTs;
    }
}