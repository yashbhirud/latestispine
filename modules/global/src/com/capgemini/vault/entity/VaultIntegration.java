package com.capgemini.vault.entity;

import com.haulmont.chile.core.annotations.Composition;
import com.haulmont.chile.core.annotations.MetaProperty;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseIdentityIdEntity;
import com.haulmont.cuba.core.entity.Creatable;
import com.haulmont.cuba.core.entity.SoftDelete;
import com.haulmont.cuba.core.entity.Updatable;
import com.haulmont.cuba.core.global.DdlGeneration;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.CREATE_ONLY)
@Table(name = "vault_integration")
@Entity(name = "vault_Integration")
@NamePattern("%s %s|intAsset,intName")
public class VaultIntegration extends BaseIdentityIdEntity implements SoftDelete, Updatable, Creatable {

    private static final long serialVersionUID = -5844118955948469430L;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_ts")
    private Date createTs;

    @Composition
    @OneToMany(mappedBy = "vaultIntegration")
    private List<VaultIntegrationApi> intApiDetails;

    @Column(name = "created_by", length = 50)
    private String createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "delete_ts")
    private Date deleteTs;

    @Column(name = "deleted_by", length = 50)
    private String deletedBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_ts")
    private Date updateTs;

    @Column(name = "updated_by", length = 50)
    private String updatedBy;

    @Column(name = "int_asset_id", nullable = false)
    @NotNull
    private String intAsset;

    @Column(name = "int_biz_owner")
    private String intBizOwner;

    @Column(name = "int_code_repo", length = 500)
    private String intCodeRepo;

    @Column(name = "int_dependency")
    private String intDependency;

    @Lob
    @Column(name = "int_desc")
    private String intDesc;

    @Column(name = "int_dest")
    private String intDest;

    @Column(name = "int_func_spec_loc", length = 500)
    private String intFuncSpecLoc;

    @Column(name = "int_impl_pattern", length = 500)
    private String intImplPattern;

    @Column(name = "int_lob", length = 50)
    private String intLob;

    @Column(name = "int_msg_size")
    private Integer intMsgSize;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "int_mst_biz_unit_id")
    private VaultMaster intMstBizUnit;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "int_mst_complexity_id")
    private VaultMaster intMstComplexity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "int_mst_country_id")
    private VaultMaster intMstCountry;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "int_mst_design_pattern_id")
    private VaultMaster intMstDesignPattern;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "int_mst_dest_payload_type_id")
    private VaultMaster intMstDestPayloadType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "int_mst_dest_protocol_id")
    private VaultMaster intMstDestProtocol;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "int_mst_ops_id")
    private VaultMaster intMstOps;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "int_mst_payload_sec_class_id")
    private VaultMaster intMstPayloadSecClass;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "int_mst_platform_id")
    private VaultMaster intMstPlatform;

    @Column(name = "int_platform_version")
    @Lob
    private String intPlatformVersion;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "int_mst_priority_id")
    private VaultMaster intMstPriority;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "int_mst_process_type_id")
    private VaultMaster intMstProcessType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "int_mst_region_id")
    private VaultMaster intMstRegion;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "int_mst_sdlc_status_id")
    private VaultMaster intMstSdlcStatus;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "int_mst_special_ops_id")
    private VaultMaster intMstSpecialOps;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "int_mst_src_payload_type_id")
    private VaultMaster intMstSrcPayloadType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "int_mst_src_protocol_id")
    private VaultMaster intMstSrcProtocol;

    @Column(name = "int_name", length = 500)
    private String intName;

    @Column(name = "int_owner")
    private String intOwner;

    @Column(name = "int_payload")
    private String intPayload;

    @Column(name = "int_proj")
    private String intProj;

    @Lob
    @Column(name = "int_rem")
    private String intRem;

    @Column(name = "int_src_sys")
    private String intSrcSys;

    @Column(name = "int_tech_spec_loc", length = 500)
    private String intTechSpecLoc;

    @Column(name = "int_throughput")
    private Integer intThroughput;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "int_ups_id")
    private VaultUploadSummary intUps;

    @Column(name = "INT_WORKFLOW_STATUS")
    private String intWorkflowStatus;

    @Column(name = "int_workstream")
    private String intWorkstream;

    @Column(name = "int_release")
    private String intRelease;

    @Column(name = "int_developer")
    private String intDeveloper;

    @Column(name = "int_architect")
    private String intArchitect;

    @Column(name = "int_version")
    private String intVersion;

    public String getIntDeveloper() {
        return intDeveloper;
    }

    public void setIntDeveloper(String intDeveloper) {
        this.intDeveloper = intDeveloper;
    }

    public String getIntArchitect() {
        return intArchitect;
    }

    public void setIntArchitect(String intArchitect) {
        this.intArchitect = intArchitect;
    }

    public String getIntVersion() {
        return intVersion;
    }

    public void setIntVersion(String intVersion) {
        this.intVersion = intVersion;
    }

    public List<VaultIntegrationApi> getIntApiDetails() {
        return intApiDetails;
    }

    public void setIntApiDetails(List<VaultIntegrationApi> intApiDetails) {
        this.intApiDetails = intApiDetails;
    }

    public void setIntWorkflowStatus(Status intWorkflowStatus) {
        this.intWorkflowStatus = intWorkflowStatus == null ? null : intWorkflowStatus.getId();
    }

    public Status getIntWorkflowStatus() {
        return intWorkflowStatus == null ? null : Status.fromId(intWorkflowStatus);
    }

    @Override
    public Boolean isDeleted() {
        return deleteTs != null;
    }

    public String getIntWorkstream() {
        return intWorkstream;
    }

    public void setIntWorkstream(String intWorkstream) {
        this.intWorkstream = intWorkstream;
    }

    public VaultUploadSummary getIntUps() {
        return intUps;
    }

    public void setIntUps(VaultUploadSummary intUps) {
        this.intUps = intUps;
    }

    public Integer getIntThroughput() {
        return intThroughput;
    }

    public void setIntThroughput(Integer intThroughput) {
        this.intThroughput = intThroughput;
    }

    public String getIntTechSpecLoc() {

        return intTechSpecLoc;
    }

    public void setIntTechSpecLoc(String intTechSpecLoc) {
        this.intTechSpecLoc = intTechSpecLoc;
    }

    public String getIntSrcSys() {
        return intSrcSys;
    }

    public void setIntSrcSys(String intSrcSys) {
        this.intSrcSys = intSrcSys;
    }

    public String getIntRem() {
        return intRem;
    }

    public void setIntRem(String intRem) {
        this.intRem = intRem;
    }

    public String getIntProj() {
        return intProj;
    }

    public void setIntProj(String intProj) {
        this.intProj = intProj;
    }

    public String getIntPayload() {
        return intPayload;
    }

    public void setIntPayload(String intPayload) {
        this.intPayload = intPayload;
    }

    public String getIntOwner() {
        if(StringUtils.isBlank(intOwner)){
            return null;
        }
        if(intOwner.matches("^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$"))
            return (String.format("<a href=\"mailto:%s\" target=\"_blank\">%s</a>", intOwner, intOwner));
        else
            return intOwner;
    }

    public void setIntOwner(String intOwner) {
        this.intOwner = intOwner;
    }

    public String getIntName() {
        return intName;
    }

    public void setIntName(String intName) {
        this.intName = intName;
    }

    public VaultMaster getIntMstSrcProtocol() {
        return intMstSrcProtocol;
    }

    public void setIntMstSrcProtocol(VaultMaster intMstSrcProtocol) {
        this.intMstSrcProtocol = intMstSrcProtocol;
    }

    public VaultMaster getIntMstSrcPayloadType() {
        return intMstSrcPayloadType;
    }

    public void setIntMstSrcPayloadType(VaultMaster intMstSrcPayloadType) {
        this.intMstSrcPayloadType = intMstSrcPayloadType;
    }

    public VaultMaster getIntMstSpecialOps() {
        return intMstSpecialOps;
    }

    public void setIntMstSpecialOps(VaultMaster intMstSpecialOps) {
        this.intMstSpecialOps = intMstSpecialOps;
    }

    public VaultMaster getIntMstSdlcStatus() {
        return intMstSdlcStatus;
    }

    public void setIntMstSdlcStatus(VaultMaster intMstSdlcStatus) {
        this.intMstSdlcStatus = intMstSdlcStatus;
    }

    public VaultMaster getIntMstRegion() {
        return intMstRegion;
    }

    public void setIntMstRegion(VaultMaster intMstRegion) {
        this.intMstRegion = intMstRegion;
    }

    public VaultMaster getIntMstProcessType() {
        return intMstProcessType;
    }

    public void setIntMstProcessType(VaultMaster intMstProcessType) {
        this.intMstProcessType = intMstProcessType;
    }

    public VaultMaster getIntMstPriority() {
        return intMstPriority;
    }

    public void setIntMstPriority(VaultMaster intMstPriority) {
        this.intMstPriority = intMstPriority;
    }

    public String getIntPlatformVersion() {
        return intPlatformVersion;
    }

    public void setIntPlatformVersion(String intPlatformVersion) {
        this.intPlatformVersion = intPlatformVersion;
    }

    public VaultMaster getIntMstPlatform() {
        return intMstPlatform;
    }

    public void setIntMstPlatform(VaultMaster intMstPlatform) {
        this.intMstPlatform = intMstPlatform;
    }

    public VaultMaster getIntMstPayloadSecClass() {
        return intMstPayloadSecClass;
    }

    public void setIntMstPayloadSecClass(VaultMaster intMstPayloadSecClass) {
        this.intMstPayloadSecClass = intMstPayloadSecClass;
    }

    public VaultMaster getIntMstOps() {
        return intMstOps;
    }

    public void setIntMstOps(VaultMaster intMstOps) {
        this.intMstOps = intMstOps;
    }

    public VaultMaster getIntMstDestProtocol() {
        return intMstDestProtocol;
    }

    public void setIntMstDestProtocol(VaultMaster intMstDestProtocol) {
        this.intMstDestProtocol = intMstDestProtocol;
    }

    public VaultMaster getIntMstDestPayloadType() {
        return intMstDestPayloadType;
    }

    public void setIntMstDestPayloadType(VaultMaster intMstDestPayloadType) {
        this.intMstDestPayloadType = intMstDestPayloadType;
    }

    public VaultMaster getIntMstDesignPattern() {
        return intMstDesignPattern;
    }

    public void setIntMstDesignPattern(VaultMaster intMstDesignPattern) {
        this.intMstDesignPattern = intMstDesignPattern;
    }

    public VaultMaster getIntMstCountry() {
        return intMstCountry;
    }

    public void setIntMstCountry(VaultMaster intMstCountry) {
        this.intMstCountry = intMstCountry;
    }

    public VaultMaster getIntMstComplexity() {
        return intMstComplexity;
    }

    public void setIntMstComplexity(VaultMaster intMstComplexity) {
        this.intMstComplexity = intMstComplexity;
    }

    public VaultMaster getIntMstBizUnit() {
        return intMstBizUnit;
    }

    public void setIntMstBizUnit(VaultMaster intMstBizUnit) {
        this.intMstBizUnit = intMstBizUnit;
    }

    public Integer getIntMsgSize() {
        return intMsgSize;
    }

    public void setIntMsgSize(Integer intMsgSize) {
        this.intMsgSize = intMsgSize;
    }

    public String getIntLob() {
        return intLob;
    }

    public void setIntLob(String intLob) {
        this.intLob = intLob;
    }

    public String getIntImplPattern() {
        return intImplPattern;
    }

    public void setIntImplPattern(String intImplPattern) {
        this.intImplPattern = intImplPattern;
    }

    public String getIntFuncSpecLoc() {
        return this.intFuncSpecLoc;
    }

    public void setIntFuncSpecLoc(String intFuncSpecLoc) {
        this.intFuncSpecLoc = intFuncSpecLoc;
    }

    public String getIntDest() {
        return intDest;
    }

    public void setIntDest(String intDest) {
        this.intDest = intDest;
    }

    public String getIntDesc() {
        return intDesc;
    }

    public void setIntDesc(String intDesc) {
        this.intDesc = intDesc;
    }

    public String getIntDependency() {
        return intDependency;
    }

    public void setIntDependency(String intDependency) {
        this.intDependency = intDependency;
    }

    public String getIntCodeRepo() {
        return this.intCodeRepo;
    }

    public void setIntCodeRepo(String intCodeRepo) {
        this.intCodeRepo = intCodeRepo;
    }

    public String getIntBizOwner() {
        if(StringUtils.isBlank(intBizOwner)){
            return null;
        }
        if(intBizOwner.matches("^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$"))
            return (String.format("<a href=\"mailto:%s\" target=\"_blank\">%s</a>", intBizOwner, intBizOwner));
        else
            return intBizOwner;
    }

    public void setIntBizOwner(String intBizOwner) {
        this.intBizOwner = intBizOwner;
    }

    public String getIntAsset() {
        return intAsset;
    }

    public void setIntAsset(String intAsset) {
        this.intAsset = intAsset;
    }

    @Override
    public String getUpdatedBy() {
        return updatedBy;
    }

    @Override
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public Date getUpdateTs() {
        return updateTs;
    }

    @Override
    public void setUpdateTs(Date updateTs) {
        this.updateTs = updateTs;
    }

    @Override
    public String getDeletedBy() {
        return deletedBy;
    }

    @Override
    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    @Override
    public Date getDeleteTs() {
        return deleteTs;
    }

    @Override
    public void setDeleteTs(Date deleteTs) {
        this.deleteTs = deleteTs;
    }

    @Override
    public String getCreatedBy() {
        return createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public Date getCreateTs() {
        return createTs;
    }

    @Override
    public void setCreateTs(Date createTs) {
        this.createTs = createTs;
    }

    public String getIntRelease() {
        return intRelease;
    }

    public void setIntRelease(String intRelease) {
        this.intRelease = intRelease;
    }





    @MetaProperty(related = {"intTechSpecLoc"})
    @Transient
    public String getTechSpecLoc() {
        if(StringUtils.isBlank(intTechSpecLoc)) {
            return null;
        }
        else if(intTechSpecLoc.matches("^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"))
            return "Technical Specification";
        else
            return intTechSpecLoc;
    }

    @MetaProperty(related = {"intCodeRepo"})
    @Transient
    public String getCodeRepo() {
        if(StringUtils.isBlank(intCodeRepo)){
            return null;
        }
        else if(intCodeRepo.matches("^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"))
            return ("Code Repository link");
        else
            return intCodeRepo;
    }
    @MetaProperty(related = {"intFuncSpecLoc"})
    @Transient
    public String getFuncSpecLoc() {
        if(StringUtils.isBlank(intFuncSpecLoc)){
            return null;
        }
        else if(intFuncSpecLoc.matches("^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"))
            return ("Functional Specification Link");
        else
            return intFuncSpecLoc;
    }
}