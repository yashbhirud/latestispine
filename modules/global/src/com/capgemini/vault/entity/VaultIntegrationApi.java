package com.capgemini.vault.entity;

import com.haulmont.chile.core.annotations.MetaProperty;
import com.haulmont.cuba.core.entity.BaseIdentityIdEntity;
import com.haulmont.cuba.core.entity.Creatable;
import com.haulmont.cuba.core.entity.Updatable;
import com.haulmont.cuba.core.entity.annotation.OnDeleteInverse;
import com.haulmont.cuba.core.global.DdlGeneration;
import com.haulmont.cuba.core.global.DeletePolicy;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.util.Date;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.CREATE_ONLY)
@Table(name = "vault_integration_api")
@Entity(name = "vault_IntegrationApi")
public class VaultIntegrationApi extends BaseIdentityIdEntity implements Updatable, Creatable {
    private static final long serialVersionUID = 6138934796662791252L;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_ts")
    private Date createTs;

    @Column(name = "created_by", length = 50)
    private String createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_ts")
    private Date updateTs;

    @Column(name = "updated_by", length = 50)
    private String updatedBy;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "vault_api_id")
    @OnDeleteInverse(DeletePolicy.CASCADE)
    private VaultApi vaultApi;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "vault_integration_id")
    @OnDeleteInverse(DeletePolicy.CASCADE)
    private VaultIntegration vaultIntegration;

    public VaultIntegration getVaultIntegration() {
        return vaultIntegration;
    }

    public void setVaultIntegration(VaultIntegration vaultIntegration) {
        this.vaultIntegration = vaultIntegration;
    }

    public VaultApi getVaultApi() {
        return vaultApi;
    }

    public void setVaultApi(VaultApi vaultApi) {
        this.vaultApi = vaultApi;
    }

    @Override
    public String getUpdatedBy() {
        return updatedBy;
    }

    @Override
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public Date getUpdateTs() {
        return updateTs;
    }

    @Override
    public void setUpdateTs(Date updateTs) {
        this.updateTs = updateTs;
    }

    @Override
    public String getCreatedBy() {
        return createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public Date getCreateTs() {
        return createTs;
    }

    @Override
    public void setCreateTs(Date createTs) {
        this.createTs = createTs;
    }



}