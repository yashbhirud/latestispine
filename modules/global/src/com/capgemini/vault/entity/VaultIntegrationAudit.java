package com.capgemini.vault.entity;

import com.haulmont.chile.core.annotations.MetaProperty;
import com.haulmont.cuba.core.entity.BaseIdentityIdEntity;
import com.haulmont.cuba.core.entity.Creatable;
import com.haulmont.cuba.core.entity.SoftDelete;
import com.haulmont.cuba.core.entity.Updatable;
import com.haulmont.cuba.core.global.DdlGeneration;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.util.Date;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.CREATE_ONLY)
@Table(name = "vault_integration_audit")
@Entity(name = "vault_IntegrationAudit")
@AttributeOverrides({
        @AttributeOverride(name = "id", column = @Column(name = "avi_id"))
})
public class VaultIntegrationAudit extends BaseIdentityIdEntity implements SoftDelete, Updatable, Creatable {
    private static final long serialVersionUID = 2500437612986661863L;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_ts", nullable = false)
    private Date createTs;

    @Column(name = "created_by", nullable = false, length = 50)
    private String createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "delete_ts")
    private Date deleteTs;

    @Column(name = "deleted_by", length = 50)
    private String deletedBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_ts")
    private Date updateTs;

    @Column(name = "updated_by", length = 50)
    private String updatedBy;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "avi_int_id")
    private VaultIntegration aviInt;

    @Column(name = "avi_int_asset_id")
    private String aviIntAsset;

    @Column(name = "avi_int_biz_owner")
    private String aviIntBizOwner;

    @Column(name = "avi_int_code_repo", length = 500)
    private String aviIntCodeRepo;

    @Column(name = "avi_int_dependency")
    private String aviIntDependency;

    @Lob
    @Column(name = "avi_int_desc")
    private String aviIntDesc;

    @Column(name = "avi_int_dest")
    private String aviIntDest;

    @Column(name = "avi_int_func_spec_loc", length = 500)
    private String aviIntFuncSpecLoc;

    @Column(name = "avi_int_impl_pattern", length = 500)
    private String aviIntImplPattern;

    @Column(name = "avi_int_lob", length = 50)
    private String aviIntLob;

    @Column(name = "avi_int_msg_size")
    private Integer aviIntMsgSize;

    @JoinColumn(name = "AVI_INT_MST_BIZ_UNIT_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster aviIntMstBizUnit;

    @JoinColumn(name = "AVI_INT_MST_COMPLEXITY_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster aviIntMstComplexity;

    @JoinColumn(name = "AVI_INT_MST_COUNTRY_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster aviIntMstCountry;

    @JoinColumn(name = "AVI_INT_MST_DESIGN_PATTERN_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster aviIntMstDesignPattern;

    @JoinColumn(name = "AVI_INT_MST_DEST_PAYLOAD_TYPE_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster aviIntMstDestPayloadType;

    @JoinColumn(name = "AVI_INT_MST_DEST_PROTOCOL_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster aviIntMstDestProtocol;

    @JoinColumn(name = "AVI_INT_MST_OPS_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster aviIntMstOps;

    @JoinColumn(name = "AVI_INT_MST_PAYLOAD_SEC_CLASS_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster aviIntMstPayloadSecClass;

    @JoinColumn(name = "AVI_INT_MST_PLATFORM_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster aviIntMstPlatform;

    @JoinColumn(name = "AVI_INT_MST_PRIORITY_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster aviIntMstPriority;

    @JoinColumn(name = "AVI_INT_MST_PROCESS_TYPE_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster aviIntMstProcessType;

    @JoinColumn(name = "AVI_INT_MST_REGION_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster aviIntMstRegion;

    @JoinColumn(name = "AVI_INT_MST_SDLC_STATUS_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster aviIntMstSdlcStatus;

    @JoinColumn(name = "AVI_INT_MST_SPECIAL_OPS_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster aviIntMstSpecialOps;

    @JoinColumn(name = "AVI_INT_MST_SRC_PAYLOAD_TYPE_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster aviIntMstSrcPayloadType;

    @JoinColumn(name = "AVI_INT_MST_SRC_PROTOCOL_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VaultMaster aviIntMstSrcProtocol;

    @Column(name = "avi_int_name", length = 500)
    private String aviIntName;

    @Column(name = "avi_int_owner")
    private String aviIntOwner;

    @Lob
    @Column(name = "avi_int_payload")
    private String aviIntPayload;

    @Column(name = "avi_int_platform_version")
    private String aviIntPlatformVersion;

    @Column(name = "avi_int_proj")
    private String aviIntProj;

    @Lob
    @Column(name = "avi_int_rem")
    private String aviIntRem;

    @Column(name = "avi_int_src_sys")
    private String aviIntSrcSys;

    @Column(name = "avi_int_tech_spec_loc", length = 500)
    private String aviIntTechSpecLoc;

    @Column(name = "avi_int_throughput")
    private Integer aviIntThroughput;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "avi_int_ups_id")
    private VaultUploadSummary aviIntUps;

    @Column(name = "avi_int_workflow_status", length = 50)
    private String aviIntWorkflowStatus;

    @Column(name = "avi_int_workstream")
    private String aviIntWorkstream;

    @Column(name = "operation_name")
    private String operationName;

    @Column(name="avi_int_developer")
    private String aviIntDeveloper;

    @Column(name = "avi_int_architect")
    private String aviIntArchitect;

    @Column(name = "avi_int_version")
    private String aviIntVersion;

    public String getAviIntDeveloper() {
        return aviIntDeveloper;
    }

    public void setAviIntDeveloper(String aviIntDeveloper) {
        this.aviIntDeveloper = aviIntDeveloper;
    }

    public String getAviIntArchitect() {
        return aviIntArchitect;
    }

    public void setAviIntArchitect(String aviIntArchitect) {
        this.aviIntArchitect = aviIntArchitect;
    }

    public String getAviIntVersion() {
        return aviIntVersion;
    }

    public void setAviIntVersion(String aviIntVersion) {
        this.aviIntVersion = aviIntVersion;
    }

    public String getAviIntRelease() {
        return aviIntRelease;
    }

    public void setAviIntRelease(String aviIntRelease) {
        this.aviIntRelease = aviIntRelease;
    }

    @Column(name = "avi_int_release")
    private String aviIntRelease;

    public void setAviIntMstSrcProtocol(VaultMaster aviIntMstSrcProtocol) {
        this.aviIntMstSrcProtocol = aviIntMstSrcProtocol;
    }

    public VaultMaster getAviIntMstSrcProtocol() {
        return aviIntMstSrcProtocol;
    }

    public void setAviIntMstSrcPayloadType(VaultMaster aviIntMstSrcPayloadType) {
        this.aviIntMstSrcPayloadType = aviIntMstSrcPayloadType;
    }

    public VaultMaster getAviIntMstSrcPayloadType() {
        return aviIntMstSrcPayloadType;
    }

    public void setAviIntMstSpecialOps(VaultMaster aviIntMstSpecialOps) {
        this.aviIntMstSpecialOps = aviIntMstSpecialOps;
    }

    public VaultMaster getAviIntMstSpecialOps() {
        return aviIntMstSpecialOps;
    }

    public void setAviIntMstSdlcStatus(VaultMaster aviIntMstSdlcStatus) {
        this.aviIntMstSdlcStatus = aviIntMstSdlcStatus;
    }

    public VaultMaster getAviIntMstSdlcStatus() {
        return aviIntMstSdlcStatus;
    }

    public void setAviIntMstRegion(VaultMaster aviIntMstRegion) {
        this.aviIntMstRegion = aviIntMstRegion;
    }

    public VaultMaster getAviIntMstRegion() {
        return aviIntMstRegion;
    }

    public void setAviIntMstProcessType(VaultMaster aviIntMstProcessType) {
        this.aviIntMstProcessType = aviIntMstProcessType;
    }

    public VaultMaster getAviIntMstProcessType() {
        return aviIntMstProcessType;
    }

    public void setAviIntMstPriority(VaultMaster aviIntMstPriority) {
        this.aviIntMstPriority = aviIntMstPriority;
    }

    public VaultMaster getAviIntMstPriority() {
        return aviIntMstPriority;
    }

    public void setAviIntMstPlatform(VaultMaster aviIntMstPlatform) {
        this.aviIntMstPlatform = aviIntMstPlatform;
    }

    public VaultMaster getAviIntMstPlatform() {
        return aviIntMstPlatform;
    }

    public void setAviIntMstPayloadSecClass(VaultMaster aviIntMstPayloadSecClass) {
        this.aviIntMstPayloadSecClass = aviIntMstPayloadSecClass;
    }

    public VaultMaster getAviIntMstPayloadSecClass() {
        return aviIntMstPayloadSecClass;
    }

    public void setAviIntMstOps(VaultMaster aviIntMstOps) {
        this.aviIntMstOps = aviIntMstOps;
    }

    public VaultMaster getAviIntMstOps() {
        return aviIntMstOps;
    }

    public void setAviIntMstDestProtocol(VaultMaster aviIntMstDestProtocol) {
        this.aviIntMstDestProtocol = aviIntMstDestProtocol;
    }

    public VaultMaster getAviIntMstDestProtocol() {
        return aviIntMstDestProtocol;
    }

    public void setAviIntMstDestPayloadType(VaultMaster aviIntMstDestPayloadType) {
        this.aviIntMstDestPayloadType = aviIntMstDestPayloadType;
    }

    public VaultMaster getAviIntMstDestPayloadType() {
        return aviIntMstDestPayloadType;
    }

    public void setAviIntMstDesignPattern(VaultMaster aviIntMstDesignPattern) {
        this.aviIntMstDesignPattern = aviIntMstDesignPattern;
    }

    public VaultMaster getAviIntMstDesignPattern() {
        return aviIntMstDesignPattern;
    }

    public void setAviIntMstCountry(VaultMaster aviIntMstCountry) {
        this.aviIntMstCountry = aviIntMstCountry;
    }

    public VaultMaster getAviIntMstCountry() {
        return aviIntMstCountry;
    }

    public void setAviIntMstComplexity(VaultMaster aviIntMstComplexity) {
        this.aviIntMstComplexity = aviIntMstComplexity;
    }

    public VaultMaster getAviIntMstComplexity() {
        return aviIntMstComplexity;
    }

    public void setAviIntMstBizUnit(VaultMaster aviIntMstBizUnit) {
        this.aviIntMstBizUnit = aviIntMstBizUnit;
    }

    public VaultMaster getAviIntMstBizUnit() {
        return aviIntMstBizUnit;
    }

    @Override
    public Boolean isDeleted() {
        return deleteTs != null;
    }

    public String getOperationName() {
        return operationName;
    }

    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }

    public String getAviIntWorkstream() {
        return aviIntWorkstream;
    }

    public void setAviIntWorkstream(String aviIntWorkstream) {
        this.aviIntWorkstream = aviIntWorkstream;
    }

    public String getAviIntWorkflowStatus() {
        return aviIntWorkflowStatus;
    }

    public void setAviIntWorkflowStatus(String aviIntWorkflowStatus) {
        this.aviIntWorkflowStatus = aviIntWorkflowStatus;
    }

    public VaultUploadSummary getAviIntUps() {
        return aviIntUps;
    }

    public void setAviIntUps(VaultUploadSummary aviIntUps) {
        this.aviIntUps = aviIntUps;
    }

    public Integer getAviIntThroughput() {
        return aviIntThroughput;
    }

    public void setAviIntThroughput(Integer aviIntThroughput) {
        this.aviIntThroughput = aviIntThroughput;
    }

    public String getAviIntTechSpecLoc() {
        return aviIntTechSpecLoc;
    }

    public void setAviIntTechSpecLoc(String aviIntTechSpecLoc) {
        this.aviIntTechSpecLoc = aviIntTechSpecLoc;
    }

    public String getAviIntSrcSys() {
        return aviIntSrcSys;
    }

    public void setAviIntSrcSys(String aviIntSrcSys) {
        this.aviIntSrcSys = aviIntSrcSys;
    }

    public String getAviIntRem() {
        return aviIntRem;
    }

    public void setAviIntRem(String aviIntRem) {
        this.aviIntRem = aviIntRem;
    }

    public String getAviIntProj() {
        return aviIntProj;
    }

    public void setAviIntProj(String aviIntProj) {
        this.aviIntProj = aviIntProj;
    }

    public String getAviIntPlatformVersion() {
        return aviIntPlatformVersion;
    }

    public void setAviIntPlatformVersion(String aviIntPlatformVersion) {
        this.aviIntPlatformVersion = aviIntPlatformVersion;
    }

    public String getAviIntPayload() {
        return aviIntPayload;
    }

    public void setAviIntPayload(String aviIntPayload) {
        this.aviIntPayload = aviIntPayload;
    }

    public String getAviIntOwner() {
        if(StringUtils.isBlank(aviIntOwner)){
            return null;
        }
        if(aviIntOwner.matches("^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$"))
            return (String.format("<a href=\"mailto:%s\" target=\"_blank\">%s</a>", aviIntOwner, aviIntOwner));
        else
            return aviIntOwner;
    }

    public void setAviIntOwner(String aviIntOwner) {
        this.aviIntOwner = aviIntOwner;
    }

    public String getAviIntName() {
        return aviIntName;
    }

    public void setAviIntName(String aviIntName) {
        this.aviIntName = aviIntName;
    }

    public Integer getAviIntMsgSize() {
        return aviIntMsgSize;
    }

    public void setAviIntMsgSize(Integer aviIntMsgSize) {
        this.aviIntMsgSize = aviIntMsgSize;
    }

    public String getAviIntLob() {
        return aviIntLob;
    }

    public void setAviIntLob(String aviIntLob) {
        this.aviIntLob = aviIntLob;
    }

    public String getAviIntImplPattern() {
        return aviIntImplPattern;
    }

    public void setAviIntImplPattern(String aviIntImplPattern) {
        this.aviIntImplPattern = aviIntImplPattern;
    }

    public String getAviIntFuncSpecLoc() {
        return aviIntFuncSpecLoc;
    }

    public void setAviIntFuncSpecLoc(String aviIntFuncSpecLoc) {
        this.aviIntFuncSpecLoc = aviIntFuncSpecLoc;
    }

    public String getAviIntDest() {
        return aviIntDest;
    }

    public void setAviIntDest(String aviIntDest) {
        this.aviIntDest = aviIntDest;
    }

    public String getAviIntDesc() {
        return aviIntDesc;
    }

    public void setAviIntDesc(String aviIntDesc) {
        this.aviIntDesc = aviIntDesc;
    }

    public String getAviIntDependency() {
        return aviIntDependency;
    }

    public void setAviIntDependency(String aviIntDependency) {
        this.aviIntDependency = aviIntDependency;
    }

    public String getAviIntCodeRepo() {
        return aviIntCodeRepo;
    }

    public void setAviIntCodeRepo(String aviIntCodeRepo) {
        this.aviIntCodeRepo = aviIntCodeRepo;
    }

    public String getAviIntBizOwner() {
        if(StringUtils.isBlank(aviIntBizOwner)){
            return null;
        }
        if(aviIntBizOwner.matches("^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$"))
            return (String.format("<a href=\"mailto:%s\" target=\"_blank\">%s</a>", aviIntBizOwner, aviIntBizOwner));
        else
            return aviIntBizOwner;
    }

    public void setAviIntBizOwner(String aviIntBizOwner) {
        this.aviIntBizOwner = aviIntBizOwner;
    }

    public String getAviIntAsset() {
        return aviIntAsset;
    }

    public void setAviIntAsset(String aviIntAsset) {
        this.aviIntAsset = aviIntAsset;
    }

    public VaultIntegration getAviInt() {
        return aviInt;
    }

    public void setAviInt(VaultIntegration aviInt) {
        this.aviInt = aviInt;
    }

    @Override
    public String getUpdatedBy() {
        return updatedBy;
    }

    @Override
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public Date getUpdateTs() {
        return updateTs;
    }

    @Override
    public void setUpdateTs(Date updateTs) {
        this.updateTs = updateTs;
    }

    @Override
    public String getDeletedBy() {
        return deletedBy;
    }

    @Override
    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    @Override
    public Date getDeleteTs() {
        return deleteTs;
    }

    @Override
    public void setDeleteTs(Date deleteTs) {
        this.deleteTs = deleteTs;
    }

    @Override
    public String getCreatedBy() {
        return createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public Date getCreateTs() {
        return createTs;
    }

    @Override
    public void setCreateTs(Date createTs) {
        this.createTs = createTs;
    }

    @MetaProperty(related = {"aviIntTechSpecLoc"})
    @Transient
    public String getIntTechSpecLoc() {
        if(StringUtils.isBlank(aviIntTechSpecLoc)) {
            return null;
        }
        else if(aviIntTechSpecLoc.matches("^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"))
            return "Technical Specification";
        else
            return aviIntTechSpecLoc;
    }

    @MetaProperty(related = {"aviIntCodeRepo"})
    @Transient
    public String getIntCodeRepo() {
        if(StringUtils.isBlank(aviIntCodeRepo)){
            return null;
        }
        else if(aviIntCodeRepo.matches("^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"))
            return "Code Repository";
        else
            return aviIntCodeRepo;
    }
    @MetaProperty(related = {"aviIntFuncSpecLoc"})
    @Transient
    public String getIntFuncSpecLoc() {
        if(StringUtils.isBlank(aviIntFuncSpecLoc)){
            return null;
        }
        else if(aviIntFuncSpecLoc.matches("^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]"))
            return "Functional Specification";
        else
            return aviIntFuncSpecLoc;
    }
}