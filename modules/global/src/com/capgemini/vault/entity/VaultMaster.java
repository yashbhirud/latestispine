package com.capgemini.vault.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseIdentityIdEntity;
import com.haulmont.cuba.core.entity.Creatable;
import com.haulmont.cuba.core.entity.SoftDelete;
import com.haulmont.cuba.core.entity.Updatable;

import javax.persistence.*;
import java.util.Date;

//@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.CREATE_ONLY)
@Table(name = "vault_master")
@Entity(name = "vault_Master")
@NamePattern("%s|mstDesc")
public class VaultMaster extends BaseIdentityIdEntity implements SoftDelete, Updatable, Creatable {
    private static final long serialVersionUID = 6352716087319441101L;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_ts")
    private Date createTs;

    @Column(name = "created_by", length = 50)
    private String createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "delete_ts")
    private Date deleteTs;

    @Column(name = "deleted_by", length = 50)
    private String deletedBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_ts")
    private Date updateTs;

    @Column(name = "updated_by", length = 50)
    private String updatedBy;

    @Column(name = "mst_code", nullable = false, length = 20)
    private String mstCode;

    @Lob
    @Column(name = "mst_desc")
    private String mstDesc;

    @Column(name = "mst_isactive", nullable = false)
    private Boolean mstIsactive = false;

    @Column(name = "mst_type", nullable = false, length = 500)
    private String mstType;

    @Override
    public Boolean isDeleted() {
        return deleteTs != null;
    }

    public String getMstType() {
        return mstType;
    }

    public void setMstType(String mstType) {
        this.mstType = mstType;
    }

    public Boolean getMstIsactive() {
        return mstIsactive;
    }

    public void setMstIsactive(Boolean mstIsactive) {
        this.mstIsactive = mstIsactive;
    }

    public String getMstDesc() {
        return mstDesc;
    }

    public void setMstDesc(String mstDesc) {
        this.mstDesc = mstDesc;
    }

    public String getMstCode() {
        return mstCode;
    }

    public void setMstCode(String mstCode) {
        this.mstCode = mstCode;
    }

    @Override
    public String getUpdatedBy() {
        return updatedBy;
    }

    @Override
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public Date getUpdateTs() {
        return updateTs;
    }

    @Override
    public void setUpdateTs(Date updateTs) {
        this.updateTs = updateTs;
    }

    @Override
    public String getDeletedBy() {
        return deletedBy;
    }

    @Override
    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    @Override
    public Date getDeleteTs() {
        return deleteTs;
    }

    @Override
    public void setDeleteTs(Date deleteTs) {
        this.deleteTs = deleteTs;
    }

    @Override
    public String getCreatedBy() {
        return createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public Date getCreateTs() {
        return createTs;
    }

    @Override
    public void setCreateTs(Date createTs) {
        this.createTs = createTs;
    }
}