package com.capgemini.vault.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseIdentityIdEntity;
import com.haulmont.cuba.core.entity.Creatable;
import com.haulmont.cuba.core.global.DdlGeneration;

import javax.persistence.*;
import java.util.Date;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.CREATE_ONLY)
@Table(name = "vault_upload_summary")
@Entity(name = "vault_UploadSummary")
@NamePattern("%s|createdBy")
public class VaultUploadSummary extends BaseIdentityIdEntity implements Creatable {
    private static final long serialVersionUID = -8658884744703589574L;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_ts", nullable = false)
    private Date createTs;

    @Column(name = "created_by", nullable = false, length = 50)
    private String createdBy;

    @Column(name = "ups_datatype", length = 500)
    private String upsDatatype;

    @Column(name = "ups_error_records")
    private Integer upsErrorRecords;

    @Column(name = "ups_filename")
    private String upsFilename;

    @Column(name = "ups_lob", length = 50)
    private String upsLob;

    @Column(name = "ups_success_records")
    private Integer upsSuccessRecords;

    public Integer getUpsSuccessRecords() {
        return upsSuccessRecords;
    }

    public void setUpsSuccessRecords(Integer upsSuccessRecords) {
        this.upsSuccessRecords = upsSuccessRecords;
    }

    public String getUpsLob() {
        return upsLob;
    }

    public void setUpsLob(String upsLob) {
        this.upsLob = upsLob;
    }

    public String getUpsFilename() {
        return upsFilename;
    }

    public void setUpsFilename(String upsFilename) {
        this.upsFilename = upsFilename;
    }

    public Integer getUpsErrorRecords() {
        return upsErrorRecords;
    }

    public void setUpsErrorRecords(Integer upsErrorRecords) {
        this.upsErrorRecords = upsErrorRecords;
    }

    public String getUpsDatatype() {
        return upsDatatype;
    }

    public void setUpsDatatype(String upsDatatype) {
        this.upsDatatype = upsDatatype;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreateTs() {
        return createTs;
    }

    public void setCreateTs(Date createTs) {
        this.createTs = createTs;
    }
}