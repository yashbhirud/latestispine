package com.capgemini.vault.service;

import com.haulmont.cuba.security.entity.Group;
import com.haulmont.cuba.security.entity.User;

public interface CheckAccessService {
    String NAME = "vault_CheckAccessService";
    public boolean isGroupAdmin(User user, Group group);
}