package com.capgemini.vault.service;

import com.haulmont.cuba.security.entity.User;

import java.util.Map;

public interface iAssetCountService {
    String NAME = "vault_AssetCountService";
    public Map<String,Integer> getTotalCount(boolean bu, User user);
}