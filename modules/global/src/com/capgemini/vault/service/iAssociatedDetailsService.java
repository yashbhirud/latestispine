package com.capgemini.vault.service;

import com.capgemini.vault.entity.VaultApi;
import com.capgemini.vault.entity.VaultIntegration;

import java.util.List;

public interface iAssociatedDetailsService {
    String NAME = "vault_AssociatedDetailsService";

    public void sendAssociatedApiForApp(List<VaultApi> apiDetails);
    public void approveAssociatedApi(List<VaultApi> apiDetails, boolean isBu);
    public void rejectAssociatedApi(List<VaultApi> apiDetails);
    public void sendAssociatedIntForApp(List<VaultIntegration> intDetails);
    public void approveAssociatedInt(List<VaultIntegration> intDetails, boolean isBu);
    public void rejectAssociatedInt(List<VaultIntegration> intDetails);
    public List<?> getAssociatedIntApi(Object object);
    public List<String> getAssetList ();



}