package com.capgemini.vault.service;

import java.io.IOException;

public interface iExportService {
    String NAME = "vault_ExportService";

    public byte[] exportVault(boolean isMain) throws IOException;
    public byte[] export();
}