package com.capgemini.vault.service;

import com.capgemini.vault.entity.VaultApi;
import com.capgemini.vault.entity.VaultIntegration;
import com.haulmont.cuba.security.entity.Group;
import com.haulmont.cuba.security.entity.User;

import java.util.List;

public interface iGetEmailService {
    String NAME = "vault_GetEmailService";

    public void sendDataApprovalPendingNotification(User usr);
    public void sendDataApprovalNotification(String userLogin, String buEmail);
    public void sendDataRejectionNotification(String userLogin, String buEmail);
    public User getEndUserEmailId(String userLogin);
    public void sendResetPasswordEmail(String login, String password);
    public List<String> getIntValue(String col,String screen);
    public List<VaultApi> getApiValue(String col, String screen);
}