package com.capgemini.vault.service;

import com.capgemini.vault.entity.VaultApi;
import com.capgemini.vault.entity.VaultIntegration;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public interface iLinkIntApiDetailsService {
    String NAME = "vault_LinkIntApiDetailsService";
    public void commitIntApiDetails(Collection<VaultApi> apiDetailsList, VaultIntegration integration);
    public void commitApiIntDetails(Collection<VaultIntegration> intDetailsList, VaultApi api);
   /* public void deleteApiIntLink (List<VaultIntegration> objList,VaultApi obj,EntityManager em);
    public void deleteIntApiLink (List<VaultApi> objList,VaultIntegration obj,EntityManager em);*/

}