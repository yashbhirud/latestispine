package com.capgemini.vault.service;


import java.util.List;

public interface iLinkInvokeService {
    String NAME = "vault_LinkInvokeService";
    public List<Object> linkinvokefunction(String url);
}