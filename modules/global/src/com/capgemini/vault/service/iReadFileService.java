package com.capgemini.vault.service;

import com.capgemini.vault.entity.VaultErrSummary;
import com.haulmont.cuba.security.entity.User;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface iReadFileService {
    String NAME = "vault_ReadFileService";
    public String readVault(File fileid, String name, User user) throws IOException;
    public void persistError(List<VaultErrSummary> errorList, String errorDesc, int rowNum, String colName, String dataType);
}