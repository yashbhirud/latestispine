package com.capgemini.vault.service;

import com.capgemini.vault.entity.VaultUploadSummary;

public interface iRevertImportService {
    String NAME = "vault_RollbackService";
    public boolean rollBack(VaultUploadSummary uplobj);
}