package com.capgemini.vault.web.screens;

import com.capgemini.vault.entity.*;
import com.capgemini.vault.service.iLinkInvokeService;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.export.ByteArrayDataProvider;
import com.haulmont.cuba.gui.export.ExportDisplay;
import com.haulmont.cuba.gui.export.ExportFormat;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.Screen;
import com.haulmont.cuba.gui.screen.Subscribe;
import com.haulmont.cuba.gui.screen.UiController;
import com.haulmont.cuba.gui.screen.UiDescriptor;
import com.haulmont.cuba.security.global.UserSession;

import javax.inject.Inject;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@UiController("vault_Audit")
@UiDescriptor("Audit.xml")
public class Audit extends Screen {
    @Inject
    private DateField<Date> intToDate;
    @Inject
    private CollectionLoader<VaultApiAudit> apiDetailsAuditsDl;
    @Inject
    private CollectionLoader<VaultIntegrationAudit> intDetailsAuditsDl;
    @Inject
    private CollectionLoader<VaultMaster> statusDl;
    @Inject
    private CollectionLoader<VaultMaster> platformDl;
    @Inject
    private CollectionLoader<VaultMaster> patternDl;
    @Inject
    private CollectionLoader<VaultMaster> buDl;
    @Inject
    private DataGrid<VaultIntegrationAudit> intDetailsAuditsTable;
    @Inject
    private DataGrid<VaultApiAudit> apiDetailsAuditsTable;
    @Inject
    private Notifications notifications;
    @Inject
    private iLinkInvokeService linkInvoke;
    @Inject
    private ExportDisplay exportDisplay;
    @Inject
    private UserSession userSession;
    @Inject
    private DateField<Date> intFromDate;
    @Inject
    private DateField<Date> apiToDate;
    @Inject
    private DateField<Date> apiFromDate;

    @Subscribe
    public void onInit(InitEvent event) {
        DataGrid.HtmlRenderer renderer1 =
                intDetailsAuditsTable.createRenderer(DataGrid.HtmlRenderer.class);
        renderer1.setNullRepresentation("</span>");
        intDetailsAuditsTable.getColumn("aviIntBizOwner").setRenderer(renderer1);


        DataGrid.ClickableTextRenderer<VaultIntegrationAudit> intDetailsesTableTechSpecLocRenderer = intDetailsAuditsTable.createRenderer(DataGrid.ClickableTextRenderer.class);
        intDetailsesTableTechSpecLocRenderer.setRendererClickListener(clickableTextRendererClickEvent -> {
            boolean checkAccess = ((ExtUser) userSession.getUser()).getAllowTechSpec();
            if (!checkAccess) {
                notifications.create().withCaption("You are not authorised user to access link").show();
            }
            else{
                List<Object> list= linkInvoke.linkinvokefunction(clickableTextRendererClickEvent.getItem().getAviIntTechSpecLoc());
                if (list == null) {
                    notifications.create().withCaption("Cannot open the link").show();
                }
                else if(list.contains("saved"))
                {
                    notifications.create().withCaption("Saved to C://git").show();
                }
                else if(!list.isEmpty()){
                    exportDisplay.show(new ByteArrayDataProvider((byte[]) list.get(0)), (String) list.get(1));
                }
            }
        });
        intDetailsAuditsTable.getColumn("intTechSpecLoc").setRenderer(intDetailsesTableTechSpecLocRenderer);

        DataGrid.ClickableTextRenderer<VaultIntegrationAudit> intDetailsesTableFuncSpecLocRenderer = intDetailsAuditsTable.createRenderer(DataGrid.ClickableTextRenderer.class);
        intDetailsesTableFuncSpecLocRenderer.setRendererClickListener(clickableTextRendererClickEvent -> {
            boolean checkAccess = ((ExtUser) userSession.getUser()).getAllowFuncSpec();
            if (!checkAccess) {
                notifications.create().withCaption("You are not authorised user to access link").show();
            }
            else{
                List<Object> list= linkInvoke.linkinvokefunction(clickableTextRendererClickEvent.getItem().getAviIntFuncSpecLoc());
                if (list == null) {
                    notifications.create().withCaption("Cannot open the link").show();
                }
                else if(list.contains("saved"))
                {
                    notifications.create().withCaption("Saved to C://git").show();
                }
                else if(!list.isEmpty()) {
                    exportDisplay.show(new ByteArrayDataProvider((byte[]) list.get(0)), (String) list.get(1));
                }
            }
        });
        intDetailsAuditsTable.getColumn("intFuncSpecLoc").setRenderer(intDetailsesTableFuncSpecLocRenderer);

        DataGrid.ClickableTextRenderer<VaultIntegrationAudit> intDetailsesTableCodeRepoLocRenderer = intDetailsAuditsTable.createRenderer(DataGrid.ClickableTextRenderer.class);
        intDetailsesTableCodeRepoLocRenderer.setRendererClickListener(clickableTextRendererClickEvent -> {
            boolean checkAccess = ((ExtUser) userSession.getUser()).getAllowCodeLoc();
            if (!checkAccess) {
                notifications.create().withCaption("You are not authorised user to access link").show();
            }
            else{
                List<Object> list= linkInvoke.linkinvokefunction(clickableTextRendererClickEvent.getItem().getAviIntCodeRepo());
                if (list == null) {
                    notifications.create().withCaption("Cannot open the link").show();
                }
                else if(list.contains("saved"))
                {
                    notifications.create().withCaption("Saved to C://git").show();
                }
                else if(!list.isEmpty()) {
                    exportDisplay.show(new ByteArrayDataProvider((byte[]) list.get(0)), (String) list.get(1));
                }
            }
        });
        intDetailsAuditsTable.getColumn("intCodeRepo").setRenderer(intDetailsesTableCodeRepoLocRenderer);

        DataGrid.HtmlRenderer renderer5 =
                intDetailsAuditsTable.createRenderer(DataGrid.HtmlRenderer.class);
        renderer5.setNullRepresentation("</span>");
        intDetailsAuditsTable.getColumn("aviIntOwner").setRenderer(renderer5);

        DataGrid.HtmlRenderer renderer6 =
                apiDetailsAuditsTable.createRenderer(DataGrid.HtmlRenderer.class);
        renderer6.setNullRepresentation("</span>");
        apiDetailsAuditsTable.getColumn("avaApiBizOwner").setRenderer(renderer6);

        DataGrid.HtmlRenderer renderer7 =
                apiDetailsAuditsTable.createRenderer(DataGrid.HtmlRenderer.class);
        renderer7.setNullRepresentation("</span>");
        apiDetailsAuditsTable.getColumn("avaApiIntOwner").setRenderer(renderer7);

    }

    @Subscribe
    public void onBeforeShow(BeforeShowEvent event) {
        apiDetailsAuditsDl.load();
        intDetailsAuditsDl.load();
        statusDl.load();
        platformDl.load();
        buDl.load();
        patternDl.load();
    }


    @Subscribe("pfLookup")
    public void onPfLookupValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            intDetailsAuditsDl.setParameter("platform", event.getValue());
        } else {
            intDetailsAuditsDl.removeParameter("platform");
        }
        intDetailsAuditsDl.load();
    }

    @Subscribe("bOwnerField")
    public void onBOwnerFieldValueChange(HasValue.ValueChangeEvent<String> event) {
        if (event.getValue() != null) {
            intDetailsAuditsDl.setParameter("owner", "(?i)%" + event.getValue() + "%");
        } else {
            intDetailsAuditsDl.removeParameter("owner");
        }
        intDetailsAuditsDl.load();

    }

    @Subscribe("initField")
    public void onInitFieldValueChange(HasValue.ValueChangeEvent<String> event) {
        if (event.getValue() != null) {
            intDetailsAuditsDl.setParameter("source", "(?i)%" + event.getValue() + "%");
        } else {
            intDetailsAuditsDl.removeParameter("source");
        }
        intDetailsAuditsDl.load();

    }



    @Subscribe("patternLookup")
    public void onPatternLookupValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            intDetailsAuditsDl.setParameter("pattern", event.getValue());
        } else {
            intDetailsAuditsDl.removeParameter("pattern");
        }
        intDetailsAuditsDl.load();

    }

    @Subscribe("statLookup")
    public void onStatLookupValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            intDetailsAuditsDl.setParameter("sdlcStatus", event.getValue());
        } else {
            intDetailsAuditsDl.removeParameter("sdlcStatus");
        }
        intDetailsAuditsDl.load();

    }

    @Subscribe("nameFilter")
    public void onNameFilterValueChange(HasValue.ValueChangeEvent<String> event) {
        if (event.getValue() != null) {
            intDetailsAuditsDl.setParameter("name", "(?i)%" + event.getValue() + "%");
        } else {
            intDetailsAuditsDl.removeParameter("name");
        }
        intDetailsAuditsDl.load();
    }

    @Subscribe("lookupFieldStatus")
    public void onLookupFieldStatusValueChange(HasValue.ValueChangeEvent<Status> event) {
        if (event.getValue() != null) {
            intDetailsAuditsDl.setParameter("status",event.getValue());
        } else {
            intDetailsAuditsDl.removeParameter("status");
        }
        intDetailsAuditsDl.load();
    }

    //Apply Filter
    @Subscribe("textFieldApi")
    public void onTextFieldApiTextChange(TextInputField.TextChangeEvent event) {
        if (event.getText() != null) {
            apiDetailsAuditsDl.setParameter("name", "(?i)%" + event.getText() + "%");
        } else {
            apiDetailsAuditsDl.removeParameter("name");
        }
        apiDetailsAuditsDl.load();
    }

    @Subscribe("pfLookupField")
    public void onPfLookupFieldValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            apiDetailsAuditsDl.setParameter("platform", event.getValue());
        } else {
            apiDetailsAuditsDl.removeParameter("platform");
        }
        apiDetailsAuditsDl.load();
    }

    @Subscribe("ownerField")
    public void onOwnerFieldValueChange(HasValue.ValueChangeEvent<String> event) {
        if (event.getValue() != null) {
            apiDetailsAuditsDl.setParameter("owner", "(?i)%" + event.getValue() + "%");
        } else {
            apiDetailsAuditsDl.removeParameter("owner");
        }
        apiDetailsAuditsDl.load();
    }

    @Subscribe("patternField")
    public void onPatternFieldValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            apiDetailsAuditsDl.setParameter("pattern",  event.getValue());
        } else {
            apiDetailsAuditsDl.removeParameter("pattern");
        }
        apiDetailsAuditsDl.load();
    }

    @Subscribe("sdlcField")
    public void onSdlcFieldValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            apiDetailsAuditsDl.setParameter("sdlc",  event.getValue());
        } else {
            apiDetailsAuditsDl.removeParameter("sdlc");
        }
        apiDetailsAuditsDl.load();
    }

    @Subscribe("lookupStatus")
    public void onLookupStatusValueChange(HasValue.ValueChangeEvent<Status> event) {
        if (event.getValue() != null) {
            apiDetailsAuditsDl.setParameter("status",event.getValue());
        } else {
            apiDetailsAuditsDl.removeParameter("status");
        }
        apiDetailsAuditsDl.load();
    }

    @Subscribe("businessUnit")
    public void onBusinessUnitValueChange1(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            intDetailsAuditsDl.setParameter("bizUnit", event.getValue());
        } else {
            intDetailsAuditsDl.removeParameter("bizUnit");
        }
        intDetailsAuditsDl.load();
    }

    @Subscribe("apiBusinessUnit")
    public void onApiBusinessUnitValueChange1(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            apiDetailsAuditsDl.setParameter("apiBizUnit", event.getValue());
        } else {
            apiDetailsAuditsDl.removeParameter("apiBizUnit");
        }
        apiDetailsAuditsDl.load();
    }
    @Subscribe("intFromDate")
    public void onIntFromDateValueChange(HasValue.ValueChangeEvent<Date> event) {
        if (event.getValue() != null) {
            if(intToDate.getValue()==null){
                Calendar cal=Calendar.getInstance();
                cal.set(Calendar.HOUR_OF_DAY, 23);
                cal.set(Calendar.MINUTE,59);
                cal.set(Calendar.SECOND,59);
                Date startDate = cal.getTime();
                intToDate.setValue(startDate);
            }
            intDetailsAuditsDl.setParameter("intUpdatedTsFrom", event.getValue());
            intDetailsAuditsDl.setParameter("intUpdatedTsTo", intToDate.getValue());
        } else {
            intDetailsAuditsDl.removeParameter("intUpdatedTsFrom");
            intDetailsAuditsDl.removeParameter("intUpdatedTsTo");
        }
        intDetailsAuditsDl.load();
    }

    @Subscribe("intToDate")
    public void onIntToDateValueChange(HasValue.ValueChangeEvent<Date> event) {
        Date date = new Date();
        if (event.getValue() != null) {
            if(intFromDate.getValue()==null){
                intFromDate.setValue(date);
            }
            intDetailsAuditsDl.setParameter("intUpdatedTsFrom", intFromDate.getValue());
            intDetailsAuditsDl.setParameter("intUpdatedTsTo", event.getValue());
        } else {
            intDetailsAuditsDl.removeParameter("intUpdatedTsFrom");
            intDetailsAuditsDl.removeParameter("intUpdatedTsTo");
        }
        intDetailsAuditsDl.load();
    }

    @Subscribe("apiFromDate")
    public void onApiFromDateValueChange(HasValue.ValueChangeEvent<Date> event) {
        if (event.getValue() != null) {
            if(apiToDate.getValue()==null){
                Calendar cal=Calendar.getInstance();
                cal.set(Calendar.HOUR_OF_DAY, 23);
                cal.set(Calendar.MINUTE,59);
                cal.set(Calendar.SECOND,59);
                Date startDate = cal.getTime();
                apiToDate.setValue(startDate);
            }
            apiDetailsAuditsDl.setParameter("apiUpdatedTsFrom", event.getValue());
            apiDetailsAuditsDl.setParameter("apiUpdatedTsTo", apiToDate.getValue());
        } else {
            apiDetailsAuditsDl.removeParameter("apiUpdatedTsFrom");
            apiDetailsAuditsDl.removeParameter("apiUpdatedTsTo");
        }
        apiDetailsAuditsDl.load();
    }

    @Subscribe("apiToDate")
    public void onApiToDateValueChange(HasValue.ValueChangeEvent<Date> event) {
        Date date = new Date();
        if (event.getValue() != null) {
            if(apiFromDate.getValue()==null){
                apiFromDate.setValue(date);
            }
            apiDetailsAuditsDl.setParameter("apiUpdatedTsTo", event.getValue());
            apiDetailsAuditsDl.setParameter("apiUpdatedTsFrom", apiFromDate.getValue());
        } else {
            apiDetailsAuditsDl.removeParameter("apiUpdatedTsFrom");
            apiDetailsAuditsDl.removeParameter("apiUpdatedTsTo");
        }
        apiDetailsAuditsDl.load();
    }
}