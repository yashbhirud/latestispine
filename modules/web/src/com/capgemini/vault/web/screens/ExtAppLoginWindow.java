package com.capgemini.vault.web.screens;

import com.haulmont.cuba.gui.Screens;
import com.haulmont.cuba.gui.components.Button;
import com.haulmont.cuba.gui.components.mainwindow.AppWorkArea;
import com.haulmont.cuba.gui.screen.CloseAction;
import com.haulmont.cuba.gui.screen.OpenMode;
import com.haulmont.cuba.gui.screen.Subscribe;
import com.haulmont.cuba.web.app.loginwindow.AppLoginWindow;
import com.haulmont.cuba.gui.components.Window;

import com.haulmont.cuba.gui.UiComponents;
import javax.annotation.Nullable;
import javax.inject.Inject;

public class ExtAppLoginWindow extends AppLoginWindow implements Window.HasWorkArea{

    @Inject
    private Screens screens;

    @Inject
    private UiComponents uiComponents;
    /**
     * "Restore password" link click handler.
     */
    @Subscribe("restorePasswordBtn")
    public void onRestorePasswordBtnClick(Button.ClickEvent event) {
        // Create "RestorePassword" screen with dialog mode
        RestorePasswordScreen restorePasswordScreen = screens.create(RestorePasswordScreen.class, OpenMode.DIALOG);

        // Add a listener to be notified when the "Restore password" screen is closed with COMMIT_ACTION_ID
        restorePasswordScreen.addAfterCloseListener(afterCloseEvent -> {
            CloseAction closeAction = afterCloseEvent.getCloseAction();
            if (closeAction == WINDOW_COMMIT_AND_CLOSE_ACTION) {
                loginField.setValue(restorePasswordScreen.getLogin());
                // clear password field
                passwordField.setValue(null);
                // Set focus in login field
                passwordField.focus();
            }
        });

        // Show "RestorePassword" screen
        restorePasswordScreen.show();
    }

    @Nullable
    @Override
    public AppWorkArea getWorkArea() {
        return uiComponents.create(AppWorkArea.NAME);
    }
}