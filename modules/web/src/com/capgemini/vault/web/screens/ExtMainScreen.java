package com.capgemini.vault.web.screens;

import com.capgemini.vault.entity.Constants;
import com.capgemini.vault.service.iExportService;
import com.capgemini.vault.web.screens.vaultmaster.VaultMasterBrowse;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.Screens;
import com.haulmont.cuba.gui.components.Button;
import com.haulmont.cuba.gui.components.mainwindow.AppWorkArea;
import com.haulmont.cuba.gui.components.mainwindow.SideMenu;
import com.haulmont.cuba.gui.components.mainwindow.UserIndicator;
import com.haulmont.cuba.gui.export.ByteArrayDataProvider;
import com.haulmont.cuba.gui.export.ExportDisplay;
import com.haulmont.cuba.gui.export.ExportFormat;
import com.haulmont.cuba.gui.icons.CubaIcon;
import com.haulmont.cuba.gui.screen.Subscribe;
import com.haulmont.cuba.gui.screen.UiController;
import com.haulmont.cuba.gui.screen.UiDescriptor;
import com.haulmont.cuba.security.global.UserSession;
import com.haulmont.cuba.web.DefaultApp;
import com.haulmont.cuba.web.app.main.MainScreen;

import javax.inject.Inject;


@UiController("extMainScreen")
@UiDescriptor("ext-main-screen.xml")
public class ExtMainScreen extends MainScreen {

    @Inject
    private SideMenu sideMenu;

    @Inject
    private DefaultApp defaultApp;

    @Inject
    private UserIndicator userIndicator;
    @Inject
    private UserSession userSession;

    @Inject
    private Button collapseMenuButton;
    @Inject
    private AppWorkArea workArea;
    @Inject
    private Messages messages;
    @Inject
    private Screens screens;
    @Inject
    private iExportService exportService;
    @Inject
    private Notifications notifications;
    @Inject
    private ExportDisplay exportDisplay;
    @Inject
    private DataManager dataManager;

    @Subscribe
    public void onInit(InitEvent event) {


        SideMenu.MenuItem helpItem = sideMenu.createMenuItem("VM-iSpine");
        helpItem.setCaption("User Manual");
        helpItem.setStyleName("debug-icon");
        helpItem.setCommand(selectedItem -> screens.create(UserManual.class).show());

        SideMenu.MenuItem helpItem1 = sideMenu.createMenuItem("VM-iSpine");
        helpItem1.setCaption("Download Template");
        helpItem1.setStyleName("debug-icon");
        helpItem1.setCommand(selectedItem-> export());
        //sideMenu.addMenuItem(helpItem);

        userIndicator.setUserNameFormatter(value -> "Welcome, "+value.getFirstName());
        if(!userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_user")) && !userSession.getRoles().contains(messages.getMessage(getClass(),"role.read_only"))){
            sideMenu.getMenuItem("administration").setIcon(CubaIcon.GEAR.source());
        }
        if(!userSession.getRoles().contains(messages.getMessage(getClass(),"role.read_only"))) {
            sideMenu.getMenuItem("help").setIcon(CubaIcon.QUESTION_CIRCLE.source());
            sideMenu.getMenuItem("help").addChildItem(helpItem);
            sideMenu.getMenuItem("help").addChildItem(helpItem1);
        }
        if(!userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_user"))){
            SideMenu.MenuItem masterItem = sideMenu.createMenuItem("UM-Mdm");
            masterItem.setCaption("Masters");
            masterItem.setIcon("font-icon:LIST_ALT");
            masterItem.setStyleName("history-icon");
            masterItem.setCommand(selectedItem -> screens.create(VaultMasterBrowse.class).show());
            SideMenu.MenuItem vaultItem = sideMenu.getMenuItem("application-vault");
            int index = vaultItem.getMenu().getMenuItems().indexOf(vaultItem);
            sideMenu.addMenuItem(masterItem,index+1);
        }

        SideMenu.MenuItem logoutItem = sideMenu.createMenuItem("logout-em");
        logoutItem.setCaption("Logout");
        logoutItem.setIcon("app/images/exit.png");
        logoutItem.setStyleName("security-icon");
        logoutItem.setCommand(selectedItem -> defaultApp.logout());
        sideMenu.addMenuItem(logoutItem);

    }

        private void export() {
            byte[] b = exportService.export();
            if(b==null){
                notifications.create().withCaption("Error in downloading template. Please try again").show();
            }
            else {
                try {
                    exportDisplay.show(new ByteArrayDataProvider(b), "iSpine_Vault.xlsx", ExportFormat.XLSX);
                } catch (Exception e) {
                    notifications.create().withCaption("Error in downloading template. Please try again").show();
                }
            }
        }


}