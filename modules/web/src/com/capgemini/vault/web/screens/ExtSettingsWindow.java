package com.capgemini.vault.web.screens;

import com.haulmont.cuba.web.app.ui.core.settings.SettingsWindow;

import java.util.Map;

public class ExtSettingsWindow extends SettingsWindow {

    @Override
    public void init(Map<String, Object> params) {
        appThemeField.setEnabled(false);
        appLangField.setEnabled(false);
        timeZoneLookup.setEnabled(false);
        timeZoneAutoField.setEnabled(false);
        resetScreenSettingsBtn.setCaption("Set to Default");
    }
}