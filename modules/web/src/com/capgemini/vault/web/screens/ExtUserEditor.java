package com.capgemini.vault.web.screens;

import com.haulmont.charts.gui.amcharts.model.Export;
import com.haulmont.cuba.gui.app.security.user.edit.UserEditor;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.model.DataContext;
import com.haulmont.cuba.gui.screen.Screen;
import com.haulmont.cuba.gui.screen.Subscribe;
import com.haulmont.cuba.security.entity.User;
import com.haulmont.cuba.security.entity.UserRole;

import javax.inject.Inject;
import javax.inject.Named;

public class ExtUserEditor extends UserEditor {

    @Inject
    private Button rolesTableEditBtn;
    @Inject
    private VBoxLayout substPanel;


    @Subscribe
    public void onInit(InitEvent event) {
        if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_administrator"))){
            groupField.setValue(userSession.getUser().getGroup());
            groupField.setEditable(false);
            rolesTableEditBtn.setEnabled(false);
        }
        substPanel.setVisible(false);
    }

    @Override
    public void commitAndClose() {
        if (userSession.getRoles().contains(messages.getMessage(getClass(), "role.bu_administrator"))){
            User user = userDs.getItem();
            user.setGroup(userSession.getUser().getGroup());
        }
        super.commitAndClose();
    }
}