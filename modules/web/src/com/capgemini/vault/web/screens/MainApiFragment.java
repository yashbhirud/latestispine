package com.capgemini.vault.web.screens;

import com.capgemini.vault.entity.Status;
import com.capgemini.vault.entity.VaultApi;
import com.capgemini.vault.entity.VaultMaster;
import com.capgemini.vault.service.iExportService;
import com.capgemini.vault.web.screens.vaultapi.VaultApiEdit;
import com.capgemini.vault.web.utils.DataGridFilter;
import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.core.global.UserSessionSource;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.ScreenBuilders;
import com.haulmont.cuba.gui.WindowManager;
import com.haulmont.cuba.gui.actions.list.CreateAction;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.components.filter.FilterDelegate;
import com.haulmont.cuba.gui.export.ByteArrayDataProvider;
import com.haulmont.cuba.gui.export.ExportDisplay;
import com.haulmont.cuba.gui.export.ExportFormat;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.relatedentities.RelatedEntitiesAPI;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.security.global.UserSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

@UiController("vault_MainApiFragment")
@UiDescriptor("MainApi-fragment.xml")
public class MainApiFragment extends ScreenFragment {

    @Inject
    private DataGrid<VaultApi> apiDetailsesTable;

    @Inject
    private iExportService exportService;

    @Inject
    private RelatedEntitiesAPI relatedEntitiesAPI;
    @Inject
    private CollectionLoader<VaultApi> apiDetailsesDl;
    @Inject
    private ScreenBuilders screenBuilders;

    UserSessionSource uss = AppBeans.get(UserSessionSource.class);
    @Inject
    private UserSession userSession;

    @Inject
    private Button apiDetailsesTableRemoveBtn;
    @Inject
    private Button apiDetailsesTableEditBtn;

    @Inject
    private CollectionLoader<VaultMaster> statusDl;
    @Inject
    private CollectionLoader<VaultMaster> platformDl;
    @Inject
    private CollectionLoader<VaultMaster> patternDl;
    @Inject
    private CollectionLoader<VaultMaster> buDl;
    @Inject
    private Notifications notifications;
    @Inject
    private Button associateBtn;
    @Inject
    private ExportDisplay exportDisplay;
    @Inject
    private CollectionLoader<VaultMaster> vaultMastersDl;
    @Inject
    private Filter ftsFilter;
    @Inject
    private Filter filter;
    @Inject
    private Messages messages;

    @Named("apiDetailsesTable.create")
    private CreateAction<VaultApi> apiDetailsesTableCreate;

    private Logger log = LoggerFactory.getLogger(MainIntFragment.class);

    @Subscribe
    public void onInit(InitEvent event) {
        statusDl.load();
        platformDl.load();
        buDl.load();
        patternDl.load();

        ftsFilter.switchFilterMode(FilterDelegate.FilterMode.FTS_MODE);
        ftsFilter.setModeSwitchVisible(false);
        ftsFilter.setCollapsable(false);
        ftsFilter.setCaption(null);
        filter.setModeSwitchVisible(false);

        if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_user"))){
            apiDetailsesTableCreate.setVisible(false);
        }

        DataGrid.HtmlRenderer renderer1 =
                apiDetailsesTable.createRenderer(DataGrid.HtmlRenderer.class);
        renderer1.setNullRepresentation("</span>");
        Objects.requireNonNull(apiDetailsesTable.getColumn("apiBizOwner")).setRenderer(renderer1);

        DataGrid.HtmlRenderer renderer2 =
                apiDetailsesTable.createRenderer(DataGrid.HtmlRenderer.class);
        renderer2.setNullRepresentation("</span>");
        Objects.requireNonNull(apiDetailsesTable.getColumn("apiIntOwner")).setRenderer(renderer2);
        setReadOnlyModeAccess(userSession);

    }

    @Subscribe(target = Target.PARENT_CONTROLLER)
    private void onBeforeShowHost(Screen.BeforeShowEvent event) {
        apiDetailsesDl.load();
    }

    public void onRelatedIntBtnClick() {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("apiDetails",apiDetailsesTable.getSelected());
        paramsMap.put("screen",messages.getMessage(getClass(),"screen.main"));

        RelatedEntitiesAPI.RelatedScreenDescriptor relatedScreenDescriptor = new RelatedEntitiesAPI.RelatedScreenDescriptor();

        relatedScreenDescriptor.setOpenType(WindowManager.OpenType.NEW_TAB);
        relatedScreenDescriptor.setScreenId("vault_ApiIntegration.browse");
        relatedScreenDescriptor.setScreenParams(paramsMap);

        relatedEntitiesAPI.openRelatedScreen(apiDetailsesTable.getSelected(),
                VaultApi.class, "intApiDetails",
                relatedScreenDescriptor);
    }
    @Subscribe("filterCheck")
    public void onFilterCheckValueChange(HasValue.ValueChangeEvent<Boolean> event) {
        DataGridFilter myFilter= new DataGridFilter(apiDetailsesTable);
        if(Boolean.TRUE.equals(event.getValue())){

            myFilter.addGenericFilter(apiDetailsesDl,vaultMastersDl,"api","main");
        }else{
            myFilter.removeFilterRow(apiDetailsesDl);
        }

    }

    @Subscribe("apiDetailsesTable")
    public void onApiDetailsesTableSelection(DataGrid.SelectionEvent<VaultApi> event) {
        VaultApi apiDetails = apiDetailsesTable.getSingleSelected();
        if(apiDetails != null){
            String lob = apiDetailsesTable.getSingleSelected().getApiLob();
            if(lob.equals(userSession.getUser().getGroup().getName())){
                apiDetailsesTable.setEditorEnabled(true);
                apiDetailsesTableEditBtn.setEnabled(true);
                associateBtn.setEnabled(true);
            }else{
                apiDetailsesTable.setEditorEnabled(false);
                apiDetailsesTableEditBtn.setEnabled(false);
                apiDetailsesTableRemoveBtn.setEnabled(false);
                associateBtn.setEnabled(false);
            }
        }

    }

    @Subscribe("apiDetailsesTable.create")
    public void onApiDetailsesTableCreate(Action.ActionPerformedEvent event) {
        Map<String, Object> params = new HashMap<>();
        params.put("screen",messages.getMessage(getClass(),"screen.main"));
        params.put("editable",true);
        screenBuilders.editor(apiDetailsesTable)
                .newEntity()
                .withInitializer(apiDetail -> {          // lambda to initialize new instance
                    apiDetail.setApiWorkflowStatus(Status.APPROVED);
                    apiDetail.setApiLob(uss.getUserSession().getUser().getGroup().getName());
                })
                .withScreenClass(VaultApiEdit.class)    // specific editor screen
                .withLaunchMode(OpenMode.THIS_TAB)        // open as modal dialog
                .withOptions(new MapScreenOptions(params))
                .build()
                .show();
    }

    @Subscribe("apiDetailsesTable.edit")
    public void onApiDetailsesTableEdit(Action.ActionPerformedEvent event) {
        Map<String, Object> params = new HashMap<>();
        params.put("screen",messages.getMessage(getClass(),"screen.main"));
        params.put("editable",false);
        screenBuilders.editor(apiDetailsesTable)
                .withScreenClass(VaultApiEdit.class)    // specific editor screen
                .withLaunchMode(OpenMode.THIS_TAB)        // open as modal dialog
                .withOptions(new MapScreenOptions(params))
                .build()
                .show();
    }

    @Subscribe("textFieldApi")
    public void onTextFieldApiTextChange(TextInputField.TextChangeEvent event) {
        if (event.getText() != null) {
            apiDetailsesDl.setParameter("apiName", "(?i)%" + event.getText() + "%");
        } else {
            apiDetailsesDl.removeParameter("apiName");
        }
        apiDetailsesDl.load();
    }

    @Subscribe("pfLookupField")
    public void onPfLookupFieldValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            apiDetailsesDl.setParameter("apiMstPlatform", event.getValue());
        } else {
            apiDetailsesDl.removeParameter("apiMstPlatform");
        }
        apiDetailsesDl.load();
    }

    @Subscribe("ownerField")
    public void onOwnerFieldValueChange(HasValue.ValueChangeEvent<String> event) {
        if (event.getValue() != null) {
            apiDetailsesDl.setParameter("apiBizOwner", "(?i)%" + event.getValue() + "%");
        } else {
            apiDetailsesDl.removeParameter("apiBizOwner");
        }
        apiDetailsesDl.load();
    }

    @Subscribe("patternField")
    public void onPatternFieldValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            apiDetailsesDl.setParameter("apiMstDesignPattern",  event.getValue());
        } else {
            apiDetailsesDl.removeParameter("apiMstDesignPattern");
        }
        apiDetailsesDl.load();
    }

    @Subscribe("sdlcField")
    public void onSdlcFieldValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            apiDetailsesDl.setParameter("apiMstSdlcStatus",  event.getValue());
        } else {
            apiDetailsesDl.removeParameter("apiMstSdlcStatus");
        }
        apiDetailsesDl.load();
    }

    @Subscribe("lookupFieldStatus")
    public void onLookupFieldStatusValueChange(HasValue.ValueChangeEvent<Status> event) {
        if (event.getValue() != null) {
            apiDetailsesDl.setParameter("apiWorkflowStatus",event.getValue());
        } else {
            apiDetailsesDl.removeParameter("apiWorkflowStatus");
        }
        apiDetailsesDl.load();
    }


    public void onAssociateBtnClick() {
        Set<VaultApi> apiDetailsList = apiDetailsesTable.getSelected();
        if(apiDetailsList.isEmpty()){
            notifications.create(Notifications.NotificationType.WARNING).withCaption("Please select API").show();
            return;
        } else if(apiDetailsList.size()>1){
            notifications.create(Notifications.NotificationType.WARNING).withCaption("Please select one API at a time").show();
            return;
        }

        Map<String, Object> params = new HashMap<>();
        params.put("api", apiDetailsList.iterator().next());
        params.put("screen", messages.getMessage(getClass(),"screen.main"));

        Screen screen = screenBuilders.screen(this)
                .withScreenId("vault_ApiIntegration.edit")
                .withLaunchMode(OpenMode.DIALOG)
                .withOptions(new MapScreenOptions(params))
                .build()
                .show();
        screen.addAfterCloseListener(afterCloseEvent -> apiDetailsesDl.load());
    }

    public void onTableExcel() {
        try {
            exportDisplay.show(new ByteArrayDataProvider(exportService.exportVault(true)),"iSpine_Vault.xlsx", ExportFormat.XLSX);
        } catch (IOException e) {
            log.error("There was an error exporting file : "+e.getMessage());
        }
    }

    public void setReadOnlyModeAccess(UserSession userSession) {
        if (userSession.getRoles().contains(messages.getMessage(getClass(), "role.read_only"))) {
            associateBtn.setEnabled(false);
        }
    }


}