package com.capgemini.vault.web.screens;

import com.capgemini.vault.entity.ExtUser;
import com.capgemini.vault.entity.Status;
import com.capgemini.vault.entity.VaultIntegration;
import com.capgemini.vault.entity.VaultMaster;
import com.capgemini.vault.service.iExportService;
import com.capgemini.vault.service.iLinkInvokeService;
import com.capgemini.vault.web.screens.vaultintegration.VaultIntegrationEdit;
import com.capgemini.vault.web.utils.DataGridFilter;
import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.core.global.UserSessionSource;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.ScreenBuilders;
import com.haulmont.cuba.gui.WindowManager;
import com.haulmont.cuba.gui.actions.list.CreateAction;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.components.filter.FilterDelegate;
import com.haulmont.cuba.gui.export.ByteArrayDataProvider;
import com.haulmont.cuba.gui.export.ExportDisplay;
import com.haulmont.cuba.gui.export.ExportFormat;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.relatedentities.RelatedEntitiesAPI;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.security.global.UserSession;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.*;

@UiController("vault_MainIntFragment")
@UiDescriptor("MainInt-fragment.xml")
public class MainIntFragment extends ScreenFragment {
    @Inject
    private DataGrid<VaultIntegration> intDetailsesTable;

    @Inject
    private RelatedEntitiesAPI relatedEntitiesAPI;

    @Inject
    private CollectionLoader<VaultIntegration> intDetailsesDl;
    @Inject
    private ScreenBuilders screenBuilders;
    @Inject
    private UserSession userSession;
    @Inject
    private Button intDetailsesTableEditBtn;
    @Inject
    private Button intDetailsesTableRemoveBtn;
    @Inject
    private CollectionLoader<VaultMaster> platformDl;
    @Inject
    private CollectionLoader<VaultMaster> statusDl;
    @Inject
    private CollectionLoader<VaultMaster> patternDl;
    @Inject
    private CollectionLoader<VaultMaster> buDl;
    @Inject
    private Notifications notifications;
    @Inject
    private Button associateBtn;
    @Inject
    private CollectionLoader<VaultMaster> vaultMastersDl;
    @Inject
    private Filter ftsFilter;
    @Inject
    private ExportDisplay exportDisplay;
    @Inject
    private iExportService exportService;
    @Inject
    private Messages messages;
    @Inject
    private iLinkInvokeService linkInvoke;

    @Named("intDetailsesTable.create")
    private CreateAction<VaultIntegration> intDetailsesTableCreate;

    @Subscribe(target = Target.PARENT_CONTROLLER)
    private void onBeforeShowHost(Screen.BeforeShowEvent event) {
        intDetailsesDl.load();
    }

    @Inject
    private Filter filter;

    UserSessionSource uss = AppBeans.get(UserSessionSource.class);

    @Subscribe
    public void onInit(InitEvent event) {
        /*filter.setCollapsable(false);
        filter.setCaption(null);*/
        platformDl.load();
        statusDl.load();
        buDl.load();
        patternDl.load();

        ftsFilter.switchFilterMode(FilterDelegate.FilterMode.FTS_MODE);
        ftsFilter.setModeSwitchVisible(false);
        ftsFilter.setCollapsable(false);
        ftsFilter.setCaption(null);
        filter.setModeSwitchVisible(false);


        if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_user"))){
            intDetailsesTableCreate.setVisible(false);
        }

        DataGrid.HtmlRenderer renderer1 =
                intDetailsesTable.createRenderer(DataGrid.HtmlRenderer.class);
        renderer1.setNullRepresentation("</span>");
        Objects.requireNonNull(intDetailsesTable.getColumn("intBizOwner")).setRenderer(renderer1);

        DataGrid.HtmlRenderer renderer5 =
                intDetailsesTable.createRenderer(DataGrid.HtmlRenderer.class);
        renderer5.setNullRepresentation("</span>");
        Objects.requireNonNull(intDetailsesTable.getColumn("intOwner")).setRenderer(renderer5);


        DataGrid.ClickableTextRenderer<VaultIntegration> intDetailsesTableTechSpecLocRenderer = intDetailsesTable.createRenderer(DataGrid.ClickableTextRenderer.class);
        intDetailsesTableTechSpecLocRenderer.setRendererClickListener(clickableTextRendererClickEvent -> {
            boolean checkAccess = ((ExtUser) userSession.getUser()).getAllowTechSpec();
            if (!checkAccess) {
                notifications.create().withCaption("You are not authorised user to access link").show();
            }
            else{
                List<Object> list= linkInvoke.linkinvokefunction(clickableTextRendererClickEvent.getItem().getIntTechSpecLoc());
                if (list == null) {
                    notifications.create().withCaption("Cannot open the link").show();
                }
                else if(list.contains("saved"))
                {
                    notifications.create().withCaption("Saved to C://git").show();
                }
                else if(!list.isEmpty()){
                    exportDisplay.show(new ByteArrayDataProvider((byte[]) list.get(0)), (String) list.get(1));
                }
            }
        });
        intDetailsesTable.getColumn("techSpecLoc").setRenderer(intDetailsesTableTechSpecLocRenderer);

        DataGrid.ClickableTextRenderer<VaultIntegration> intDetailsesTableFuncSpecLocRenderer = intDetailsesTable.createRenderer(DataGrid.ClickableTextRenderer.class);
        intDetailsesTableFuncSpecLocRenderer.setRendererClickListener(clickableTextRendererClickEvent -> {
            boolean checkAccess = ((ExtUser) userSession.getUser()).getAllowFuncSpec();
            if (!checkAccess) {
                notifications.create().withCaption("You are not authorised user to access link").show();
            }
            else{
                List<Object> list= linkInvoke.linkinvokefunction(clickableTextRendererClickEvent.getItem().getIntFuncSpecLoc());
                if (list == null) {
                    notifications.create().withCaption("Cannot open the link").show();
                }
                else if(list.contains("saved"))
                {
                    notifications.create().withCaption("Saved to C://git").show();
                }
                else if(!list.isEmpty()){
                    exportDisplay.show(new ByteArrayDataProvider((byte[]) list.get(0)), (String) list.get(1));
                }
            }
        });
        intDetailsesTable.getColumn("funcSpecLoc").setRenderer(intDetailsesTableFuncSpecLocRenderer);

        DataGrid.ClickableTextRenderer<VaultIntegration> intDetailsesTableCodeRepoLocRenderer = intDetailsesTable.createRenderer(DataGrid.ClickableTextRenderer.class);
        intDetailsesTableCodeRepoLocRenderer.setRendererClickListener(clickableTextRendererClickEvent -> {
            boolean checkAccess = ((ExtUser) userSession.getUser()).getAllowCodeLoc();
            if (!checkAccess) {
                notifications.create().withCaption("You are not authorised user to access link").show();
            }
            else{
                List<Object> list= linkInvoke.linkinvokefunction(clickableTextRendererClickEvent.getItem().getIntCodeRepo());
                if (list == null) {
                    notifications.create().withCaption("Cannot open the link").show();
                }
                else if(list.contains("saved"))
                {
                    notifications.create().withCaption("Saved to C://git").show();
                }
                else if(!list.isEmpty()){
                    exportDisplay.show(new ByteArrayDataProvider((byte[]) list.get(0)), (String) list.get(1));
                }
            }
        });
        intDetailsesTable.getColumn("codeRepo").setRenderer(intDetailsesTableCodeRepoLocRenderer);



        setReadOnlyModeAccess(userSession);
    }

    @Subscribe("intDetailsesTable.create")
    public void onIntDetailsesTableCreate(Action.ActionPerformedEvent event) {
        Map<String, Object> params = new HashMap<>();
        params.put("screen",messages.getMessage(getClass(),"screen.main"));
        params.put("editable",true);
        screenBuilders.editor(intDetailsesTable)
                .newEntity()
                .withInitializer(intDetail -> {          // lambda to initialize new instance
                    intDetail.setIntWorkflowStatus(Status.APPROVED);
                    intDetail.setIntLob(uss.getUserSession().getUser().getGroup().getName());
                })
                .withScreenClass(VaultIntegrationEdit.class)    // specific editor screen
                .withLaunchMode(OpenMode.THIS_TAB)        // open as modal dialog
                .withOptions(new MapScreenOptions(params))
                .build()
                .show();
    }

    @Subscribe("intDetailsesTable.edit")
    public void onIntDetailsesTableEdit(Action.ActionPerformedEvent event) {
        Map<String, Object> params = new HashMap<>();
        params.put("screen",messages.getMessage(getClass(),"screen.main"));
        params.put("editable",false);
        screenBuilders.editor(intDetailsesTable)
                .withScreenClass(VaultIntegrationEdit.class)    // specific editor screen
                .withLaunchMode(OpenMode.THIS_TAB)        // open as modal dialog
                .withOptions(new MapScreenOptions(params))
                .build()
                .show();
    }

    @Subscribe("filterCheck")
    public void onFilterCheckValueChange(HasValue.ValueChangeEvent<Boolean> event) {
        DataGridFilter filter= new DataGridFilter(intDetailsesTable);
        if(Boolean.TRUE.equals(event.getValue())){
            filter.addGenericFilter(intDetailsesDl,vaultMastersDl,"int","main");
        }else{
            filter.removeFilterRow(intDetailsesDl);
        }
    }

    @Subscribe("intDetailsesTable")
    public void onIntDetailsesTableSelection(DataGrid.SelectionEvent<VaultIntegration> event) {
        VaultIntegration intDetails = intDetailsesTable.getSingleSelected();
        if(intDetails != null){
            String lob = intDetailsesTable.getSingleSelected().getIntLob();
            if(lob.equals(userSession.getUser().getGroup().getName())){
                intDetailsesTable.setEditorEnabled(true);
                intDetailsesTableEditBtn.setEnabled(true);
                associateBtn.setEnabled(true);
            }else{
                intDetailsesTable.setEditorEnabled(false);
                intDetailsesTableEditBtn.setEnabled(false);
                intDetailsesTableRemoveBtn.setEnabled(false);
                associateBtn.setEnabled(false);
            }
        }
    }

    public void onRelatedApiButtonClick() {

        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("intDetails",intDetailsesTable.getSelected());
        paramsMap.put("screen",messages.getMessage(getClass(),"screen.main"));
        RelatedEntitiesAPI.RelatedScreenDescriptor relatedScreenDescriptor = new RelatedEntitiesAPI.RelatedScreenDescriptor();


        relatedScreenDescriptor.setOpenType(WindowManager.OpenType.NEW_TAB);
        relatedScreenDescriptor.setScreenId("vault_IntegrationApi.browse");
        relatedScreenDescriptor.setScreenParams(paramsMap);

        relatedEntitiesAPI.openRelatedScreen(intDetailsesTable.getSelected(),
                VaultIntegration.class, "intApiDetails", relatedScreenDescriptor);
    }

    @Subscribe("nameFilter")
    public void onNameFilterValueChange(HasValue.ValueChangeEvent<String> event) {
        if (event.getValue() != null) {
            intDetailsesDl.setParameter("intName", "(?i)%" + event.getValue() + "%");
        } else {
            intDetailsesDl.removeParameter("intName");
        }
        intDetailsesDl.load();
    }

    @Subscribe("lookupFieldStatus")
    public void onLookupFieldStatusValueChange(HasValue.ValueChangeEvent<Status> event) {
        if (event.getValue() != null) {
            intDetailsesDl.setParameter("intWorkflowStatus",event.getValue());
        } else {
            intDetailsesDl.removeParameter("intWorkflowStatus");
        }
        intDetailsesDl.load();
    }

    @Subscribe("pfLookup")
    public void onPfLookupValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            intDetailsesDl.setParameter("intMstPlatform", event.getValue());
        } else {
            intDetailsesDl.removeParameter("intMstPlatform");
        }
        intDetailsesDl.load();
    }

    @Subscribe("bOwnerField")
    public void onBOwnerFieldValueChange(HasValue.ValueChangeEvent<String> event) {
        if (event.getValue() != null) {
            intDetailsesDl.setParameter("intBizOwner", "(?i)%" + event.getValue() + "%");
        } else {
            intDetailsesDl.removeParameter("intBizOwner");
        }
        intDetailsesDl.load();

    }

    @Subscribe("initField")
    public void onInitFieldValueChange(HasValue.ValueChangeEvent<String> event) {
        if (event.getValue() != null) {
            intDetailsesDl.setParameter("intSrcSys", "(?i)%" + event.getValue() + "%");
        } else {
            intDetailsesDl.removeParameter("intSrcSys");
        }
        intDetailsesDl.load();

    }

    @Subscribe("patternLookup")
    public void onPatternLookupValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            intDetailsesDl.setParameter("intMstDesignPattern", event.getValue());
        } else {
            intDetailsesDl.removeParameter("intMstDesignPattern");
        }
        intDetailsesDl.load();

    }

    @Subscribe("statLookup")
    public void onStatLookupValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            intDetailsesDl.setParameter("intMstSdlcStatus", event.getValue());
        } else {
            intDetailsesDl.removeParameter("intMstSdlcStatus");
        }
        intDetailsesDl.load();

    }

    public void onAssociateBtnClick() {

        Set<VaultIntegration> intDetailsList = intDetailsesTable.getSelected();
        if(intDetailsList.isEmpty()){
            notifications.create(Notifications.NotificationType.WARNING).withCaption("Please select Integration").show();
            return;
        } else if(intDetailsList.size()>1){
            notifications.create(Notifications.NotificationType.WARNING).withCaption("Please select one Integration at a time").show();
            return;
        }

        Map<String, Object> params = new HashMap<>();
        params.put("integration", intDetailsList.iterator().next());
        params.put("screen", messages.getMessage(getClass(),"screen.main"));

        Screen screen = screenBuilders.screen(this)
                .withScreenId("vault_IntegrationApi.edit")
                .withLaunchMode(OpenMode.DIALOG)
                .withOptions(new MapScreenOptions(params))
                .build()
                .show();
        screen.addAfterCloseListener(afterCloseEvent -> intDetailsesDl.load());
    }

    public void onTableExcel() {
        try {
            exportDisplay.show(new ByteArrayDataProvider(exportService.exportVault(true)),"iSpine_Vault.xlsx", ExportFormat.XLSX);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void setReadOnlyModeAccess(UserSession userSession) {
        if (userSession.getRoles().contains(messages.getMessage(getClass(), "role.read_only"))) {
            associateBtn.setEnabled(false);
        }
    }
}