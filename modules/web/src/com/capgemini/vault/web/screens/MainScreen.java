package com.capgemini.vault.web.screens;

import com.haulmont.cuba.gui.screen.Screen;
import com.haulmont.cuba.gui.screen.UiController;
import com.haulmont.cuba.gui.screen.UiDescriptor;

@UiController("vault_MainScreen")
@UiDescriptor("Main-screen.xml")
public class MainScreen extends Screen {
}