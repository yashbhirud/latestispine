package com.capgemini.vault.web.screens;

import com.capgemini.vault.entity.*;
import com.capgemini.vault.service.iAssociatedDetailsService;
import com.capgemini.vault.service.iExportService;
import com.capgemini.vault.service.iGetEmailService;
import com.capgemini.vault.web.screens.vaultapi.VaultApiEdit;
import com.capgemini.vault.web.utils.DataGridFilter;
import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.core.global.UserSessionSource;
import com.haulmont.cuba.gui.Dialogs;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.ScreenBuilders;
import com.haulmont.cuba.gui.WindowManager;
import com.haulmont.cuba.gui.actions.list.EditAction;
import com.haulmont.cuba.gui.actions.list.ExcelAction;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.components.filter.FilterDelegate;
import com.haulmont.cuba.gui.export.ByteArrayDataProvider;
import com.haulmont.cuba.gui.export.ExportDisplay;
import com.haulmont.cuba.gui.export.ExportFormat;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.relatedentities.RelatedEntitiesAPI;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.security.global.UserSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@UiController("vault_StagingApiFragment")
@UiDescriptor("stagingApi-fragment.xml")
public class StagingApiFragment extends ScreenFragment {
    private Logger log = LoggerFactory.getLogger(StagingApiFragment.class);

    @Inject
    private CollectionLoader<VaultApi> apiDetailsesDl;

    @Inject
    private RelatedEntitiesAPI relatedEntitiesAPI;

    @Inject
    private DataGrid<VaultApi> apiDetailsesTable;

    @Inject
    private Button sAbtn;

    @Inject
    private Button rejBtn;

    @Inject
    private Button AppBtn;
    @Inject
    private CollectionContainer<VaultApi> apiDetailsesDc;
    @Inject
    private DataManager dataManager;
    @Inject
    private Notifications notifications;
    @Inject
    private UserSession userSession;
    @Inject
    private Filter filter;

    UserSessionSource uss = AppBeans.get(UserSessionSource.class);

    @Inject
    private ScreenBuilders screenBuilders;
    @Inject
    private Button showButton;

    @Inject
    private Dialogs dialogs;
    @Inject
    private iExportService exportService;
    @Inject
    private ExportDisplay exportDisplay;
    @Named("apiDetailsesTable.excel")
    private ExcelAction apiDetailsesTableExcel;

    @Inject
    private CollectionLoader<VaultMaster> statusDl;
    @Inject
    private CollectionLoader<VaultMaster> platformDl;
    @Inject
    private CollectionLoader<VaultMaster> patternDl;
    @Inject
    private CollectionLoader<VaultMaster> buDl;
    @Inject
    private iAssociatedDetailsService associatedDetailsService;
    @Named("apiDetailsesTable.edit")
    private EditAction<VaultApi> apiDetailsesTableEdit;
    @Inject
    private CollectionLoader<VaultMaster> vaultMastersDl;
    @Inject
    private Filter ftsFilter;
    @Inject
    private Messages messages;
    @Inject
    private iGetEmailService getEmailService;
    @Inject
    private Button associateBtn;

    @Subscribe
    public void onInit(InitEvent event) {
        if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_administrator")) || userSession.getRoles().contains(messages.getMessage(getClass(),"role.company_administrator"))){
            AppBtn.setVisible(true);
            rejBtn.setVisible(true);
            sAbtn.setVisible(false);
        }else{
            AppBtn.setVisible(false);
            rejBtn.setVisible(false);
        }
        filter.setCollapsable(false);
        filter.setCaption(null);
        statusDl.load();
        platformDl.load();
        buDl.load();
        patternDl.load();
        apiDetailsesTableEdit.setAfterCommitHandler(e -> apiDetailsesDl.load());
        apiDetailsesTableEdit.setAfterCloseHandler(e -> apiDetailsesDl.load());

        ftsFilter.switchFilterMode(FilterDelegate.FilterMode.FTS_MODE);
        ftsFilter.setModeSwitchVisible(false);
        ftsFilter.setCollapsable(false);
        ftsFilter.setCaption(null);
        filter.setModeSwitchVisible(false);

        DataGrid.HtmlRenderer renderer1 =
                apiDetailsesTable.createRenderer(DataGrid.HtmlRenderer.class);
        renderer1.setNullRepresentation("</span>");
        apiDetailsesTable.getColumn("apiBizOwner").setRenderer(renderer1);

        DataGrid.HtmlRenderer renderer2 =
                apiDetailsesTable.createRenderer(DataGrid.HtmlRenderer.class);
        renderer2.setNullRepresentation("</span>");
        apiDetailsesTable.getColumn("apiIntOwner").setRenderer(renderer2);

        setReadOnlyModeAccess(userSession);

    }

    @Install(to = "apiDetailsesTable", subject = "rowStyleProvider")
    private String apiDetailsesTableRowStyleProvider(VaultApi apiDetails) {
        return Status.ERROR.equals(apiDetails.getApiWorkflowStatus()) ? "green-color" : null;
    }
    @Subscribe("filterCheck")
    public void onFilterCheckValueChange(HasValue.ValueChangeEvent<Boolean> event) {
        DataGridFilter myFilter= new DataGridFilter(apiDetailsesTable);
        if(Boolean.TRUE.equals(event.getValue())){

            myFilter.addGenericFilter(apiDetailsesDl,vaultMastersDl,"api","staging");
        }else{
            myFilter.removeFilterRow(apiDetailsesDl);
        }

    }

    @Subscribe("apiDetailsesTable.create")
    public void onApiDetailsesTableCreate(Action.ActionPerformedEvent event) {
        Map<String, Object> params = new HashMap<>();
        params.put("screen",messages.getMessage(getClass(),"screen.staging"));
        params.put("editable",true);
        Screen apiDetails = screenBuilders.editor(apiDetailsesTable)
                .newEntity()
                .withInitializer(apiDetail -> {          // lambda to initialize new instance
                    apiDetail.setApiWorkflowStatus(Status.DRAFT);
                    apiDetail.setApiLob(uss.getUserSession().getUser().getGroup().getName());
                })
                .withScreenClass(VaultApiEdit.class)    // specific editor screen
                .withLaunchMode(OpenMode.THIS_TAB)        // open as modal dialog
                .withOptions(new MapScreenOptions(params))
                .build()
                .show();
        apiDetails.addAfterCloseListener(afterCloseEvent -> apiDetailsesDl.load());

    }

    @Subscribe("apiDetailsesTable.edit")
    public void onApiDetailsesTableEdit(Action.ActionPerformedEvent event) {
        Map<String, Object> params = new HashMap<>();
        params.put("screen",messages.getMessage(getClass(),"screen.staging"));
        params.put("editable",false);
        Screen apiDetails = screenBuilders.editor(apiDetailsesTable)
                .withScreenClass(VaultApiEdit.class)    // specific editor screen
                .withLaunchMode(OpenMode.THIS_TAB)        // open as modal dialog
                .withOptions(new MapScreenOptions(params))
                .build()
                .show();
        apiDetails.addAfterCloseListener(afterCloseEvent -> apiDetailsesDl.load());

    }

    @Subscribe(target = Target.PARENT_CONTROLLER)
    private void onBeforeShowHost(Screen.BeforeShowEvent event) {
        if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_administrator"))){
            apiDetailsesDl.setQuery("select e from vault_Api e where e.apiWorkflowStatus <> 'AP' and e.apiLob = :grp order by e.updateTs desc, e.createTs desc");
            apiDetailsesDl.setParameter("grp", uss.getUserSession().getUser().getGroup().getName());
            apiDetailsesDl.load();
        }else if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_user")) || userSession.getRoles().contains(messages.getMessage(getClass(),"role.read_only"))){
            apiDetailsesDl.setQuery("select e from vault_Api e where e.apiWorkflowStatus <> 'AP' and e.createdBy = :user order by e.updateTs desc, e.createTs desc");
            apiDetailsesDl.setParameter("user", uss.getUserSession().getUser().getLogin());
            apiDetailsesDl.load();
        }else{
            apiDetailsesDl.setQuery("select e from vault_Api e where e.apiWorkflowStatus <> 'AP' order by e.updateTs desc, e.createTs desc");
            apiDetailsesDl.load();
        }
    }

    public void setDs(VaultUploadSummary upl){

        apiDetailsesDl.setParameter("apiUps",upl);
        apiDetailsesDl.setParameter("apiWorkflowStatus","DT");
        showButton.setVisible(true);
    }

    public void onRelatedClick() {

        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("apiDetails",apiDetailsesTable.getSelected());
        paramsMap.put("screen",messages.getMessage(getClass(),"screen.staging"));

        RelatedEntitiesAPI.RelatedScreenDescriptor relatedScreenDescriptor = new RelatedEntitiesAPI.RelatedScreenDescriptor();
        relatedScreenDescriptor.setOpenType(WindowManager.OpenType.NEW_TAB);
        relatedScreenDescriptor.setScreenId("vault_ApiIntegration.browse");
        relatedScreenDescriptor.setScreenParams(paramsMap);

        relatedEntitiesAPI.openRelatedScreen(apiDetailsesTable.getSelected(),
                VaultApi.class, "intApiDetails",
                relatedScreenDescriptor);
    }

    public void onSAbtnClick() {

        dialogs.createOptionDialog()
                .withCaption("Send For Approval")
                .withMessage("Do you want to send associated Integration Details for Approval ?")
                .withType(Dialogs.MessageType.CONFIRMATION)
                .withActions(
                        new DialogAction(DialogAction.Type.YES).withHandler(e -> sendForApp(true)),
                        new DialogAction(DialogAction.Type.NO).withHandler( e-> sendForApp(false)),
                        new DialogAction(DialogAction.Type.CLOSE)
                        {

                        }
                )
                .show();
    }

    private void sendForApp(boolean include){
        boolean errFlag = false;
        Set<VaultApi> apiDetailsList = apiDetailsesTable.getSelected();
        for(VaultApi apiDetails : apiDetailsList){
            if(include){
                List<VaultIntegration> intDetailsList = (List<VaultIntegration>) associatedDetailsService.getAssociatedIntApi(apiDetails);
                errFlag = intDetailsList.stream().filter(o -> o.getIntWorkflowStatus().equals(Status.ERROR)).findFirst().isPresent();
                associatedDetailsService.sendAssociatedIntForApp(intDetailsList);
            }
            if(apiDetails.getApiWorkflowStatus().equals(Status.DRAFT)){
                VaultApi apiDetail = apiDetailsesDc.getItem(apiDetails);
                apiDetail.setApiWorkflowStatus(Status.PENDING_FOR_APPROVAL);
                dataManager.commit(apiDetail);
            }
            else{
                errFlag = errFlag || apiDetails.getApiWorkflowStatus().equals(Status.ERROR);
            }

        }
        apiDetailsesDl.load();
        if(!errFlag)
            notifications.create(Notifications.NotificationType.TRAY).withCaption("Sent for Approval").show();
        else
            notifications.create(Notifications.NotificationType.TRAY).withCaption("Some record(s) are not sent for approval. Check if they are in error or already sent for approval.").show();
        getEmailService.sendDataApprovalPendingNotification(userSession.getUser());
    }

    public void onAppBtnClick() {

        dialogs.createOptionDialog()
                .withCaption("Approve")
                .withMessage("Do you want to approve associated Integration Details ?")
                .withType(Dialogs.MessageType.CONFIRMATION)
                .withActions(
                        new DialogAction(DialogAction.Type.YES).withHandler(e -> approveDetails(true)),
                        new DialogAction(DialogAction.Type.NO).withHandler( e-> approveDetails(false)),
                        new DialogAction(DialogAction.Type.CLOSE)
                        {

                        }
                )
                .show();
    }

    private void approveDetails(boolean include){
        int flag=0;
        Set<VaultApi> apiDetailsList = apiDetailsesTable.getSelected();
        if(include) {
            for (VaultApi apiDetails : apiDetailsList) {
                List<VaultIntegration> intDetailsList = (List<VaultIntegration>) associatedDetailsService.getAssociatedIntApi(apiDetails);
                associatedDetailsService.approveAssociatedInt(intDetailsList,(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_administrator")) || userSession.getRoles().contains(messages.getMessage(getClass(),"role.company_administrator"))));
            }
        }
        for (VaultApi apiDetails : apiDetailsList) {
            if (apiDetails.getApiWorkflowStatus().equals(Status.PENDING_FOR_APPROVAL) ||((userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_administrator")) || userSession.getRoles().contains(messages.getMessage(getClass(),"role.company_administrator"))) && apiDetails.getApiWorkflowStatus() != Status.ERROR)) {
                VaultApi apiDetail = apiDetailsesDc.getItem(apiDetails);
                apiDetail.setApiWorkflowStatus(Status.APPROVED);
                dataManager.commit(apiDetail);

            }
            else if(apiDetails.getApiWorkflowStatus()==Status.ERROR){
                flag=1;
            }
            else if(apiDetails.getApiWorkflowStatus().equals(Status.DRAFT) && !apiDetails.getCreatedBy().equals(userSession.getUser().getLogin())){
                flag=1;
            }
        }
        apiDetailsesDl.load();
        if(flag==1){
            notifications.create(Notifications.NotificationType.TRAY).withCaption("Some records are not approved. Please check if records are in Error or Draft.").show();
        }
        else
            notifications.create(Notifications.NotificationType.TRAY).withCaption("Approved").show();
    }

    public void onRejBtnClick() {

        dialogs.createOptionDialog()
                .withCaption("Reject")
                .withMessage("Do you want to reject associated Integration Details ?")
                .withType(Dialogs.MessageType.CONFIRMATION)
                .withActions(
                        new DialogAction(DialogAction.Type.YES).withHandler(e -> rejectDetails(true)),
                        new DialogAction(DialogAction.Type.NO).withHandler( e-> rejectDetails(false)),
                        new DialogAction(DialogAction.Type.CLOSE)
                        {
                        }
                )
                .show();

    }

    private void rejectDetails(boolean include){
        Set<VaultApi> apiDetailsList = apiDetailsesTable.getSelected();
        if(include){
            for(VaultApi apiDetails : apiDetailsList){
                List<VaultIntegration> intDetailsList = (List<VaultIntegration>) associatedDetailsService.getAssociatedIntApi(apiDetails);
                associatedDetailsService.rejectAssociatedInt(intDetailsList);
                if(apiDetails.getApiWorkflowStatus().equals(Status.PENDING_FOR_APPROVAL)){
                    VaultApi apiDetail = apiDetailsesDc.getItem(apiDetails);
                    apiDetail.setApiWorkflowStatus(Status.DRAFT);
                    dataManager.commit(apiDetail);
                }
            }
        }else{
            for(VaultApi apiDetails : apiDetailsList)
            {
                if(apiDetails.getApiWorkflowStatus().equals(Status.PENDING_FOR_APPROVAL)){
                    VaultApi apiDetail = apiDetailsesDc.getItem(apiDetails);
                    apiDetail.setApiWorkflowStatus(Status.DRAFT);
                    dataManager.commit(apiDetail);
                }
            }
        }
        apiDetailsesDl.load();
        notifications.create(Notifications.NotificationType.TRAY).withCaption("Rejected").show();
    }

    public void onShowButtonClick() {
        apiDetailsesDl.removeParameter("apiUps");
        apiDetailsesDl.removeParameter("apiWorkflowStatus");
        apiDetailsesDl.load();
        showButton.setVisible(false);
    }

    public void onAssociateBtnClick() {
        Set<VaultApi> apiDetailsList = apiDetailsesTable.getSelected();
        if(apiDetailsList.isEmpty()){
            notifications.create(Notifications.NotificationType.WARNING).withCaption("Please select API").show();
            return;
        } else if(apiDetailsList.size()>1){
            notifications.create(Notifications.NotificationType.WARNING).withCaption("Please select one API at a time").show();
            return;
        }

        Map<String, Object> params = new HashMap<>();
        params.put("api", apiDetailsList.iterator().next());
        params.put("screen", messages.getMessage(getClass(),"screen.staging"));

        Screen screen = screenBuilders.screen(this)
                .withScreenId("vault_ApiIntegration.edit")
                .withLaunchMode(OpenMode.DIALOG)
                .withOptions(new MapScreenOptions(params))
                .build()
                .show();
        screen.addAfterCloseListener(afterCloseEvent -> apiDetailsesDl.load());
    }

    public void onTableExcel() {
        try {
            exportDisplay.show(new ByteArrayDataProvider(exportService.exportVault(false)),"iSpine_Vault.xlsx", ExportFormat.XLSX);
        } catch (IOException e) {
            log.error("There was an error exporting file : " + e.getMessage());
        }
    }

    //Apply Filter
    @Subscribe("textFieldApi")
    public void onTextFieldApiTextChange(TextInputField.TextChangeEvent event) {
        if (event.getText() != null) {
            apiDetailsesDl.setParameter("apiName", "(?i)%" + event.getText() + "%");
        } else {
            apiDetailsesDl.removeParameter("apiName");
        }
        apiDetailsesDl.load();
    }

    @Subscribe("pfLookupField")
    public void onPfLookupFieldValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            apiDetailsesDl.setParameter("apiMstPlatform", event.getValue());
        } else {
            apiDetailsesDl.removeParameter("apiMstPlatform");
        }
        apiDetailsesDl.load();
    }

    @Subscribe("ownerField")
    public void onOwnerFieldValueChange(HasValue.ValueChangeEvent<String> event) {
        if (event.getValue() != null) {
            apiDetailsesDl.setParameter("apiBizOwner", "(?i)%" + event.getValue() + "%");
        } else {
            apiDetailsesDl.removeParameter("apiBizOwner");
        }
        apiDetailsesDl.load();
    }

    @Subscribe("patternField")
    public void onPatternFieldValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            apiDetailsesDl.setParameter("apiMstDesignPattern",  event.getValue());
        } else {
            apiDetailsesDl.removeParameter("apiMstDesignPattern");
        }
        apiDetailsesDl.load();
    }

    @Subscribe("sdlcField")
    public void onSdlcFieldValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            apiDetailsesDl.setParameter("apiMstSdlcStatus",  event.getValue());
        } else {
            apiDetailsesDl.removeParameter("apiMstSdlcStatus");
        }
        apiDetailsesDl.load();
    }

    @Subscribe("lookupFieldStatus")
    public void onLookupFieldStatusValueChange(HasValue.ValueChangeEvent<Status> event) {
        if (event.getValue() != null) {
            apiDetailsesDl.setParameter("apiWorkflowStatus",event.getValue());
        } else {
            apiDetailsesDl.removeParameter("apiWorkflowStatus");
        }
        apiDetailsesDl.load();
    }
    public void setReadOnlyModeAccess(UserSession userSession){
        if(userSession.getRoles().contains(messages.getMessage(getClass(), "role.read_only"))){
            sAbtn.setEnabled(false);
            associateBtn.setEnabled(false);
        }
    }

}