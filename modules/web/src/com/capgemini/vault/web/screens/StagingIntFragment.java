package com.capgemini.vault.web.screens;

import com.capgemini.vault.entity.*;
import com.capgemini.vault.service.iAssociatedDetailsService;
import com.capgemini.vault.service.iExportService;
import com.capgemini.vault.service.iGetEmailService;
import com.capgemini.vault.service.iLinkInvokeService;
import com.capgemini.vault.web.screens.vaultintegration.VaultIntegrationEdit;
import com.capgemini.vault.web.utils.DataGridFilter;
import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.core.global.UserSessionSource;
import com.haulmont.cuba.gui.Dialogs;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.ScreenBuilders;
import com.haulmont.cuba.gui.WindowManager;
import com.haulmont.cuba.gui.actions.list.ExcelAction;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.components.filter.FilterDelegate;
import com.haulmont.cuba.gui.export.ByteArrayDataProvider;
import com.haulmont.cuba.gui.export.ExportDisplay;
import com.haulmont.cuba.gui.export.ExportFormat;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.relatedentities.RelatedEntitiesAPI;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.security.global.UserSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.*;

@UiController("vault_StagingIntFragment")
@UiDescriptor("stagingInt-fragment.xml")
public class StagingIntFragment extends ScreenFragment {

    private Logger log = LoggerFactory.getLogger(StagingIntFragment.class);

    @Inject
    private CollectionLoader<VaultIntegration> intDetailsesDl;

    @Inject
    private CollectionContainer<VaultIntegration> intDetailsesDc;

    @Inject
    private RelatedEntitiesAPI relatedEntitiesAPI;

    @Inject
    private DataGrid<VaultIntegration> intDetailsesTable;

    @Inject
    private DataManager dataManager;

    @Inject
    private Notifications notifications;

    @Inject
    private Button approveBtn;

    @Inject
    private Button rejectBtn;

    @Inject
    private UserSession userSession;

    @Inject
    private Button sendBtn;

    @Inject
    private Filter filter;

    UserSessionSource uss = AppBeans.get(UserSessionSource.class);

    @Inject
    private ScreenBuilders screenBuilders;

    @Inject
    private Button showButton;

    @Inject
    private iExportService exportService;
    @Inject
    private Dialogs dialogs;

    @Named("intDetailsesTable.excel")
    private ExcelAction intDetailsesTableExcel;

    @Inject
    private ExportDisplay exportDisplay;

    @Inject
    private CollectionLoader<VaultMaster> platformDl;

    @Inject
    private CollectionLoader<VaultMaster> statusDl;
    @Inject
    private CollectionLoader<VaultMaster> patternDl;
    @Inject
    private CollectionLoader<VaultMaster> buDl;

    @Inject
    private iAssociatedDetailsService associatedDetailsService;
    @Inject
    private CollectionLoader<VaultMaster> vaultMastersDl;
    @Inject
    private Filter ftsFilter;

    @Inject
    private Messages messages;

    @Inject
    private iGetEmailService getEmailService;

    @Inject
    private Button associateBtn;

    @Inject
    private iLinkInvokeService linkInvoke;
    @Inject
    private Button exportBtn;


    @Subscribe("intDetailsesTable.create")
    public void onIntDetailsesTableCreate1(Action.ActionPerformedEvent event) {

        Map<String, Object> params = new HashMap<>();
        params.put("screen",messages.getMessage(getClass(),"screen.staging"));
        params.put("editable",true);
        screenBuilders.editor(intDetailsesTable)
                .newEntity()
                .withInitializer(intDetail -> {          // lambda to initialize new instance
                    intDetail.setIntWorkflowStatus(Status.DRAFT);
                    intDetail.setIntLob(uss.getUserSession().getUser().getGroup().getName());
                })
                .withScreenClass(VaultIntegrationEdit.class)    // specific editor screen******
                .withLaunchMode(OpenMode.THIS_TAB)        // open as modal dialog
                .withOptions(new MapScreenOptions(params))
                .build()
                .show();
    }

    @Subscribe("intDetailsesTable.edit")
    public void onIntDetailsesTableEdit(Action.ActionPerformedEvent event) {

        Map<String, Object> params = new HashMap<>();
        params.put("screen",messages.getMessage(getClass(),"screen.staging"));
        params.put("editable",false);
        screenBuilders.editor(intDetailsesTable)
                .withScreenClass(VaultIntegrationEdit.class)    // specific editor screen******
                .withLaunchMode(OpenMode.THIS_TAB)        // open as modal dialog
                .withOptions(new MapScreenOptions(params))
                .build()
                .show();
    }

    @Install(to = "intDetailsesTable", subject = "rowStyleProvider")
    private String intDetailsesTableRowStyleProvider(VaultIntegration intDetails) {
        return Status.ERROR.equals(intDetails.getIntWorkflowStatus()) ? "green-color" : null;
    }

    @Subscribe("filterCheck")
    public void onFilterCheckValueChange(HasValue.ValueChangeEvent<Boolean> event) {
        DataGridFilter filter= new DataGridFilter(intDetailsesTable);
        if(Boolean.TRUE.equals(event.getValue())){
            filter.addGenericFilter(intDetailsesDl,vaultMastersDl,"int","staging");
        }else{
            filter.removeFilterRow(intDetailsesDl);
        }

    }


    @Subscribe(target = Target.PARENT_CONTROLLER)
    private void onBeforeShowHost(Screen.BeforeShowEvent event) {
        if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_administrator"))){
            intDetailsesDl.setQuery("select e from vault_Integration e where e.intWorkflowStatus <> 'AP' and e.intLob = :grp order by e.updateTs desc, e.createTs desc");
            intDetailsesDl.setParameter("grp", uss.getUserSession().getUser().getGroup().getName());
            intDetailsesDl.load();
        }else if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_user"))){
            intDetailsesDl.setQuery("select e from vault_Integration e where e.intWorkflowStatus <> 'AP' and e.createdBy = :user order by e.updateTs desc, e.createTs desc");
            intDetailsesDl.setParameter("user", uss.getUserSession().getUser().getLogin());
            intDetailsesDl.load();
        }else{
            intDetailsesDl.setQuery("select e from vault_Integration e where e.intWorkflowStatus <> 'AP' order by e.updateTs desc, e.createTs desc");
            intDetailsesDl.load();
        }

    }

    public void setDs(VaultUploadSummary upl){

        intDetailsesDl.setParameter("intUps",upl);
        showButton.setVisible(true);
    }

    @Subscribe(target = Target.PARENT_CONTROLLER)
    public void onInit(Screen.InitEvent event) {
        if (userSession.getRoles().contains(messages.getMessage(getClass(), "role.bu_administrator")) || userSession.getRoles().contains(messages.getMessage(getClass(), "role.company_administrator"))) {
            approveBtn.setVisible(true);
            rejectBtn.setVisible(true);
            sendBtn.setVisible(false);
        } else {
            approveBtn.setVisible(false);
            rejectBtn.setVisible(false);
        }
        filter.setCollapsable(false);
        filter.setCaption(null);

        platformDl.load();
        statusDl.load();
        buDl.load();
        patternDl.load();

        ftsFilter.switchFilterMode(FilterDelegate.FilterMode.FTS_MODE);
        ftsFilter.setModeSwitchVisible(false);
        ftsFilter.setCollapsable(false);
        ftsFilter.setCaption(null);
        filter.setModeSwitchVisible(false);


        DataGrid.HtmlRenderer renderer1 =
                intDetailsesTable.createRenderer(DataGrid.HtmlRenderer.class);
        renderer1.setNullRepresentation("</span>");
        intDetailsesTable.getColumn("intBizOwner").setRenderer(renderer1);

        DataGrid.HtmlRenderer renderer5 =
                intDetailsesTable.createRenderer(DataGrid.HtmlRenderer.class);
        renderer5.setNullRepresentation("</span>");
        intDetailsesTable.getColumn("intOwner").setRenderer(renderer5);

        DataGrid.ClickableTextRenderer<VaultIntegration> intDetailsesTableTechSpecLocRenderer = intDetailsesTable.createRenderer(DataGrid.ClickableTextRenderer.class);
        intDetailsesTableTechSpecLocRenderer.setRendererClickListener(clickableTextRendererClickEvent -> {
            boolean checkAccess = ((ExtUser) userSession.getUser()).getAllowTechSpec();
            if (!checkAccess) {
                notifications.create().withCaption("You are not authorised user to access link").show();
            }
            else{
                List<Object> list= linkInvoke.linkinvokefunction(clickableTextRendererClickEvent.getItem().getIntTechSpecLoc());
                if (list == null) {
                    notifications.create().withCaption("Cannot open the link.Please check the url").show();
                }
                else if(list.contains("saved"))
                {
                    notifications.create().withCaption("Saved to C://git").show();
                }
                else if(!list.isEmpty()){
                    exportDisplay.show(new ByteArrayDataProvider((byte[]) list.get(0)), (String) list.get(1));
                }
            }
        });
        intDetailsesTable.getColumn("techSpecLoc").setRenderer(intDetailsesTableTechSpecLocRenderer);

        DataGrid.ClickableTextRenderer<VaultIntegration> intDetailsesTableFuncSpecLocRenderer = intDetailsesTable.createRenderer(DataGrid.ClickableTextRenderer.class);
        intDetailsesTableFuncSpecLocRenderer.setRendererClickListener(clickableTextRendererClickEvent -> {
            boolean checkAccess = ((ExtUser) userSession.getUser()).getAllowFuncSpec();
            if (!checkAccess) {
                notifications.create().withCaption("You are not authorised user to access link").show();
            }
            else{
                List<Object> list= linkInvoke.linkinvokefunction(clickableTextRendererClickEvent.getItem().getIntFuncSpecLoc());
                if (list == null) {
                    notifications.create().withCaption("Cannot open the link").show();
                }
                else if(list.contains("saved"))
                {
                    notifications.create().withCaption("Saved to C://git").show();
                }
                else if(!list.isEmpty()){
                    exportDisplay.show(new ByteArrayDataProvider((byte[]) list.get(0)), (String) list.get(1));
                }
            }
        });
        intDetailsesTable.getColumn("funcSpecLoc").setRenderer(intDetailsesTableFuncSpecLocRenderer);

        DataGrid.ClickableTextRenderer<VaultIntegration> intDetailsesTableCodeRepoLocRenderer = intDetailsesTable.createRenderer(DataGrid.ClickableTextRenderer.class);
        intDetailsesTableCodeRepoLocRenderer.setRendererClickListener(clickableTextRendererClickEvent -> {
            boolean checkAccess = ((ExtUser) userSession.getUser()).getAllowCodeLoc();
            if (!checkAccess) {
                notifications.create().withCaption("You are not authorised user to access link").show();
            }
            else{
                List<Object> list= linkInvoke.linkinvokefunction(clickableTextRendererClickEvent.getItem().getIntCodeRepo());
                if (list == null) {
                    notifications.create().withCaption("Cannot open the link").show();
                }
                else if(list.contains("saved"))
                {
                    notifications.create().withCaption("Saved to C://git").show();
                }
                else if(!list.isEmpty()){
                    exportDisplay.show(new ByteArrayDataProvider((byte[]) list.get(0)), (String) list.get(1));
                }
            }
        });
        intDetailsesTable.getColumn("codeRepo").setRenderer(intDetailsesTableCodeRepoLocRenderer);
        setReadOnlyModeAccess(userSession);
        intDetailsesDl.load();
        System.out.println(intDetailsesTable.getItems().size());
        if(intDetailsesDc.getItems().isEmpty()){
            exportBtn.setVisible(false);
        }
    }


    public void onRelatedApiClick() {

        Map<String, Object> paramsMap = new HashMap<>();
        //************
        paramsMap.put("intDetails",intDetailsesTable.getSelected());
        paramsMap.put("screen",messages.getMessage(getClass(),"screen.staging"));
        RelatedEntitiesAPI.RelatedScreenDescriptor relatedScreenDescriptor = new RelatedEntitiesAPI.RelatedScreenDescriptor();
        relatedScreenDescriptor.setOpenType(WindowManager.OpenType.NEW_TAB);//**
        relatedScreenDescriptor.setScreenId("vault_IntegrationApi.browse");
        relatedScreenDescriptor.setScreenParams(paramsMap);

        relatedEntitiesAPI.openRelatedScreen(intDetailsesTable.getSelected(),
                VaultIntegration.class, "intApiDetails", relatedScreenDescriptor);
    }



    public void onSendBtnClick() {
        dialogs.createOptionDialog()
                .withCaption("Send For Approval")
                .withMessage("Do you want to send associated API Details for Approval ?")
                .withType(Dialogs.MessageType.CONFIRMATION)
                .withActions(
                        new DialogAction(DialogAction.Type.YES).withHandler(e -> sendForApp(true)),
                        new DialogAction(DialogAction.Type.NO).withHandler( e-> sendForApp(false)),
                        new DialogAction(DialogAction.Type.CLOSE)
                        {

                        }
                )
                .show();


    }

    private void sendForApp(boolean include){
        int flag=0;
        Set<VaultIntegration> intDetailsList = intDetailsesTable.getSelected();
        for(VaultIntegration intDetails : intDetailsList){
            if(include){
                List<VaultApi> apiDetailsList = (List<VaultApi>) associatedDetailsService.getAssociatedIntApi(intDetails);
                associatedDetailsService.sendAssociatedApiForApp(apiDetailsList);
            }
            if(intDetails.getIntWorkflowStatus().equals(Status.DRAFT)){
                    VaultIntegration intDetail = intDetailsesDc.getItem(intDetails);
                    intDetail.setIntWorkflowStatus(Status.PENDING_FOR_APPROVAL);
                    dataManager.commit(intDetail);
            }
            else
                flag=1;
            intDetailsesDl.load();
            }
        if(flag==0)
            notifications.create(Notifications.NotificationType.TRAY).withCaption("Sent for Approval").show();
        else if(flag==1)
            notifications.create(Notifications.NotificationType.TRAY).withCaption("Some record(s) are not sent for approval. Check if they are in error or already sent for approval.").show();
        getEmailService.sendDataApprovalPendingNotification(userSession.getUser());
    }


    public void onApproveBtnClick() {

        dialogs.createOptionDialog()
                .withCaption("Approve")
                .withMessage("Do you want to approve associated API Details ?")
                .withType(Dialogs.MessageType.CONFIRMATION)
                .withActions(
                        new DialogAction(DialogAction.Type.YES).withHandler(e -> approveDetails(true)),
                        new DialogAction(DialogAction.Type.NO).withHandler( e-> approveDetails(false)),
                        new DialogAction(DialogAction.Type.CLOSE)
                        {

                        }
                )
                .show();

    }

    private void approveDetails(boolean include){
        String buEmail = userSession.getUser().getEmail();
        int flag=0;
        String userLogin = null;
        Set<VaultIntegration> intDetailsList = intDetailsesTable.getSelected();
        if(include){
            for(VaultIntegration intDetails : intDetailsList){
                List<VaultApi> apiDetailsList = (List<VaultApi>) associatedDetailsService.getAssociatedIntApi(intDetails);
                associatedDetailsService.approveAssociatedApi(apiDetailsList,(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_administrator")) || userSession.getRoles().contains(messages.getMessage(getClass(),"role.company_administrator"))));

            }
        }
        for(VaultIntegration intDetails : intDetailsList){
            userLogin = intDetails.getCreatedBy();
            if (intDetails.getIntWorkflowStatus().equals(Status.PENDING_FOR_APPROVAL) || ((userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_administrator")) || userSession.getRoles().contains(messages.getMessage(getClass(),"role.company_administrator"))) && intDetails.getIntWorkflowStatus() != Status.ERROR)) {
                VaultIntegration intDetail = intDetailsesDc.getItem(intDetails);
                intDetail.setIntWorkflowStatus(Status.APPROVED);
                dataManager.commit(intDetail);

            }
            else if(intDetails.getIntWorkflowStatus()==Status.ERROR){
                flag=1;
            }
            else if(intDetails.getIntWorkflowStatus().equals(Status.DRAFT) && !intDetails.getCreatedBy().equals(userSession.getUser().getLogin())){
                flag=1;
            }
        }
        intDetailsesDl.load();
        if(flag==1){
            notifications.create(Notifications.NotificationType.TRAY).withCaption("Some records are not approved. Please check if records are in Error or Draft.").show();
        }
        else
            notifications.create(Notifications.NotificationType.TRAY).withCaption("Approved").show();
        getEmailService.sendDataApprovalNotification(userLogin, buEmail);

    }

    public void onRejectBtnClick() {
        dialogs.createOptionDialog()
                .withCaption("Reject")
                .withMessage("Do you want to reject associated API Details ?")
                .withType(Dialogs.MessageType.CONFIRMATION)
                .withActions(
                        new DialogAction(DialogAction.Type.YES).withHandler(e -> rejectDetails(true)),
                        new DialogAction(DialogAction.Type.NO).withHandler( e-> rejectDetails(false)),
                        new DialogAction(DialogAction.Type.CLOSE)
                        {
                        }
                )
                .show();
    }

    private void rejectDetails(boolean include){
        String userLogin = null;
        String buEmail = userSession.getUser().getEmail();
        Set<VaultIntegration> intDetailsList = intDetailsesTable.getSelected();
        if(include){
            for(VaultIntegration intDetails : intDetailsList){
                userLogin = intDetails.getCreatedBy();
                List<VaultApi> apiDetailsList = (List<VaultApi>) associatedDetailsService.getAssociatedIntApi(intDetails);
                associatedDetailsService.rejectAssociatedApi(apiDetailsList);
                if(intDetails.getIntWorkflowStatus().equals(Status.PENDING_FOR_APPROVAL)){
                    VaultIntegration intDetail = intDetailsesDc.getItem(intDetails);
                    intDetail.setIntWorkflowStatus(Status.DRAFT);
                    dataManager.commit(intDetail);
                }
                intDetailsesDl.load();
            }
        }else{
            for(VaultIntegration intDetails : intDetailsList){
                userLogin = intDetails.getCreatedBy();
                if(intDetails.getIntWorkflowStatus().equals(Status.PENDING_FOR_APPROVAL)){
                    VaultIntegration intDetail = intDetailsesDc.getItem(intDetails);
                    intDetail.setIntWorkflowStatus(Status.DRAFT);
                    dataManager.commit(intDetail);
                }
                intDetailsesDl.load();
            }
        }
        notifications.create(Notifications.NotificationType.TRAY).withCaption("Rejected").show();
        getEmailService.sendDataRejectionNotification(userLogin, buEmail);
    }

    public void onShowButtonClick() {
        intDetailsesDl.removeParameter("intUps");
        intDetailsesDl.load();
        showButton.setVisible(false);
    }

    public void onAssociateBtnClick() {

        Set<VaultIntegration> intDetailsList = intDetailsesTable.getSelected();
        if(intDetailsList.isEmpty()){
            notifications.create(Notifications.NotificationType.WARNING).withCaption("Please select Integration").show();
            return;
        } else if(intDetailsList.size()>1){
            notifications.create(Notifications.NotificationType.WARNING).withCaption("Please select one Integration at a time").show();
            return;
        }

        Map<String, Object> params = new HashMap<>();
        params.put("integration", intDetailsList.iterator().next());
        params.put("screen", messages.getMessage(getClass(),"screen.staging"));

        Screen screen = screenBuilders.screen(this)
                .withScreenId("vault_IntegrationApi.edit")
                .withLaunchMode(OpenMode.DIALOG)
                .withOptions(new MapScreenOptions(params))
                .build()
                .show();
        screen.addAfterCloseListener(afterCloseEvent -> intDetailsesDl.load());
    }

    public void onTableExcel() {
        try {
            exportDisplay.show(new ByteArrayDataProvider(exportService.exportVault(false)),"iSpine_Vault.xlsx", ExportFormat.XLSX);
        } catch (IOException e) {
            log.error("There was an error exporting file : "+e.getMessage());
        }

    }

    @Subscribe("pfLookup")
    public void onPfLookupValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            intDetailsesDl.setParameter("intMstPlatform", event.getValue());
        } else {
            intDetailsesDl.removeParameter("intMstPlatform");
        }
        intDetailsesDl.load();
    }

    @Subscribe("bOwnerField")
    public void onBOwnerFieldValueChange(HasValue.ValueChangeEvent<String> event) {
        if (event.getValue() != null) {
            intDetailsesDl.setParameter("intBizOwner", "(?i)%" + event.getValue() + "%");
        } else {
            intDetailsesDl.removeParameter("intBizOwner");
        }
        intDetailsesDl.load();

    }

    @Subscribe("initField")
    public void onInitFieldValueChange(HasValue.ValueChangeEvent<String> event) {
        if (event.getValue() != null) {
            intDetailsesDl.setParameter("intSrcSys", "(?i)%" + event.getValue() + "%");
        } else {
            intDetailsesDl.removeParameter("intSrcSys");
        }
        intDetailsesDl.load();

    }

    @Subscribe("patternLookup")
    public void onPatternLookupValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            intDetailsesDl.setParameter("intMstDesignPattern", event.getValue());
        } else {
            intDetailsesDl.removeParameter("intMstDesignPattern");
        }
        intDetailsesDl.load();

    }

    @Subscribe("statLookup")
    public void onStatLookupValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            intDetailsesDl.setParameter("intMstSdlcStatus", event.getValue());
        } else {
            intDetailsesDl.removeParameter("intMstSdlcStatus");
        }
        intDetailsesDl.load();

    }

    @Subscribe("nameFilter")
    public void onNameFilterValueChange(HasValue.ValueChangeEvent<String> event) {
        if (event.getValue() != null) {
            intDetailsesDl.setParameter("intName", "(?i)%" + event.getValue() + "%");
        } else {
            intDetailsesDl.removeParameter("intName");
        }
        intDetailsesDl.load();
    }

    @Subscribe("lookupFieldStatus")
    public void onLookupFieldStatusValueChange(HasValue.ValueChangeEvent<Status> event) {
        if (event.getValue() != null) {
            intDetailsesDl.setParameter("intWorkflowStatus",event.getValue());
        } else {
            intDetailsesDl.removeParameter("intWorkflowStatus");
        }
        intDetailsesDl.load();
    }

    public void setReadOnlyModeAccess(UserSession userSession){
        if(userSession.getRoles().contains(messages.getMessage(getClass(), "role.read_only"))) {
            associateBtn.setEnabled(false);
            sendBtn.setEnabled(false);
        }
    }

}