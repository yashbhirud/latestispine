package com.capgemini.vault.web.screens;

import com.capgemini.vault.entity.VaultUploadSummary;
import com.haulmont.cuba.gui.components.TabSheet;
import com.haulmont.cuba.gui.screen.Screen;
import com.haulmont.cuba.gui.screen.UiController;
import com.haulmont.cuba.gui.screen.UiDescriptor;

import javax.inject.Inject;

@UiController("vault_StagingScreen")
@UiDescriptor("staging-screen.xml")
public class StagingScreen extends Screen {
    @Inject
    private TabSheet detailsTabsheet;

    @Inject
    private StagingIntFragment intFragment;
    @Inject
    private StagingApiFragment apiFragment;

    public void setDataSource(VaultUploadSummary upl){
        if (upl.getUpsDatatype().equals("APIs")){
            detailsTabsheet.setSelectedTab(detailsTabsheet.getTab("tabApi"));
            apiFragment.setDs(upl);
            return;
        }
        detailsTabsheet.setSelectedTab(detailsTabsheet.getTab("tabInt"));
        intFragment.setDs(upl);
    }
}