package com.capgemini.vault.web.screens;

import com.haulmont.cuba.core.entity.FileDescriptor;
import com.haulmont.cuba.gui.components.BrowserFrame;
import com.haulmont.cuba.gui.components.FileDescriptorResource;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.Screen;
import com.haulmont.cuba.gui.screen.Subscribe;
import com.haulmont.cuba.gui.screen.UiController;
import com.haulmont.cuba.gui.screen.UiDescriptor;

import javax.inject.Inject;

@UiController("vault_UserManual")
@UiDescriptor("user-Manual.xml")
public class UserManual extends Screen {

    @Inject
    private CollectionLoader<FileDescriptor> fileDescriptorsDl;

    @Inject
    private CollectionContainer<FileDescriptor> fileDescriptorsDc;

    @Inject
    private BrowserFrame pdfPreview;

    @Subscribe
    public void onInit(InitEvent event) {
        fileDescriptorsDl.load();
        FileDescriptor fd = fileDescriptorsDc.getItems().get(0);
        pdfPreview.setSource(FileDescriptorResource.class).setFileDescriptor(fd)
                .setMimeType("application/pdf");

    }

}