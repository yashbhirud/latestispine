package com.capgemini.vault.web.screens.dashboard;


import com.haulmont.addon.dashboard.web.annotation.DashboardWidget;
import com.haulmont.addon.dashboard.web.events.DashboardEvent;
import com.haulmont.addon.dashboard.web.widget.RefreshableWidget;
import com.haulmont.bali.events.Subscription;
import com.haulmont.charts.gui.amcharts.model.Align;
import com.haulmont.charts.gui.amcharts.model.Color;
import com.haulmont.charts.gui.amcharts.model.Label;
import com.haulmont.charts.gui.components.charts.PieChart;
import com.haulmont.charts.gui.data.DataProvider;
import com.haulmont.charts.gui.data.ListDataProvider;
import com.haulmont.charts.gui.data.MapDataItem;
import com.haulmont.cuba.core.entity.KeyValueEntity;
import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.core.global.UserSessionSource;
import com.haulmont.cuba.gui.model.DataContext;
import com.haulmont.cuba.gui.model.KeyValueCollectionContainer;
import com.haulmont.cuba.gui.model.KeyValueCollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.security.global.UserSession;

import javax.inject.Inject;
import java.util.*;
import java.util.function.Consumer;

@UiController("vault_ApiPie")
@UiDescriptor("Api-Pie.xml")
@DashboardWidget(name="Api Pie")
public class ApiPie extends ScreenFragment implements RefreshableWidget {

    @Inject
    private KeyValueCollectionLoader apiLoader;
    @Inject
    private PieChart apiPieChart;
    @Inject
    private UserSession userSession;

    UserSessionSource uss = AppBeans.get(UserSessionSource.class);
    @Inject
    private KeyValueCollectionContainer ApiDc;
    @Inject
    private Messages messages;


    @Subscribe
    public void onInit(InitEvent event) {

        setData();
        apiPieChart.addLabels(new Label().setText("Workflow Status wise APIs").setX("60").setY("20").setSize(12).setBold(false).setColor(Color.DARKBLUE));
    }

    private void setData(){
        if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_administrator"))){
            apiLoader.setQuery("select (case when (e.apiWorkflowStatus = 'DT') then 'Staging'" +
                    " when (e.apiWorkflowStatus = 'AP') then 'Main' " +
                    "when (e.apiWorkflowStatus = 'PA') then 'Pending' " +
                    "when (e.apiWorkflowStatus = 'ER') then 'Error' " +
                    "else '' end),count(e.api) from vault_Api e where (e.apiLob =:grp or e.apiWorkflowStatus='AP') group by e.apiWorkflowStatus");
            apiLoader.setParameter("grp", uss.getUserSession().getUser().getGroup().getName());
            apiLoader.load();
        }else if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_user"))){
            apiLoader.setQuery("select (case when (e.apiWorkflowStatus = 'DT') then 'Staging'" +
                    " when (e.apiWorkflowStatus = 'AP') then 'Main' " +
                    "when (e.apiWorkflowStatus = 'PA') then 'Pending' " +
                    "when (e.apiWorkflowStatus = 'ER') then 'Error' " +
                    "else '' end),count(e.api) from vault_Api e where (e.createdBy = :user or e.apiWorkflowStatus='AP') group by e.apiWorkflowStatus");
            apiLoader.setParameter("user", uss.getUserSession().getUser().getLogin());
            apiLoader.load();
        }else{
            apiLoader.setQuery("select (case when (e.apiWorkflowStatus = 'DT') then 'Staging'" +
                    " when (e.apiWorkflowStatus = 'AP') then 'Main' " +
                    "when (e.apiWorkflowStatus = 'PA') then 'Pending' " +
                    "when (e.apiWorkflowStatus = 'ER') then 'Error' " +
                    "else '' end),count(e.api) from vault_Api e group by e.apiWorkflowStatus");
            apiLoader.load();
        }

        KeyValueCollectionContainer keyValueCollectionLoader = apiLoader.getContainer();
        List<KeyValueEntity> list1 = keyValueCollectionLoader.getItems();

        ListDataProvider dataProvider = new ListDataProvider();

        for (KeyValueEntity e : list1){

            String color = null;
            if("Error".equals(e.getValue("Status"))){
                color = "#fc2c03";
            }
            if("Error".equals(e.getValue("Staging"))){
                color = "#407ab8";
            }
            if("Error".equals(e.getValue("Main"))){
                color = "#52b840";
            }
            if("Error".equals(e.getValue("Pending"))){
                color = "#e6db12";
            }
            dataProvider.addItem(new MapDataItem().add("Status", e.getValue("Status")).add("Count", e.getValue("Count")).add("color", color));
        }

        apiPieChart.setDataProvider(dataProvider);
    }

    @Override
    public void refresh(DashboardEvent dashboardEvent) {
        setData();
        apiPieChart.repaint();
    }
}