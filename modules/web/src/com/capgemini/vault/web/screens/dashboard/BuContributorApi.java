package com.capgemini.vault.web.screens.dashboard;

import com.haulmont.addon.dashboard.web.annotation.DashboardWidget;
import com.haulmont.addon.dashboard.web.events.DashboardEvent;
import com.haulmont.addon.dashboard.web.widget.RefreshableWidget;
import com.haulmont.charts.gui.amcharts.model.Color;
import com.haulmont.charts.gui.amcharts.model.Label;
import com.haulmont.charts.gui.components.charts.PieChart;
import com.haulmont.cuba.gui.model.KeyValueCollectionLoader;
import com.haulmont.cuba.gui.screen.ScreenFragment;
import com.haulmont.cuba.gui.screen.Subscribe;
import com.haulmont.cuba.gui.screen.UiController;
import com.haulmont.cuba.gui.screen.UiDescriptor;

import javax.inject.Inject;

@UiController("vault_BuContributorApi")
@UiDescriptor("bu-contributor-api.xml")
@DashboardWidget(name="Bu Contributor Api")
public class BuContributorApi extends ScreenFragment implements RefreshableWidget {

    @Inject
    private PieChart apiPieChart;
    @Inject
    private KeyValueCollectionLoader apiLoader;

    @Subscribe
    public void onInit(InitEvent event) {
        apiPieChart.addLabels(new Label().setText("API-Top Contributors").setX("10").setY("10").setSize(11).setBold(false).setColor(Color.DARKBLUE)).setMarginTop(0).setMarginBottom(0);
        setData();
    }

    private void setData(){
        apiLoader.setQuery("select e.apiLob,count(e) cnt from vault_Api e where e.apiWorkflowStatus='AP' group by e.apiLob order by cnt desc");
        apiLoader.setMaxResults(10);
        apiLoader.load();
    }

    @Override
    public void refresh(DashboardEvent dashboardEvent) {
        setData();
        apiPieChart.repaint();
    }
}