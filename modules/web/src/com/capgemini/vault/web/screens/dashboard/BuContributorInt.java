package com.capgemini.vault.web.screens.dashboard;

import com.haulmont.addon.dashboard.web.annotation.DashboardWidget;
import com.haulmont.addon.dashboard.web.events.DashboardEvent;
import com.haulmont.addon.dashboard.web.widget.RefreshableWidget;
import com.haulmont.charts.gui.amcharts.model.Color;
import com.haulmont.charts.gui.amcharts.model.Label;
import com.haulmont.charts.gui.components.charts.PieChart;
import com.haulmont.cuba.gui.model.KeyValueCollectionLoader;
import com.haulmont.cuba.gui.screen.ScreenFragment;
import com.haulmont.cuba.gui.screen.Subscribe;
import com.haulmont.cuba.gui.screen.UiController;
import com.haulmont.cuba.gui.screen.UiDescriptor;

import javax.inject.Inject;

@UiController("vault_BuContributorInt")
@UiDescriptor("bu-contributor-int.xml")
@DashboardWidget(name="Bu Contributor Int")
public class BuContributorInt extends ScreenFragment implements RefreshableWidget {
    @Inject
    private PieChart intPieChart;

    @Subscribe
    public void onInit(InitEvent event) {
        intPieChart.addLabels(new Label().setText("Integration-Top Contributors").setX("10").setY("10").setSize(11).setBold(false).setColor(Color.DARKBLUE)).setMarginTop(0).setMarginBottom(0);
            setData();
    }
    @Inject
    private KeyValueCollectionLoader integrationLoader;

    @Override
    public void refresh(DashboardEvent dashboardEvent) {
        setData();
        intPieChart.repaint();
    }

    private void setData(){
        integrationLoader.setQuery("select e.intLob,count(e) cnt from vault_Integration e where e.intWorkflowStatus='AP' group by e.intLob order by cnt desc");
        integrationLoader.setMaxResults(10);
        integrationLoader.load();
    }
}