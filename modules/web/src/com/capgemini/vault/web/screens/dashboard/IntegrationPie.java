package com.capgemini.vault.web.screens.dashboard;

import com.haulmont.addon.dashboard.web.annotation.DashboardWidget;
import com.haulmont.addon.dashboard.web.events.DashboardEvent;
import com.haulmont.addon.dashboard.web.widget.RefreshableWidget;
import com.haulmont.charts.gui.amcharts.model.Color;
import com.haulmont.charts.gui.amcharts.model.Label;
import com.haulmont.charts.gui.components.charts.PieChart;
import com.haulmont.charts.gui.data.ListDataProvider;
import com.haulmont.charts.gui.data.MapDataItem;
import com.haulmont.cuba.core.entity.KeyValueEntity;
import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.core.global.UserSessionSource;
import com.haulmont.cuba.gui.model.*;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.security.global.UserSession;
import org.springframework.context.event.EventListener;

import javax.inject.Inject;
import java.util.List;

@UiController("vault_IntegrationPie")
@UiDescriptor("integration-pie.xml")
@DashboardWidget(name="Integration Pie")
public class IntegrationPie extends ScreenFragment implements RefreshableWidget {

    @Inject
    private KeyValueCollectionLoader integrationLoader;

    @Inject
    private PieChart intPieChart;
    @Inject
    private UserSession userSession;

    UserSessionSource uss = AppBeans.get(UserSessionSource.class);
    @Inject
    private Messages messages;

    @Subscribe
    public void onInit(InitEvent event) {

        setData();
        intPieChart.addLabels(new Label().setText("Workflow Status wise Integrations").setX("20").setY("20").setSize(12).setBold(false).setColor(Color.DARKBLUE));
    }

    private void setData(){

        if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_administrator"))){
            integrationLoader.setQuery("select " +
                    "(case \n" +
                    "                when (e.intWorkflowStatus = 'DT') then 'Staging' \n" +
                    "                when (e.intWorkflowStatus = 'AP') then 'Main' \n" +
                    "                when (e.intWorkflowStatus = 'PA') then 'Pending' \n" +
                    "                when (e.intWorkflowStatus = 'ER') then 'Error' \n" +
                    "                else '' end), count(e.intAsset) from vault_Integration e where (e.intLob =:grp or e.intWorkflowStatus='AP') group by e.intWorkflowStatus");
            integrationLoader.setParameter("grp", uss.getUserSession().getUser().getGroup().getName());
            integrationLoader.load();
        }else if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_user"))){
            integrationLoader.setQuery("select " +
                    "(case \n" +
                    "                when (e.intWorkflowStatus = 'DT') then 'Staging' \n" +
                    "                when (e.intWorkflowStatus = 'AP') then 'Main' \n" +
                    "                when (e.intWorkflowStatus = 'PA') then 'Pending' \n" +
                    "                when (e.intWorkflowStatus = 'ER') then 'Error' \n" +
                    "                else '' end), count(e.intAsset) from vault_Integration e where (e.createdBy = :user or e.intWorkflowStatus='AP') group by e.intWorkflowStatus");
            integrationLoader.setParameter("user", uss.getUserSession().getUser().getLogin());
            integrationLoader.load();
        }else{
            integrationLoader.setQuery("select " +
                    "(case \n" +
                    "                when (e.intWorkflowStatus = 'DT') then 'Staging' \n" +
                    "                when (e.intWorkflowStatus = 'AP') then 'Main' \n" +
                    "                when (e.intWorkflowStatus = 'PA') then 'Pending' \n" +
                    "                when (e.intWorkflowStatus = 'ER') then 'Error' \n" +
                    "                else '' end), count(e.intAsset) from vault_Integration e group by e.intWorkflowStatus");
            integrationLoader.load();
        }
        KeyValueCollectionContainer keyValueCollectionLoader = integrationLoader.getContainer();
        List<KeyValueEntity> list1 = keyValueCollectionLoader.getItems();

        ListDataProvider dataProvider = new ListDataProvider();

        for (KeyValueEntity e : list1){

            String color = null;
            if("Error".equals(e.getValue("intWorkflowStatus"))){
                color = "#fc2c03";
            }
            if("Staging".equals(e.getValue("intWorkflowStatus"))){
                color = "#407ab8";
            }
            if("Main".equals(e.getValue("intWorkflowStatus"))){
                color = "#52b840";
            }
            if("Pending".equals(e.getValue("intWorkflowStatus"))){
                color = "#e6db12";
            }
            dataProvider.addItem(new MapDataItem().add("intWorkflowStatus", e.getValue("intWorkflowStatus")).add("count", e.getValue("count")).add("color", color));
        }

        intPieChart.setDataProvider(dataProvider);
    }

    @Override
    public void refresh(DashboardEvent dashboardEvent) {
        setData();
        intPieChart.repaint();
    }


}