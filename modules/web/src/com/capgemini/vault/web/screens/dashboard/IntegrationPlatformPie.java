package com.capgemini.vault.web.screens.dashboard;

import com.haulmont.addon.dashboard.web.annotation.DashboardWidget;
import com.haulmont.addon.dashboard.web.events.DashboardEvent;
import com.haulmont.addon.dashboard.web.widget.RefreshableWidget;
import com.haulmont.charts.gui.amcharts.model.Color;
import com.haulmont.charts.gui.amcharts.model.Label;
import com.haulmont.charts.gui.components.charts.PieChart;
import com.haulmont.charts.gui.data.ListDataProvider;
import com.haulmont.charts.gui.data.MapDataItem;
import com.haulmont.cuba.core.entity.KeyValueEntity;
import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.core.global.UserSessionSource;
import com.haulmont.cuba.gui.model.KeyValueCollectionContainer;
import com.haulmont.cuba.gui.model.KeyValueCollectionLoader;
import com.haulmont.cuba.gui.screen.ScreenFragment;
import com.haulmont.cuba.gui.screen.Subscribe;
import com.haulmont.cuba.gui.screen.UiController;
import com.haulmont.cuba.gui.screen.UiDescriptor;
import com.haulmont.cuba.security.global.UserSession;

import javax.inject.Inject;
import java.util.List;

@UiController("vault_IntegrationPlatformPie")
@UiDescriptor("Integration-Platform-Pie.xml")
@DashboardWidget(name="Integration Platform Pie")
public class IntegrationPlatformPie extends ScreenFragment implements RefreshableWidget {

    @Inject
    private UserSession userSession;

    UserSessionSource uss = AppBeans.get(UserSessionSource.class);
    @Inject
    private KeyValueCollectionLoader integrationPlatformLoader;
    @Inject
    private PieChart integrationPlatformPieChart;
    @Inject
    private Messages messages;


    @Subscribe
    public void onInit(InitEvent event) {

        setData();
        integrationPlatformPieChart.addLabels(new Label().setText("Platform wise Integrations").setX("10").setY("10").setSize(11).setBold(false).setColor(Color.DARKBLUE)).setMarginTop(0).setMarginBottom(0);
    }

    private void setData(){
        if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_administrator"))){
            integrationPlatformLoader.setQuery("select e.intMstPlatform.mstCode,count(e.intAsset) from vault_Integration e where (e.intWorkflowStatus = 'AP') group by e.intMstPlatform.mstCode");
            integrationPlatformLoader.load();
        }else if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_user"))){
            integrationPlatformLoader.setQuery("select e.intMstPlatform.mstCode,count(e.intAsset) from vault_Integration e where (e.intWorkflowStatus = 'AP') group by e.intMstPlatform.mstCode");
            integrationPlatformLoader.load();
        }else{
            integrationPlatformLoader.setQuery("select e.intMstPlatform.mstCode,count(e.intAsset) from vault_Integration e where e.intWorkflowStatus = 'AP' group by e.intMstPlatform.mstCode");
            integrationPlatformLoader.load();
        }

        KeyValueCollectionContainer keyValueCollectionLoader = integrationPlatformLoader.getContainer();
        List<KeyValueEntity> list1 = keyValueCollectionLoader.getItems();

        ListDataProvider dataProvider = new ListDataProvider();

        for (KeyValueEntity e : list1){

            String color = null;
            if("SAP".equals(e.getValue("Platform"))){
                color = "#fc2c03";
            }
            if("APIGEE".equals(e.getValue("Platform"))){
                color = "#407ab8";
            }
            if("WM".equals(e.getValue("Platform"))){
                color = "#52b840";
            }
            if("MULE".equals(e.getValue("Platform"))){
                color = "#e6db12";
            }
            if("IBM".equals(e.getValue("Platform"))){
                color = "#DEB887";
            }
            if("AZURE".equals(e.getValue("Platform"))){
                color = "#FFE5CC";
            }
            if("ORACLE".equals(e.getValue("Platform"))){
                color = "#8A2BE2";
            }
            if("BOOMI".equals(e.getValue("Platform"))){
                color = "#FF7F50";
            }
            if("CLOUDTRAIL".equals(e.getValue("Platform"))){
                color = "#E9967A";
            }
            dataProvider.addItem(new MapDataItem().add("Platform", e.getValue("Platform")).add("Count", e.getValue("Count")).add("color", color));
        }

        integrationPlatformPieChart.setDataProvider(dataProvider);
    }
    @Override
    public void refresh(DashboardEvent dashboardEvent) {
        setData();
        integrationPlatformPieChart.repaint();
    }
}