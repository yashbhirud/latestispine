package com.capgemini.vault.web.screens.dashboard;

import com.haulmont.addon.dashboard.web.annotation.DashboardWidget;
import com.haulmont.addon.dashboard.web.events.DashboardEvent;
import com.haulmont.addon.dashboard.web.widget.RefreshableWidget;
import com.haulmont.charts.gui.amcharts.model.Color;
import com.haulmont.charts.gui.amcharts.model.Label;
import com.haulmont.charts.gui.components.charts.PieChart;
import com.haulmont.charts.gui.data.ListDataProvider;
import com.haulmont.charts.gui.data.MapDataItem;
import com.haulmont.cuba.core.entity.KeyValueEntity;
import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.core.global.UserSessionSource;
import com.haulmont.cuba.gui.model.KeyValueCollectionContainer;
import com.haulmont.cuba.gui.model.KeyValueCollectionLoader;
import com.haulmont.cuba.gui.screen.ScreenFragment;
import com.haulmont.cuba.gui.screen.Subscribe;
import com.haulmont.cuba.gui.screen.UiController;
import com.haulmont.cuba.gui.screen.UiDescriptor;
import com.haulmont.cuba.security.global.UserSession;

import javax.inject.Inject;
import java.util.List;

@UiController("vault_IntegrationStatusPie")
@UiDescriptor("Integration-Status-Pie.xml")
@DashboardWidget(name="Integration Status Pie")
public class IntegrationStatusPie extends ScreenFragment implements RefreshableWidget {

    @Inject
    private UserSession userSession;

    UserSessionSource uss = AppBeans.get(UserSessionSource.class);

    @Inject
    private PieChart integrationStatusPieChart;
    @Inject
    private KeyValueCollectionLoader integrationStatusLoader;
    @Inject
    private Messages messages;

    @Subscribe
    public void onInit(InitEvent event) {

        setData();
        integrationStatusPieChart.addLabels(new Label().setText("SDLC Status wise Integrations").setX("30").setY("20").setSize(12).setBold(false).setColor(Color.DARKBLUE));
    }

    private void setData(){

        if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_administrator"))){
            integrationStatusLoader.setQuery("select e.intMstSdlcStatus.mstCode,count(e.intAsset) from vault_Integration e where (e.intWorkflowStatus = 'AP') group by e.intMstSdlcStatus.mstCode");
            integrationStatusLoader.load();
        }else if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_user"))){
            integrationStatusLoader.setQuery("select e.intMstSdlcStatus.mstCode,count(e.intAsset) from vault_Integration e where (e.intWorkflowStatus = 'AP') group by e.intMstSdlcStatus.mstCode");
            integrationStatusLoader.load();
        }else{
            integrationStatusLoader.setQuery("select e.intMstSdlcStatus.mstCode,count(e.intAsset) from vault_Integration e where e.intWorkflowStatus = 'AP' group by e.intMstSdlcStatus.mstCode");
            integrationStatusLoader.load();
        }

        KeyValueCollectionContainer keyValueCollectionLoader = integrationStatusLoader.getContainer();
        List<KeyValueEntity> list1 = keyValueCollectionLoader.getItems();

        ListDataProvider dataProvider = new ListDataProvider();

        for (KeyValueEntity e : list1){

            String color = null;
            if("Proposed".equals(e.getValue("Status"))){
                color = "#fc2c03";
            }
            if("Planned".equals(e.getValue("Status"))){
                color = "#FF7F50";
            }
            if("Designed".equals(e.getValue("Status"))){
                color = "#52b840";
            }
            if("Built".equals(e.getValue("Status"))){
                color = "#e6db12";
            }
            if("Released".equals(e.getValue("Status"))){
                color = "#DEB887";
            }
            dataProvider.addItem(new MapDataItem().add("Status", e.getValue("Status")).add("Count", e.getValue("Count")).add("color", color));
        }

        integrationStatusPieChart.setDataProvider(dataProvider);
    }

    @Override
    public void refresh(DashboardEvent dashboardEvent) {
        setData();
        integrationStatusPieChart.repaint();
    }
}