package com.capgemini.vault.web.screens.dashboard;

import com.capgemini.vault.service.iAssetCountService;
import com.haulmont.addon.dashboard.web.annotation.DashboardWidget;
import com.haulmont.addon.dashboard.web.events.DashboardEvent;
import com.haulmont.addon.dashboard.web.widget.RefreshableWidget;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.gui.components.HBoxLayout;
import com.haulmont.cuba.gui.components.Label;
import com.haulmont.cuba.gui.screen.ScreenFragment;
import com.haulmont.cuba.gui.screen.Subscribe;
import com.haulmont.cuba.gui.screen.UiController;
import com.haulmont.cuba.gui.screen.UiDescriptor;
import com.haulmont.cuba.security.global.UserSession;

import javax.inject.Inject;
import java.util.Map;

@UiController("vault_TotalAsset")
@UiDescriptor("total-asset.xml")
@DashboardWidget(name="TotalAsset")
public class TotalAsset extends ScreenFragment implements RefreshableWidget {
    @Inject
    private HBoxLayout buVbox;
    @Inject
    private iAssetCountService assetCountService;
    @Inject
    private Label<String> apivalue;
    @Inject
    private Label<String> intValue;
    @Inject
    private Label<String> buApiLabel;
    @Inject
    private Label<String> buApiValue;
    @Inject
    private Label<String> buIntLabel;
    @Inject
    private Label<String> buIntValue;
    @Inject
    private UserSession userSession;
    @Inject
    private Messages messages;
    @Inject
    private Label<String> myIntLabelValue;
    @Inject
    private Label<String> myIntLabel;
    @Inject
    private Label<String> myApiValue;
    @Inject
    private Label<String> myApiLabel;
    @Inject
    private Label<String> buLabel;

    @Subscribe
    public void onInit(InitEvent event) {
        setdata();
        setUserData();
    }

    private void setdata(){
        Map<String,Integer> valueMap = assetCountService.getTotalCount(false,null);
        if(!userSession.getRoles().contains(messages.getMessage(getClass(),"role.company_administrator"))){
            setBuData();
            buVbox.setVisible(true);
        }

        if(!valueMap.isEmpty()){
            if(valueMap.get("Integration") == null){
                intValue.setValue("0");
            }else{
                intValue.setValue(String.valueOf(valueMap.get("Integration")));
            }
            if(valueMap.get("Api") == null){
                apivalue.setValue("0");
            }else{
                apivalue.setValue(String.valueOf(valueMap.get("Api")));
            }
        }
    }

    private void setBuData(){
        Map<String,Integer> valueApiMap = assetCountService.getTotalCount(true,userSession.getUser());
        buLabel.setValue(userSession.getUser().getGroup().getName());
        if(!valueApiMap.isEmpty()){
            if(valueApiMap.get("Integration") == null){
                buIntValue.setValue("0");
            }else{
                buIntValue.setValue(String.valueOf(valueApiMap.get("Integration")));
            }
            if(valueApiMap.get("Api") == null){
                buApiValue.setValue("0");
            }else{
                buApiValue.setValue(String.valueOf(valueApiMap.get("Api")));
            }
        }
    }

    private void setUserData(){

        Map<String,Integer> valueApiMap = assetCountService.getTotalCount(false,userSession.getUser());
        if(!valueApiMap.isEmpty()){
            if(valueApiMap.get("Integration") == null){
                myIntLabelValue.setValue("0");
            }else{
                myIntLabelValue.setValue(String.valueOf(valueApiMap.get("Integration")));
            }
            if(valueApiMap.get("Api") == null){
                myApiValue.setValue("0");
            }else{
                myApiValue.setValue(String.valueOf(valueApiMap.get("Api")));
            }
        }
    }
    @Override
    public void refresh(DashboardEvent dashboardEvent) {
        setdata();
        setBuData();
    }
}