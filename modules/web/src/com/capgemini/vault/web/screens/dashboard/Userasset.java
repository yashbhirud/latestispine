package com.capgemini.vault.web.screens.dashboard;

import com.capgemini.vault.service.iAssetCountService;
import com.haulmont.addon.dashboard.web.annotation.DashboardWidget;
import com.haulmont.cuba.gui.components.Label;
import com.haulmont.cuba.gui.screen.ScreenFragment;
import com.haulmont.cuba.gui.screen.Subscribe;
import com.haulmont.cuba.gui.screen.UiController;
import com.haulmont.cuba.gui.screen.UiDescriptor;
import com.haulmont.cuba.security.global.UserSession;

import javax.inject.Inject;
import java.util.Map;

@UiController("vault_Userasset")
@UiDescriptor("userAsset.xml")
@DashboardWidget(name="UserAsset")
public class Userasset extends ScreenFragment {
    @Inject
    private Label<String> apiLabel;
    @Inject
    private Label<String> apiLabelValue;
    @Inject
    private Label<String> intLabel;
    @Inject
    private Label<String> intLabelValue;
    @Inject
    private iAssetCountService assetCountService;
    @Inject
    private UserSession userSession;

    @Subscribe
    public void onInit(InitEvent event) {
        setData();
    }

    private void setData(){

        Map<String,Integer> valueApiMap = assetCountService.getTotalCount(false,userSession.getUser());
        intLabel.setValue(userSession.getUser().getName()+"-"+"Integration");
        apiLabel.setValue(userSession.getUser().getName()+"-"+"Api");
        if(!valueApiMap.isEmpty()){
            if(valueApiMap.get("Integration") == null){
                intLabelValue.setValue("0");
            }else{
                intLabelValue.setValue(String.valueOf(valueApiMap.get("Integration")));
            }
            if(valueApiMap.get("Api") == null){
                apiLabelValue.setValue("0");
            }else{
                apiLabelValue.setValue(String.valueOf(valueApiMap.get("Api")));
            }
        }

    }
}