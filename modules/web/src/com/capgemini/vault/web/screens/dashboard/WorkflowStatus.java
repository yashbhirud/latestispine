package com.capgemini.vault.web.screens.dashboard;

import com.haulmont.addon.dashboard.web.annotation.DashboardWidget;
import com.haulmont.addon.dashboard.web.events.DashboardEvent;
import com.haulmont.addon.dashboard.web.widget.RefreshableWidget;
import com.haulmont.charts.gui.amcharts.model.Color;
import com.haulmont.charts.gui.amcharts.model.Label;
import com.haulmont.charts.gui.components.charts.PieChart;
import com.haulmont.charts.gui.data.ListDataProvider;
import com.haulmont.charts.gui.data.MapDataItem;
import com.haulmont.cuba.core.entity.KeyValueEntity;
import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.core.global.UserSessionSource;
import com.haulmont.cuba.gui.model.KeyValueCollectionContainer;
import com.haulmont.cuba.gui.model.KeyValueCollectionLoader;
import com.haulmont.cuba.gui.screen.ScreenFragment;
import com.haulmont.cuba.gui.screen.Subscribe;
import com.haulmont.cuba.gui.screen.UiController;
import com.haulmont.cuba.gui.screen.UiDescriptor;
import com.haulmont.cuba.security.global.UserSession;

import javax.inject.Inject;
import java.util.List;

@UiController("vault_WorkflowStatus")
@UiDescriptor("workflow-status.xml")
@DashboardWidget(name="Workflow Pie")
public class WorkflowStatus extends ScreenFragment implements RefreshableWidget {
    @Inject
    private PieChart apiPieChart;
    @Inject
    private KeyValueCollectionLoader apiLoader;
    @Inject
    private KeyValueCollectionLoader intLoader;
    @Inject
    private PieChart intPieChart;

    @Inject
    private UserSession userSession;

    UserSessionSource uss = AppBeans.get(UserSessionSource.class);
    @Inject
    private Messages messages;

    @Subscribe
    public void onInit(InitEvent event) {
        setIntData();
        intPieChart.addLabels(new Label().setText("Integration").setX("10").setY("10").setSize(11).setBold(false).setColor(Color.DARKBLUE)).setMarginTop(0).setMarginBottom(0);
        setApiData();
        apiPieChart.addLabels(new Label().setText("API").setX("10").setY("10").setSize(11).setBold(false).setColor(Color.DARKBLUE)).setMarginTop(0).setMarginBottom(0);
    }

    private void setIntData(){
        if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_administrator"))){
            intLoader.setQuery("select " +
                    "(case \n" +
                    "                when (e.intWorkflowStatus = 'DT') then 'Staging' \n" +
                    "                when (e.intWorkflowStatus = 'AP') then 'Main' \n" +
                    "                when (e.intWorkflowStatus = 'PA') then 'Pending' \n" +
                    "                when (e.intWorkflowStatus = 'ER') then 'Error' \n" +
                    "                else '' end), count(e.intAsset) from vault_Integration e where (e.intLob =:grp or e.intWorkflowStatus='AP') group by e.intWorkflowStatus");
            intLoader.setParameter("grp", uss.getUserSession().getUser().getGroup().getName());
            intLoader.load();
        }else if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_user"))){
            intLoader.setQuery("select " +
                    "(case \n" +
                    "                when (e.intWorkflowStatus = 'DT') then 'Staging' \n" +
                    "                when (e.intWorkflowStatus = 'AP') then 'Main' \n" +
                    "                when (e.intWorkflowStatus = 'PA') then 'Pending' \n" +
                    "                when (e.intWorkflowStatus = 'ER') then 'Error' \n" +
                    "                else '' end), count(e.intAsset) from vault_Integration e where (e.createdBy = :user or e.intWorkflowStatus='AP') group by e.intWorkflowStatus");
            intLoader.setParameter("user", uss.getUserSession().getUser().getLogin());
            intLoader.load();
        }else{
            intLoader.setQuery("select " +
                    "(case \n" +
                    "                when (e.intWorkflowStatus = 'DT') then 'Staging' \n" +
                    "                when (e.intWorkflowStatus = 'AP') then 'Main' \n" +
                    "                when (e.intWorkflowStatus = 'PA') then 'Pending' \n" +
                    "                when (e.intWorkflowStatus = 'ER') then 'Error' \n" +
                    "                else '' end), count(e.intAsset) from vault_Integration e group by e.intWorkflowStatus");
            intLoader.load();
        }
        KeyValueCollectionContainer keyValueCollectionLoader = intLoader.getContainer();
        List<KeyValueEntity> list1 = keyValueCollectionLoader.getItems();

        ListDataProvider dataProvider = new ListDataProvider();

        for (KeyValueEntity e : list1){

            String color = null;
            if(e.getValue("intWorkflowStatus").equals("Error")){
                color = "#fc2c03";
            }
            if(e.getValue("intWorkflowStatus").equals("Staging")){
                color = "#407ab8";
            }
            if(e.getValue("intWorkflowStatus").equals("Main")){
                color = "#52b840";
            }
            if(e.getValue("intWorkflowStatus").equals("Pending")){
                color = "#e6db12";
            }
            dataProvider.addItem(new MapDataItem().add("intWorkflowStatus", e.getValue("intWorkflowStatus")).add("count", e.getValue("count")).add("color", color));
        }

        intPieChart.setDataProvider(dataProvider);
    }

    private void setApiData(){
        if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_administrator"))){
            apiLoader.setQuery("select (case when (e.apiWorkflowStatus = 'DT') then 'Staging'" +
                    " when (e.apiWorkflowStatus = 'AP') then 'Main' " +
                    "when (e.apiWorkflowStatus = 'PA') then 'Pending' " +
                    "when (e.apiWorkflowStatus = 'ER') then 'Error' " +
                    "else '' end),count(e.api) from vault_Api e where (e.apiLob =:grp or e.apiWorkflowStatus='AP') group by e.apiWorkflowStatus");
            apiLoader.setParameter("grp", uss.getUserSession().getUser().getGroup().getName());
            apiLoader.load();
        }else if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_user"))){
            apiLoader.setQuery("select (case when (e.apiWorkflowStatus = 'DT') then 'Staging'" +
                    " when (e.apiWorkflowStatus = 'AP') then 'Main' " +
                    "when (e.apiWorkflowStatus = 'PA') then 'Pending' " +
                    "when (e.apiWorkflowStatus = 'ER') then 'Error' " +
                    "else '' end),count(e.api) from vault_Api e where (e.createdBy = :user or e.apiWorkflowStatus='AP') group by e.apiWorkflowStatus");
            apiLoader.setParameter("user", uss.getUserSession().getUser().getLogin());
            apiLoader.load();
        }else{
            apiLoader.setQuery("select (case when (e.apiWorkflowStatus = 'DT') then 'Staging'" +
                    " when (e.apiWorkflowStatus = 'AP') then 'Main' " +
                    "when (e.apiWorkflowStatus = 'PA') then 'Pending' " +
                    "when (e.apiWorkflowStatus = 'ER') then 'Error' " +
                    "else '' end),count(e.api) from vault_Api e group by e.apiWorkflowStatus");
            apiLoader.load();
        }

        KeyValueCollectionContainer keyValueCollectionLoader = apiLoader.getContainer();
        List<KeyValueEntity> list1 = keyValueCollectionLoader.getItems();

        ListDataProvider dataProvider = new ListDataProvider();

        for (KeyValueEntity e : list1){

            String color = null;
            if(e.getValue("Status").equals("Error")){
                color = "#fc2c03";
            }
            if(e.getValue("Status").equals("Staging")){
                color = "#407ab8";
            }
            if(e.getValue("Status").equals("Main")){
                color = "#52b840";
            }
            if(e.getValue("Status").equals("Pending")){
                color = "#e6db12";
            }
            dataProvider.addItem(new MapDataItem().add("Status", e.getValue("Status")).add("Count", e.getValue("Count")).add("color", color));
        }

        apiPieChart.setDataProvider(dataProvider);
    }

    @Override
    public void refresh(DashboardEvent dashboardEvent) {
        setIntData();
        setApiData();
        apiPieChart.repaint();
        intPieChart.repaint();
    }
}