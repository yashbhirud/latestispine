package com.capgemini.vault.web.screens.vaultapi;

import com.capgemini.vault.entity.Status;
import com.capgemini.vault.entity.VaultMaster;
import com.haulmont.cuba.gui.components.HasValue;
import com.haulmont.cuba.gui.components.TextInputField;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.capgemini.vault.entity.VaultApi;

import javax.inject.Inject;

@UiController("vault_Api.browse")
@UiDescriptor("vault-api-browse.xml")
@LookupComponent("vaultApisTable")
@LoadDataBeforeShow
public class VaultApiBrowse extends StandardLookup<VaultApi> {
    @Inject
    private CollectionLoader<VaultApi> apiDetailsesDl;

    @Subscribe("textFieldApi")
    public void onTextFieldApiTextChange(TextInputField.TextChangeEvent event) {
        if (event.getText() != null) {
            apiDetailsesDl.setParameter("name", "(?i)%" + event.getText() + "%");
        } else {
            apiDetailsesDl.removeParameter("name");
        }
        apiDetailsesDl.load();
    }

    @Subscribe("pfLookupField")
    public void onPfLookupFieldValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            apiDetailsesDl.setParameter("platform", event.getValue());
        } else {
            apiDetailsesDl.removeParameter("platform");
        }
        apiDetailsesDl.load();
    }

    @Subscribe("buLookupField")
    public void onBuLookupFieldValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            apiDetailsesDl.setParameter("bu", event.getValue());
        } else {
            apiDetailsesDl.removeParameter("bu");
        }
        apiDetailsesDl.load();
    }

    @Subscribe("ownerField")
    public void onOwnerFieldValueChange(HasValue.ValueChangeEvent<String> event) {
        if (event.getValue() != null) {
            apiDetailsesDl.setParameter("owner", "(?i)%" + event.getValue() + "%");
        } else {
            apiDetailsesDl.removeParameter("owner");
        }
        apiDetailsesDl.load();
    }

    @Subscribe("patternField")
    public void onPatternFieldValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            apiDetailsesDl.setParameter("pattern",  event.getValue());
        } else {
            apiDetailsesDl.removeParameter("pattern");
        }
        apiDetailsesDl.load();
    }

    @Subscribe("sdlcField")
    public void onSdlcFieldValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            apiDetailsesDl.setParameter("sdlc",  event.getValue());
        } else {
            apiDetailsesDl.removeParameter("sdlc");
        }
        apiDetailsesDl.load();
    }

    @Subscribe("lookupFieldStatus")
    public void onLookupFieldStatusValueChange(HasValue.ValueChangeEvent<Status> event) {
        if (event.getValue() != null) {
            apiDetailsesDl.setParameter("status",event.getValue());
        } else {
            apiDetailsesDl.removeParameter("status");
        }
        apiDetailsesDl.load();
    }
}