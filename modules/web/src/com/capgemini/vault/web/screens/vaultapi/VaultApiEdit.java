package com.capgemini.vault.web.screens.vaultapi;

import com.capgemini.vault.entity.Status;
import com.capgemini.vault.entity.VaultIntegration;
import com.capgemini.vault.entity.VaultMaster;
import com.capgemini.vault.service.iAssociatedDetailsService;
import com.capgemini.vault.service.iLinkIntApiDetailsService;
import com.capgemini.vault.web.screens.vaultintegrationapi.VaultApiIntegrationEdit;
import com.haulmont.cuba.core.entity.Entity;
import com.haulmont.cuba.core.global.CommitContext;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.gui.Dialogs;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.ScreenBuilders;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.model.DataContext;
import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.screen.*;
import com.capgemini.vault.entity.VaultApi;
import com.haulmont.cuba.security.global.UserSession;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@UiController("vault_Api.edit")
@UiDescriptor("vault-api-edit.xml")
@EditedEntityContainer("vaultApiDc")
@LoadDataBeforeShow
public class VaultApiEdit extends StandardEditor<VaultApi> {

    @Inject
    private ScreenBuilders screenBuilders;
    /*@Inject
    private InstanceContainer<VaultApi> apiDetailsDc;*/

    @Inject
    private  InstanceContainer<VaultApi> vaultApiDc;

    private List<VaultIntegration> associatedIntDetails;

    @Inject
    private iLinkIntApiDetailsService linkIntApiDetailsService;

    @Inject
    private iAssociatedDetailsService iAssociatedDetailsService;

    @Inject
    private TextField<String> apiIdField;

    private String screenName;
    @Inject
    private TextField<String> apiNameField;
    @Inject
    private LookupField<VaultMaster> apiRegionField;
    @Inject
    private LookupField<VaultMaster> apiBrandOrBuField;
    @Inject
    private LookupField<VaultMaster> apiPlatformField;
    @Inject
    private TextField<String> apiPlatformVersionField;
    @Inject
    private LookupField<VaultMaster> apiDesignPatternField;
    @Inject
    private LookupField<VaultMaster> apiProcessingTypeField;
    @Inject
    private LookupField<VaultMaster> apiOpsField;
    @Inject
    private LookupField<VaultMaster> apiSrcProtocolField;
    @Inject
    private LookupField<VaultMaster> apiDestPayloadTypeField;
    @Inject
    private LookupField<VaultMaster> apiSrcPayloadTypeField;
    @Inject
    private LookupField<VaultMaster> apiDestProtocolField;
    @Inject
    private LookupField<VaultMaster> apiPayloadSecurClassField;
    @Inject
    private TextField<String> apiBusinessOwnerField;
    @Inject
    private LookupField<VaultMaster> apiStatusField;
    @Inject
    private Notifications notifications;
    @Inject
    private UserSession userSession;
    @Inject
    private Dialogs dialogs;

    private boolean isEditable;
    @Inject
    private Messages messages;
    @Inject
    private DataManager dataManager;




    public void onAssociateBtnClick() {

        Map<String, Object> params = new HashMap<>();
        params.put("api", vaultApiDc.getItem());
        params.put("screen", screenName);

        VaultApiIntegrationEdit editScreen = screenBuilders.screen(this)
                .withScreenClass(VaultApiIntegrationEdit.class)
                .withOptions(new MapScreenOptions(params))
                .withLaunchMode(OpenMode.DIALOG)
                .withAfterCloseListener(afterCloseEvent -> {
                    VaultApiIntegrationEdit otherScreen = afterCloseEvent.getScreen();
                    if (afterCloseEvent.closedWith(StandardOutcome.COMMIT)) {
                        associatedIntDetails = otherScreen.getResult();
                    }
                    getScreenData().loadAll();
                })
                .build();
        editScreen.show();
    }

    @Subscribe(target = Target.DATA_CONTEXT)
    public void onPreCommit(DataContext.PreCommitEvent event) {
        if(!event.getModifiedInstances().isEmpty()){
            VaultApi vaultApi = getEditedEntity();
            vaultApi.setApiWorkflowStatus(Status.DRAFT);
        }
    }

    @Override
    protected void commitAndClose(Action.ActionPerformedEvent event) {
        VaultApi apiDetails = vaultApiDc.getItem();
        List<String> validations = new ArrayList<>();
        List<String> nullColumns=checkNullColumns(apiDetails);
        List<String> incorrectAssests = checkAssetIDs(apiDetails);
        String caption = null;
        if(!nullColumns.isEmpty() && !incorrectAssests.isEmpty()){
            caption = "Missing Mandatory Columns and Incorrect Asset Ids:";
            validations.addAll(nullColumns);
            validations.addAll(incorrectAssests);
        }else if(!incorrectAssests.isEmpty()){
            caption = "Asset Ids not found:";
            validations.addAll(incorrectAssests);
        }else{
            caption = "Missing Mandatory Columns";
            validations.addAll(nullColumns);
        }
        String delim = "\n";
        String res = String.join(delim, validations);
        if(!validations.isEmpty()){
            dialogs.createOptionDialog()
                    .withCaption(caption)
                    .withMessage(res+"\n\nDo you still want to save API?")
                    .withType(Dialogs.MessageType.CONFIRMATION)
                    .withActions(
                            new DialogAction(DialogAction.Type.YES).withHandler(e -> {
                                apiDetails.setApiWorkflowStatus(Status.ERROR);
                                super.commit(event);
                                if(associatedIntDetails!=null){
                                    linkIntApiDetailsService.commitApiIntDetails(associatedIntDetails,vaultApiDc.getItem());
                                }
                                close(WINDOW_CLOSE_ACTION);

                            }),
                            new DialogAction(DialogAction.Type.NO),
                            new DialogAction(DialogAction.Type.CLOSE)
                    )
                    .show();
        }
        else {
            if (apiDetails.getApiWorkflowStatus().equals(Status.ERROR)) {
                    apiDetails.setApiWorkflowStatus(Status.DRAFT);
            }
            super.commit(event);
            if(associatedIntDetails!=null){
                linkIntApiDetailsService.commitApiIntDetails(associatedIntDetails,vaultApiDc.getItem());
            }
            close(WINDOW_CLOSE_ACTION);
        }

    }
    @Subscribe
    public void onInit(InitEvent event) {

        ScreenOptions screenOptions = event.getOptions();
        if(screenOptions instanceof MapScreenOptions){
            screenName = (String) ((MapScreenOptions) screenOptions).getParams().get("screen");
            isEditable = (boolean)((MapScreenOptions) screenOptions).getParams().get("editable");
        }
        if(screenName.equals(messages.getMessage(getClass(),"screen.main"))){
            apiIdField.setRequired(true);
            apiNameField.setRequired(true);
            apiRegionField.setRequired(true);
            apiBrandOrBuField.setRequired(true);
            apiPlatformField.setRequired(true);
            apiPlatformVersionField.setRequired(true);
            apiDesignPatternField.setRequired(true);
            apiProcessingTypeField.setRequired(true);
            apiOpsField.setRequired(true);
            apiSrcProtocolField.setRequired(true);
            apiDestPayloadTypeField.setRequired(true);
            apiSrcPayloadTypeField.setRequired(true);
            apiDestProtocolField.setRequired(true);
            apiPayloadSecurClassField.setRequired(true);
            apiBusinessOwnerField.setRequired(true);
            apiStatusField.setRequired(true);
        }
        if(!isEditable){
            apiIdField.setEditable(false);
        }
    }
    public List<String> checkNullColumns (VaultApi apiDetails){
        List<String> nullColumns = new ArrayList<>();
        if (apiDetails.getApi() == null) {
            nullColumns.add("Api ID");
        }
        if (apiDetails.getApiName() == null) {
            nullColumns.add("Name");
        }
        if (apiDetails.getApiMstRegion() == null) {
            nullColumns.add("Region");
        }
        if (apiDetails.getApiMstBizUnit() == null) {
            nullColumns.add("Business Unit");
        }
        if (apiDetails.getApiMstPlatform() == null) {
            nullColumns.add("Platform");
        }
        if (apiDetails.getApiPlatformVersion() == null) {
            nullColumns.add("Platform Version");
        }
        if (apiDetails.getApiMstOps() == null) {
            nullColumns.add("Operations");
        }
        if (apiDetails.getApiMstDesignPattern() == null) {
            nullColumns.add("Design Pattern");
        }
        if (apiDetails.getApiMstProcessType() == null) {
            nullColumns.add("Processing Type");
        }
        if (apiDetails.getApiMstSrcProtocol() == null) {
            nullColumns.add("Source Protocol");
        }
        if (apiDetails.getApiMstSrcPayloadType() == null) {
            nullColumns.add("Source Payload Type");
        }
        if (apiDetails.getApiMstDestProtocol() == null) {
            nullColumns.add("Destination Protocol");
        }
        if (apiDetails.getApiMstDestPayloadType() == null) {
            nullColumns.add("Destination Payload Type");
        }
        if (apiDetails.getApiMstPayloadSecurClass() == null) {
            nullColumns.add("Security Classification");
        }
        if (apiDetails.getApiBizOwner() == null) {
            nullColumns.add("Business Owner");
        }
        if (apiDetails.getApiMstSdlcStatus() == null) {
            nullColumns.add("SDLC Status");
        }
        return nullColumns;
    }

    public List<String> checkAssetIDs (VaultApi apiDetails){
        List<String> incorrectassest = new ArrayList<>();
        String assetIds = apiDetails.getIntAsset();
        if(StringUtils.isNotBlank(assetIds)){
            String[] assetIdArr = assetIds.split(",");
            List<String> assetList = iAssociatedDetailsService.getAssetList();
            for(String id : assetIdArr){
                if(!assetList.contains(id)){
                    incorrectassest.add(id);
                }
            }
        }
        return incorrectassest;
    }
}