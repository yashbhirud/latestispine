package com.capgemini.vault.web.screens.vaulterrsummary;

import com.capgemini.vault.entity.VaultUploadSummary;
import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.core.global.UserSessionSource;
import com.haulmont.cuba.gui.components.HasValue;
import com.haulmont.cuba.gui.components.TextInputField;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.capgemini.vault.entity.VaultErrSummary;
import com.haulmont.cuba.security.entity.User;
import com.haulmont.cuba.security.global.UserSession;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.Map;

@UiController("vault_ErrSummary.browse")
@UiDescriptor("vault-err-summary-browse.xml")
@LookupComponent("vaultErrSummariesTable")
@LoadDataBeforeShow
public class VaultErrSummaryBrowse extends StandardLookup<VaultErrSummary> {
    @Inject
    private CollectionLoader<VaultErrSummary> errorSummariesDl;

    @Inject
    private UserSession userSession;

    UserSessionSource uss = AppBeans.get(UserSessionSource.class);
    @Inject
    private Messages messages;

    @Subscribe
    public void onInit(InitEvent event) {
        if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_administrator"))){
            errorSummariesDl.setQuery("select e from vault_ErrSummary e where e.createdBy IN (select u.login from sec$User u where u.group.id=:group)");
            errorSummariesDl.setParameter("group", uss.getUserSession().getUser().getGroup().getId());
        }else if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_user"))){
            errorSummariesDl.setQuery("select e from vault_ErrSummary e where e.createdBy = :user");
            errorSummariesDl.setParameter("user",uss.getUserSession().getUser().getLogin());
        }
        else{
            errorSummariesDl.setQuery("select e from vault_ErrSummary e");
        }
        errorSummariesDl.load();
    }

    @Subscribe("userFilter")
    public void onUserFilterValueChange(HasValue.ValueChangeEvent<User> event) {

        if (event.getValue() != null) {
            errorSummariesDl.setParameter("user",event.getValue().getLogin());
        } else {
            errorSummariesDl.removeParameter("user");
        }
        errorSummariesDl.load();

    }
    @Subscribe("rowFilter")
    public void onRowFilterValueChange(HasValue.ValueChangeEvent<String> event) {
        if (event.getValue() != null) {
            BigDecimal rowNo = new BigDecimal(event.getValue());
            errorSummariesDl.setParameter("row",rowNo);
        } else {
            errorSummariesDl.removeParameter("row");
        }
        errorSummariesDl.load();
    }

    @Subscribe("fileFilter")
    public void onFileFilterTextChange(TextInputField.TextChangeEvent event) {
        if (event.getText() != null) {
            errorSummariesDl.setParameter("file","(?i)%" + event.getText() + "%");
        } else {
            errorSummariesDl.removeParameter("file");
        }
        errorSummariesDl.load();

    }

    @Subscribe("columnFilter")
    public void onColumnFilterValueChange(HasValue.ValueChangeEvent<String> event) {

        if (event.getValue() != null) {
            errorSummariesDl.setParameter("col","(?i)%" +event.getValue() + "%");
        } else {
            errorSummariesDl.removeParameter("col");
        }
        errorSummariesDl.load();

    }

    public void setDs(VaultUploadSummary upl){
        errorSummariesDl.setQuery("select e from vault_ErrSummary e where e.errUps = :upl order by e.createTs desc");
        errorSummariesDl.setParameter("upl",upl);
        Map<String,Object> parametersMap = errorSummariesDl.getParameters();
        if(parametersMap.containsKey("group")){
            errorSummariesDl.removeParameter("group");
        }
        errorSummariesDl.load();
    }
}
