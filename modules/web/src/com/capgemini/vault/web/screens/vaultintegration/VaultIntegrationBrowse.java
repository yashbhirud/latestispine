package com.capgemini.vault.web.screens.vaultintegration;

import com.haulmont.cuba.gui.screen.*;
import com.capgemini.vault.entity.VaultIntegration;

@UiController("vault_Integration.browse")
@UiDescriptor("vault-integration-browse.xml")
@LookupComponent("vaultIntegrationsTable")
@LoadDataBeforeShow
public class VaultIntegrationBrowse extends StandardLookup<VaultIntegration> {
}