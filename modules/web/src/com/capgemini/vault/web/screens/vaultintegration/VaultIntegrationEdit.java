package com.capgemini.vault.web.screens.vaultintegration;

import com.capgemini.vault.entity.Status;
import com.capgemini.vault.entity.VaultMaster;
import com.haulmont.cuba.core.entity.Entity;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.gui.Dialogs;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.components.Action;
import com.haulmont.cuba.gui.components.DialogAction;
import com.haulmont.cuba.gui.components.LookupField;
import com.haulmont.cuba.gui.components.TextField;
import com.haulmont.cuba.gui.model.DataContext;
import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.screen.*;
import com.capgemini.vault.entity.VaultIntegration;
import com.haulmont.cuba.security.global.UserSession;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@UiController("vault_Integration.edit")
@UiDescriptor("vault-integration-edit.xml")
@EditedEntityContainer("vaultIntegrationDc")
@LoadDataBeforeShow
public class VaultIntegrationEdit extends StandardEditor<VaultIntegration> {

    @Inject
    private LookupField<Status> intWorkflowStatusField;
    @Inject
    private InstanceContainer<VaultIntegration> vaultIntegrationDc;
    @Inject
    private TextField<String> intAssetField;
    @Inject
    private TextField<String> intNameField;
    @Inject
    private LookupField<VaultMaster> intRegionField;
    @Inject
    private LookupField<VaultMaster> intBrandOrBuField;
    @Inject
    private LookupField<VaultMaster> intPlatformField;
    @Inject
    private TextField<String> intPlatformVersionField;
    @Inject
    private LookupField<VaultMaster> intDesignPatternField;
    @Inject
    private LookupField<VaultMaster> intProcessingTypeField;
    @Inject
    private LookupField<VaultMaster> intCrudField;
    @Inject
    private TextField<String> intImplPatternField;
    @Inject
    private LookupField<VaultMaster> intSrcProtocolField;
    @Inject
    private LookupField<VaultMaster> intDestPayloadTypeField;
    @Inject
    private LookupField<VaultMaster> intSrcPayloadTypeField;
    @Inject
    private LookupField<VaultMaster> intDestProtocolField;
    @Inject
    private LookupField<VaultMaster> intPayloadSecurClassField;
    @Inject
    private TextField<String> technicalSpecLocationField;
    @Inject
    private TextField<String> intBizOwnerField;
    @Inject
    private LookupField<VaultMaster> intSdlcStatusField;
    @Inject
    private Messages messages;

    public void setStatus(Status status){
        intWorkflowStatusField.setValue(status);
    }

    private String screenName;

    private boolean isEditable;

    @Inject
    private Dialogs dialogs;


    @Subscribe(target = Target.DATA_CONTEXT)
    public void onPreCommit(DataContext.PreCommitEvent event) {
        if(!event.getModifiedInstances().isEmpty()){
            VaultIntegration vaultInt = getEditedEntity();
            vaultInt.setIntWorkflowStatus(Status.DRAFT);
        }
    }

    @Override
    protected void commitAndClose(Action.ActionPerformedEvent event) {
        VaultIntegration intDetails = vaultIntegrationDc.getItem();
        List<String> nullColumns=checkNullColumns(intDetails);
        String delim = "\n";
        String res = String.join(delim, nullColumns);
        if (!nullColumns.isEmpty()) {
            dialogs.createOptionDialog()
                    .withCaption("Missing Mandatory Columns")
                    .withMessage(res+"\n\nDo you still want to save Integration?")
                    .withType(Dialogs.MessageType.CONFIRMATION)
                    .withActions(
                            new DialogAction(DialogAction.Type.YES).withHandler(e -> {
                                intDetails.setIntWorkflowStatus(Status.ERROR);
                                super.commitAndClose(event);
                            }),
                            new DialogAction(DialogAction.Type.NO),
                            new DialogAction(DialogAction.Type.CLOSE)
                    )
                    .show();
        }
        else{
            if (intDetails.getIntWorkflowStatus().equals(Status.ERROR)) {
                intDetails.setIntWorkflowStatus(Status.DRAFT);
            }
            super.commitAndClose(event);
        }

    }
    @Subscribe
    public void onInit(InitEvent event) {

        ScreenOptions screenOptions = event.getOptions();
        if(screenOptions instanceof MapScreenOptions){
            screenName = (String) ((MapScreenOptions) screenOptions).getParams().get("screen");
            isEditable = (boolean)((MapScreenOptions) screenOptions).getParams().get("editable");
        }
        if(screenName.equals(messages.getMessage(getClass(),"screen.main"))){
            intAssetField.setRequired(true);
            intNameField.setRequired(true);
            intRegionField.setRequired(true);
            intBrandOrBuField.setRequired(true);
            intPlatformField.setRequired(true);
            intPlatformVersionField.setRequired(true);
            intDesignPatternField.setRequired(true);
            intProcessingTypeField.setRequired(true);
            intCrudField.setRequired(true);
            intImplPatternField.setRequired(true);
            intSrcProtocolField.setRequired(true);
            intDestPayloadTypeField.setRequired(true);
            intSrcPayloadTypeField.setRequired(true);
            intDestProtocolField.setRequired(true);
            intPayloadSecurClassField.setRequired(true);
            technicalSpecLocationField.setRequired(true);
            intBizOwnerField.setRequired(true);
            intSdlcStatusField.setRequired(true);
        }
        if(!isEditable){
            intAssetField.setEditable(false);
        }
    }
    public List<String> checkNullColumns (VaultIntegration intDetails){
        List<String> nullColumns = new ArrayList<>();
        if (intDetails.getIntAsset() == null) {
            nullColumns.add("Asset ID");
        }
        if (intDetails.getIntName() == null) {
            nullColumns.add("Name");
        }
        if (intDetails.getIntMstRegion() == null) {
            nullColumns.add("Region");
        }
        if (intDetails.getIntMstBizUnit() == null) {
            nullColumns.add("Business Unit");
        }
        if (intDetails.getIntMstPlatform() == null) {
            nullColumns.add("Platform");
        }
        if (intDetails.getIntPlatformVersion() == null) {
            nullColumns.add("Platform Version");
        }
        if (intDetails.getIntMstOps() == null) {
            nullColumns.add("Operations");
        }
        if (intDetails.getIntMstDesignPattern() == null) {
            nullColumns.add("Design Pattern");
        }
        if (intDetails.getIntMstProcessType() == null) {
            nullColumns.add("Processing Type");
        }
        if (intDetails.getIntImplPattern() == null) {
            nullColumns.add("Implementation Pattern");
        }
        if (intDetails.getIntMstSrcProtocol() == null) {
            nullColumns.add("Source Protocol");
        }
        if (intDetails.getIntMstSrcPayloadType() == null) {
            nullColumns.add("Source Payload Type");
        }
        if (intDetails.getIntMstDestProtocol() == null) {
            nullColumns.add("Destination Protocol");
        }
        if (intDetails.getIntMstDestPayloadType() == null) {
            nullColumns.add("Destination Payload Type");
        }
        if (intDetails.getIntMstPayloadSecClass() == null) {
            nullColumns.add("Security Classification");
        }
        if (intDetails.getIntTechSpecLoc() == null) {
            nullColumns.add("Technical Specification");
        }
        if (intDetails.getIntBizOwner() == null) {
            nullColumns.add("Business Owner");
        }
        if (intDetails.getIntMstSdlcStatus() == null) {
            nullColumns.add("SDLC Status");
        }
        return nullColumns;
    }
}