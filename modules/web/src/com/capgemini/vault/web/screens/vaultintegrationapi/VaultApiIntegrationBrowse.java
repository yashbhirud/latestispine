package com.capgemini.vault.web.screens.vaultintegrationapi;

import com.capgemini.vault.entity.*;
import com.capgemini.vault.service.iLinkInvokeService;
import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.core.global.UserSessionSource;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.WindowParam;
import com.haulmont.cuba.gui.components.DataGrid;
import com.haulmont.cuba.gui.components.HasValue;
import com.haulmont.cuba.gui.export.ByteArrayDataProvider;
import com.haulmont.cuba.gui.export.ExportDisplay;
import com.haulmont.cuba.gui.export.ExportFormat;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.security.global.UserSession;

import javax.inject.Inject;
import java.util.List;

@UiController("vault_ApiIntegration.browse")
@UiDescriptor("vault-api-integration-browse.xml")
@LookupComponent("vaultIntegrationApisTable")
@LoadDataBeforeShow
public class VaultApiIntegrationBrowse extends StandardLookup<VaultIntegrationApi> {

    @WindowParam
    private String screen;

    @Inject
    private CollectionLoader<VaultIntegrationApi> intApiDetailsesDl;

    UserSessionSource uss = AppBeans.get(UserSessionSource.class);
    @Inject
    private UserSession userSession;
    @Inject
    private Messages messages;
    @Inject
    private DataGrid<VaultIntegrationApi> vaultIntegrationApisTable;
    @Inject
    private Notifications notifications;
    @Inject
    private ExportDisplay exportDisplay;
    @Inject
    private iLinkInvokeService linkInvoke;

    @Subscribe
    public void onInit(InitEvent event) {
        DataGrid.HtmlRenderer renderer1 =
                vaultIntegrationApisTable.createRenderer(DataGrid.HtmlRenderer.class);
        renderer1.setNullRepresentation("</span>");
        vaultIntegrationApisTable.getColumn("vaultIntegration.intBizOwner").setRenderer(renderer1);

        DataGrid.HtmlRenderer renderer5 =
                vaultIntegrationApisTable.createRenderer(DataGrid.HtmlRenderer.class);
        renderer5.setNullRepresentation("</span>");
        vaultIntegrationApisTable.getColumn("vaultIntegration.intOwner").setRenderer(renderer5);

        DataGrid.ClickableTextRenderer<VaultIntegrationApi> intDetailsesTableTechSpecLocRenderer = vaultIntegrationApisTable.createRenderer(DataGrid.ClickableTextRenderer.class);
        intDetailsesTableTechSpecLocRenderer.setRendererClickListener(clickableTextRendererClickEvent -> {
            boolean checkAccess = ((ExtUser) userSession.getUser()).getAllowTechSpec();
            if (!checkAccess) {
                notifications.create().withCaption("You are not authorised user to access link").show();
            }
            else{
                List<Object> list= linkInvoke.linkinvokefunction(clickableTextRendererClickEvent.getItem().getVaultIntegration().getIntTechSpecLoc());
                if (list == null) {
                    notifications.create().withCaption("Cannot open the link").show();
                }else if(list.contains("saved"))
                {
                    notifications.create().withCaption("Saved to C://git").show();
                }
                else if(!list.isEmpty()){
                    exportDisplay.show(new ByteArrayDataProvider((byte[]) list.get(0)), (String) list.get(1));
                }
            }
        });
        vaultIntegrationApisTable.getColumn("vaultIntegration.techSpecLoc").setRenderer(intDetailsesTableTechSpecLocRenderer);

        DataGrid.ClickableTextRenderer<VaultIntegrationApi> intDetailsesTableFuncSpecLocRenderer = vaultIntegrationApisTable.createRenderer(DataGrid.ClickableTextRenderer.class);
        intDetailsesTableFuncSpecLocRenderer.setRendererClickListener(clickableTextRendererClickEvent -> {
            boolean checkAccess = ((ExtUser) userSession.getUser()).getAllowFuncSpec();
            if (!checkAccess) {
                notifications.create().withCaption("You are not authorised user to access link").show();
            }
            else{
                List<Object> list= linkInvoke.linkinvokefunction(clickableTextRendererClickEvent.getItem().getVaultIntegration().getIntFuncSpecLoc());
                if (list == null) {
                    notifications.create().withCaption("Cannot open the link").show();
                }else if(list.contains("saved"))
                {
                    notifications.create().withCaption("Saved to C://git").show();
                }
                else if(!list.isEmpty()){
                    exportDisplay.show(new ByteArrayDataProvider((byte[]) list.get(0)), (String) list.get(1));
                }
            }
        });
        vaultIntegrationApisTable.getColumn("vaultIntegration.funcSpecLoc").setRenderer(intDetailsesTableFuncSpecLocRenderer);

        DataGrid.ClickableTextRenderer<VaultIntegrationApi> intDetailsesTableCodeRepoLocRenderer = vaultIntegrationApisTable.createRenderer(DataGrid.ClickableTextRenderer.class);
        intDetailsesTableCodeRepoLocRenderer.setRendererClickListener(clickableTextRendererClickEvent -> {
            boolean checkAccess = ((ExtUser) userSession.getUser()).getAllowCodeLoc();
            if (!checkAccess) {
                notifications.create().withCaption("You are not authorised user to access link").show();
            }
            else{
                List<Object> list= linkInvoke.linkinvokefunction(clickableTextRendererClickEvent.getItem().getVaultIntegration().getIntCodeRepo());
                if (list == null) {
                    notifications.create().withCaption("Cannot open the link").show();
                }
                else if(list.contains("saved"))
                {
                    notifications.create().withCaption("Saved to C://git").show();
                }
                else if(!list.isEmpty()){
                    exportDisplay.show(new ByteArrayDataProvider((byte[]) list.get(0)), (String) list.get(1));
                }
            }
        });
        vaultIntegrationApisTable.getColumn("vaultIntegration.codeRepo").setRenderer(intDetailsesTableCodeRepoLocRenderer);
    }

    @Override
    protected void initActions(InitEvent event) {
        super.initActions(event);
        if(screen.equals(messages.getMessage(getClass(),"screen.main"))){
            if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_administrator"))){
                intApiDetailsesDl.setQuery("select e from vault_IntegrationApi e where e.vaultIntegration.intWorkflowStatus = 'AP' and e.vaultIntegration.intLob = :grp");
                intApiDetailsesDl.setParameter("grp", uss.getUserSession().getUser().getGroup().getName());
            }else if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_user"))){
                intApiDetailsesDl.setQuery("select e from vault_IntegrationApi e where e.vaultIntegration.intWorkflowStatus = 'AP' and e.vaultIntegration.createdBy = :user");
                intApiDetailsesDl.setParameter("user",uss.getUserSession().getUser().getLogin());
            }else{
                intApiDetailsesDl.setQuery("select e from vault_IntegrationApi e where e.vaultIntegration.intWorkflowStatus = 'AP'");
            }
        }else{

            if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_administrator"))){
                intApiDetailsesDl.setQuery("select e from vault_IntegrationApi e where e.vaultIntegration.intLob = :grp");
                intApiDetailsesDl.setParameter("grp", uss.getUserSession().getUser().getGroup().getName());
            }else if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_user"))){
                intApiDetailsesDl.setQuery("select e from vault_IntegrationApi e where e.vaultIntegration.createdBy = :user");
                intApiDetailsesDl.setParameter("user",uss.getUserSession().getUser().getLogin());
            }else{
                intApiDetailsesDl.setQuery("select e from vault_IntegrationApi e");
            }
        }
        intApiDetailsesDl.load();
    }


    private void doSave(){
        intApiDetailsesDl.load();
    }

    @Subscribe("pfLookup")
    public void onPfLookupValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            intApiDetailsesDl.setParameter("platform", event.getValue());
        } else {
            intApiDetailsesDl.removeParameter("platform");
        }
        intApiDetailsesDl.load();
    }

    @Subscribe("buLookup")
    public void onBuLookupValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            intApiDetailsesDl.setParameter("bu", event.getValue());
        } else {
            intApiDetailsesDl.removeParameter("bu");
        }
        intApiDetailsesDl.load();

    }

    @Subscribe("bOwnerField")
    public void onBOwnerFieldValueChange(HasValue.ValueChangeEvent<String> event) {
        if (event.getValue() != null) {
            intApiDetailsesDl.setParameter("owner", "(?i)%" + event.getValue() + "%");
        } else {
            intApiDetailsesDl.removeParameter("owner");
        }
        intApiDetailsesDl.load();

    }

    @Subscribe("initField")
    public void onInitFieldValueChange(HasValue.ValueChangeEvent<String> event) {
        if (event.getValue() != null) {
            intApiDetailsesDl.setParameter("source", "(?i)%" + event.getValue() + "%");
        } else {
            intApiDetailsesDl.removeParameter("source");
        }
        intApiDetailsesDl.load();

    }

    @Subscribe("patternLookup")
    public void onPatternLookupValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            intApiDetailsesDl.setParameter("pattern", event.getValue());
        } else {
            intApiDetailsesDl.removeParameter("pattern");
        }
        intApiDetailsesDl.load();

    }

    @Subscribe("statLookup")
    public void onStatLookupValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            intApiDetailsesDl.setParameter("sdlcStatus", event.getValue());
        } else {
            intApiDetailsesDl.removeParameter("sdlcStatus");
        }
        intApiDetailsesDl.load();

    }

    @Subscribe("nameFilter")
    public void onNameFilterValueChange(HasValue.ValueChangeEvent<String> event) {
        if (event.getValue() != null) {
            intApiDetailsesDl.setParameter("name", "(?i)%" + event.getValue() + "%");
        } else {
            intApiDetailsesDl.removeParameter("name");
        }
        intApiDetailsesDl.load();
    }

    @Subscribe("lookupFieldStatus")
    public void onLookupFieldStatusValueChange(HasValue.ValueChangeEvent<Status> event) {
        if (event.getValue() != null) {
            intApiDetailsesDl.setParameter("status",event.getValue());
        } else {
            intApiDetailsesDl.removeParameter("status");
        }
        intApiDetailsesDl.load();
    }

}