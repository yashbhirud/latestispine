package com.capgemini.vault.web.screens.vaultintegrationapi;

import com.capgemini.vault.entity.VaultApi;
import com.capgemini.vault.entity.VaultIntegration;
import com.capgemini.vault.service.iLinkIntApiDetailsService;
import com.haulmont.cuba.core.global.*;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.components.Button;
import com.haulmont.cuba.gui.components.HasValue;
import com.haulmont.cuba.gui.components.TwinColumn;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.capgemini.vault.entity.VaultIntegrationApi;
import com.haulmont.cuba.security.global.UserSession;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@UiController("vault_ApiIntegration.edit")
@UiDescriptor("vault-api-integration-edit.xml")
@EditedEntityContainer("vaultIntegrationApiDc")
@LoadDataBeforeShow
public class VaultApiIntegrationEdit extends StandardEditor<VaultIntegrationApi> {
    @Inject
    private iLinkIntApiDetailsService linkIntApiDetailsService;
    @Inject
    private TwinColumn<VaultIntegration> apiIntTwinColumn;
    UserSessionSource uss = AppBeans.get(UserSessionSource.class);
    @Inject
    private MetadataTools metadataTools;
    @Inject
    private Notifications notifications;
    @Inject
    private CollectionLoader<VaultApi> apiDetailsesDl;
    @Inject
    private CollectionContainer<VaultIntegrationApi> intApiDetailsesDc;
    @Inject
    private Metadata metadata;
    @Inject
    private UserSession userSession;
    @Inject
    private CollectionLoader<VaultIntegration> intDetailsesDl;
    VaultApi apiDetails;
    @Inject
    private CollectionLoader<VaultIntegration> filterDl;

    private String screenName;

    private List<VaultIntegration> associatedIntList = new ArrayList<>();
    @Inject
    private Messages messages;

    private Boolean isBuAdmin;
    private Boolean isUser;
    private Boolean isAdmin;

    @Subscribe
    public void onInit(InitEvent event) {

        VaultIntegrationApi intApiDetails = metadata.create(VaultIntegrationApi.class);
        setEntityToEdit(intApiDetails);

        collectScreenParams(event);
        isBuAdmin=userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_administrator"));
        isUser=userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_user"));
        isAdmin=userSession.getRoles().contains(messages.getMessage(getClass(),"role.company_administrator"));
        setDataSource();
        setTwinColumnSelectedValues();
    }

    @Subscribe
    public void onAfterShow(AfterShowEvent event) {
        setTwinColumnSelectedValues();
    }



    private void collectScreenParams(InitEvent event){
        ScreenOptions options = event.getOptions();
        if (options instanceof MapScreenOptions) {
            apiDetails = (VaultApi) ((MapScreenOptions) options).getParams().get("api");
            screenName = (String) ((MapScreenOptions) options).getParams().get("screen");
        }
    }

    private void setDataSource(){
        intDetailsesDl.setQuery("select e from vault_Integration e order by e.updateTs desc, e.createTs desc");
        if(isBuAdmin) {
            intDetailsesDl.setParameter("grp", uss.getUserSession().getUser().getGroup().getName());
            intDetailsesDl.removeParameter("user");
            filterDl.setParameter("grp", uss.getUserSession().getUser().getGroup().getName());
            filterDl.removeParameter("user");
            if (screenName.equals(messages.getMessage(getClass(), "screen.main"))) {
                intDetailsesDl.setParameter("status", "AP");
                intDetailsesDl.setParameter("status", "AP");
            } else {
                intDetailsesDl.removeParameter("status");
                filterDl.removeParameter("status");
            }
        }else if(isAdmin) {
            intDetailsesDl.removeParameter("grp");
            intDetailsesDl.removeParameter("user");
            filterDl.removeParameter("grp");
            filterDl.removeParameter("user");
            if (screenName.equals(messages.getMessage(getClass(), "screen.main"))) {
                intDetailsesDl.setParameter("status", "AP");
                intDetailsesDl.setParameter("status", "AP");
            } else {
                intDetailsesDl.removeParameter("status");
                filterDl.removeParameter("status");
            }
        }
        else{
            intDetailsesDl.setParameter("grp",uss.getUserSession().getUser().getGroup().getName());
            intDetailsesDl.setParameter("user", uss.getUserSession().getUser().getLogin());
            filterDl.setParameter("user", uss.getUserSession().getUser().getLogin());
            filterDl.setParameter("grp",uss.getUserSession().getUser().getGroup().getName());
            if(screenName.equals(messages.getMessage(getClass(),"screen.main"))){
                intDetailsesDl.setParameter("status","AP");
                intDetailsesDl.setParameter("status","AP");
            }else{
                intDetailsesDl.removeParameter("status");
                filterDl.removeParameter("status");
            }

        }
        intDetailsesDl.load();
        filterDl.load();
    }

    private void setTwinColumnSelectedValues(){
        List<VaultIntegrationApi> intApiDetailsList = intApiDetailsesDc.getItems();
        List<VaultIntegration> intSpecificList = intApiDetailsList.stream().filter(e -> e.getVaultApi().equals(apiDetails))
                .map(a ->a.getVaultIntegration())
                .collect(Collectors.toList());
        apiIntTwinColumn.setValue(intSpecificList);
    }

    @Subscribe("showbutton")
    public void onShowbuttonClick(Button.ClickEvent event) {

        StringBuilder sb = new StringBuilder();
        Collection<VaultIntegration> value = apiIntTwinColumn.getValue();
        if (value == null) {
            sb.append("null");
        } else {
            for (VaultIntegration intDetail : value) {
                sb.append(metadataTools.getInstanceName(intDetail))
                        .append("\n");
            }
        }
        notifications.create()
                .withCaption(sb.toString())
                .show();
    }

    @Subscribe("okBtn")
    public void onOkBtnClick(Button.ClickEvent event) {
        if(!screenName.equals("Edit")){
            linkIntApiDetailsService.commitApiIntDetails(apiIntTwinColumn.getValue(),apiDetails);
        }
        associatedIntList = apiIntTwinColumn.getValue().stream().collect(Collectors.toList());
        close(StandardOutcome.COMMIT);
    }

    public void onCancelBtnClick() {
        //Close without saving any changes
        close(WINDOW_CLOSE_ACTION);
    }


    @Subscribe("filterLookup")
    public void onFilterLookupValueChange(HasValue.ValueChangeEvent<VaultIntegration> event) {
        if (event.getValue() != null) {
            VaultIntegration value = event.getValue();
            Collection <VaultIntegration> list = apiIntTwinColumn.getValue();
            Collection <VaultIntegration> intList = new ArrayList<>(list);
            intList.add(value);
            Collection<String> assetids = intList.stream().map(e -> e.getIntAsset()).collect(Collectors.toList());
            if(list.isEmpty()){
                intDetailsesDl.setQuery("select e from vault_Integration e where e.intAsset = '"+value.getIntAsset()+"'");
                if(isBuAdmin){
                    intDetailsesDl.removeParameter("user");
                }
                if(isAdmin){
                    intDetailsesDl.removeParameter("user");
                    intDetailsesDl.removeParameter("grp");
                }
            }else{
                String ids = assetids.stream().collect(Collectors.joining("','", "'", "'"));
                intDetailsesDl.setQuery("select e from vault_Integration e where e.intAsset IN ("+ids+")");
                intDetailsesDl.setParameter("user", uss.getUserSession().getUser().getLogin());
                if(isBuAdmin){
                    intDetailsesDl.removeParameter("user");
                }
                if(isAdmin){
                    intDetailsesDl.removeParameter("user");
                    intDetailsesDl.removeParameter("grp");
                }
            }
            intDetailsesDl.load();

        }else{
            setDataSource();
        }

    }

    public List<VaultIntegration> getResult() {
        return associatedIntList;
    }
}