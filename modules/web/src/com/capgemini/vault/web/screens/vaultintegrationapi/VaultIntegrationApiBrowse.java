package com.capgemini.vault.web.screens.vaultintegrationapi;

import com.capgemini.vault.entity.Status;
import com.capgemini.vault.entity.VaultIntegration;
import com.capgemini.vault.entity.VaultMaster;
import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.core.global.UserSessionSource;
import com.haulmont.cuba.gui.ScreenBuilders;
import com.haulmont.cuba.gui.WindowParam;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.capgemini.vault.entity.VaultIntegrationApi;
import com.haulmont.cuba.gui.screen.LookupComponent;
import com.haulmont.cuba.security.global.UserSession;

import javax.inject.Inject;
import java.util.Set;

@UiController("vault_IntegrationApi.browse")
@UiDescriptor("vault-integration-api-browse.xml")
@LookupComponent("vaultIntegrationApisTable")
@LoadDataBeforeShow
public class VaultIntegrationApiBrowse extends StandardLookup<VaultIntegrationApi> {

    @WindowParam
    private String screen;

    @Inject
    private UserSession userSession;

    @Inject
    private CollectionLoader<VaultIntegrationApi> intApiDetailsesDl;

    UserSessionSource uss = AppBeans.get(UserSessionSource.class);
    @Inject
    private Messages messages;
    @Inject
    private DataGrid<VaultIntegrationApi> vaultIntegrationApisTable;

    @Subscribe
    public void onInit(InitEvent event) {
        DataGrid.HtmlRenderer renderer1 =
                vaultIntegrationApisTable.createRenderer(DataGrid.HtmlRenderer.class);
        renderer1.setNullRepresentation("</span>");
        vaultIntegrationApisTable.getColumn("vaultApi.apiIntOwner").setRenderer(renderer1);

        DataGrid.HtmlRenderer renderer2 =
                vaultIntegrationApisTable.createRenderer(DataGrid.HtmlRenderer.class);
        renderer2.setNullRepresentation("</span>");
        vaultIntegrationApisTable.getColumn("vaultApi.apiBizOwner").setRenderer(renderer2);
    }

    @Override
    protected void initActions(InitEvent event) {
        super.initActions(event);
        if(screen.equals(messages.getMessage(getClass(),"screen.main"))){
            if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_administrator"))){
                intApiDetailsesDl.setQuery("select e from vault_IntegrationApi e where e.vaultApi.apiWorkflowStatus = 'AP' and e.vaultApi.apiLob = :grp");
                intApiDetailsesDl.setParameter("grp", uss.getUserSession().getUser().getGroup().getName());
            }else if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_user"))){
                intApiDetailsesDl.setQuery("select e from vault_IntegrationApi e where e.vaultApi.apiWorkflowStatus = 'AP' and e.vaultApi.createdBy = :user");
                intApiDetailsesDl.setParameter("user",uss.getUserSession().getUser().getLogin());
            }else{
                intApiDetailsesDl.setQuery("select e from vault_IntegrationApi e where e.vaultApi.apiWorkflowStatus = 'AP'");
            }
        }else{
            if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_administrator"))){
                intApiDetailsesDl.setQuery("select e from vault_IntegrationApi e where e.vaultApi.apiLob = :grp");
                intApiDetailsesDl.setParameter("grp", uss.getUserSession().getUser().getGroup().getName());
            }else if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_user"))){
                intApiDetailsesDl.setQuery("select e from vault_IntegrationApi e where e.vaultApi.createdBy = :user");
                intApiDetailsesDl.setParameter("user",uss.getUserSession().getUser().getLogin());
            }else{
                intApiDetailsesDl.setQuery("select e from vault_IntegrationApi e");
            }

        }

        intApiDetailsesDl.load();
    }

    private void doSave(){
        intApiDetailsesDl.load();
    }

    @Subscribe("textFieldApi")
    public void onTextFieldApiTextChange(TextInputField.TextChangeEvent event) {
        if (event.getText() != null) {
            intApiDetailsesDl.setParameter("name", "(?i)%" + event.getText() + "%");
        } else {
            intApiDetailsesDl.removeParameter("name");
        }
        intApiDetailsesDl.load();
    }

    @Subscribe("pfLookupField")
    public void onPfLookupFieldValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            intApiDetailsesDl.setParameter("platform", event.getValue());
        } else {
            intApiDetailsesDl.removeParameter("platform");
        }
        intApiDetailsesDl.load();
    }

    @Subscribe("buLookupField")
    public void onBuLookupFieldValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            intApiDetailsesDl.setParameter("bu", event.getValue());
        } else {
            intApiDetailsesDl.removeParameter("bu");
        }
        intApiDetailsesDl.load();
    }

    @Subscribe("ownerField")
    public void onOwnerFieldValueChange(HasValue.ValueChangeEvent<String> event) {
        if (event.getValue() != null) {
            intApiDetailsesDl.setParameter("owner", "(?i)%" + event.getValue() + "%");
        } else {
            intApiDetailsesDl.removeParameter("owner");
        }
        intApiDetailsesDl.load();
    }

    @Subscribe("patternField")
    public void onPatternFieldValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            intApiDetailsesDl.setParameter("pattern",  event.getValue());
        } else {
            intApiDetailsesDl.removeParameter("pattern");
        }
        intApiDetailsesDl.load();
    }

    @Subscribe("sdlcField")
    public void onSdlcFieldValueChange(HasValue.ValueChangeEvent<VaultMaster> event) {
        if (event.getValue() != null) {
            intApiDetailsesDl.setParameter("sdlc",  event.getValue());
        } else {
            intApiDetailsesDl.removeParameter("sdlc");
        }
        intApiDetailsesDl.load();
    }

    @Subscribe("lookupFieldStatus")
    public void onLookupFieldStatusValueChange(HasValue.ValueChangeEvent<Status> event) {
        if (event.getValue() != null) {
            intApiDetailsesDl.setParameter("status",event.getValue());
        } else {
            intApiDetailsesDl.removeParameter("status");
        }
        intApiDetailsesDl.load();
    }
}
