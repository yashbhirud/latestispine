package com.capgemini.vault.web.screens.vaultintegrationapi;

import com.capgemini.vault.entity.VaultApi;
import com.capgemini.vault.entity.VaultIntegration;
import com.capgemini.vault.service.iLinkIntApiDetailsService;
import com.haulmont.cuba.core.global.*;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.components.HasValue;
import com.haulmont.cuba.gui.components.LookupField;
import com.haulmont.cuba.gui.components.TwinColumn;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.capgemini.vault.entity.VaultIntegrationApi;
import com.haulmont.cuba.security.global.UserSession;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@UiController("vault_IntegrationApi.edit")
@UiDescriptor("vault-integration-api-edit.xml")
@EditedEntityContainer("vaultIntegrationApiDc")
@LoadDataBeforeShow
public class VaultIntegrationApiEdit extends StandardEditor<VaultIntegrationApi> {

    @Inject
    private UserSession userSession;

    UserSessionSource uss = AppBeans.get(UserSessionSource.class);

    @Inject
    private iLinkIntApiDetailsService linkIntApiDetailsService;

    @Inject
    private TwinColumn<VaultApi> intTwinColumn;

    @Inject
    private CollectionContainer<VaultIntegrationApi> intApiDetailsesDc;

    @Inject
    private MetadataTools metadataTools;

    @Inject
    private Notifications notifications;

    @Inject
    private Metadata metadata;

    @Inject
    private CollectionLoader<VaultApi> apiDetailsesDl;

    @Inject
    private CollectionLoader<VaultApi> filterDl;

    private VaultIntegration intDetails;

    private String screenName;

    @Inject
    private Messages messages;
    private Boolean isBuAdmin;
    private Boolean isUser;
    private Boolean isAdmin;

    @Subscribe
    public void onInit(InitEvent event) {
        VaultIntegrationApi intApiDetails = metadata.create(VaultIntegrationApi.class);
        setEntityToEdit(intApiDetails);
        collectScreenParams(event);
        isBuAdmin=userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_administrator"));
        isUser=userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_user"));
        isAdmin=userSession.getRoles().contains(messages.getMessage(getClass(),"role.company_administrator"));
        setDataSource();
        setTwinColumnSelectedValues();

    }

    @Subscribe
    public void onAfterShow(AfterShowEvent event) {
        setTwinColumnSelectedValues();
    }

    private void collectScreenParams(InitEvent event){
        ScreenOptions options = event.getOptions();
        if (options instanceof MapScreenOptions) {
            intDetails = (VaultIntegration) ((MapScreenOptions) options).getParams().get("integration");
            screenName = (String) ((MapScreenOptions) options).getParams().get("screen");
        }
    }

    private void setDataSource(){
        apiDetailsesDl.setQuery("select e from vault_Api e order by e.updateTs desc, e.createTs desc");
        if(isBuAdmin){
            apiDetailsesDl.setParameter("grp",uss.getUserSession().getUser().getGroup().getName());
            apiDetailsesDl.removeParameter("user");
            filterDl.setParameter("grp",uss.getUserSession().getUser().getGroup().getName());
            filterDl.removeParameter("user");
            if(screenName.equals(messages.getMessage(getClass(),"screen.main"))){
                apiDetailsesDl.setParameter("status","AP");
                apiDetailsesDl.setParameter("status","AP");
            }else{
                apiDetailsesDl.removeParameter("status");
                filterDl.removeParameter("status");
            }
        }else if(isAdmin){
            apiDetailsesDl.removeParameter("grp");
            apiDetailsesDl.removeParameter("user");
            filterDl.removeParameter("grp");
            filterDl.removeParameter("user");
            if(screenName.equals(messages.getMessage(getClass(),"screen.main"))){
                apiDetailsesDl.setParameter("status","AP");
                apiDetailsesDl.setParameter("status","AP");
            }else{
                apiDetailsesDl.removeParameter("status");
                filterDl.removeParameter("status");
            }
        }else{
            apiDetailsesDl.setParameter("grp",uss.getUserSession().getUser().getGroup().getName());
            apiDetailsesDl.setParameter("user", uss.getUserSession().getUser().getLogin());
            filterDl.setParameter("user", uss.getUserSession().getUser().getLogin());
            filterDl.setParameter("grp",uss.getUserSession().getUser().getGroup().getName());
            if(screenName.equals(messages.getMessage(getClass(),"screen.main"))){
                apiDetailsesDl.setParameter("status","AP");
                apiDetailsesDl.setParameter("status","AP");
            }else{
                apiDetailsesDl.removeParameter("status");
                filterDl.removeParameter("status");
            }

        }
        apiDetailsesDl.load();
        filterDl.load();
    }

    private void setTwinColumnSelectedValues(){
        List<VaultIntegrationApi> intApiDetailsList = intApiDetailsesDc.getItems();
        List<VaultApi> apiSpecificList = intApiDetailsList.stream().filter(e -> e.getVaultIntegration().equals(intDetails))
                .map(a ->a.getVaultApi())
                .collect(Collectors.toList());
        intTwinColumn.setValue(apiSpecificList);
    }

    public void onOkBtnClick() {
        linkIntApiDetailsService.commitIntApiDetails(intTwinColumn.getValue(),intDetails);
        close(WINDOW_CLOSE_ACTION);
    }

    public void onCancelBtnClick() {
        //Close without saving any changes
        close(WINDOW_CLOSE_ACTION);
    }

    @Subscribe("apiLookup")
    public void onApiLookupValueChange(HasValue.ValueChangeEvent<VaultApi> event) {
        if (event.getValue() != null) {
            VaultApi value = event.getValue();
            Collection <VaultApi> list = intTwinColumn.getValue();
            Collection <VaultApi> apiList = new ArrayList<>(list);
            apiList.add(value);
            Collection<String> apiids = apiList.stream().map(e -> e.getApi()).collect(Collectors.toList());
            if(list.isEmpty()){
                apiDetailsesDl.setQuery("select e from vault_Api e where e.api = '"+value.getApi()+"'");
                if(isBuAdmin){
                    apiDetailsesDl.removeParameter("user");
                }
                if(isAdmin){
                    apiDetailsesDl.removeParameter("user");
                    apiDetailsesDl.removeParameter("grp");
                }

            }else{
                String ids = apiids.stream().collect(Collectors.joining("','", "'", "'"));
                apiDetailsesDl.setQuery("select e from vault_Api e where e.api IN ("+ids+")");
                apiDetailsesDl.setParameter("user", uss.getUserSession().getUser().getLogin());
                if(isBuAdmin){
                    apiDetailsesDl.removeParameter("user");
                }
                if(isAdmin){
                    apiDetailsesDl.removeParameter("user");
                    apiDetailsesDl.removeParameter("grp");
                }
            }
            apiDetailsesDl.load();

        }else{
            setDataSource();
        }
    }

    @Subscribe("intLookupField")
    public void onIntLookupFieldValueChange(HasValue.ValueChangeEvent<VaultIntegration> event) {

        VaultIntegration integration = event.getValue();
        List<VaultIntegrationApi> intApiDetailsList = intApiDetailsesDc.getItems();
        List<VaultApi> apiSpecificList = intApiDetailsList.stream().filter(e -> e.getVaultIntegration().equals(integration))
                .map(a ->a.getVaultApi())
                .collect(Collectors.toList());
        intTwinColumn.setValue(apiSpecificList);
    }

    public void onShowButtonClick() {
        StringBuilder sb = new StringBuilder();
        Collection<VaultApi> value = intTwinColumn.getValue();
        if (value == null) {
            sb.append("null");
        } else {
            for (VaultApi apiDetail : value) {
                sb.append(metadataTools.getInstanceName(apiDetail))
                        .append("\n");
            }
        }
        notifications.create()
                .withCaption(sb.toString())
                .show();
    }
}