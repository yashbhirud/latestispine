package com.capgemini.vault.web.screens.vaultmaster;

import com.capgemini.vault.entity.VaultIntegration;
import com.google.common.base.Strings;
import com.haulmont.bali.util.ParamsMap;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.gui.UiComponents;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.data.CollectionDatasource;
import com.haulmont.cuba.gui.data.Datasource;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.capgemini.vault.entity.VaultMaster;
import com.haulmont.cuba.gui.xml.layout.ComponentsFactory;
import com.haulmont.cuba.security.entity.User;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

@UiController("vault_Master.browse")
@UiDescriptor("vault-master-browse.xml")
public class VaultMasterBrowse extends EntityCombinedScreen {
   @Inject
   private CollectionDatasource masterDataManagementsDs;
    @Inject
    private LookupField<VaultMaster> codeFilter;
    @Inject
    private Datasource<VaultMaster> masterDataManagementDs;
    @Inject
    private TextField<String> typeFilter;

    @Inject
    private FieldGroup fieldGroup;

    @Inject
    private Datasource<VaultMaster> masterDataTypesDs;
    @Inject
    protected UiComponents uiComponents;
    @Inject
    private DataManager dataManager;
    @Inject
    private GroupTable table;
    String value;

    private LookupField<String> typesLookup;

    @Override
    public void init(Map<String, Object> params) {
        super.init(params);
        List<VaultMaster> masterList =dataManager.load(VaultMaster.class).query("select e from vault_Master e").list();
        List<String> list = masterList.stream().map(VaultMaster::getMstType).collect(Collectors.toSet()).stream().collect(Collectors.toList());
         typesLookup = uiComponents.create(LookupField.class);
        typesLookup.setWidth("20%");
        typesLookup.setOptionsList(list);
        typesLookup.setEditable(true);
        fieldGroup.getField("mstType").setComponent(typesLookup);
        /*masterDataManagementsDs.refresh();
        masterDataTypesDs.setQuery("select e from vault_Master e");*/
        typesLookup.addValueChangeListener(e->{
            typesLookup.setValue(e.getValue());
            VaultMaster master= masterDataManagementDs.getItem();
            master.setMstType(e.getValue());
        });
        codeFilter.addValueChangeListener(e -> {
            VaultMaster masterData = e.getValue();
            if(masterData!=null){
                masterDataManagementsDs.refresh();
            }else{
                masterDataManagementsDs.refresh(ParamsMap.empty());
            }
        });
        typeFilter.addValueChangeListener(e -> {
            value = e.getValue();
            masterDataManagementsDs.refresh(ParamsMap.empty());
            if(value!=null){
                masterDataManagementsDs.refresh(ParamsMap.of("type",value));

            }
        });
    }
    @Subscribe
    protected void onBeforeShow(BeforeShowEvent event) {

    }
    @Subscribe("table")
    public void onTableSelection(GroupTable.SelectionEvent<VaultMaster> event) {
        Set<VaultMaster> master= event.getSelected();
        String type=null;
        for (VaultMaster master1:master)
        {
            System.out.println(master1.getMstType());
             type=master1.getMstType();
        }
        typesLookup.setValue(type);
    }


}