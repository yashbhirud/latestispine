package com.capgemini.vault.web.screens.vaultuploadsummary;

import com.capgemini.vault.service.iReadFileService;
import com.capgemini.vault.service.iRevertImportService;
import com.capgemini.vault.web.screens.StagingScreen;
import com.capgemini.vault.web.screens.vaulterrsummary.VaultErrSummaryBrowse;
import com.haulmont.cuba.core.entity.Entity;
import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.core.global.UserSessionSource;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.ScreenBuilders;
import com.haulmont.cuba.gui.components.Button;
import com.haulmont.cuba.gui.components.FileUploadField;
import com.haulmont.cuba.gui.components.GroupTable;
import com.haulmont.cuba.gui.components.HasValue;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.capgemini.vault.entity.VaultUploadSummary;
import com.haulmont.cuba.gui.upload.FileUploadingAPI;
import com.haulmont.cuba.security.entity.User;
import com.haulmont.cuba.security.global.UserSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@UiController("vault_UploadSummary.browse")
@UiDescriptor("vault-upload-summary-browse.xml")
@LookupComponent("vaultUploadSummariesTable")
@LoadDataBeforeShow
public class VaultUploadSummaryBrowse extends StandardLookup<VaultUploadSummary>{

    @Inject
    private Notifications notifications;
    @Inject
    private FileUploadField upload;
    @Inject
    private FileUploadingAPI fileUploadingAPI;
    @Inject
    private iReadFileService readFileService;
    @Inject
    private CollectionLoader<VaultUploadSummary> uploadSummariesDl;
    @Inject
    private ScreenBuilders screenBuilders;
    DataManager dataManager = AppBeans.get(DataManager.class);

    UserSessionSource uss = AppBeans.get(UserSessionSource.class);
    @Inject
    private UserSession userSession;
    @Inject
    private iRevertImportService rollbackService;
    @Inject
    private GroupTable<VaultUploadSummary> vaultUploadSummariesTable;
    @Inject
    private Messages messages;
    @Inject
    private Button rollbackbtn;
    private Logger log = LoggerFactory.getLogger(VaultUploadSummaryBrowse.class);



    @Subscribe
    public void onInit(InitEvent event) {
        if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_administrator"))){
            uploadSummariesDl.setQuery("select e from vault_UploadSummary e where e.createdBy IN (select u.login from sec$User u where u.group.id=:group)");
            uploadSummariesDl.setParameter("group", uss.getUserSession().getUser().getGroup().getId());
        }else if(userSession.getRoles().contains(messages.getMessage(getClass(),"role.bu_user"))){
            uploadSummariesDl.setQuery("select e from vault_UploadSummary e where e.createdBy = :user");
            uploadSummariesDl.setParameter("user", uss.getUserSession().getUser().getLogin());
        }else{
            uploadSummariesDl.setQuery("select e from vault_UploadSummary e");
        }
        uploadSummariesDl.load();
        setReadOnlyModeAccess(userSession);
    }


    @Subscribe("upload")
    public void onUploadFileUploadSucceed(FileUploadField.FileUploadSucceedEvent event) throws IOException {

            File fileid = fileUploadingAPI.getFile(upload.getFileId());
            String msg = readFileService.readVault(fileid, upload.getFileName(), uss.getUserSession().getUser());
            notifications.create().withCaption(msg).show();
            uploadSummariesDl.load();
    }


    @Subscribe("FileNameFilter")
    public void onFileNameFilterValueChange(HasValue.ValueChangeEvent<String> event) {
        if (event.getValue() != null) {
            uploadSummariesDl.setParameter("name", "(?i)%" + event.getValue() + "%");
        } else {
            uploadSummariesDl.removeParameter("name");
        }
        uploadSummariesDl.load();
    }

    @Subscribe("UserFilter")
    public void onUserFilterValueChange(HasValue.ValueChangeEvent<User> event) {
        if (event.getValue() != null) {
            uploadSummariesDl.setParameter("User",event.getValue().getLogin());
        } else {
            uploadSummariesDl.removeParameter("User");
        }
        uploadSummariesDl.load();
    }



    public void openStagingScreen(Entity item, String columnId) {
        VaultUploadSummary upl = (VaultUploadSummary)item;
        StagingScreen screen = screenBuilders.screen(this)
                .withScreenClass(StagingScreen.class)
                .withLaunchMode(OpenMode.NEW_TAB)
                .build();
        screen.setDataSource(upl);
        screen.show();
    }

    public void openErrorScreen(Entity item, String columnId) {
        VaultUploadSummary upl = (VaultUploadSummary)item;
        VaultErrSummaryBrowse screen = screenBuilders.screen(this)
                .withScreenClass(VaultErrSummaryBrowse.class)
                .withLaunchMode(OpenMode.NEW_TAB)
                .build();
        screen.setDs(upl);
        screen.show();

    }

    public void onRollbackbtnClick() {
        List<VaultUploadSummary> integrations = new ArrayList<>();
        Set<VaultUploadSummary> upl = vaultUploadSummariesTable.getSelected();
        if(upl.isEmpty()){
            notifications.create().withCaption("Select record to rollback").show();
            uploadSummariesDl.load();
            return;
        }
        boolean isSuccessAPI=true;
        boolean isSuccessInt=true;

        for(VaultUploadSummary u: upl){
            if(u.getUpsDatatype().equals("Integrations")){
                integrations.add(u);
            }
            else {
                isSuccessAPI = rollbackService.rollBack(u);
            }
        }
        for(VaultUploadSummary u: integrations){
            isSuccessInt = rollbackService.rollBack(u);
        }
        if(isSuccessAPI && isSuccessInt){
            notifications.create().withCaption("Rollback Successful").show();
        }
        else{
            notifications.create().withCaption("Approved records can not be Rolled back").show();
        }
        uploadSummariesDl.load();
    }
    public void setReadOnlyModeAccess(UserSession userSession) {
        if (userSession.getRoles().contains(messages.getMessage(getClass(), "role.read_only"))) {
            rollbackbtn.setEnabled(false);
            upload.setEnabled(false);
        }
    }
}