package com.capgemini.vault.web.utils;

import com.capgemini.vault.entity.Constants;
import com.capgemini.vault.entity.VaultApi;
import com.capgemini.vault.entity.VaultIntegration;
import com.capgemini.vault.entity.VaultMaster;
import com.capgemini.vault.service.iGetEmailService;
import com.capgemini.vault.service.iRevertImportService;
import com.haulmont.bali.util.ParamsMap;
import com.haulmont.chile.core.datatypes.impl.EnumClass;
import com.haulmont.chile.core.model.MetaProperty;
import com.haulmont.chile.core.model.MetaPropertyPath;

import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.core.global.filter.Clause;
import com.haulmont.cuba.core.global.filter.LogicalCondition;
import com.haulmont.cuba.core.global.filter.LogicalOp;
import com.haulmont.cuba.core.global.filter.QueryFilter;
import com.haulmont.cuba.gui.components.DataGrid;
import com.haulmont.cuba.gui.components.DataGrid.Column;
import com.haulmont.cuba.gui.components.DataGrid.HeaderCell;
import com.haulmont.cuba.gui.components.LookupField;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.xml.layout.ComponentsFactory;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DataGridFilter{

    @Inject
    private DataManager dataManager;
    /*@Inject
    private Persistence persistence;*/

    public iGetEmailService getEmailService;

    private DataGrid.HeaderRow filterHeaderRow;
    private HashMap<String, HashMap<String,String>> filterMap;
    private HashMap<String,Object> filterValueMap;
    private DataGrid targetDataGrid;
    private ComponentsFactory componentsFactory;
    private DataGrid resetGrid;

    protected Messages messages = AppBeans.get(Messages.class);

    //constructor
    public DataGridFilter(DataGrid datagrid) {
        targetDataGrid = datagrid;
        resetGrid = datagrid;
        targetDataGrid.getItems();
        filterMap = new HashMap<>();
        filterValueMap = new HashMap<>();
        componentsFactory = AppBeans.get(ComponentsFactory.class);

        filterHeaderRow = targetDataGrid.appendHeaderRow();
    }

    public void removeFilterRow(CollectionLoader collectionLoader){
        targetDataGrid.removeHeaderRow(1);
        if(targetDataGrid.getHeaderRowCount() >1){
            targetDataGrid.removeHeaderRow(1);
        }
        Map<String,Object> parametersMap = collectionLoader.getParameters();
        Set<String> keys = new HashSet<>(parametersMap.keySet());
        for(String key : keys){
            if(!key.equals("user") && !key.equals("grp")){
                collectionLoader.removeParameter(key);
            }
        }
        collectionLoader.load();
    }

    public <E> void addGenericFilter(CollectionLoader collectionLoader,CollectionLoader masterDataManagementsDl,String type,String screen) {
        List<Column> colList = targetDataGrid.getColumns();
        for (Column col : colList) {
            if(col.getId().equals("intMsgSize") || col.getId().equals("intThroughput") || col.getId().equals("apiIntMsgSize") || col.getId().equals("apiThroughput")){
                System.out.println("Inside if");
                continue;
            }
            addFilterToColumn(col,collectionLoader,masterDataManagementsDl,type,screen);
        }
    }

    private void addFilterToColumn(Column col, CollectionLoader collectionLoader, CollectionLoader masterDataManagementsDl,String type, String screen) {

        String columnId = col.getId();
        MetaPropertyPath mpp = col.getPropertyPath();
        if (mpp !=null && mpp.getMetaProperty() != null && !mpp.getMetaProperty().isReadOnly()) {

            MetaProperty metaProp = mpp.getMetaProperties()[0];
            String mppString = mpp.toString();
            String filterCode = mpp.getPath()[0];

            HeaderCell filterCell;

            //For Enums use lookupField for filter input, else use TextField
            if (metaProp.getRange().isEnum()) {

                LookupField lookupField = componentsFactory.createComponent(LookupField.class);
                lookupField.setId(mppString);
                lookupField.setWidth("100%");
                lookupField.setStyleName("tiny");
                lookupField.setOptionsEnum((Class<? extends EnumClass>) (metaProp.getJavaType()));

                lookupField.addValueChangeListener(listener->{
                    if (lookupField.getValue() == null) {
                           filterMap.remove(mppString);
                        filterValueMap.put(filterCode,lookupField.getValue());
                    }else {
                        HashMap<String,String> innerFilterMap = new HashMap<>();
                        filterMap.put(mppString, innerFilterMap);
                        innerFilterMap.put("value", EnumClass.class.cast(lookupField.getValue()).getId().toString().toLowerCase());
                        innerFilterMap.put("mainClause", "lower(cast(e." + mppString +" varchar(100))) like :custom$" + mppString);
                        filterValueMap.put(filterCode,lookupField.getValue());
                    }
                    applyFilter(collectionLoader);
                    //applyGridFilter();
                });
                filterCell = filterHeaderRow.getCell(columnId);
                filterCell.setComponent(lookupField);
            } else {
                if (metaProp.getRange().isDatatype()) {

                    if (metaProp.getJavaType().equals(Boolean.class)) {
                        //Lookup filter with true/false values
                        LookupField lookupField = componentsFactory.createComponent(LookupField.class);
                        lookupField.setOptionsMap(ParamsMap.of("True", "true", "False", "false"));
                        lookupField.setStyleName("tiny");
                        lookupField.setWidth("100%");

                        lookupField.addValueChangeListener(listener->{
                            if (lookupField.getValue() == null) {
                                filterMap.remove(mppString);
                            }else {
                                HashMap<String,String> innerFilterMap = new HashMap<>();
                                    filterMap.put(mppString, innerFilterMap);
                                innerFilterMap.put("value", lookupField.getValue().toString());
                                innerFilterMap.put("mainClause", "lower(cast(e." + mppString +" varchar(100))) like :custom$" + mppString);
                            }
                            applyGridFilter();
                        });
                        filterCell = filterHeaderRow.getCell(columnId);
                        filterCell.setComponent(lookupField);

                    } else {
                        LookupField lookupField = componentsFactory.createComponent(LookupField.class);
                        lookupField.setId(mppString);
                        lookupField.setWidth("100%");
                        lookupField.setStyleName("tiny");

                            if (type.equals("int")) {
                                List<String> list = AppBeans.get(iGetEmailService.class).getIntValue(col.getId(), screen);
                                lookupField.setOptionsList(list);
                            } else {
                                List<VaultApi> list = AppBeans.get(iGetEmailService.class).getApiValue(col.getId(), screen);
                                lookupField.setOptionsList(list);
                            }

                        lookupField.addValueChangeListener(listener -> {
                            if (lookupField.getValue() == null) {
                                filterMap.remove(mppString);
                                filterValueMap.put(filterCode, lookupField.getValue());
                            } else {
                                filterValueMap.put(filterCode, lookupField.getValue());
                            }
                            applyFilter(collectionLoader);
                        });
                        filterCell = filterHeaderRow.getCell(columnId);
                        filterCell.setComponent(lookupField);
                        //Textfield filter
                        /*TextField textField = componentsFactory.createComponent(TextField.class);
                        textField.setId(mppString);
                        textField.setWidth("100%");
                        textField.setStyleName("tiny");


                        textField.addValueChangeListener(listener->{
                            if((textField.getValue() == null)) {
                                filterMap.remove(mppString);
                                filterValueMap.put(filterCode,textField.getValue());
                            }else {
                                HashMap<String,String> innerFilterMap = new HashMap<>();
                                filterMap.put(mppString, innerFilterMap);
                                innerFilterMap.put("value", "%" + textField.getRawValue().toLowerCase()+"%");
                                innerFilterMap.put("mainClause", "lower(cast(e." + mppString +" varchar(100))) like :custom$" + mppString);
                                filterValueMap.put(filterCode,"(?i)%" + textField.getValue() + "%");
                            }
                        });
                        textField.addEnterPressListener(e -> {
                            applyFilter(collectionLoader);
                        });

                        filterCell = filterHeaderRow.getCell(columnId);
                        filterCell.setComponent(textField);*/
                    }
                }else if (metaProp.getJavaType().equals(VaultMaster.class))
                {
                    LookupField lookupField = componentsFactory.createComponent(LookupField.class);
                    lookupField.setId(mppString);
                    lookupField.setWidth("100%");
                    lookupField.setStyleName("tiny");
                    masterDataManagementsDl.setQuery("select e from vault_Master e where e.mstType = :type");
                    String key = messages.getMainMessage(mppString);
                    masterDataManagementsDl.setParameter("type",key);
                    masterDataManagementsDl.load();
                    lookupField.setOptionsList(masterDataManagementsDl.getContainer().getItems());

                    lookupField.addValueChangeListener(listener->{
                        if (lookupField.getValue() == null) {
                            filterMap.remove(mppString);
                            filterValueMap.put(filterCode,lookupField.getValue());
                        }else {
                            filterValueMap.put(filterCode,lookupField.getValue());
                        }
                        applyFilter(collectionLoader);
                    });
                    filterCell = filterHeaderRow.getCell(columnId);
                    filterCell.setComponent(lookupField);

                }
            }
        }

    }

    //processes filtering
    private void applyFilter(CollectionLoader<VaultIntegration> collectionLoader) {

            Set<String> keys = filterValueMap.keySet();
            for (String key : keys) {
                if (key.equals("api")) {
                    if (filterValueMap.get(key) == null) {
                        collectionLoader.removeParameter("apiID");
                    } else {
                        collectionLoader.setParameter("apiID", filterValueMap.get(key));
                    }
                } else {
                    if (filterValueMap.get(key) == null) {
                        collectionLoader.removeParameter(key);
                    } else {
                        collectionLoader.setParameter(key, filterValueMap.get(key));
                    }
                }
            }
            collectionLoader.load();
    }


    @SuppressWarnings("unchecked")
    public void applyGridFilter() {

        if (targetDataGrid.getDatasource().isModified()) {
            targetDataGrid.getFrame().showNotification("æœªä¿å­˜ã®å¤‰æ›´ãŒã‚ã‚‹ãŸã‚æ¤œç´¢ã§ãã¾ã›ã‚“");
        } else {

            if (filterMap.size() == 0) {
                resetQuery();
                targetDataGrid.getDatasource().refresh();
            } else {

                targetDataGrid.getDatasource().setQueryFilter(null);
                LogicalCondition andCondition = new LogicalCondition("", LogicalOp.AND);
                for (String key : filterMap.keySet()) {

                    andCondition.getConditions().add(new Clause("", filterMap.get(key).get("mainClause"), filterMap.get(key).get("joinClause"), null, null));
                }


                QueryFilter queryFilter = new QueryFilter(andCondition);
                targetDataGrid.getDatasource().setQueryFilter(queryFilter);
                Map<String,String> queryMap = filterMap.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().get("value")));
                targetDataGrid.getDatasource().refresh(queryMap);

            }
        }
    }
    private void resetQuery() {
        targetDataGrid.getDatasource().setQueryFilter(null);
    }


}
